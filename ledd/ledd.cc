#include <iostream>
#include <string>
#include <getopt.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <thread>
#include <errno.h>
#include <sys/types.h>
#include <libgen.h>
#include <ext/stdio_filebuf.h>
#include <sys/un.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <pigpio.h>
#include <systemd/sd-daemon.h>

using namespace std;

#define DEFAULT_SOCKET "/run/ledd/socket"

#define GPIO_MIN 0
#define GPIO_MAX 39

#define DEFAULT_PWM_FREQUENCY 8000
#define DEFAULT_SAMPLE_RATE 5

#define DISABLE_FIFO_IF 1
#define DISABLE_SOCK_IF 2

enum { RED, GREEN,BLUE};
static const char* gpio_names[3] = {"red", "green", "blue"};

static int gpio[3] = {-1, -1, -1};
static int active_low = 0;

static int sample_rate   = DEFAULT_SAMPLE_RATE;
static int pwm_frequency = DEFAULT_PWM_FREQUENCY;

const char* sockpath = DEFAULT_SOCKET;


static void print_help(void) {
	static const char* help_msg = "\
Usage: ledd [options]\n\
Options:\n\
    -h           This help text\n\
	-r <number>  GPIO pin number for the red LED channel\n\
	-g <number>  GPIO pin number for the green LED channel\n\
	-b <number>  GPIO pin number for the blue LED channel\n\
	-l           The RGB LED is active low\n\
	-s <number>  Sample rate in us (1,2,4,*5,8,10)\n\
	-f <number>  PWM frequency (default 8000)\n\
	-u <pathname> UNIX domain socket to listen (%s)\n\
";

	fprintf(stdout, help_msg, DEFAULT_SOCKET);
	exit(EXIT_SUCCESS);
}


static int set_color(int r, int g, int b) {
	if (gpioPWM(gpio[RED],   active_low ? 255 - r : r)   != 0) return -1;
	if (gpioPWM(gpio[GREEN], active_low ? 255 - g : g) != 0) return -1;
	if (gpioPWM(gpio[BLUE],  active_low ? 255 - b : b)  != 0) return -1;
	return 0;
}


static int parse_color(int* dst, const char* src) {
	size_t len = strlen(src);
	int j, v;

	if (len != 6) return -1;

	memset(dst, 0, sizeof(dst[0]) * 3);
	for(int i = 0; i < 6; i++) {
		if (src[i] >= '0' && src[i] <= '9')
			v = src[i] - '0';
		else if (src[i] >= 'a' && src[i] <= 'f')
			v = src[i] - 'a' + 10;
		else if (src[i] >= 'A' && src[i] <= 'F')
			v = src[i] - 'A' + 10;
		else
			return -1;

		j = i / 2;
		dst[j] = dst[j] << 4;
		dst[j] += v;
	}
	return 0;
}


static int mkpath(char* path, mode_t mode) {
	for (char* p = strchr(path + 1, '/'); p; p = strchr(p + 1, '/')) {
		*p = '\0';
		if (mkdir(path, mode) == -1) {
			if (errno != EEXIST) {
				*p = '/';
				return -1;
			}
		}
        *p = '/';
    }
    return 0;
}


static void client(int fd) {
    __gnu_cxx::stdio_filebuf<char> filebuf(fd, ios::in);
    istream input(&filebuf);

	int color[3];

	for (string line; getline(input, line); ) {
		if (parse_color(color, line.c_str()) == 0)
			set_color(color[RED], color[GREEN], color[BLUE]);
	}

	printf("Client %d closed connection\n", fd);
	close(fd);
}


static void server(int s, const char* pathname) {
	int c;
	char* tmp = NULL;
	struct sockaddr_un server_addr, client_addr;
	socklen_t len;

	if (s < 0) {
		s = socket(AF_UNIX, SOCK_STREAM, 0);
		if (s < 0) goto error;

		memset(&server_addr, 0, sizeof(server_addr));
		server_addr.sun_family = AF_UNIX;
		strncpy(server_addr.sun_path, pathname, sizeof(server_addr.sun_path) - 1);

		tmp = strdup(pathname);
		if (mkpath(tmp, 0777) < 0) goto error;
		free(tmp);
		tmp = NULL;

		if (unlink(pathname) < 0 && errno != ENOENT)
			goto error;

		if (bind(s, (struct sockaddr*)&server_addr, sizeof(server_addr)) < 0)
			goto error;

		if (chmod(pathname, 0666) < 0) goto error;

		if (listen(s, 10) < 0) goto error;
	}

	while(1) {
		c = accept(s, (struct sockaddr*)&client_addr, &len);
		if (c < 0) goto error;
		printf("Client %d connected\n", c);
		thread t(client, c);
		t.detach();
	}

	close(s);
	return;
error:
	if (tmp) free(tmp);
	fprintf(stderr, "Error: %s\n", strerror(errno));
	if (s >= 0) close(s);
}


int main(int argc, char** argv) {
	int opt, fd = -1, n;
	int rc = EXIT_FAILURE;

	while((opt = getopt(argc, argv, "hr:g:b:ls:f:u:")) != -1) {
		switch(opt) {
		case 'h': print_help();                 break;
		case 'r': gpio[RED] = atoi(optarg);     break;
		case 'g': gpio[GREEN] = atoi(optarg);   break;
		case 'b': gpio[BLUE] = atoi(optarg);    break;
		case 'l': active_low = true;            break;
		case 's': sample_rate = atoi(optarg);   break;
		case 'f': pwm_frequency = atoi(optarg); break;
		case 'u': sockpath = strdup(optarg);    break;
		default:
			fprintf(stderr, "Use the -h option for list of supported "
				"program arguments.\n");
			exit(EXIT_FAILURE);
		}
	}

	for(int i = 0; i < 3; i++) {
		if (gpio[i] == -1) {
			fprintf(stderr, "Please configure GPIO pin numbers for all three channels\n");
			exit(EXIT_FAILURE);
		}

		if (gpio[i] < GPIO_MIN || gpio[i] > GPIO_MAX) {
			fprintf(stderr, "Invalid GPIO pin number for the %s channel (allowed range is <%d,%d>\n", gpio_names[i], GPIO_MIN, GPIO_MAX);
			exit(EXIT_FAILURE);
		}
	}

	if (getuid() != 0) {
		fprintf(stderr, "This program must be run under root.\n");
		exit(EXIT_FAILURE);
	}

	printf("Using pigpio version %d\n", gpioVersion());
	printf("Pigpio reports hardware revision %x\n", gpioHardwareRevision());

	if (gpioCfgInterfaces(DISABLE_FIFO_IF | DISABLE_SOCK_IF) != 0) {
		fprintf(stderr, "Error while disabling FIFO and socket interfaces\n");
		exit(EXIT_FAILURE);
	}

	printf("Setting sample rate to %d us\n", sample_rate);
	if (gpioCfgClock(sample_rate, 1, 0) != 0) {
		fprintf(stderr, "Error while configuring sample rate\n");
		exit(EXIT_FAILURE);
	}

	if (gpioInitialise() == PI_INIT_FAILED) {
		fprintf(stderr, "Failed to initialize the pigpio library.\n");
		exit(EXIT_FAILURE);
	}

	printf("Using PWM frequency %d Hz\n", pwm_frequency);
	for(int i = 0; i < 3; i++) {
		if (gpioSetMode(gpio[i], PI_OUTPUT) != 0) {
			fprintf(stderr, "Error while configuring GPIO pin %d\n", gpio[i]);
			goto error;
		}
		if (gpioSetPWMfrequency(gpio[i], pwm_frequency) != pwm_frequency) {
			fprintf(stderr, "Error while configuring PWM frequency on GPIO pin %d\n", gpio[i]);
			goto error;
		}
	}

	printf("LED GPIO configuration: red=%d, green=%d, blue=%d, active_low=%d\n", gpio[RED], gpio[GREEN], gpio[BLUE], active_low);

	n = sd_listen_fds(0);
	if (n > 1) {
		fprintf(stderr, "Too many file descriptors received from systemd\n");
		goto error;
	} else if (n == 1) {
		fd = SD_LISTEN_FDS_START + 0;
	} else {
		printf("Listening on '%s'\n", sockpath);
	}

	server(fd, sockpath);
	rc = EXIT_SUCCESS;
error:
	set_color(0, 0, 0);
	gpioTerminate();
	return rc;
}
