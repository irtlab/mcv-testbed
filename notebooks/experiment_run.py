import os
import re
from events import Events


class ExperimentRun(object):
    def __init__(self, db, data):
        self.db = db
        self._data = data
        self._events = None
        events = self.get_events(db, self._data)
        if events and len(events) > 0:
            self._events = Events(events)

    @property
    def data(self):
        return self._data

    @property
    def settings(self):
        docs = self.db.events.find({'$and': [
            {'timestamp': {'$lte': self._data['start']}},
            {'event': 'API_UPDATE_SETTINGS'}
        ]}).sort([('timestamp', -1)]).limit(1)
        docs = list(docs)
        if docs and len(docs) > 0:
            v = docs[0]
            return v['data']['settings']
        return None

    @property
    def id(self):
        return str(self._data['_id'])

    @property
    def start(self):
        return self._data['start']

    @property
    def end(self):
        return self._data['end']

    @property
    def subject_environment(self):
        if 'subject_environment' in self._data:
            return self._data['subject_environment']
        return None

    @property
    def events(self):
        return self._events

    @property
    def dispatcher_terminal(self):
        if 'dispatcher_terminal' in self._data:
            return self._data['dispatcher_terminal']
        return None

    @staticmethod
    def get_events(db, data):
        docs = db.events.find({'timestamp': {'$gte': data['start'], '$lte': data['end']}})
        if docs:
            return sorted(list(docs), key = lambda i: i['timestamp'])
        return None

    def get_filename(self, terminal):
        # data = sorted(self._data, key = lambda i: i['timestamp'], reverse=False)
        if len(self._events.data) > 0:
            for e in self._events.data:
                if e['event'] == 'RUN_START' and re.search(rf'{terminal}', e['terminal']) is not None:
                    return os.path.basename(e['data']['speaker'])
        # In case if 'RUN_START' event generated before the experiment run started (this should not happen usually)
        # I have to look up the most recent RUN_START event before the start of that experiment.
        docs = self.db.events.find({'$and': [
            {'timestamp': {'$lte': self._data['start']}},
            {'event': 'RUN_START'}
        ]}).sort([('timestamp', -1)]).limit(1)
        docs = list(docs)
        if docs and len(docs) > 0:
            v = docs[0]
            if re.search(rf'{terminal}', v['terminal']) is not None:
                return os.path.basename(v['data']['speaker'])
        return None
