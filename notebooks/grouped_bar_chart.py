import datetime
import csv
import numpy as np
import matplotlib.pyplot as plt
from utils import seconds_to_minutes


class GroupedBarChart(object):
    def __init__(self):
        self.title = ''
        self.labels = []
        self.total_time_means = []
        self.speaking_time_means = []
        self.speaking_time_pct_means = []
        self.dispatcher_speaking_time_means = []
        self.subjet_speaking_time_means = []

    def set_title(self, title_name):
        self.title = title_name

    # The order of the data list matters.
    def append(self, data):
        self.labels.append(data[0])
        self.total_time_means.append(data[1])
        self.speaking_time_means.append(data[2])
        self.speaking_time_pct_means.append(data[3])
        self.dispatcher_speaking_time_means.append(data[4])
        self.subjet_speaking_time_means.append(data[5])

    def draw(self, to_file = False):
        x = np.arange(len(self.labels)) # the label locations
        width = 0.20                    # the width of the bars

        fig, ax = plt.subplots()
        rects1 = ax.bar(x + width, self.total_time_means, width, label='Total time')
        rects2 = ax.bar(x + width * 2, self.speaking_time_means, width, label='Total Speaking time')
        rects3 = ax.bar(x + width * 3, self.dispatcher_speaking_time_means, width, label='Dispatcher speaking time')
        rects4 = ax.bar(x + width * 4, self.subjet_speaking_time_means, width, label='Subject speaking time')

        # Add some text for labels, title and custom x-axis tick labels, etc.
        ax.set_ylabel('Time in seconds')
        ax.set_xlabel('Subjects')
        ax.set_title(f'Experiment: {self.title}')
        ax.set_xticks(x, self.labels)
        ax.legend(fontsize='xx-small')

        # ax.bar_label(rects1, padding=5)
        # ax.bar_label(rects2, padding=5)

        # For each bar in the chart, add a text label.
        step = 1
        for bar in ax.patches:
            # The text annotation for each bar should be its height.
            bar_value = bar.get_height()
            # You can do any type of formatting here though.
            text = f'{seconds_to_minutes(bar_value)}m'
            # This will give the middle of each bar on the x-axis.
            text_x = bar.get_x() + bar.get_width() / 2
            # get_y() is where the bar starts so we add the height to it.
            text_y = bar.get_y() + bar_value
            # If you want the text to be the same color as the bar or if you want a consistent
            # color, you can just set it as a constant, e.g. #222222
            bar_color = bar.get_facecolor()
            ax.text(text_x, text_y, text, ha='center', va='bottom', color=bar_color, size=4)

        fig.tight_layout()
        if to_file == True:
            fig.savefig("result.pdf", bbox_inches='tight')
        else:
            plt.show()
