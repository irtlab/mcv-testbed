from pymongo import MongoClient

from experiment import Experiment
from utils import convert_sec_to_date
from utils import write_data_to_csv_file
from grouped_bar_chart import GroupedBarChart
from audio import Audio

DB_URL = 'mongodb://localhost'
db_client = MongoClient(DB_URL)
db = db_client.mcv


def main():
    process_audio_files = True
    write_chart_to_file = True
    write_to_csv_file = False
    write_to_csv_file_data = []

    expr = Experiment(db, 'interactive')
    chart = GroupedBarChart()
    chart.set_title(expr.name)

    subject_idx = 0
    for expr_run in expr.expr_run_list:
        print(f'Processing data for the experiment run: {expr_run.id}')
        print(f'Start: {expr_run.start}. End: {expr_run.end}')
        if expr_run.events == None:
            print(f'Events for the `{expr.name}` experiment is not found')
            print('')
            continue

        subject_idx = subject_idx + 1
        dispatcher_speaking_sec = expr_run.events.dispatcher_speaking_sec(expr_run.dispatcher_terminal)
        total_speaking_sec = expr_run.events.total_speaking_sec
        subject_speaking_sec = total_speaking_sec - dispatcher_speaking_sec

        print(f'The experiment took: {convert_sec_to_date(expr_run.events.duration_sec)}')   
        print(f'Total speaking took: {convert_sec_to_date(total_speaking_sec)}')
        print(f'Total speaking (%): {expr_run.events.total_speaking_pct}% of total experiment time')
        print(f'Dispatcher speaking: {convert_sec_to_date(dispatcher_speaking_sec)}')
        print(f'Subject speaking: {convert_sec_to_date(subject_speaking_sec)}')
        print('')

        # The argument order matters
        chart.append([
            subject_idx,
            expr_run.events.duration_sec,
            expr_run.events.total_speaking_sec,
            expr_run.events.total_speaking_pct,
            dispatcher_speaking_sec,
            subject_speaking_sec
        ])

        if process_audio_files == True:
            audio = Audio(expr_run)
            audio.draw(subject_idx, write_chart_to_file)

        if expr_run.subject_environment and write_to_csv_file == True:
            write_to_csv_file_data.append([
                str(subject_idx),
                str(expr_run.events.duration_sec),
                str(total_speaking_sec),
                str(dispatcher_speaking_sec),
                str(subject_speaking_sec),
                str(expr_run.settings['ptt_delay']),
                str(expr_run.settings['tx_delay']),
                str(expr_run.settings['codec']),
                expr_run.subject_environment[0]['answer'],  # Native English Speaker
                expr_run.subject_environment[1]['answer'],  # Location Environment
                expr_run.subject_environment[2]['answer'],  # Noise Environment
                expr_run.subject_environment[3]['answer'],  # Visual Environment
                expr_run.subject_environment[4]['answer'],  # Familiarity with the NATO
                expr_run.subject_environment[5]['answer'],  # Familiarity with vode games
            ])

    if write_to_csv_file == True:
        write_data_to_csv_file(expr.name, write_to_csv_file_data)
    chart.draw(write_chart_to_file)


if __name__ == "__main__":
    main()
