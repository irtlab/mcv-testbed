import sys
from experiment_run import ExperimentRun


class Experiment(object):
    def __init__(self, db, type):
        experiments = self.get_experimenst(db, type)
        if experiments == None:
            sys.exit('No experiments to process')
        self._data = self.selecte_experiment(experiments)
        self.expr_run_list = self.get_expr_run(db, self._data)
        if self.expr_run_list == None:
            sys.exit(f'Experiment Run for the `{self._data["name"]}` experiment is not found')

    @property
    def name(self):
        return self._data['name']

    @property
    def id(self):
        return self._data['_id']

    @property
    def type(self):
        return self._data['type']

    @staticmethod
    def get_experimenst(db, expr_type):
        docs = db.experiments.find({ 'type': expr_type })
        if docs: return list(docs)
        return None

    @staticmethod
    def selecte_experiment(experiments):
        slelected_idx = -1
        print('Select the experiment you want to process')
        for idx, expr in enumerate(experiments):
            print(f"{idx + 1}. {expr['name']}")
        slelected_idx = int(input('Enter: '))
        if slelected_idx > len(experiments) or slelected_idx < 1:
            sys.exit('Error: Please provide correct number from the list. Out of range.')
        return experiments[slelected_idx - 1]

    @staticmethod
    def get_expr_run(db, expr):
        docs = db.experiment_run.find({ 'experiment': expr['_id'] })
        if docs:
            expr_run_list = []
            for expr_run in docs:
                expr_run_list.append(ExperimentRun(db, expr_run))
            return expr_run_list
        return None
