import datetime
import csv
import numpy as np
import matplotlib.pyplot as plt


def convert_sec_to_date(seconds):
    return str(datetime.timedelta(seconds=seconds))
    """
    seconds = seconds % (24 * 3600)
    hour = seconds // 3600
    seconds %= 3600
    minutes = seconds // 60
    seconds %= 60
    return f'{int(hour)} hours {int(minutes)} minutes {int(seconds)} seconds'
    """


def write_data_to_csv_file(expr_name, data):
    expr_name = expr_name.replace(" ", "")
    with open(f'{expr_name}.csv', mode='w') as csv_file:
        csv_writer = csv.writer(csv_file)
        csv_writer.writerow([
            'Subject',
            'Total Time (sec)',
            'Speaking Time (sec)',
            'Dispatcher Speaking Time (sec)',
            'Subject Speaking Time (sec)',
            'PTT (ms)',
            'MTE (ms)',
            'Codec',
            'Native English Speaker',
            'Location Environment',
            'Noise Environment',
            'Visual Environment',
            'Familiarity with the NATO',
            'Familiarity with video games',
        ])
        csv_writer.writerows(data)


def seconds_to_minutes(seconds):
    return round(seconds / 60, 2)
