from scipy.io import wavfile
import numpy as np
import matplotlib.pyplot as plt


file_path = '/srv/mcv/wavs/'

class Audio(object):
    def __init__(self, expr_run):
        self.expr_run_id = expr_run.id
        self._metadata = {}
        for name in ['ut1', 'ut2', 'ut3']:
            filename = expr_run.get_filename(name)
            if filename is not None:
                self._metadata[name] = wavfile.read(f'{file_path}{filename}')

    @property
    def metadata(self):
        return self._metadata

    def draw(self, subject_idx, to_file = False):
        if not len(self._metadata): return

        i = 1
        for name in self._metadata:
            # TODO: It might be a good idea to calculate the following four values and keep
            # in a dictonary for every terminal. 
            rate, data = self._metadata[name]
            sound = data / 2.0**15
            length_in_s = sound.shape[0] / rate
            time = np.arange(sound.shape[0]) / sound.shape[0] * length_in_s

            ax = plt.subplot(3, 2, i)
            plt.plot(time, sound, 'r')
            plt.xlabel("Time [s]", fontsize='x-small')
            plt.ylabel(f"Amplitude", fontsize='x-small')
            ax.set_title(f'Amplitude at User Terminal {name}', fontsize='small')

            spectrum = np.fft.rfft(sound)
            freq = np.fft.rfftfreq(sound.size, d=1. / rate)
            spectrum_abs = np.abs(spectrum)

            ax = plt.subplot(3, 2, i + 1)
            plt.plot(freq, spectrum_abs)
            plt.xlabel("Frequency [Hz]", fontsize='x-small')
            plt.ylabel(f"Level", fontsize='x-small')
            ax.set_title(f'Frequency spectrum at User Terminal {name}', fontsize='small')

            i = i + 2

        plt.tight_layout()
        if to_file == True:
            plt.savefig(f'{subject_idx}_audio_result_{self.expr_run_id}.pdf', bbox_inches='tight')
        else:
            plt.show()
        plt.figure().clear()
        plt.close()
        plt.cla()
        plt.clf()
