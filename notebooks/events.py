import re


class Events(object):
    def __init__(self, data):
        self._data = data

    @property
    def data(self):
        return self._data

    @property
    def duration_sec(self):
        duration = self._data[len(self._data) - 1]['timestamp'] - self._data[0]['timestamp']
        return int(duration.total_seconds())

    @property
    def total_speaking_sec(self):
        return self.get_speaking_time('all')

    def dispatcher_speaking_sec(self, terminal_name):
        return self.get_speaking_time(terminal_name)

    @property
    def total_speaking_pct(self):
        return self.get_pct_of(self.total_speaking_sec, self.duration_sec)

    @staticmethod
    def get_pct_of(value, total):
        return int((value / total) * 100)

    # TODO: I must double check this method and improve it
    def get_speaking_time(self, terminal = 'all'):
        speaking_sec = 0
        start = 0
        for event in self._data:
            if event['event'] == 'SPEAKERMIC_SWITCH' and 'data' in event and event['data']['state'] == True:
                start = event['timestamp']
            match = 'all'
            if terminal != 'all':
                match = re.search(rf'{terminal}', event['terminal'])
            if event['event'] == 'PTT_RELEASE' and start != 0 and match is not None:
                duration = event['timestamp'] - start
                speaking_sec = speaking_sec + duration.total_seconds()
        return int(speaking_sec)
