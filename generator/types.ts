// NOTE: For now this file must be synchronized with frontend/types.ts

import type { PercentBreakdown } from '@janakj/lib/parse';


export interface ExperimentRun {
    _id: string;
    subject: {
        _id: string;
        name: string;
        email: string;
    },
    experiment: string;
    experimenter: string;
    start: Date | number;
    end: Date | number;
    failed: boolean;
    reason: string;
    status: 'Finished' | 'Running' | 'Ready to start' | 'Generating audio' | 'Failed to generate audio';
    subject_environment: {
        question: string;
        answer: string;
    }[];
    audio?: string[];
}


export interface AudioSettings {
    codec: string;
    expr_name: string;
    step_number: number;
    mic: string | null;
    rx_loss: number | Record<string, any> | null;
    rx_error: number | null;
    text: string;
    gender: 'male' | 'female';
    ssml_text: string;
}


export type ExperimentAudioSettings = {
    gender: 'male' | 'female' | PercentBreakdown<'male' | 'female'>,
    codec: string | PercentBreakdown<string>,
    frame_loss?: FrameLossType | PercentBreakdown<FrameLossType>,
    bit_errors?: number | PercentBreakdown<number>
};


export type FrameLossType = number | {
    type: 'gilbert-elliot';
    pgb: number;
    pbg: number;
};


export interface Experiment {
    type: 'interactive' | 'listening';
    _id: string;
    name: string;
    author: string;
    settings?: ExperimentAudioSettings;
    consent_form?: string;
    practice_expr?: boolean;
    subject_notes?: string;
    finish_notes?: string;
    randomize_steps?: boolean;
    normalize_audio?: boolean;
    files?: ExperimentFile[];
    steps?: ExperimentStep[];
}


export interface ExperimentStep {
    settings: Partial<AudioSettings>;
    answer: SingleChoiceAnswer | MultipleChoiceAnswer | FreeFormAnswer;
    correct_answer?: string;
    audio?: string;
}

export interface ExperimentFile {
    id: string;
    name: string;
    lastModified: number;
    size: number;
    type: string;
    url: string;
}


export interface Answer {
    description?: string;
}


export interface ChoiceAnswer extends Answer {
    choices: string[];
    randomizeChoice?: boolean;
}


export interface SingleChoiceAnswer extends ChoiceAnswer {
    type: 'single_choice'
}


export interface MultipleChoiceAnswer extends ChoiceAnswer {
    type: 'multiple_choice'
}


export interface FreeFormAnswer extends Answer {
    type: 'free_form_text'
}
