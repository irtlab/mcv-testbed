import axios, { AxiosInstance } from 'axios';
import stdio from 'stdio';
import nunjucks from 'nunjucks';
import { randomInt } from 'crypto';
import 'dotenv/config';
import { promises as fs } from 'fs';
import { env } from 'process';
import { AudioSettings, Experiment, ExperimentAudioSettings, ExperimentStep, FrameLossType } from './types';
import { parsePercentBreakdown, PercentBreakdown } from '@janakj/lib/parse';
import { shuffle as shuffleInPlace } from '@janakj/lib/random';

const DEFAULT_BACKEND_URL = 'https://mcv-testbed.cs.columbia.edu';
const DEFAULT_CONFIG = 'config/mturk-nato-license-4.json';


type InputData = {
    experiment_name: string,
    number_of_steps: number,
    text_generator_type: string,
    text_generator_pattern: string,
    template_text: string,
    excluded_characters: Array<string>,
    nato_alphabet: boolean,
    ssml: boolean,
    break_after_comma: number,
    server_side?: boolean;
    randomize_steps?: boolean;
    normalize_audio?: boolean;
} & ExperimentAudioSettings;


interface GenTextResult {
    text: string,
    correct_answer: string
}


const nato_phonetic_alphabet: Readonly<Partial<Record<string | number, string>>> = {
    A: 'Alfa',
    B: 'Bravo',
    C: 'Charlie',
    D: 'Delta',
    E: 'Echo',
    F: 'Foxtrot',
    G: 'Golf',
    H: 'Hotel',
    I: 'India',
    J: 'Juliett',
    K: 'Kilo',
    L: 'Lima',
    M: 'Mike',
    N: 'November',
    O: 'Oscar',
    P: 'Papa',
    Q: 'Quebec',
    R: 'Romeo',
    S: 'Sierra',
    T: 'Tango',
    U: 'Uniform',
    V: 'Victor',
    W: 'Whiskey',
    X: 'X-ray',
    Y: 'Yankee',
    Z: 'Zulu',
    0: 'Zero',
    1: 'One',
    2: 'Two',
    3: 'Three',
    4: 'Four',
    5: 'Five',
    6: 'Six',
    7: 'Seven',
    8: 'Eight',
    9: 'Nine'
};


function verifyInputData(data: InputData) {
    if (!data.experiment_name || typeof data.experiment_name !== 'string') {
        throw new Error('The experiment name (experiment_name field) must be provided, and it must be string type.');
    }

    if (!data.number_of_steps || typeof data.number_of_steps !== 'number') {
        throw new Error('The number of steps (number_of_steps field) must be provided, and it must be number type >0.');
    }

    if (data.text_generator_type !== 'license plate') {
        throw new Error("The text template must be 'license plate'.");
    } else if (!data.text_generator_pattern.length) {
        throw new Error('Please provide license plate pattern in L and D format (L = letter, D = digit).');
    }

    // TODO verify pattern.

    if (data.nato_alphabet !== false && data.nato_alphabet !== true) {
        throw new Error('The NATO alphabet (nato_alphabet field) must be true or false.');
    }

    parsePercentBreakdown<'male' | 'female'>(data.gender);
    parsePercentBreakdown<string>(data.codec);

    if (data.frame_loss !== undefined)
        parsePercentBreakdown<FrameLossType>(data.frame_loss);

    if (data.bit_errors !== undefined)
        parsePercentBreakdown<number>(data.bit_errors);
}



// Convert percentage to count and round down.
function getPctCount(pct: number, total_count: number): number {
    return Math.floor((pct * total_count) / 100);
}


function shuffle<T>(data: T[]): T[] {
    const array = [...data];
    shuffleInPlace(array);
    return array;
}


function fillArray<T>(config: T | PercentBreakdown<T>, number_of_steps: number, randomize_steps?: boolean): T[] {
    const items = parsePercentBreakdown<T>(config);

    const result: T[] = [];
    items.forEach((([pct, val]) => {
        const count = getPctCount(pct, number_of_steps);
        for(let i = 0; i < count; i++) result.push(val);
    }));

    // This is because I always round down in getPctCount function.
    if (result.length && result.length !== number_of_steps) {
        result.push(result[result.length - 1]);
    }

    // Randomize orders so it will be mixed.
    return randomize_steps ? shuffle<T>(result) : result;
}


function getRandomValue(arr: Array<string>): string {
    if (!Number.isSafeInteger(0) || !Number.isSafeInteger(arr.length))
        throw new Error('Ranges in getRandomValue are not safe integers');
    return arr[randomInt(0, arr.length)];
}


function generateLicensePlateText(pattern: string, letters: string[], numbers: string[], nato_alphabet: boolean) {
    const result: GenTextResult = { text: '', correct_answer: '' };
    for (let i = 0; i < pattern.length; i++) {
        if (pattern[i] !== 'L' && pattern[i] !== 'D') {
            result.text += pattern[i];
        } else {
            const random_val = pattern[i] === 'L' ? getRandomValue(letters) : getRandomValue(numbers);
            result.correct_answer += random_val;
            if (nato_alphabet) {
                result.text += nato_phonetic_alphabet[random_val];
            } else {
                result.text += random_val;
            }
        }
    }

    return result;
}


function getText(config: InputData, letters: Array<string>, numbers: Array<string>) {
    let result: GenTextResult = { text: '', correct_answer: '' };

    switch (config.text_generator_type) {
        case 'license plate':
            result = generateLicensePlateText(config.text_generator_pattern, letters, numbers, config.nato_alphabet);
            break;

        default:
            throw new Error(`Unsupported text generator '${config.text_generator_type}''`);
    }

    if (config.template_text.length) {
        result.text = nunjucks.renderString(config.template_text, { value: result.text });
    }

    return result;
}


// TODO. Currently, this can set only break element with time.
// This function should be more generic
function textToSSML(config: InputData, text: string) {
    let result = '';
    for (let i = 0; i < text.length; i++) {
        if (text[i] === ',' || text[i] === ':') result += ` <break time="${config.break_after_comma}ms"/>`;
        else result += text[i];
    }

    return result;
}


async function getExperimentID(api: AxiosInstance, experiment_name: string) {
    try {
        const { data } = await api.get<[{ _id: string, name: string }]>('/experiment');
        for (let i = 0; i < data.length; i++) {
            if (data[i].name === experiment_name) return data[i]._id;
        }
        throw new Error(`Experiment ${experiment_name} not found`);
    } catch (error: any) {
        const res = error.response;
        throw res && res.data ? res.data : res || error;
    }
}


function abort(error, signame = 'SIGTERM') {
    if (typeof error !== 'undefined') console.error('Aborting: ', error);
    process.kill(process.pid, signame);
}


function printPercentBreakdown<T>(value: T | PercentBreakdown<T>) {
    const parsed = parsePercentBreakdown(value);
    return parsed
        .map(([ pct, val ]) => `${pct}%: ${JSON.stringify(val)}`)
        .join(', ');
}


function printSettings(data: InputData) {
    console.log(`SETTINGS
  Experiment Name:        ${data.experiment_name}
  Server Side:            ${data.server_side === true ? 'YES' : 'NO'}
  Number of Steps:        ${data.number_of_steps}
  Randomize Steps:        ${data.randomize_steps === true ? 'YES' : 'NO'}
  Normalize Audio:        ${data.normalize_audio === true ? 'YES' : 'NO'}
  Text Generator Type:    ${data.text_generator_type}
  Text Generator Pattern: ${data.text_generator_pattern.length ? data.text_generator_pattern : 'Not applied'}
  Template Text           ${data.template_text.length ? data.template_text : 'Not applied'}
  Excluded Characters:    ${data.excluded_characters.join(', ')}
  Nato Alphabet:          ${data.nato_alphabet ? 'YES' : 'NO'}
  Gender:                 ${printPercentBreakdown(data.gender)}
  Codec:                  ${printPercentBreakdown(data.codec)}
  ${data.frame_loss ? `Frame loss:             ${printPercentBreakdown(data.frame_loss)}` : ''}
  ${data.bit_errors ? `Bit errors:             ${printPercentBreakdown(data.bit_errors)}` : ''}
  `);
}


function printApplyingStep(settings: Partial<AudioSettings>, i: number, count: number) {
    let be_fl_text = '';
    if (settings.rx_loss !== null && settings.rx_error !== null) {
        be_fl_text = `loss=${JSON.stringify(settings.rx_loss)}%,BER=${settings.rx_error}%`;
    } else if (settings.rx_loss !== null) {
        be_fl_text = `loss=${JSON.stringify(settings.rx_loss)}%`;
    } else {
        be_fl_text = `BER=${settings.rx_error}%`;
    }

    console.log(`${i}/${count}: Impairing text "${settings.text}" using parameters [gender=${settings.gender},codec=${settings.codec},${be_fl_text}]`);
}


async function getImpairedAudio(api: AxiosInstance, body: any, accept = 'application/vnd+mcv-testbed-url') {
    try {
        const { data } = await api.post('/impair', body, {
            headers: { accept }
        });
        return data as string;
    } catch (error: any) {
        const res = error.response;
        throw res && res.data ? res.data : res || error;
    }
}


export async function updateExperiment(api: AxiosInstance, id: string, body: Partial<Experiment>) {
    try {
        const { data } = await api.post(`/experiment/${id}`, body);
        return data;
    } catch (error: any) {
        const res = error.response;
        throw res && res.data ? res.data : res || error;
    }
}


(async () => {
    nunjucks.configure({ autoescape: true });

    const opts = stdio.getopt({
        configFile: {
            key: 'f', args: 1, default: env.INPUT_FILE || DEFAULT_CONFIG, description: 'Absolute path to the config.json file'
        },
        backendURL: {
            key: 'b', args: 1, default: env.BACKEND_URL || DEFAULT_BACKEND_URL, description: 'Backend API base URL'
        },
        dryRun: {
            key: 'd', args: 0, required: false, description: 'Enable dry-run mode (no experiment modifications performed)'
        }
    });

    if (opts === null)
        throw new Error('Error while parsing command line arguments');

    if (typeof opts.configFile !== 'string' || opts.configFile.length === 0)
        throw new Error('Invalid or missing configuration filename');

    if (typeof opts.backendURL !== 'string' || opts.backendURL.length === 0)
        throw new Error('Invalid or missing backend URL');

    if (opts.dryRun)
        console.log('This is a dry run, no experiment modifications will be performed\n');

    const config: InputData = JSON.parse(await fs.readFile(opts.configFile, { encoding: 'utf8' }));
    verifyInputData(config);

    const api = axios.create({ baseURL: `${opts.backendURL}/api`, withCredentials: false });
    const expr_id = await getExperimentID(api, config.experiment_name);

    // I do this to easily filter excluded characters.
    const excluded_characters: Record<string, number> = {};
    config.excluded_characters.forEach(i => { excluded_characters[i] = 1; });

    printSettings(config);

    const in_letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('').filter(l => excluded_characters[l] === undefined);
    const in_numbers = '0123456789'.split('').filter(l => excluded_characters[l] === undefined);

    // TODO. Currently, the markup language cannot be set in the config file.
    const steps: ExperimentStep[] = new Array(config.number_of_steps).fill({} as ExperimentStep).map((_e, i) => {
        const txt_obj = getText(config, in_letters, in_numbers);
        return {
            settings: {
                expr_name: config.experiment_name,
                step_number: i + 1,
                mic: '-3', // '-3' is a Google TTS state of the impaired file. This is needed in the UI side to set microphone input type.
                text: txt_obj.text,
                ssml_text: config.ssml ? textToSSML(config, txt_obj.text) : ''
            },
            answer: {type: 'free_form_text'},
            correct_answer: txt_obj.correct_answer,
            audio: ''
        };
    });

    const experiment: Partial<Experiment> = { steps };
    if (config.randomize_steps !== undefined) experiment.randomize_steps = config.randomize_steps;
    if (config.normalize_audio !== undefined) experiment.normalize_audio = config.normalize_audio;

    if (config.server_side) {
        // If this is an experiment with server-side generated impairments, do
        // nothing to the array of steps. Impaired audio files will be generated
        // for each test subject individually once they have started an
        // experiment run. We just need to save experiment-level settings in the
        // experiment.
        experiment.settings = {
            gender: config.gender,
            codec: config.codec,
            frame_loss: config.frame_loss,
            bit_errors: config.bit_errors
        };
    } else {
        // If we are not generating a server-side experiment, generate settings
        // and impaired audio files for all steps right now and upload the data
        // to the backend server
        const codec = fillArray<string>(config.codec, config.number_of_steps, config.randomize_steps);
        const frame_loss = fillArray<FrameLossType>(config.frame_loss || 0, config.number_of_steps, config.randomize_steps);
        const bit_errors = fillArray<number>(config.bit_errors || 0, config.number_of_steps, config.randomize_steps);
        const gender = fillArray<string>(config.gender, config.number_of_steps, config.randomize_steps);

        // TODO. I need to test and see Google TTS request limits so I can use Promise.all()
        for (let i = 0; i < steps.length; i++) {
            const { settings } = steps[i];
            settings.codec = codec[i];
            settings.rx_loss = frame_loss[i] !== undefined ? frame_loss[i] : null;
            settings.rx_error = bit_errors[i] !== undefined ? bit_errors[i] : null;
            settings.gender = gender[i] as 'male' | 'female';

            printApplyingStep(steps[i].settings, i + 1, steps.length);
            if (opts.dryRun) {
                await getImpairedAudio(api, steps[i].settings, 'audio/wav');
            } else {
                steps[i].audio = await getImpairedAudio(api, steps[i].settings);
            }
        }
    }

    if (!opts.dryRun) {
        await updateExperiment(api, expr_id, experiment);
    }
    console.log('');
    if (opts.dryRun) {
        console.log(`NOT updating '${config.experiment_name}' (dry run)`);
    } else {
        console.log(`The expriment '${config.experiment_name}' was updated successfully!`);
    }
    console.log('');

})().catch(abort);
