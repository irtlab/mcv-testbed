# MCV Experiment Generator
A small TypeScript command line program to automatically generate MCV experiment steps.

## Install
```bash
git clone git@github.com:irtlab/mcv-testbed.git
cd generator
npm install
```

## Usage
The program takes the following command line arguments:
```bash
-f, --configFile <ARG1>       Absolute path to the config.json file ("config.json" by default)
-b, --backendURL <ARG1>       Backend API base URL ("https://mcv-testbed.cs.columbia.edu" by default)
-d, --dryRun                  Enable dry-run mode (no experiment modifications performed)
```
You also can provide the above values through environment variables by creating `.env` file in the root directory and providing the following variables:
```
BACKEND_URL=<Backend server's URL>
INPUT_FILE=<config.json file path>
```

Once you setup a configuration file (see: [Config File Format](#configfileformat)) you can generate experiment steps by running the following command:
```bash
npm run dev
```

## Config File Format <a name="configfileformat"></a>
Here is the config JSON file format (see [config.json](https://gitlab.com/irtlab/mcv-testbed/-/blob/master/generator/config.json) file as an example)
```
{
  "experiment_name":        "<Experiment name>",
  "number_of_steps":        "<Total steps>",
  "text_generator_type":    "<Text generator type>",
  "text_generator_pattern": "<A text generator pattern>"
  "template_text":          "<Template text pattern in nunjucks library format>",
  "break_after_comma":      "<Break after comman in milliseconds>",
  "ssml":                   "<true | false>",
  "number_of_characters":   "<A number of character>",
  "excluded_characters":    "<A list of excluded values>",
  "nato_alphabet":          "<true | false>",
  "gender": {
      "female": "<percentage to be distributed>",
      "male": "<percentage to be distributed>"
  },
  "codec": {
      "<codec name>": "<percentage to be distributed>",
      ...
  },
  "frame_loss": {
      "<frame loss value>": "<percentage to be distributed>",
      ...
  },
  "bit_errors": {
      "<bit errors value>": "<percentage to be distributed>",
      ...
  }
}
```

##### Explanation
| Parameter | Description |
| :--- | :--- |
| `experiment_name` | Full name of the experiment |
| `number_of_steps` | A number of steps (samples) |
| `text_generator_type` | Text generator type. Currently, the only supported type is 'license plate' |
| `text_generator_pattern` | Generate a text based on pattern in L and D format (L = letter, D = digit). For example, "LDD-LLL"|
| `template_text` | Template text pattern in nunjucks library format. This can be set an empty string if no template is required |
| `break_after_comma` | Speech break after comma. Default is 250ms |
| `ssml` | Enable or disable Speech Synthesis Markup Language (SSML) |
| `number_of_characters` | A number of characters in the text. This is not considered if the text pattern is given |
| `excluded_characters` | A list of the excluded characters |
| `nato_alphabet` | `true` if the NATO alphabet needs to be generated, otherwise `false` |
| `gender` | Percentage of the male (`gender.male` field) and female (`gender.female` field) voice out of the `number_of_steps` |
| `codec` | The key is a codec name and the value is a percentage out of the `number_of_steps` to be distributed |
| `frame_loss` | The key is a frame loss percentage and the value is a percentage out of the `number_of_steps` to be distributed |
| `bit_errors` | The key is a frame loss percentage and the value is a percentage out of the `number_of_steps` to be distributed |
