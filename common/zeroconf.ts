import debug from 'debug';
import dbus from 'dbus-next';
import { EventEmitter } from 'events';
import { TimeoutError } from './errors';


const dbg = debug('mcv:zeroconf'),
    prefix = 'org.freedesktop';


const DEFAULT_TTL = 5 * 60;

const IN  = 1,
    TXT = 16,
    PTR = 12,
    SRV = 33;

const AVAHI_IF_UNSPEC      = -1,
    AVAHI_PROTO_UNSPEC   = -1,
    AVAHI_PUBLISH_UNIQUE = 1;

const [
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    AVAHI_GROUP_UNCOMMITED,
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    AVAHI_GROUP_REGISTERING,
    AVAHI_GROUP_ESTABLISHED,
    AVAHI_GROUP_COLLISION,
    AVAHI_GROUP_FAILURE
] = [...Array(5).keys()];


// A global persistent connection to the Avahi daemon reused by simple functions
// such as resolve_service.
let avahiConnection;


async function connectAvahi() {
    const sysbus = dbus.systemBus();
    const obj    = await sysbus.getProxyObject(`${prefix}.Avahi`, '/');
    const avahi  = await obj.getInterface(`${prefix}.Avahi.Server`);
    return { sysbus, avahi };
}


/**
 * Return an absolute domain name by optionally appending the origin to it. If
 * given an absolute name (ends with .), the origin is not appended.
 *
 * @param name Domain name (absolute or relative)
 * @param origin (DNS origin to append to relative names)
 */
function withOrigin(name, origin) {
    let o = origin;
    if (typeof origin !== 'undefined' && !origin.endsWith('.'))
        o = `${origin}.`;

    if (!name.endsWith('.')) return `${name}.${o}`;
    return name;
}


/**
 * Escape special characters and values < 0x20 in a DNS label.
 * @param text Label text
 */
function escape(text) {
    let ord, rv = '';
    for(const c of text) {
        switch(c) {
            case '"':
            case '(':
            case ')':
            case '.':
            case ';':
            case '\\':
            case '$':
                rv += '\\';
                rv += c;
                break;

            case '@':
                ord = c.charCodeAt(0);
                rv += `\\${Number(ord).toString().padStart(3, '0')}`;
                break;

            default:
                ord = c.charCodeAt(0);
                if (ord <= 32) rv += `\\${Number(ord).toString().padStart(3, '0')}`;
                else rv += c;
                break;
        }
    }
    return rv;
}


/**
 * Encode an unsigned 16-bit integer into DNS wire-format
 * @param val 16-bit unsigned integer to be encoded
 * @returns An array of two numbers in network byte order
 */
function encodeUint16(val) {
    if (val < 0 || val > 65535)
        throw new Error(`Value ${val} out of range`);
    return [(val >> 8) & 0xff, val & 0xff];
}


/**
 * Encodes a domain name in string representation into a set of labels in DNS
 * wire format.
 * @param name String with a domain name
 * @returns An array of bytes with labels in DNS wire format
 */
function encodeName(name: string) {
    const rv: number[] = [], labels = name.split('.');

    for(const l of labels) {
        rv.push(l.length);
        for(let i = 0; i < l.length; i++)
            rv.push(l.charCodeAt(i));
    }
    return rv;
}


/**
 * Create a DNS SRV rdata object out of the given parameters.
 * @param hostname The target hostname
 * @param port Port number
 * @param priority Priority (default is 0)
 * @param weight Weight (default is 0)
 * @returns An array if integers in DNS SRV wire format
 */
function encodeSRV(hostname, port, priority=0, weight=0) {
    if (port < 0 || port > 65535)
        throw new Error(`Invalid port number "${port}"`);

    if (priority < 0 || priority > 65535)
        throw new Error(`Invalid priority value "${priority}"`);

    if (weight < 0 || weight > 65535)
        throw new Error(`Invalid weight value "${weight}"`);

    return Array.prototype.concat(
        encodeUint16(priority),
        encodeUint16(weight),
        encodeUint16(port),
        encodeName(hostname));
}


/**
 * Build a DNS TXT record in wire-format out of the given attributes. The
 * attributes will be encoded as key=value pairs.
 * @param attrs A dictionary of key-value pairs
 * @returns An array of bytes with a DNS TXT record in wire format
 */
function encodeTXT(attrs: Record<string, any>) {
    let l: number[] = [];
    Object.keys(attrs).forEach(k => {
        const v = attrs[k];
        if (typeof v === 'undefined')
            l = l.concat(encodeName(k));
        else
            l = l.concat(encodeName(`${k}=${v}`));
    });
    if (l.length === 0) l.push(0);
    return l;
}


/**
 * Publish a service through ZeroConf via Avahi Daemon.
 *
 * The argument 'params' is an object which can contain the following additional
 * parameters:
 * - domain   : Domain to publish into (instead of '.local')
 * - hostname : Target hostname
 * - ttl      : DNS TTL value
 * - weight   : DNS SRV weight value
 * - priority : DNS SRV priority value
 * - txt      : A dictionary of key=value pairs to advertise via DNS TXT
 *
 * @param name The name of the service (string)
 * @param type Service type, e.g., "_http._tcp"
 * @param port Port number
 * @param params Additional parameters
 */
export async function publishService(name, type, port, params: Record<string, any>={}) {
    if (port < 0 || port > 65535)
        throw new Error(`Invalid port number "${port}"`);

    if (typeof avahiConnection === 'undefined')
        avahiConnection = connectAvahi();

    const { sysbus, avahi } = await avahiConnection;
    dbg(`Connected to Avahi daemon, publishing service '${name}.${type}'`);

    const path  = await avahi.EntryGroupNew();
    const gobj  = await sysbus.getProxyObject(`${prefix}.Avahi`, path);
    const group = await gobj.getInterface(`${prefix}.Avahi.EntryGroup`);

    let origin = params.domain || await avahi.GetDomainName();
    if (!origin.endsWith('.')) origin = `${origin}.`;

    const hostname = withOrigin(params.hostname || await avahi.GetHostName(), origin);

    const ttl      = params.ttl      || DEFAULT_TTL,
        priority = params.priority || 0,
        weight   = params.weight   || 0,
        txt      = params.txt      || {};

    const ptr_name = withOrigin(type, origin),
        srv_name = withOrigin(`${escape(name)}.${type}`, origin),
        dsd_name = withOrigin('_services._dns-sd._udp', origin);

    await Promise.all([
        group.AddRecord(AVAHI_IF_UNSPEC, AVAHI_PROTO_UNSPEC, 0, ptr_name,
            IN, PTR, ttl, encodeName(srv_name)),
        group.AddRecord(AVAHI_IF_UNSPEC, AVAHI_PROTO_UNSPEC, AVAHI_PUBLISH_UNIQUE, srv_name,
            IN, SRV, ttl, encodeSRV(hostname, port, priority, weight)),
        group.AddRecord(AVAHI_IF_UNSPEC, AVAHI_PROTO_UNSPEC, AVAHI_PUBLISH_UNIQUE, srv_name,
            IN, TXT, ttl, encodeTXT(txt)),
        group.AddRecord(AVAHI_IF_UNSPEC, AVAHI_PROTO_UNSPEC, 0, dsd_name,
            IN, PTR, ttl, encodeName(ptr_name)),
    ]);

    return new Promise((resolve, reject) => {
        const listener = state => {
            let error;
            switch(state) {
                case AVAHI_GROUP_ESTABLISHED:
                    group.off('StateChanged', listener);
                    resolve(group);
                    break;

                case AVAHI_GROUP_COLLISION:
                    group.off('StateChanged', listener);
                    error = new Error(`ZeroConf naming conflict detected`);
                    error.group = group;
                    reject(error);
                    break;

                case AVAHI_GROUP_FAILURE:
                    group.off('StateChanged', listener);
                    error = new Error(`Unknown error while publishing DNS records through Avahi`);
                    error.group = group;
                    reject(error);
                    break;

                default:
                    break;
            }
        };

        group.on('StateChanged', listener);
        group.Commit().catch(error => reject(error));
    });
}


export async function avahiHostname() {
    if (typeof avahiConnection === 'undefined')
        avahiConnection = connectAvahi();
    const { avahi } = await avahiConnection;
    return avahi.GetHostName();
}


/*
 *
 */
export class ServiceBrowser extends EventEmitter {
    private srvType: any;
    private domain: string | undefined;
    public services: Record<string, any>;
    private browser?: any;

    constructor(srv_type, domain) {
        super();
        this.srvType = srv_type;
        this.domain = domain;
        this.services = Object.create(null);
        this._on_message = this._on_message.bind(this);
    }

    async start() {
        const { avahi, sysbus } = await connectAvahi();
        const domain = this.domain || await avahi.GetDomainName();

        dbg(`Connected to Avahi daemon, creating service browser for '${this.srvType}.${domain}'`);
        sysbus.on('message', this._on_message);

        const path   = await avahi.ServiceBrowserNew(AVAHI_IF_UNSPEC, AVAHI_PROTO_UNSPEC, this.srvType, domain, 0);
        const obj    = await sysbus.getProxyObject(`${prefix}.Avahi`, path);
        this.browser = await obj.getInterface(`${prefix}.Avahi.ServiceBrowser`);

        this.browser.on('Failure', error => this.emit('error', error));
    }


    _on_message(msg) {
        const { type, body, member } = msg;
        if (type !== 4) return;
        if (msg.interface !== `${prefix}.Avahi.ServiceBrowser`) return;

        const name = body[2];
        if (member === 'ItemNew') {
            if (this.services[name]) return;
            this.services[name] = true;
            this.emit('found', name);
        } else if (member === 'ItemRemove') {
            if (!this.services[name]) return;
            delete this.services[name];
            this.emit('lost', name);
        } else if (member === 'AllForNow') {
            this.emit('all_for_now');
        } else if (member === 'CacheExhausted') {
            this.emit('cache_exhausted');
        }
    }

    stop() {
        if (!this.browser) return Promise.resolve();
        return this.browser.Free();
    }
}


export async function resolve_service(name, type, domain) {
    if (typeof name !== 'string' || typeof type !== 'string')
        throw new Error(`Invalid arguments ${name} ${type}`);

    if (typeof avahiConnection === 'undefined')
        avahiConnection = connectAvahi();

    const { avahi } = await avahiConnection;
    try {
        const rv = await avahi.ResolveService(
            AVAHI_IF_UNSPEC, AVAHI_PROTO_UNSPEC,
            name, type, domain || await avahi.GetDomainName(),
            AVAHI_PROTO_UNSPEC, 0);

        return { hostname: rv[5], port: rv[8] };
    } catch(error: any) {
        if (error instanceof dbus.DBusError)
            if (error.type === `${prefix}.Avahi.TimeoutError`)
                throw new TimeoutError(error);
        throw error;
    }
}
