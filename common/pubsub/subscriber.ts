/* eslint-disable @typescript-eslint/ban-ts-comment */
import debug from 'debug';
import EventEmitter from 'events';
import sleep from '@janakj/lib/sleep';
import type { Socket as ClientSocket } from 'socket.io-client';

const dbg = debug('mcv:pubsub:subscriber');


/**
 * Create and maintain a subscription with a remote publisher
 *
 * This object represents a subscription to a particular room on a remote
 * publisher. Invoke the method subscribe() to create a subscription. Invoke the
 * method unsubscribe() to terminate the subscription.
 *
 * The promises returned by subscribe() and unsubscribe() resolve once the
 * subscription has been successfully created/terminated, i.e., after the
 * connection has been established in case of subscribe().
 *
 * An active subscription will be automatically terminated if the underlying
 * WebSocket connection closes. The user can invoke subscribe() again in that
 * case to re-create the subscription.
 *
 * The object emits the following events:
 *   - established: The subscription has been established
 *   - terminated: The subscription has been terminated
 *   - failed: A request to establish or terminate subscription failed
 *
 * The attribute "established" is set to true if the subscription has been
 * established and to false otherwise.
 */
export class Subscription extends EventEmitter {
    private sock: ClientSocket;
    protected room: string;
    protected established: boolean;

    constructor(sock: ClientSocket, room: string) {
        super();
        this.sock = sock;
        this.room = room;
        this.disconnected = this.disconnected.bind(this);
        this.established = false;
    }

    // @ts-ignore
    on(name, listener) {
        if (name === 'established' || name === 'terminated' || name === 'failed' || name === 'reset')
            return super.on(name, listener);

        return this.sock.on(`${this.room}:${name}`, listener);
    }

    // @ts-ignore
    off(name, listener) {
        if (name === 'established' || name === 'terminated' || name === 'failed' || name === 'reset')
            return super.on(name, listener);

        return this.sock.off(`${this.room}:${name}`, listener);
    }

    disconnected() {
        this.established = false;
        this.emit('terminated');
    }

    async subscribe() {
        const emit_subscribe = async () => {
            return new Promise((resolve, reject) => {
                this.sock.emit('subscribe', this.room, (error, value) => {
                    if (error !== null) {
                        reject(new Error(error));
                        this.emit("failed", error);
                    } else {
                        this.established = true;
                        resolve(value);
                        this.emit("established", value);
                    }
                });
            });
        };

        const doSubscribe = async() => {
            const rv = await emit_subscribe();
            this.sock.once('disconnect', this.disconnected);
            return rv;
        };

        if (this.sock.connected) return doSubscribe();

        return new Promise((resolve, reject) => {
            const onError = (e: Error) => {
                this.sock.off('connect', onConnect);
                reject(e);
                this.emit("failed", e);
            };
            const onConnect = () => {
                this.sock.off('connect_error', onError);
                this.sock.off('error', onError);
                resolve(doSubscribe());
            };
            this.sock.once('connect', onConnect);
            this.sock.once('connect_error', onError);
            this.sock.once('error', onError);
        });
    }

    async unsubscribe() {
        if (!this.sock.connected)
            throw new Error(`Error while unsubscribing from ${this.room}: Socket disconnected`);

        return new Promise((resolve, reject) => {
            this.sock.emit('unsubscribe', this.room, (error, value) => {
                if (error !== null) {
                    reject(new Error(error));
                } else {
                    this.established = false;
                    resolve(value);
                    this.sock.off('disconnect', this.disconnected);
                    this.emit('terminated');
                }
            });
        });
    }
}


/**
 * A specialized version of Subscription (above) that automatically renews
 * itself when the connection is closed or aborted. Emits "reset" when that
 * happens. After a successful subscription the "reset" event will carry the data
 * returned by the subscribe operation. Other times there will be no data.
 */
export class RenewingSubscription extends Subscription {
    private _wait: number;
    private _stopped?: boolean;
    private _subscribed?: boolean;

    constructor(sock: ClientSocket, room: string, wait=1000) {
        super(sock, room);
        this._wait = wait;
        this._restart = this._restart.bind(this);
    }

    async _restart() {
        await sleep(this._wait);
        if (!this._stopped) return this.start();
    }

    async start() {
        this._stopped = false;

        this.emit('reset');

        if (!this._subscribed) {
            this.on('terminated', this._restart);
            this.on('failed', this._restart);
            this._subscribed = true;
        }

        try {
            const data = await this.subscribe();
            this.emit('reset', data);
        } catch(e) {
            dbg(`Subscribe to ${this.room} failed: ${e}`);
        }
    }

    async stop() {
        this._stopped = true;

        this.off('failed', this._restart);
        this.off('terminated', this._restart);
        this._subscribed = false;

        this.emit('reset');

        if (this.established) {
            try {
                await this.unsubscribe();
            } catch(e) {
                dbg(`Unsubscribe from ${this.room} failed: ${e}`);
            }
        }
    }
}
