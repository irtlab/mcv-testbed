/* eslint-disable @typescript-eslint/ban-ts-comment */
import debug from 'debug';
import abort from '@janakj/lib/abort';
import type { Server, Socket } from 'socket.io';

const dbg = debug('mcv:pubsub:publisher');


export function initPublisher(wsServer: Server) {
    const publishers: Record<any, any> = {};

    function setupPubsub(sock: Socket) {
        sock.on('subscribe', async (publisher, cb) => {
            if (publisher in publishers === false) {
                cb(`Unknown publisher '${publisher}'`);
                return;
            }
            const subscribe = publishers[publisher];
            dbg(`[${sock.id}] Subscribing to publisher '${publisher}'`);

            // Invoke publisher's subscribe method.
            let unsubscribe, value;
            try {
                const rv = subscribe((name, ...args) => wsServer
                    .to(publisher)
                    .emit(`${publisher}:${name}`, ...args), wsServer, publisher, sock);

                // If the subscribe method returns a promise, crash the
                // application to indicate a bug in the code. We need the
                // unsubscribe method right away and cannot wait for the promise
                // to resolve.
                if (rv instanceof Promise)
                    abort(`Bug in publisher ${publisher}: Subscribe method returned a Promise`);

                [ unsubscribe, value ] = rv instanceof Array ? rv : [ rv ];
            } catch(error: any) {
                // If the subscribe method failed, pass the error to the caller
                // to indicate that we've failed to create the subscription.
                cb(error.message);
                return;
            }

            const invokeUnsubscribe = () => {
                dbg(`[${sock.id}] Unsubscribing from publisher '${publisher}'`);
                return unsubscribe ? unsubscribe(wsServer, publisher, sock) : undefined;
            };

            // If the target provided an unsubscribe function, make sure it is
            // invoked when the client unsubscribes.
            const onUnsubscribe = async (name, callback) => {
                if (name !== publisher) return;

                try {
                    callback(null, await invokeUnsubscribe());
                } catch(error: any) {
                    callback(error.message);
                    return;
                }
                sock.off('disconnect', invokeUnsubscribe);
                sock.off('unsubscribe', onUnsubscribe as any);
                await sock.leave(publisher);
            };

            await sock.join(publisher);
            sock.on('unsubscribe', onUnsubscribe);
            sock.once('disconnect', invokeUnsubscribe);

            // Await the value returned by the publisher and return it to the
            // subscriber via the callback method. If the await throws, invoke the
            // callback with an error message instead.
            try {
                cb(null, await value);
            } catch(error: any) {
                // The initial value cannot be obtained and we are subscribed. Clear the
                // subscription and indicate an error to the client via the callback.
                sock.off('disconnect', invokeUnsubscribe);
                sock.off('unsubscribe', onUnsubscribe as any);
                await sock.leave(publisher);
                // Invoke unsubscribe, but don't await the return value. We can do
                // nothing with this anyway. If obtaining initial value failed and
                // unsubscribe also fails, we get an unhandled promise rejection.
                invokeUnsubscribe();
                cb(error.message);
            }
        });
    }

    // Automatically handle requests to join/leave a particular room.
    wsServer.on('connection', s => {
        dbg(`WebSocket client connected: ${s.id}`);
        setupPubsub(s);
        s.on('disconnect', () => {
            dbg(`[${s.id}] disconnected`);
        });
    });

    return publishers;
}
