export class TimeoutError extends Error {
    constructor(error) {
        super(error.text);
    }
}