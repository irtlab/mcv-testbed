import debug from 'debug';
import axios, { AxiosInstance } from 'axios';
import EventEmitter from 'events';
import { io, Socket } from 'socket.io-client';
import { RenewingSubscription } from './pubsub/subscriber';
import sleep from '@janakj/lib/sleep';
import { TimeoutError } from './errors';


const dbg = debug('mcv:terminal');


function queryString(attrs) {
    const qs = Object.entries(attrs).map(([k, v]) => `${k}=${v}`).join('&');
    return qs.length ? `?${qs}` : '';
}


async function* lineReader(reader: ReadableStreamDefaultReader<Uint8Array>) {
    const utf8 = new TextDecoder("utf-8");
    const eol = /\r\n|\n|\r/gm;

    let { value, done } = await reader.read();
    let s: string = value ? utf8.decode(value) : '';

    let i = 0, match;
    for (;;) {
        match = eol.exec(s);
        if (!match) {
            if (done) break;

            const rem = s.substr(i);
            ({ value, done } = await reader.read());

            s = s ? utf8.decode(value) : '';
            s = rem + s;

            i = 0;
            eol.lastIndex = 0;
            continue;
        }
        yield s.substring(i, match.index);
        i = eol.lastIndex;
    }
    if (i < s.length) yield s.substr(i);
}


class APIError extends Error {
    response: any;
    request: any;
    node: any;

    constructor(node, error) {
        if (error.response) {
            const data = error.response.data || {},
                msg  = data.message || error.response.statusText;
            super(`${node.name}: ${msg}`);
            this.response = error.response.data;
        } else if (error.request) {
            super(`${node.name}: No response received: ${error}`);
            this.request = error.request;
        } else {
            super(`${node.name}: ${error.message}`);
        }
        this.node = node;
    }
}


export class UserTerminal extends EventEmitter {
    private opts: Record<string | number, any>;
    public name: string;
    private resolveSRV: () => Promise<string>;
    private client: AxiosInstance;
    private _subs: Record<string | number, any>;
    public online: boolean;
    public timesync: boolean;
    public indicator: any;
    public state: { call: boolean, ptt: boolean };
    private sock?: Socket;

    constructor(name: string, resolveSRV: (rec: string) => Promise<string>, opts={}) {
        super();

        this.opts = {...opts};
        this.opts.subscribe = this.opts.subscribe ? true : false;
        this.opts.withCredentials = this.opts.withCredentials ? true : false;

        this.opts.timeout = this.opts.timeout === undefined ? 5000 : parseInt(this.opts.timeout);
        if (isNaN(this.opts.timeout))
            throw new Error('Invalid timeout value');

        this.name = name;
        this.resolveSRV = () => resolveSRV(name);

        this.client = axios.create({
            timeout          : this.opts.timeout,
            maxContentLength : 1024 * 1024 * 1024,
            maxRedirects     : 0,
            withCredentials  : this.opts.withCredentials
        });

        this.client.interceptors.response.use(
            res => res.data,
            error => {
                if (error.response && error.response.status === 401) {
                    if (typeof this.opts.authCallback === 'function')
                        return this.opts.authCallback();
                }
                throw new APIError(this, error);
            }
        );

        this._subs = Object.create(null);

        this.online    = false;
        this.timesync  = false;
        this.indicator = undefined;
        this.state     = { call: false, ptt: false };

        this._connect    = this._connect.bind(this);
        this._disconnect = this._disconnect.bind(this);
        this._timesync   = this._timesync.bind(this);
        this._state      = this._state.bind(this);
        this._indicator  = this._indicator.bind(this);
    }

    static select(match, ids: string[]) {
        // If no match argument is provided, return the entire list of user
        // terminal IDs.
        if (typeof match === 'undefined' || match === null)
            return ids;

        match = Array.isArray(match) ? match : [ match ];

        const rv = {};
        for(const m of match) {
            if (typeof m === 'string') {
                // If the match component is a string, then it is an ID of a user
                // terminal, so simply add it to the result array.
                rv[m] = true;
            } else if (m instanceof RegExp) {
                // If the match component is a regular expression, apply it to the
                // terminal ids stored in the online attribute. Add any IDs that match
                // to the result array.
                for(const id of ids)
                    if (m.test(id)) rv[id] = true;
            } else {
                throw new Error(`Unsupported type ${typeof m} in match argument`);
            }
        }
        return Object.keys(rv);
    }

    async _subscribe() {
        const str = await this.resolveSRV();

        if (str.startsWith('/')) {
            this.sock = io({
                path       : `${str}/socket.io/`,
                transports : ['websocket']
            });
        } else {
            const { origin, pathname } = new URL(str);
            this.sock = io(origin, {
                path       : `${pathname}/socket.io/`,
                transports : ['websocket']
            });
        }

        this.sock.on('connect', this._connect);
        this.sock.on('disconnect', this._disconnect);

        this._subs.timesync = new RenewingSubscription(this.sock, 'timesync');
        this._subs.timesync.on('change', this._timesync);
        this._subs.timesync.on('reset', this._timesync);
        this._subs.timesync.start();

        this._subs.state = new RenewingSubscription(this.sock, 'state');
        this._subs.state.on('change', this._state);
        this._subs.state.on('reset', this._state);
        this._subs.state.start();

        this._subs.indicator = new RenewingSubscription(this.sock, 'indicator');
        this._subs.indicator.on('change', this._indicator);
        this._subs.indicator.on('reset', this._indicator);
        this._subs.indicator.start();
    }

    _unsubscribe() {
        this._subs.timesync.off('change', this._timesync);
        this._subs.timesync.off('reset', this._timesync);
        this._subs.timesync.stop();

        this._subs.state.off('change', this._state);
        this._subs.state.off('reset', this._state);
        this._subs.state.stop();

        this._subs.indicator.off('change', this._indicator);
        this._subs.indicator.off('reset', this._indicator);
        this._subs.indicator.stop();

        if (this.sock) {
            this.sock.off('disconnect', this._disconnect);
            this.sock.off('connect', this._connect);

            if (this.sock.connected) this._disconnect();
            this.sock.disconnect();
            delete this.sock;
        }
    }

    start() {
        if (this.opts.subscribe) return this._subscribe();
        return Promise.resolve();
    }

    stop() {
        if (this.opts.subscribe) return this._unsubscribe();
        return Promise.resolve();
    }

    _connect() {
        dbg(`[${this.name}] WebSocket connected`);
        this.online = true;
        this.emit('online', this.online);
    }

    _disconnect() {
        dbg(`[${this.name}] WebSocket disconnected`);
        this.online = false;
        this.emit('online', this.online);
    }

    _timesync(data) {
        if (typeof data === 'undefined') {
            this.timesync = false;
        } else {
            this.timesync = typeof data.offset !== 'undefined' && data.offset < 0.001;
        }
        this.emit('timesync', this.timesync);
    }

    _state(state) {
        Object.assign(this.state, state || { call: false, ptt: false });
        this.emit('state', this.state);
    }

    _indicator(state) {
        this.indicator = state;
        this.emit('indicator', this.indicator);
    }

    url = async (path, query={}) => {
        const url = `${await this.resolveSRV()}/${path}${queryString(query)}`;
        dbg(url);
        return url;
    };

    GET    = async (path, query={}, ...a) => this.client.get   (await this.url(path, query), ...a);
    POST   = async (path, query={}, ...a) => this.client.post  (await this.url(path, query), ...a);
    PUT    = async (path, query={}, ...a) => this.client.put   (await this.url(path, query), ...a);
    DELETE = async (path, query={}, ...a) => this.client.delete(await this.url(path, query), ...a);

    api = {
        GET    : (path, ...a) => this.GET   (`api/${path}`, ...a),
        POST   : (path, ...a) => this.POST  (`api/${path}`, ...a),
        PUT    : (path, ...a) => this.PUT   (`api/${path}`, ...a),
        DELETE : (path, ...a) => this.DELETE(`api/${path}`, ...a)
    };

    checkTime = () => this.api.GET('timesync');
    platform  = () => this.api.GET('platform');
    config    = () => this.api.GET('config');

    getSettings       = () => this.api.GET('settings');
    loadDefaults      = () => this.api.GET('settings/default');
    resetSettings     = () => this.api.DELETE('settings');
    applySettings     = () => this.api.POST('settings/apply');
    supportedSettings = () => this.api.GET('settings/supported');
    updateSettings    = (s, ignoreUnknown?) => this.api.POST(`settings`, {}, s,
        ignoreUnknown ? { params: { ignore_unknown: true } } : undefined);

    fetchEvents  = (...a) => this.api.GET('events', ...a);
    deleteEvents = (...a) => this.api.DELETE('events', ...a);

    invokePager = () => this.api.POST('pager');

    getRun = () => this.api.GET('run');

    startRun = (id) => this.api.POST(`run/start/${id}`);
    stopRun = () => this.api.POST('run/stop');

    getMedia = async (filename: string) => {
        return this.client.get(await this.url(`api/media/${filename}`), {
            responseType: 'arraybuffer'
        });
    };

    setPtt = (state) => new Promise((resolve, reject) => {
        if (!this.sock) return;
        this.sock.emit('ptt', state, (error, value) => {
            if (error !== null) reject(new Error(error));
            else resolve(value);
        });
    });

    async restart(timeout=120000, args={}) {
        // Send a POST request to the restart API endpoint and wait for the 202
        // Accepted. We cannot use the built-in API methods here because the
        // 'restart' endpoint uses chunked encoding.
        // eslint-disable-next-line no-undef
        const res = await fetch(await this.url('api/restart', args), { method : 'POST' });
        if (!res.ok) throw new Error(`[${this.name}] Restart error: ${res.statusText}`);
        if (res.status !== 202)
            throw new Error(`[${this.name}] Unexpected response code ${res.status} (expected 202)`);

        if (res.body === null)
            throw new Error(`Bug: Got null body`);

        const line = lineReader(res.body.getReader());

        // The first line is always present and contains a JSON document with the ID
        // of the current boot session and uptime. We will compare it to the new
        // boot session ID to determine if the terminal really restarted.
        let { value, done } = await line.next();
        if (done || typeof value !== 'string')
            throw new Error(`[${this.name}] Invald response to restart request`);

        let bootId;
        try {
            bootId = JSON.parse(value).boot_id;
            if (typeof bootId !== 'string')
                throw new Error(`[${this.name}] Terminal did not provide current boot ID`);
        } catch(error) {
            throw new APIError(this, error);
        }

        // If we get a second line, it is an error message indicating that the
        // request to reboot cannot be honored.
        ({ value, done } = await line.next());
        if (!done)
            throw new Error(`[${this.name}] Restart error: ${value}`);

        // If no timeout has been provided, do not wait for the terminal to come
        // back online
        if (timeout === 0) return;

        const Ts = 2000, Tp = 1000;

        // Wait for the terminal to come back online.
        for(let i = 0; i < Math.trunc(timeout / (Ts + Tp)); i++) {
            await sleep(Ts);
            dbg(`[${this.name}] GETting /api/restart`);
            try {
                const body = await this.api.GET('restart', {}, { timeout : Tp });
                if (typeof (body as any).boot_id !== 'string')
                    throw new Error(`[${this.name}] Response is missing boot_id attribute`);

                if ((body as any).boot_id === bootId)
                    throw new Error(`[${this.name}] Terminal failed to restart`);

                return;
            } catch(e: any) {
                // Got response, but not 200 OK, or did not get any response
                if (e.response || e.request) continue;
                // We failed to send the request, there's not telling whether the node
                // is online, so re-throw the exception.
                if (!(e instanceof TimeoutError)) throw e;
            }
        }
        throw new Error(`[${this.name}] Terminal did not come back online within ${timeout} ms`);
    }

    async ping(timeout=1000) {
        try {
            await this.api.GET('ping', {}, { timeout });
            return true;
        } catch(e: any) {
            // Got response, but not 200 OK
            if (e.response) return false;
            // Did not get any response
            if (e.request) return false;
            // We failed to send the request, there's not telling whether the node is
            // online, so re-throw the exception.
            throw e;
        }
    }
}
