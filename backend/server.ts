import http from 'http';
import https from 'https';
import logger from 'debug';
import { promises as fs } from 'fs';
import { readFileSync } from 'fs';
import morgan from 'morgan';
import express from 'express';
import abort from '@janakj/lib/abort';
import { parseNumber,  parseBool } from '@janakj/lib/parse';
import { mongoConnect } from './db';
import initAuthentication from './auth';
import serveAPI from './api';
import * as defaults from './defaults';
import taskRunner from './task';


const debug = logger('mcv:server');
const devMode = process.env.NODE_ENV === 'development';


const startServer = (server: http.Server, port?: number, address?: string): Promise<string> => new Promise((resolve, reject) => {
    server.once('error', reject);
    server.listen(port, address, () => {
        const addr = server.address();
        if ((typeof addr === "string") || addr == null) {
            reject(`Unsupported address type`);
        } else {
            const scheme = server instanceof https.Server ? 'https' : 'http';
            const addrStr = addr.family === 'IPv6' ? `[${addr.address}]` : `${addr.address}`;
            resolve(`${scheme}://${addrStr}:${addr.port}`);
        }
    });
});


async function createHTTPSServer(crtFilename: string, keyFilename: string, app: express.Application) {
    const args: https.ServerOptions = {};

    const interval = parseNumber(process.env.CERTIFICATE_CHECK_INTERVAL || defaults.CERTIFICATE_CHECK_INTERVAL, 1);

    debug(`Loading TLS server certificate from file '${crtFilename}'`);
    args.cert = await fs.readFile(crtFilename);

    debug(`Loading TLS private key from file '${keyFilename}'`);
    args.key = await fs.readFile(keyFilename);

    const server = https.createServer(args, app);

    const { context } = (server as any)._sharedCreds;
    let curCrt = args.cert.toString('hex');
    let curKey = args.key.toString('hex');

    setInterval(() => {
        try {
            const crt = readFileSync(crtFilename);
            const crtHex = crt.toString('hex');
            if (crtHex !== curCrt) {
                debug(`Loading new TLS server certificate`);
                context.setCert(crt);
                curCrt = crtHex;
            }
        } catch(error: any) {
            debug(`Failed to read TLS server certificate from ${crtFilename}: ${error.message}`);
        }

        try {
            const key = readFileSync(keyFilename);
            const keyHex = key.toString('hex');
            if (keyHex !== curKey) {
                debug(`Loading new TLS key`);
                context.setKey(key);
                curKey = keyHex;
            }
        } catch(error: any) {
            debug(`Failed to read TLS key from ${keyFilename}: ${error.message}`);
        }
    }, interval);

    return server;
}


function parseListen(value?: string): { address?: string, port?: number } {
    if (value === undefined) return {};
    value = value.trim();
    if (value.length === 0) return {};

    let chunks, address, port;
    if (value.startsWith('[')) {
        value = value.slice(1).replace(/\]:?/, ' ').trim();
        chunks = value.split(' ');
    } else {
        chunks = value.split(':');
    }

    switch(chunks.length) {
        case 1:
            if (chunks[0].search(/[.:]/) !== -1) address = chunks[0];
            else port = chunks[0];
            break;

        case 2:
            address = chunks[0];
            port = chunks[1];
            break;

        default:
            throw new Error(`Unknown address:port format "${value}"`);
    }

    if (address !== undefined) address = address.trim();
    if (port !== undefined) port = port.trim();

    if (port !== undefined) {
        try {
            port = parseNumber(port, 0, 65535);
        } catch(error) {
            throw new Error(`Invalid port number ${port}`);
        }
    }

    return { address, port };
}


(async function () {
    debug(`Starting in ${devMode ? 'development' : 'production'} mode`);
    const env = process.env;
    const opts = {
        listen         : env.LISTEN,                        // IP address and port number to listen on for HTTP(S) requests
        authentication : parseBool(env.AUTHENTICATION),     // Enable user authentication
        tlsCertificate : env.TLS_CERTIFICATE,               // Path to the TLS server certificate file
        tlsKey         : env.TLS_KEY,                       // Path to the TLS private key file
        backendAPI     : parseBool(env.BACKEND_API),        // Server backend APIs
        frontend       : parseBool(env.FRONTEND),           // Server frontend code
        jupyterHubURL  : env.JUPYTER_HUB_URL,               // Enable Jupyter proxy
        utProxy        : parseBool(env.USER_TERMINAL_PROXY) // Enable user terminal proxy
    };

    if (!process.env.MONGODB_URL) throw new Error('Please configure environment variable MONGODB_URL');

    debug(`Connecting to MongoDB at ${process.env.MONGODB_URL}`);
    await mongoConnect(process.env.MONGODB_URL);
    debug('Connected to MongoDB');

    const { address, port } = parseListen(opts.listen);

    const app = express();
    app.use(morgan(devMode ? 'dev' : 'combined'));

    let server: http.Server | https.Server;
    if (opts.tlsCertificate) {
        if (!opts.tlsKey)
            throw new Error(`Please also configure TLS_KEY`);

        debug('Creating HTTPS server');
        server = await createHTTPSServer(opts.tlsCertificate, opts.tlsKey, app);
    } else {
        debug('Creating HTTP server');
        server = http.createServer(app);
    }

    // Disable timeouts to enable big audio file uploads and downloads.
    server.setTimeout(0);
    server.headersTimeout = 0;

    if (opts.authentication) {
        debug(`Enabling authentication`);
        initAuthentication(app, port);
    } else {
        debug('Authentication is disabled');
    }

    if (opts.backendAPI) {
        debug('Initializing backend APIs');
        const api = await serveAPI();
        app.use('/api', api);
    }

    if (opts.utProxy) {
        debug('Initializing user terminal HTTP(S)/WebSocket proxy');
        const createProxy = (await import('./proxy')).default;
        const proxy = createProxy(server, '/proxy');
        app.use('/proxy', proxy);
    }

    if (opts.jupyterHubURL) {
        debug(`Initializing JupyterHub HTTP(S)/WebSocket proxy for ${opts.jupyterHubURL}`);
        const createJupyterProxy = (await import('./jupyter')).default;
        const jupyterProxy = createJupyterProxy('/jupyter', server, opts.jupyterHubURL);
        app.use('/jupyter', jupyterProxy);
    }

    if (opts.frontend) {
        debug('Serving frontend code');
        const serveFrontend = (await import('./frontend')).default;
        serveFrontend(app, devMode);
    }

    const url = await startServer(server, port, address);
    debug(`HTTP(S) server is listening on ${url}`);

    debug(`Starting background task runner`);
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const tasks = taskRunner();
}()).catch(abort);
