import express from 'express';
import path from 'path';
import cors from 'cors';
import { homedir } from 'os';
import { promises as fs } from 'fs';
import { ServiceBrowser } from '@mcv/common/zeroconf';
import media_api from './api/media';
import experiment_api from './api/experiment';
import { mongoDb as db } from './db';
import run_api from './api/run';
import config_api from './api/config';
import subject_api from './api/subject';
import consent_api from './api/consent';
import { events_api } from './api/events';
import { browser_api } from './api/browser';
import impair_api from './api/impair';
import logger from 'debug';
import * as defaults from './defaults';
import { api as taskApi } from './task';
import textApi from './api/text';


const debug = logger('mcv:api');


export default async function initAPI() {
    const env = process.env;
    const opts = {
        db                 : env.MONGODB_URL,                                 // MongoDB database URL
        dataDir            : env.DATA_DIR || defaults.DATA_DIR,               // Directory for runtime data (related to ${process.env.HOME} if not absolute)
        wavDir             : env.WAV_DIR || defaults.WAV_DIR,                 // Directory for sound files, (related to runtime data directory if not absolute)
        maxUploadSize      : env.MAX_UPLOAD_SIZE || defaults.MAX_UPLOAD_SIZE, // Maximum size for data uploads
        googleTTSandSTTKey : env.GOOGLE_TTS_STT_API_KEY
    };

    const api = express.Router();
    api.use(cors({ origin: true, credentials: true }));

    opts.dataDir = path.resolve(homedir(), opts.dataDir);
    debug(`DATA_DIR resolved to ${opts.dataDir}`);
    try {
        const stat = await fs.stat(opts.dataDir);
        if (!stat.isDirectory())
            throw new Error(`Path ${opts.dataDir} is not a directory`);
    } catch(e) {
        throw new Error(`Director ${opts.dataDir} does not exit`);
    }

    opts.wavDir = path.resolve(opts.dataDir, opts.wavDir);
    debug(`WAV_DIR resolved to ${opts.wavDir}`);

    if (opts.googleTTSandSTTKey === undefined)
        debug(`Warning: API key for Google Speech-to-text and Text-to-speech is NOT set`);

    debug('Initializing ZeroConf service browser API');
    api.use('/browser', browser_api());

    debug('Initializing media server');
    const { api: mediaAPI, uploadMediaFile } = media_api(opts);
    api.use('/media', mediaAPI);

    debug('Initializing experiment API');
    api.use('/experiment', await experiment_api(db, opts, uploadMediaFile));

    debug('Initializing experiment run API');
    api.use('/experiment_run', await run_api(db));

    debug('Initializing configuration API');
    api.use('/configuration', await config_api(db));

    debug('Initializing subject data API');
    api.use('/subject_data', await subject_api(db));

    debug('Initializing consent form API');
    api.use('/consent_form', await consent_api(db, opts));

    debug('Initializing TTS and STT API');
    api.use('/text', textApi(opts));

    debug('Initializing impair API');
    api.use('/impair', impair_api(opts, uploadMediaFile));

    debug('Initializing task API');
    api.use('/task', await taskApi());

    const browser = new ServiceBrowser('_ut._tcp', process.env.DNS_SD_DOMAIN);
    void browser.start();

    debug('Initializing events API');
    api.use('/events', await events_api(db, browser, uploadMediaFile));

    return api;
}
