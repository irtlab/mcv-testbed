import { Db, MongoClient } from 'mongodb';

export let mongoDb: Db;

export async function mongoConnect(url: string): Promise<void> {
    const mongoClient = new MongoClient(url, { retryWrites: true });
    mongoDb = mongoClient.db();
    await mongoClient.connect();
}
