/* eslint-disable @typescript-eslint/no-var-requires */
import express from 'express';
import path from 'path';
import { createRequire } from 'module';
import { fileURLToPath } from 'url';
import logger from 'debug';

const debug = logger('mcv:frontend');


export default function serveFrontend(app: express.Application, devMode: boolean) {
    const __dirname = path.dirname(fileURLToPath(import.meta.url));
    const backendDir = path.resolve(__dirname, '..'); // tsc outputs compiled code to a subdirectory
    const frontendDir = path.resolve(backendDir, '../frontend');
    const publicDir = path.resolve(frontendDir, 'public');

    if (devMode) {
        debug('Serving in development mode with hot reloading');
        // If we are running in development mode, dynamically load the
        // webpack-dev-middleware plugin and generate bundles for the frontend
        // on the fly. Re-generate the bundle if any of its dependencies change.

        // We need to change into the frontend directory to make the webpack
        // middleware work. Otherwise it won't be able to find its modules.
        const oldDir = process.cwd();
        process.chdir(frontendDir);
        try {
            const require = createRequire(`${frontendDir}/index.js`);

            const webpackConfig = require('./webpack.config.js');
            webpackConfig.mode = process.env.NODE_ENV;

            const webpack = require('webpack');
            const devMiddleware = require('webpack-dev-middleware');
    
            const compiler = webpack(webpackConfig);
    
            app.use(devMiddleware(compiler, {
                publicPath: webpackConfig.output.publicPath
            }));

            const hotMiddleware = require('webpack-hot-middleware');
            app.use(hotMiddleware(compiler));
    
            app.get('*', (_req, res, next) => {
                const fn = path.resolve(compiler.outputPath, 'index.html');
                compiler.outputFileSystem.readFile(fn, (error, data) => {
                    if (error) return next(error);
                    res.set('content-type','text/html');
                    res.send(data);
                    res.end();
                });
            });    
        } finally {
            process.chdir(oldDir);
        }
    } else {
        debug('Serving bundled code');

        // If we're running in production mode, serve pre-generated bundle files
        // from the folder 'bundle' with express.static module. If the client
        // requests a file from the bundle folder that does not exist, generate
        // an error rather than continuing to other plugins.

        app.use('/frontend', express.static(publicDir, {
            fallthrough: false
        }));

        // The following route is here to support browser-side routing. When
        // react-router (or a similar framework) changes the HTTP URL of the
        // site and the user reload the page, we need to serve the main
        // index.html file. react-router will take care of navigating the user
        // to the correct component within the site.
        app.get('*', (_req, res) => {
            res.sendFile(path.resolve(publicDir, 'index.html'));
        });
    }
}
