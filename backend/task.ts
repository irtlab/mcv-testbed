/*
 * NOTE: This feature uses MongoDB change streams. Change streams are only
 * available in a database that is part of a replica set (and thus have an op
 * log). To be able to use this module, you may need to switch your MongoDB
 * instance from standalone mode to a single-node replica set. This can be
 * accomplished via the --replSet "mcv" command line option. Then enter mongo
 * shell and initialize the replica set with rs.initiate().
 *
 * If you run your MongoDB database in a Docker container and the database fails
 * to become replica set master upon restart, do the following in mongo shell:
 *
 *     cfg = rs.config()
 *     cfg.members[0].host="127.0.0.1:27017"
 *     rs.reconfig(cfg, { "force": true })
 */

import logger from 'debug';
import { ObjectId } from 'mongodb';
import { mongoDb as db } from './db';
import express from 'express';
import { jsonify, NotFoundError } from '@janakj/lib/http';

const debug = logger('mcv:task');
const collectionName = 'tasks';


type ResultType = { value: any } | { error: string };


interface Task {
    _id: ObjectId;
    function: string;
    completed: boolean;
    experiment_run: string;
    arguments?: any[];
    result?: ResultType;
}


function isTask(v: any): v is Task
{
    if (typeof v !== 'object') return false;
    if (typeof v.function !== 'string') return false;
    if (v.arguments !== undefined && !Array.isArray(v.arguments)) return false;
    if (v.result !== undefined && typeof v.result !== 'object') return false;
    return true;
}


const taskFunctions: Record<string, (...args) => any | Promise<any>> = {};


export function registerTaskFunction(name: string, f: (...args) => any | Promise<any>)
{
    if (taskFunctions[name] !== undefined)
        throw new Error(`Task function ${name} already exists`);

    taskFunctions[name] = f;
}


let pendingTasks: Record<string, Task> = {};


async function createCollection() {
    try {
        await db.createCollection(collectionName);
    } catch (error: any) {
        if (error.codeName !== 'NamespaceExists') throw error;
    }
}


async function loadPending()
{
    const collection = db.collection(collectionName);

    debug(`Loading pending tasks from MongoDB`);
    const cursor = collection.find({ result: { $exists: false }});
    while(await cursor.hasNext()) {
        const task = await cursor.next();
        if (task === null) continue;
        const id = task._id.toString();

        if (!isTask(task)) {
            debug(`Task ${id} has invalid descriptor, skipping`);
            continue;
        }

        pendingTasks[id] = task;
    }
    cursor.close();
}


export async function createTask(name: string, exprRunID: string, ...args)
{
    const collection = db.collection(collectionName);
    const res = await collection.insertOne({ function: name, completed: false, experiment_run: exprRunID, arguments: args });
    return res.insertedId.toString();
}


async function completeTask(id: string, result: ResultType, completed: boolean)
{
    debug(`Completing task ${id}`);
    const _id = new ObjectId(id);
    const collection = db.collection(collectionName);
    await collection.updateOne({ _id }, { $set: { result, completed }});
    delete pendingTasks[id];
}


async function deleteTask(id: string)
{
    debug(`Deleting task ${id}`);
    const _id = new ObjectId(id);
    const collection = db.collection(collectionName);
    await collection.deleteOne({ _id });
}


async function processActiveTasks()
{
    for(const [id, { function: name, experiment_run: exprRunID, arguments: args }] of Object.entries(pendingTasks)) {
        debug(`Starting task ${id}`);
        const f = taskFunctions[name];
        let value;
        let taskCompletedError = '';
        if (f === undefined) {
            debug(`Skipping task ${id}: Unknown task function '${name}'`);
            await completeTask(id, { error: `Unknown task function '${name}'` }, false);
        } else {
            try {
                value = await f.call(null, ...(args || []));
                await completeTask(id, { value }, true);
            } catch(error: any) {
                taskCompletedError = error.message ? `${error.message}` : `${error}`;
                debug(`Task ${id} failed: ${error}`);
                await completeTask(id, { error: `${error}` }, false);
            }
        }

        // The bellow code updates the given ExperimentRun's status in order to show the status in the UI side.
        try {
            const collection = db.collection(collectionName);
            const exprRunColl = db.collection('experiment_run');
            if (taskCompletedError.length === 0) {
                const [exprRunTaskFound, exprRunFound] = await Promise.all([
                    collection.findOne({ experiment_run: exprRunID, completed: false }),
                    exprRunColl.findOne({ _id: new ObjectId(exprRunID) })
                ]);
                // If no more tasks for the given ExperimentRun, then set status to 'Ready to start' if the task manager
                // wasn't failed before. It is possible that the ExperimentRun is deleted while the task manager is processing
                // the tasks. If it is the case, then do thing.
                if (!exprRunTaskFound && exprRunFound && !exprRunFound.failed) {
                    await exprRunColl.updateOne({ _id: exprRunFound._id }, { $set: { status: 'Ready to start' }});
                }
            } else {
                await exprRunColl.updateOne({ _id: new ObjectId(exprRunID) }, { $set: { status: 'Failed to generate audio', failed: true, reason: taskCompletedError }});
            }
        } catch (error: any) {
            debug(`Updating experiment run (${exprRunID}) status failed: ${error}`);
        }
    }
}


export default async function taskRunner()
{
    for(;;) {
        debug(`Creating MongoDB collection '${collectionName}' if it does not exist`);
        await createCollection();

        debug(`Creating MongoDB change stream for collection '${collectionName}'`);
        const collection = db.collection(collectionName);
        const stream = collection.watch([{ '$match' : { 'operationType' : 'insert' }}]);

        pendingTasks = {};
        await loadPending();

        try {
            for(;;) {
                await processActiveTasks();
                const { fullDocument: task } = await stream.next();
                if (task === undefined) continue;
                if (!isTask(task))
                    throw new Error('Got invalid task descriptor from change stream');
                pendingTasks[task._id.toString()] = task;
            }
        } catch(e) {
            debug(`MongoDB change stream reported error`);
            /* do nothing */
        } finally {
            debug(`Closing MongoDB change stream`);
            try {
                await stream.close();
            } catch(e) {
                /* do nothing */
            }
        }
    }
}


export async function pickupTask(id: string)
{
    const _id = new ObjectId(id);
    const collection = db.collection(collectionName);

    // Find the corresponding task descriptor in the Mongo database
    const task = await collection.findOne({ _id });
    if (task === null) throw new NotFoundError(`Task ${_id} not found`);
    if (!isTask(task)) throw new Error('Got invalid task descriptor');

    // If the task already has a result (either success or failure), return
    // it and delete the descriptor.
    if (task.result !== undefined) {
        await deleteTask(id);
        const  res = task.result;
        if ('value' in res) return res.value;
        throw new Error(res.error);
    }

    // If the task descriptor does not have a result yet, wait for it.
    const stream = collection.watch([]);

    try {
        for(;;) {
            const { operationType, documentKey, updateDescription } = await stream.next();
            if (operationType !== 'update') continue;
            if (documentKey === undefined) continue;
            if (!_id.equals((documentKey as any)._id)) continue;
            if (updateDescription === undefined) continue;
            if (updateDescription.updatedFields.result === undefined) continue;

            await deleteTask(id);
            const res = updateDescription.updatedFields.result;
            if ('value' in res) return res.value;
            throw new Error(res.error);
        }
    } finally {
        try {
            await stream.close();
        } catch(error) { /* do nothing */ }
    }
}


export async function api() {
    const api = express.Router();
    api.use(express.json());

    await createCollection();
    const collection = db.collection(collectionName);

    api.get('/:id', jsonify(async req => {
        const _id = new ObjectId(req.params.id);
        const task = await collection.findOne({ _id });
        if (task === null) throw new NotFoundError(`Task ${_id} not found`);
        return task;
    }));

    api.post('/pickup/:id', jsonify(async (req, res) => {
        const _id = new ObjectId(req.params.id);

        // Find the corresponding task descriptor in the Mongo database
        const task = await collection.findOne({ _id });
        if (task === null) throw new NotFoundError(`Task ${_id} not found`);
        if (!isTask(task)) throw new Error('Got invalid task descriptor');

        // If the task already has a result (either success or failure), return
        // it and delete the descriptor.
        if (task.result !== undefined) {
            await deleteTask(req.params.id);
            return task.result;
        }

        // If the task descriptor does not have a result yet, wait for it.
        const stream = collection.watch([]);

        req.once('close', () => { void stream.close(); });

        res.setTimeout(0);
        debug(`Starting watcher for task ${req.params.id}`);
        try {
            for(;;) {
                const { operationType, documentKey, updateDescription } = await stream.next();
                if (operationType !== 'update') continue;
                if (documentKey === undefined) continue;
                if (!_id.equals((documentKey as any)._id)) continue;
                if (updateDescription === undefined) continue;
                if (updateDescription.updatedFields.result === undefined) continue;

                await deleteTask(req.params.id);
                return updateDescription.updatedFields.result;
            }
        } finally {
            try {
                debug(`Closing watcher for task ${req.params.id}`);
                await stream.close();
            } catch(error) { /* do nothing */ }
        }
    }));

    return api;
}