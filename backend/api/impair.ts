import express from 'express';
import { promises as fs, createWriteStream } from 'fs';
import { exec } from 'child_process';
import path from 'path';
import tmp from 'tmp';
import parseDataURL from 'data-urls';
import Lock from '@janakj/lib/mutex';
import fetch from 'node-fetch';
import { v4 as uuidv4 } from 'uuid';
import { jsonify, jsonifyError, BadRequestError } from '@janakj/lib/http';
import { authorize } from '../auth';
import logger from 'debug';
import * as defaults from '../defaults';
import { say, transcribe, calculateWER } from './text';
import { createTask, registerTaskFunction } from '../task';
import { mongoDb as db } from '../db';


const debug = logger('mcv:api:impair');

const AMBE_URI = process.env.AMBE_URI || defaults.AMBE_URI;

const CTYPE_URL  = 'application/vnd+mcv-testbed-url';
const CTYPE_TASK = 'application/vnd+mcv-testbed-task';


const codecRates = {
    l16   : 8000,
    g711u : 8000,
    g711a : 8000,
    gsm   : 8000,
    amrnb : 8000,
    amrwb : 16000,
    p25p1 : 8000,
    p25p2 : 8000,
    opus  : 48000
};


const byteLength = (count: number) => Math.floor(count / 8) + Number(((count % 8) > 0));


function getFileDescription(settings) {
    return `Experiment: ${settings.expr_name}. Step: ${settings.step_number}. Speaker: ${settings.gender}`;
}


// If timeout is greater than 0 it means that a kill signal will automatically
// be sent after the given time period. Note that the parameter timeout takes a
// value in milliseconds.
function invoke(cmd, timeout = 0) {
    return new Promise((resolve, reject) => {
        exec(cmd, { timeout }, (error: any, stdout: any, stderr: any) => {
            if (error === null) resolve([stdout, stderr]);
            else {
                error.stdout = stdout;
                error.stderr = stderr;
                reject(error);
            }
        });
    });
}


function resample({ dst, src }, opts: any = {}) {
    const bits = opts.bits || 16;
    const rate = opts.rate || 8000;
    const channels = opts.channels || 1;

    debug(`${src} | resample(${rate},${bits},${channels}) | ${dst}`);
    return invoke(`sox ${src} -t wav -b ${bits} ${dst} rate ${rate} channels ${channels}`);
}


function adjustVolume({ dst, src }, volume) {
    const vol = volume / 100;
    debug(`${src} | adjustVolume(${vol}) | ${dst}`);
    return invoke(`sox -v ${vol} -t wav ${src} -t wav ${dst}`);
}


function trimStart({ dst, src }, duration) {
    debug(`${src} | trimStart(${duration}) | ${dst}`);
    return invoke(`sox -t wav ${src} -t wav ${dst} trim ${duration}`);
}


function mix({ dst, src: srcs }) {
    let cmd = 'sox --combine mix-power';

    for (const src of srcs) cmd += ` -t wav ${src}`;

    cmd += ` -t wav ${dst}`;
    debug(`[${srcs}] | mix() | ${dst}`);
    return invoke(cmd);
}


function codecCmd(direction: any = {}, name: any = null) {
    name = name || 'p25p1';
    return `codec -a ${AMBE_URI} -i ${direction.src} -o ${direction.dst} -c ${name}`;
}


function encode(...args) {
    debug(`(${args[0].src}) | encode(${args[1]}) | ${args[0].dst}`);
    return invoke(`${codecCmd(...args)} -O encode`);
}


function decode(...args) {
    debug(`(${args[0].src}) | decode(${args[1]}) | ${args[0].dst}`);
    return invoke(`${codecCmd(...args)} -O decode`);
}


async function loadFrames(filename) {
    const data = await fs.readFile(filename);
    const rv: Array<any> = [];
    let i = 0;
    let bits: number;
    let bytes: number;
    let buf;

    while (data.length - i) {
        if ((data.length - i) < 2) throw new Error(`File ${filename} has invalid format`);

        bits = data.readUInt16BE(i);
        bytes = byteLength(bits);
        i += 2;

        if ((data.length - i) < bytes) throw new Error(`File ${filename} has invalid format`);
        buf = data.slice(i, i + bytes);
        buf.bits = bits;

        rv.push(buf);
        i += bytes;
    }

    return rv;
}


async function saveFrames(filename, data) {
    const file = await fs.open(filename, 'w');
    const bits = Buffer.allocUnsafe(2);

    for (const frame of data) {
        bits.writeUInt16BE(frame.bits);
        // eslint-disable-next-line no-await-in-loop
        await file.write(bits);

        if (frame.bits > 0) {
        // eslint-disable-next-line no-await-in-loop
            await file.write(frame);
        }
    }

    return file.close();
}


function independentFrameLoss(input: Buffer[], p: number) {
    return input.map(frame => {
        let f: Buffer;
        if (Math.random() < p) {
            f = Buffer.allocUnsafe(0);
            (f as any).bits = 0;
        } else {
            f = Buffer.from(frame);
            (f as any).bits = (frame as any).bits;
        }
        return f;
    });
}


function simplifiedGilbertElliotFrameLoss(input: Buffer[], config: any): Buffer[] {
    let state = 'good' as 'good' | 'bad';

    const pbg = config.pbg === undefined ? (1 - config.p) / config.k : config.pbg;
    const pgb = config.pgb === undefined ? config.p / config.k : config.pgb;

    // Make the initial state randomized so that even the first frame has a
    // chance of being dropped.
    if (Math.random() < pgb) state = 'bad';

    return input.map((frame: any) => {
        let f: any;

        switch(state) {
            case 'good':
                f = Buffer.from(frame);
                f.bits = frame.bits;
                if (Math.random() < pgb) state = 'bad';
                break;

            case 'bad':
                f = Buffer.allocUnsafe(0);
                f.bits = 0;
                if (Math.random() < pbg) state = 'good';
                break;
        }

        return f;
    });
}


async function dropFrames({ dst, src }, value) {
    debug(`(${src}) | dropFrames(${JSON.stringify(value)}) | ${dst}`);
    const input = await loadFrames(src);
    let output;

    if (typeof value === 'number') {
        output = independentFrameLoss(input, value / 100);
    } else if (value.type === 'independent') {
        output = independentFrameLoss(input, value.p);
    } else if (value.type === 'gilbert-elliot') {
        output = simplifiedGilbertElliotFrameLoss(input, value);
    } else {
        throw new Error('Unsupported frame loss type');
    }
    await saveFrames(dst, output);
}


function independentBitErrors(data, p: number) {
    return data.map((frame: any) => {
        const f: any = Buffer.allocUnsafe(frame.length);
        frame.copy(f);
        f.bits = frame.bits;
        for (let i = 0; i < f.bits; i++) {
            if (Math.random() < p) {
                const j = Math.floor(i / 8);
                // eslint-disable-next-line no-param-reassign, no-bitwise
                f[j] ^= (1 << (i % 8));
            }
        }
        return f;
    });
}


function simplifiedGilbertElliotBitErrors(input: Buffer[], config): Buffer[] {
    let state = 'good' as 'good' | 'bad';

    const pbg = config.pbg === undefined ? (1 - config.p) / config.k : config.pbg;
    const pgb = config.pgb === undefined ? config.p / config.k : config.pgb;
    const interval = config.interval === undefined ? 1 : config.interval;

    // Make the initial state randomized
    if (Math.random() < pgb) state = 'bad';

    let b = 0;
    return input.map((frame: any) => {
        const f: any = Buffer.allocUnsafe(frame.length);
        frame.copy(f);
        f.bits = frame.bits;

        let j: number;
        for (let i = 0; i < f.bits; i++) {
            switch(state) {
                case 'good':
                    if ((b % interval) === 0)
                        if (Math.random() < pgb)
                            state = 'bad';
                    break;

                case 'bad':
                    j = Math.floor(i / 8);
                    f[j] ^= (1 << (i % 8));
                    if ((b % interval) === 0)
                        if (Math.random() < pbg)
                            state = 'good';
                    break;
            }
            b += 1;
        }

        return f;
    });
}



async function addBitErrors({ dst, src }, value) {
    debug(`(${src}) | addBitErrors(${JSON.stringify(value)}) | ${dst}`);
    const input = await loadFrames(src);
    let output;

    if (typeof value === 'number') {
        output = independentBitErrors(input, value / 100);
    } else if (value.type === 'independent') {
        output = independentBitErrors(input, value.p);
    } else if (value.type === 'gilbert-elliot') {
        output = simplifiedGilbertElliotBitErrors(input, value);
    } else {
        throw new Error('Unsupported bit error type');
    }
    await saveFrames(dst, output);
}


export async function move(dst, src) {
    const dirname = path.dirname(dst);

    // First make sure that the target directory exists
    await fs.mkdir(dirname, { recursive: true });
    await fs.rename(src, dst);

    // Invoke fsync on the directory that contains the file to make sure it exists
    // in case the file was newly created.
    const d = await fs.open(dirname, 'r');
    await d.sync();
    await d.close();
}


async function createTmpFilename() {
    return new Promise<string>((resolve, reject) => {
        tmp.tmpName((err, pathname) => {
            if (err) reject(err);
            else resolve(pathname);
        });
    });
}


function settingsToPipe({
    mic, mic_volume, noise, noise_volume, codec, ptt_delay, rx_loss, rx_error
}) {
    const pipe: Array<any> = [];

    if (typeof mic !== 'string') throw new Error('Please configure audio file to use as mic input');

    if (typeof codec !== 'string') throw new Error('Please configure codec');

    const mVol = typeof mic_volume === 'number' ? [adjustVolume, mic_volume] : null;
    const nVol = typeof noise_volume === 'number' ? [adjustVolume, noise_volume] : null;

    if (typeof noise === 'string') {
        pipe.push([[resample, { rate: 48000 }], [resample, { rate: 48000 }]]);

        if (mVol || nVol) pipe.push([mVol, nVol]);
        pipe.push([mix]);
    } else if (mVol) pipe.push([mVol]);

    if (typeof ptt_delay === 'number') pipe.push([[trimStart, ptt_delay / 1000]]);

    const rate = codecRates[codec];
    if (typeof rate === 'undefined') throw new Error(`Unsupported codec '${codec}'`);

    pipe.push([[resample, { rate: rate }]]);
    pipe.push([[encode, codec]]);

    if (typeof rx_error === 'number' || typeof (rx_error || {}).type === 'string')
        pipe.push([[addBitErrors, rx_error]]);

    if (typeof rx_loss === 'number' || typeof (rx_loss || {}).type === 'string')
        pipe.push([[dropFrames, rx_loss]]);

    pipe.push([[decode, codec]]);
    return pipe;
}


async function run(dst, settings) {
    const pipe = settingsToPipe(settings);
    const env = { src: settings.noise ? [settings.mic, settings.noise] : [settings.mic] };
    const tmps: Array<any> = [];

    try {
        for (const cmds of pipe) {
            // eslint-disable-next-line no-await-in-loop
            env.src = await Promise.all(cmds.map(async (cmdline, i) => {
                let cmd;
                let args: Array<any> = [];
                if (Array.isArray(cmdline)) {
                    cmd = cmdline[0];
                    args = cmdline.slice(1);
                } else {
                    cmd = cmdline;
                }

                let s;
                if (cmds.length !== env.src.length) {
                    if (cmds.length === 1) s = env.src;
                    else throw new Error('Input-output mismatch');
                } else {
                    s = env.src[i];
                }

                let tmpDst: any = '';
                if (cmd !== null) {
                    tmpDst = await createTmpFilename();
                    tmps.push(tmpDst);
                    await cmd({ dst: tmpDst, src: s }, ...args);
                } else {
                    tmpDst = s;
                }

                return tmpDst;
            }));
        }

        await move(dst, env.src[0]);
    } finally {
        await Promise.allSettled(tmps.map((fn) => fs.unlink(fn)));
    }
}


async function bufferToTmpFile(data: Buffer) {
    const dst = await createTmpFilename();
    try {
        const f = await fs.open(dst, 'w');
        await f.write(data);
        await f.close();
        return dst;
    } catch(e) {
        try {
            await fs.unlink(dst);
        } catch(e) { /* ignore errors */ }
        throw e;
    }
}


async function urlToTmpFile(url: string, mimeType) {
    const dst = await createTmpFilename();
    try {
        if (url.startsWith('data:')) {
            const d = parseDataURL(url);
            if (d === null) throw new Error("Couldn't parse data URL");

            if (mimeType && d.mimeType.toString() !== mimeType) throw new Error('Invalid MIME type');

            const f = await fs.open(dst, 'w');
            await f.write(d.body);
            await f.close();
        } else {
            const res = await fetch(url);
            if (!res.ok) {
                throw new Error(`${res.status} ${res.statusText}`);
            }

            await new Promise((resolve, reject) => {
                if (res.body === null) {
                    reject(new Error('Got null body'));
                    return;
                }
                const f = createWriteStream(dst);
                f.once('error', reject);
                f.once('close', resolve);
                res.body.pipe(f);
            });
        }
        return dst;
    } catch(e) {
        try {
            await fs.unlink(dst);
        } catch(e) { /* ignore errors */ }
        throw e;
    }
}


export default function (opts, uploadMediaFile) {
    const api = express.Router();
    const lock = new Lock();

    api.use(express.json({ limit: opts.maxUploadSize }));


    const locked = (fn) => async (req, res, next) => {
        await lock.acquire();
        try {
            return await fn(req, res, next);
        } finally {
            lock.release();
        }
    };


    async function generateImpairedAudio(settings: any): Promise<string>
    {
        if (typeof settings       !== 'object') throw new BadRequestError('Request body must be a settings JSON object');
        if (typeof settings.codec !== 'string') throw new BadRequestError('Setting codec is missing');
        if (typeof settings.mic   !== 'string') throw new BadRequestError('Setting mic is missing');

        const ops: Promise<string>[] = [];
        if (settings.mic === '-3') {
            debug('Generating microphone input via Google TTS');
            const wav = await say(settings, opts);
            ops.push(bufferToTmpFile(wav));
        } else {
            ops.push(urlToTmpFile(settings.mic, 'audio/wav'));
        }

        if (settings.noise) {
            debug('Got background noise audio input');
            ops.push(urlToTmpFile(settings.noise, 'audio/wav'));
        }

        try {
            const rv = await Promise.all(ops);
            settings.mic = rv[0];
            if (settings.noise) settings.noise = rv[1];
        } catch(e: any) {
            await Promise.allSettled(ops.map(op => op.then(fn => fs.unlink(fn))));
            throw e;
        }

        const output = await createTmpFilename();
        try {
            debug('Applying impairments');
            await run(output, settings);
            return output;
        } catch (e) {
            await fs.unlink(output).catch(() => { /* do nothing */ });
            throw e;
        } finally {
            await Promise.allSettled(ops.map(op => op.then(fn => fs.unlink(fn))));
        }
    }


    async function getMedialURL(filename: string, settings: any) {
        const data = await fs.readFile(filename);
        debug('Transcribing impaired audio using Google STT');
        const transcript = await transcribe(data, opts);
        const attrs: Record<string, string> = {
            description: getFileDescription(settings)
        };

        if (typeof settings.text === 'string')
            attrs.original_transcript = settings.text;

        if (transcript) {
            attrs.transcript = transcript;
            debug('Calculating word error rate');
            attrs.wer = calculateWER(settings.text, transcript || '').toFixed(1);
        }

        // I set type any so mongoDB's insertOne function won't complain that type string is not
        // assignable to type objectId when I set _id to outputFilename.
        const outputFilename: any = `impaired_${uuidv4()}`;

        debug(`Uploading impaired audio file to media server`);
        const rv = await uploadMediaFile(outputFilename, data, attrs);

        debug(`Inserting impaired audio file's metadata to MongoDB`);
        await db.collection('impaired_files_metadata').insertOne({
            _id: outputFilename,
            settings: settings,
            time: Date.now()
        });

        return rv;
    }


    registerTaskFunction('impair', async (settings) => {
        const output = await generateImpairedAudio(settings);
        try {
            return await getMedialURL(output, settings);
        } finally {
            fs.unlink(output).catch(() => { /* do nothing */ });
        }
    });


    api.post('/', jsonify(locked((req, res) => {
        const settings = req.body;

        // Generating the response may take a while, especially if the hardware
        // AMBE chip is used.
        res.setTimeout(0);

        // Disable caching. The file will be re-generated on each POST based on
        // the settings.
        res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
        res.header('Expires', '-1');
        res.header('Pragma', 'no-cache');

        async function json() {
            const output = await generateImpairedAudio(settings);
            debug(`Sending impaired audio as data URL in JSON format`);
            try {
                const data = await fs.readFile(output);
                res.json(`data:audio/wav;base64,${data.toString('base64')}`);
            } catch (e: any) {
                jsonifyError(res, e);
            } finally {
                fs.unlink(output).catch(() => { /* do nothing */ });
            }
        }

        async function wav() {
            const output = await generateImpairedAudio(settings);
            debug('Sending impaired audio in raw audio/wav format');
            res.type('audio/wav');
            res.sendFile(output, (error) => {
                fs.unlink(output).catch(() => { /* do nothing */ });
                if (error) jsonifyError(res, error);
            });
        }

        async function url() {
            res.type(CTYPE_URL);
            const output = await generateImpairedAudio(settings);
            debug(`Sending media server URL`);
            try {
                res.send(await getMedialURL(output, settings));
            } finally {
                fs.unlink(output).catch(() => { /* do nothing */ });
            }
        }

        async function task() {
            res.type(CTYPE_TASK);
            debug(`Sending background task id`);
            res.send(await createTask('impair', settings));
        }

        res.format({
            '*/*'              : wav,
            'application/json' : json,
            'audio/wav'        : wav,
            [CTYPE_URL]        : url,
            [CTYPE_TASK]       : task,
            default            : wav
        });
        return undefined;
    })));

    return api;
}
