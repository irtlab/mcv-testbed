import express from 'express';
import mongodb from 'mongodb';
import {
    jsonify,
    NotFoundError,
    BadRequestError
} from '@janakj/lib/http';
import { parsePercentBreakdown } from '@janakj/lib/parse';
import type { ExperimentAudioSettings, Experiment } from '../types';
import { authorize } from '../auth';

const { ObjectId } = mongodb;
const expts_coll_str = 'experiments';
const authz_coll_str = 'subject_data_authz_keys';


async function insertAuthzKey(coll, id: string, exprID: string) {
    const rv = await coll.insertOne({
        _id: new ObjectId(id),
        experiment: exprID,
        create_subject: true
    });
    return rv;
}


function checkExperimentSettings(settings?: ExperimentAudioSettings) {
    if (settings === undefined) return;

    parsePercentBreakdown(settings.gender);
    parsePercentBreakdown(settings.codec);

    if (settings.frame_loss !== undefined)
        parsePercentBreakdown(settings.frame_loss);

    if (settings.bit_errors !== undefined)
        parsePercentBreakdown(settings.bit_errors);
}


function verifyUpdateData(current_id, body): Experiment | null {
    const data = {...body};
    if (Object.prototype.hasOwnProperty.call(data, '_id')) {
        data._id = new ObjectId(data._id);
        if (current_id.toString() !== data._id.toString()) return null;

        if (Object.prototype.hasOwnProperty.call(data, 'consent_form') && data.consent_form) {
            data.consent_form = new ObjectId(data.consent_form);
        }
    }
    return data;
}


function decodeDataURL(url) {
    return Buffer.from(url.split(',')[1], 'base64');
}


async function setup_experiments_coll(db: mongodb.Db) {
    const experiment_validator = {
        validator: {
            $jsonSchema: {
                bsonType: 'object',
                required: ['name', 'type', 'author'],
                properties: {
                    name: {
                        bsonType: 'string',
                        minLength: 1,
                        description: 'Experiment name'
                    },
                    type: {
                        bsonType: 'string',
                        description: 'Experiment type'
                    },
                    author: {
                        bsonType: 'string',
                        description: 'Experiment author name'
                    },
                    consent_form: {
                        bsonType: 'objectId',
                        description: 'ObjectID of the selected consent form'
                    }
                }
            }
        }
    };

    // Since v3.6.0 createCollection will no longer returned a cached Collection
    // instance if a collection already exists in the database, rather it will
    // return a server error stating that the collection already exists.
    try {
        await db.createCollection(expts_coll_str, experiment_validator);
    } catch (error: any) {
        if (error.codeName !== 'NamespaceExists') throw error;
    }

    // Note that experiment name must be unique. I do not set _id field
    // (indexed by default) as an experiment name because the user should
    // be able to update the experiment name.
    await db.createIndex(expts_coll_str, {name: 1}, {unique: true});
}


export default async function experiment_api(db: mongodb.Db, opts, uploadMediaFile) {
    await setup_experiments_coll(db);
    const coll = db.collection(expts_coll_str);
    const authzKeysColl = db.collection(authz_coll_str);

    const api = express.Router();
    api.use(express.json({ limit: opts.maxUploadSize }));

    api.get(/^\/$/, authorize, jsonify(async (): Promise<Experiment[]> => {
        const rv = await coll.find({}).toArray() as unknown as Experiment[];
        return rv;
    }));


    api.post(/^\/$/, authorize, jsonify(async (req): Promise<Experiment> => {
        const data = req.body as Omit<Experiment, '_id'>;
        if (Object.prototype.hasOwnProperty.call(data, '_id'))
            throw new BadRequestError('Document failed validation');

        if (data.consent_form)
            (data as any).consent_form = new ObjectId(data.consent_form);

        checkExperimentSettings(data.settings);

        if (data.type === 'mturk') {
            data.mturk_id = (new ObjectId()).toString();
        }

        const { insertedId } = await coll.insertOne(data);
        await insertAuthzKey(authzKeysColl, data.mturk_id, insertedId.toString());
        return { _id: insertedId.toString(), ...data };
    }));


    api.post('/:id', authorize, jsonify(async (req): Promise<Experiment> => {
        const query = {_id: new ObjectId(req.params.id)};
        const data = verifyUpdateData(query._id, req.body);
        if (!data) throw new BadRequestError('Document failed validation');
        checkExperimentSettings(data.settings);

        if (data.type === 'mturk') {
            data.mturk_id = (new ObjectId()).toString();
            await insertAuthzKey(authzKeysColl, data.mturk_id, req.params.id);
        }

        const update = {$set: data};
        // Note that updateOne does not return the updated document this is
        // why I use findOneAndUpdate, but it requires a write lock for the
        // duration of the operation.
        const rv = await coll.findOneAndUpdate(query, update, { returnDocument: 'after' });
        if (!rv) throw new NotFoundError('Document does not exist');
        return rv.value as Experiment;
    }));


    api.delete(/^\/$/, authorize, jsonify(async (_req, res) => {
        await coll.drop();
        res.status(204).end();
    }));


    api.get('/:id', jsonify(async (req): Promise<Experiment> => {
        const rv = await coll.findOne({_id: new ObjectId(req.params.id)});
        if (!rv) throw new NotFoundError('Document does not exist');
        return rv as unknown as Experiment;
    }));


    api.put('/:id', authorize, jsonify(async (req): Promise<Experiment> => {
        const query = {_id: new ObjectId(req.params.id)};
        const data = verifyUpdateData(query._id, req.body);
        if (!data) throw new BadRequestError('Document failed validation');
        checkExperimentSettings(data.settings);

        if (data.files !== undefined) {
            if (!Array.isArray(data.files))
                throw new BadRequestError('Attribute files must be array');

            data.files = await Promise.all(data.files.map(async (f) => {
                const { name, url } = f;
                if (!url.startsWith('data:')) return f;
                return {
                    ...f,
                    url: await uploadMediaFile(`.${name}`, decodeDataURL(url))
                };
            }));
        }

        if (data.type === 'mturk') {
            data.mturk_id = (new ObjectId()).toString();
            await insertAuthzKey(authzKeysColl, data.mturk_id, req.params.id);
        }

        // Note that replaceOne does not return the updated document this is
        // why I use findOneAndReplace, but it requires a write lock for the
        // duration of the operation.
        const rv = await coll.findOneAndReplace(query, data, { returnDocument: 'after' });
        if (!rv) throw new NotFoundError('Document does not exist');
        return rv.value as Experiment;
    }));


    api.delete('/:id', authorize, jsonify(async (req, res) => {
        await coll.deleteOne({_id: new ObjectId(req.params.id)});
        res.status(204).end();
    }));

    return api;
}
