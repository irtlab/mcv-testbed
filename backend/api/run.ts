import logger from 'debug';
import express from 'express';
import mongodb from 'mongodb';
import { distance as levenshtein_distance } from 'fastest-levenshtein';
import { jsonify, NotFoundError, BadRequestError } from '@janakj/lib/http';
import { PercentBreakdown, parsePercentBreakdown } from '@janakj/lib/parse';
import { shuffle as shuffleInPlace } from '@janakj/lib/random';
import type { Experiment, FrameLossType, ExperimentRun, AudioSettings, ExpRunAnalitics } from '../types';
import { authorize } from '../auth';
import { createTask, pickupTask } from '../task';
import e from 'express';

const debug = logger('mcv:api:run');

const expr_coll_str = 'experiment_run';

const { ObjectId } = mongodb;


async function setup_coll(db: mongodb.Db) {
    const coll_validator = {
        validator: {
            $jsonSchema: {
                bsonType: 'object',
                required: ['experiment', 'experimenter', 'subject'],
                properties: {
                    experiment: {
                        bsonType: 'objectId',
                        description: 'ObjectID of the running experiment'
                    },
                    experimenter: {
                        bsonType: 'string',
                        minLength: 1,
                        description: 'The identity of the experimenter'
                    },
                    subject: {
                        bsonType: 'object',
                        description: 'The identity of the test subject involved in the experiment'
                    }
                }
            }
        }
    };

    // Since v3.6.0 createCollection will no longer returned a cached Collection
    // instance if a collection already exists in the database, rather it will
    // return a server error stating that the collection already exists.
    try {
        await db.createCollection(expr_coll_str, coll_validator);
    } catch (error: any) {
        if (error.codeName !== 'NamespaceExists') throw error;
    }
}


function verifyUpdateData(current_id, body): ExperimentRun | null {
    const data = {...body};
    if (Object.prototype.hasOwnProperty.call(data, '_id')) {
        data._id = new ObjectId(data._id);
        if (current_id.toString() !== data._id.toString()) return null;
    }
    return data;
}


function shuffle<T>(data: T[]): T[] {
    const array = [...data];
    shuffleInPlace(array);
    return array;
}


// Convert percentage to count and round down.
function getPctCount(pct: number, total_count: number): number {
    return Math.floor((pct * total_count) / 100);
}


function fillArray<T>(config: T | PercentBreakdown<T>, number_of_steps: number, randomize_steps?: boolean): T[] {
    const items = parsePercentBreakdown<T>(config);

    const result: T[] = [];
    items.forEach((([pct, val]) => {
        const count = getPctCount(pct, number_of_steps);
        for(let i = 0; i < count; i++) result.push(val);
    }));

    // This is because I always round down in getPctCount function.
    if (result.length && result.length !== number_of_steps) {
        result.push(result[result.length - 1]);
    }

    // Randomize orders so it will be mixed.
    return randomize_steps ? shuffle<T>(result) : result;
}


function printApplyingStep(settings: Partial<AudioSettings>, i: number, count: number) {
    let be_fl_text = '';
    if (settings.rx_loss !== null && settings.rx_error !== null) {
        be_fl_text = `loss=${settings.rx_loss}%,BER=${settings.rx_error}%`;
    } else if (settings.rx_loss !== null) {
        be_fl_text = `loss=${settings.rx_loss}%`;
    } else {
        be_fl_text = `BER=${settings.rx_error}%`;
    }

    debug(`${i}/${count}: Imparing text "${settings.text}" using parameters [gender=${settings.gender},codec=${settings.codec},${be_fl_text}]`);
}


async function generateAudio(exp: Experiment, run: Omit<ExperimentRun, '_id'>)
{
    if (!exp.steps || !exp.settings) return;
    if (run.audio === undefined) run.audio = ([] as string[]).fill('');

    const config    = exp.settings;
    const codec     = fillArray<string>(config.codec, exp.steps.length, exp.randomize_steps);
    const frameLoss = fillArray<FrameLossType>(config.frame_loss || 0, exp.steps.length, exp.randomize_steps);
    const bitErrors = fillArray<number>(config.bit_errors || 0, exp.steps.length, exp.randomize_steps);
    const gender    = fillArray<string>(config.gender, exp.steps.length, exp.randomize_steps);

    for(let i = 0; i < exp.steps.length; i++) {
        const step = exp.steps[i];
        const audio = run.audio[i] || step.audio;
        if (audio !== undefined && audio !== '') continue;

        const settings = JSON.parse(JSON.stringify(step.settings));
        settings.codec    = codec[i];
        settings.rx_loss  = frameLoss[i] !== undefined ? frameLoss[i] : null;
        settings.rx_error = bitErrors[i] !== undefined ? bitErrors[i] : null;
        settings.gender   = gender[i] as 'male' | 'female';

        printApplyingStep(settings, i + 1, exp.steps.length);
        run.audio[i] = `task:${await createTask('impair', run['_id'].toString() ,settings)}`;
    }
}


export default async function experiment_run_api(db: mongodb.Db) {
    await setup_coll(db);
    const coll = db.collection(expr_coll_str);

    const api = express.Router();
    api.use(express.json());


    api.get('/:id', jsonify(async (req): Promise<ExperimentRun> => {
        const rv = await coll.findOne({_id: new ObjectId(req.params.id)});
        if (!rv) throw new NotFoundError('Experiment run with the given id does not exist');
        return rv as unknown as ExperimentRun;
    }));


    api.get('/:id/analitics', jsonify(async (req): Promise<ExpRunAnalitics[]> => {
        const run = await coll.findOne({_id: new ObjectId(req.params.id)});
        if (!run) throw new NotFoundError('Experiment run with the given id does not exist');

        const exp = await db.collection('experiments').findOne({_id: run.experiment});
        if (!exp) throw new NotFoundError('Experiment for the given experiment run not found');
        if (exp.type === 'interactive') throw new BadRequestError('This endpoint is not desined for interactive experiments');

        const subjectData: any = await db.collection('subject_data').findOne({experiment_run: req.params.id});
        if (!subjectData) throw new NotFoundError('Subject data for the given experiment run not found');

        const subjectRes: {[key: number]: string} = {};
        subjectData.steps.forEach(e => {
            subjectRes[e.file_idx] = e.response.toUpperCase()
        });

        const result: ExpRunAnalitics[] = [];
        for (let idx = 0; idx < exp.steps.length; idx++) {
            // It is possible that the system wasn't able to store the subject's response for the given step number.
            // In this case, we cannot do anything.
            if (subjectRes[idx]) {
                const step = exp.steps[idx];
                const correctAnswer: string = step.correct_answer.toUpperCase();
                const subjectAnswer: string = subjectRes[idx];
                const lDistance: number = levenshtein_distance(correctAnswer, subjectAnswer);
                const audioPath: string = run.audio[idx];

                let stepSettings = step.settings;
                // In this case audio files were not generated from the UI.
                if (!stepSettings.code) {
                    const audioID: string | undefined = audioPath.split('/').pop();
                    const fileMetadata = await db.collection('impaired_files_metadata').findOne({_id: audioID});
                    if (!fileMetadata) throw new NotFoundError('Audio file metadata not found');
                    stepSettings = fileMetadata.settings;
                }
                result.push({
                    step: idx + 1,
                    correct_answer: correctAnswer,
                    subject_answer: subjectAnswer,
                    audio: audioPath,
                    lscore: lDistance,
                    normalized_lscore: Number((1 - (lDistance / correctAnswer.length)).toFixed(2)),
                    settings: stepSettings
                });
            }
        }
        return result;
    }));


    // This API is designed to retrieve an experiment run for MTurk subject.
    // The main issue is that the subject document does not include experiment run _id,
    // but the experiment run does include the subject _id.
    api.get('/:id/subject_run', jsonify(async (req): Promise<ExperimentRun> => {
        const rv = await coll.findOne({'subject._id': req.params.id});
        if (!rv) throw new NotFoundError('Experiment run with the given subject id does not exist');
        return rv as unknown as ExperimentRun;
    }));


    // There are two ways (request formats) to make a POST request for inserting a document
    // or a list of documents.
    // 1. {list: [data]} format. Where the data (array elements) will be inserted.
    // 2. A document which does not contain the _id property. In this case the
    //    document will be inserted.
    //
    // In all cases it will return an inserted document or documents.
    api.post(/^\/$/, jsonify(async (req): Promise<ExperimentRun | ExperimentRun[]> => {
        const run: Omit<ExperimentRun, '_id'>[] = [];
        let listMode = false;

        if (typeof req.body !== 'object')
            throw new BadRequestError('Request body must be an object');

        if (Object.prototype.hasOwnProperty.call(req.body, 'list')) {
            listMode = true;
            if (!Array.isArray(req.body.list)) throw new BadRequestError('Propery list must be an array');
            run.push(...req.body.list);
        } else {
            run.push(req.body);
        }

        for (let i = 0; i < run.length; i++) {
            if (Object.prototype.hasOwnProperty.call(run[i], '_id'))
                throw new BadRequestError('Experiment run objects being created must not have _id property');

            const expId = new ObjectId(run[i].experiment);
            (run[i] as any).experiment = expId;
            run[i].start = 0;
            run[i].end = 0;
            run[i].failed = false;
            run[i].reason = '';
            run[i].status = 'Generating audio';

            const experiments = db.collection('experiments');
            const expr = await experiments.findOne({ _id: expId }) as unknown as Experiment;
            if (expr === null) throw new BadRequestError(`The experiment referenced by run was not found`);

            // I create MongoDB document _id field here in order to pass it to the task manager so the manager would know which
            // ExperimentRun is being processed. This would allow to get task processing status for every single experiment run.
            //
            // If the experiment is interactive, then no need to generate audio.
            run[i]['_id'] = new ObjectId();
            if (expr.type === 'interactive') run[i].status = 'Ready to start';
            else await generateAudio(expr, run[i]);
        }

        const rv = await coll.insertMany(run);

        const ret = run.map((r, i) => ({ _id: rv.insertedIds[i].toString(), ...r }));
        return listMode ? ret : ret[0];
    }));


    api.post('/:id', jsonify(async (req): Promise<ExperimentRun> => {
        const query = {_id: new ObjectId(req.params.id)};
        const data = verifyUpdateData(query._id, req.body);
        if (!data) throw new BadRequestError('Document failed validation');

        if (Object.prototype.hasOwnProperty.call(data, 'end')) {
            data.end = new Date();
            data.status = data.status ? data.status : 'Finished';
            // This should be okay, though this won't guarantee uniqueness.
            // We need this only for listening experiments and we cannot check it here.
            data.amt_survey_code = `${Date.now()}${Math.floor(Math.random() * 10000)}`;
        }

        if (Object.prototype.hasOwnProperty.call(data, 'start')) {
            data.start = new Date();
            data.status = data.status ? data.status : 'Running';
        }

        const update = {$set: data};
        // Note that updateOne does not return the updated document this is
        // why I use findOneAndUpdate, but it requires a write lock for the
        // duration of the operation.
        const rv = await coll.findOneAndUpdate(query, update, { returnDocument: 'after' });
        if (!rv || rv.value === null) throw new NotFoundError('Document does not exist');
        return rv.value as ExperimentRun;
    }));

    api.post('/:id/await', jsonify(async (req, res) => {
        const { id } = req.params;
        const _id = new ObjectId(id);

        const run = await coll.findOne({ _id }) as unknown as ExperimentRun;
        if (run === null) throw new NotFoundError(`Run ${id} not found`);

        const experiments = db.collection('experiments');
        const exp = await experiments.findOne({ _id: new ObjectId(run.experiment) }) as unknown as Experiment;
        if (exp === null) throw new NotFoundError(`The experiment was not found`);

        if (!exp.steps || !exp.settings || run.audio === undefined) {
            res.status(204).send();
            return;
        }

        for(let i = 0; i < run.audio.length; i++) {
            if (run.audio[i].startsWith('task:')) {
                const id = run.audio[i].split(':')[1];
                run.audio[i] = await pickupTask(id);

                const rv = await coll.findOneAndReplace({ _id }, run);
                if (!rv || rv.value === null)
                    throw new NotFoundError(`Experiment run disappeared from database`);
            }
        }

        res.status(204).send();
    }));

    api.post('/:id/genaudio', jsonify(async (req) => {
        const query = {_id: new ObjectId(req.params.id)};

        const run = await coll.findOne(query) as unknown as ExperimentRun;
        if (run === null) throw new NotFoundError(`Run ${req.params.id} not found`);
        run.audio = ([] as string[]).fill('');

        const experiments = db.collection('experiments');
        const exp = await experiments.findOne({ _id: new ObjectId(run.experiment) }) as unknown as Experiment;
        if (exp === null) throw new NotFoundError(`The experiment referenced by run was not found`);
        if (!exp.steps) throw new BadRequestError('No steps to generate audio');

        for (let i = 0; i < exp.steps.length; i++) {
            printApplyingStep(exp.steps[i].settings, i + 1, exp.steps.length);
            run.audio[i] = `task:${await createTask('impair', run['_id'].toString(), exp.steps[i].settings)}`;
        }

        const update = {
            $set: {
                status: 'Generating audio',
                failed: false,
                reason: '',
                audio: run.audio
            }
        };
        const rv = await coll.findOneAndUpdate(query, update, { returnDocument: 'after' });
        if (!rv || rv.value === null) throw new NotFoundError('Document does not exist');
        return rv.value as ExperimentRun;
    }));

    api.put('/:id', jsonify(async (req): Promise<ExperimentRun> => {
        const query = {_id: new ObjectId(req.params.id)};
        const data = verifyUpdateData(query._id, req.body);
        if (!data) throw new BadRequestError('Document failed validation');

        // Note that replaceOne does not return the updated document this is
        // why I use findOneAndReplace, but it requires a write lock for the
        // duration of the operation.
        const rv = await coll.findOneAndReplace(query, data, { returnDocument: 'after' });
        if (!rv || rv.value === null) throw new NotFoundError('Document does not exist');
        return rv.value as ExperimentRun;
    }));

    api.get(/^\/$/, authorize, jsonify(async (): Promise<ExperimentRun[]> => {
        const rv = await coll.find({}).toArray();
        return rv as unknown as ExperimentRun[];
    }));


    api.delete('/:id', authorize, jsonify(async (req, res) => {
        await coll.deleteOne({_id: new ObjectId(req.params.id)});
        res.status(204).end();
    }));

    return api;
}
