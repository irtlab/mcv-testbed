import express from 'express';
import mongodb from 'mongodb';
import { jsonify, NotFoundError, BadRequestError, UnauthorizedError } from '@janakj/lib/http';

const { ObjectId } = mongodb;
const subject_data_coll_str = 'subject_data';
const authz_coll_str = 'subject_data_authz_keys';


async function setup_coll(db) {
    const coll_validator = {
        validator: {
            $jsonSchema: {
                bsonType: 'object',
                required: ['name', 'email'],
                properties: {
                    name: {
                        bsonType: 'string',
                        minLength: 1,
                        description: 'The identity of the subject'
                    },
                    email: {
                        bsonType: 'string',
                        minLength: 1,
                        description: 'The email address of the subject'
                    }
                }
            }
        }
    };

    // Since v3.6.0 createCollection will no longer returned a cached Collection
    // instance if a collection already exists in the database, rather it will
    // return a server error stating that the collection already exists.
    try {
        await db.createCollection(subject_data_coll_str, coll_validator);
    } catch (error: any) {
        if (error.codeName !== 'NamespaceExists') throw error;
    }
}


function verifyUpdateData(current_id, body) {
    const data = {...body};
    if (Object.prototype.hasOwnProperty.call(data, '_id')) {
        data._id = new ObjectId(data._id);
        if (current_id.toString() !== data._id.toString()) return null;
    }
    return data;
}


export default async function subject_data_api(db: mongodb.Db) {
    await setup_coll(db);
    const coll = db.collection(subject_data_coll_str);
    const authzKeysColl = db.collection(authz_coll_str);

    const api = express.Router();
    api.use(express.json());


    api.post(/^\/$/, jsonify(async (req) => {
        // I expect that the frontend sends data in {list: [data]} format.
        if (!Object.prototype.hasOwnProperty.call(req.body, 'list')) {
            throw new BadRequestError('Document failed validation');
        }

        const data: any[] = req.body.list;
        for (let i = 0; i < data.length; i++) {
            if (Object.prototype.hasOwnProperty.call(data[i], '_id')) {
                throw new BadRequestError('Document failed validation');
            }
        }

        // Note that currently, when I insert for the first time a document schema
        // would contain only name and email properties. Remaining properties:
        // feedback, audio and demographics will be added when the subject provides data.
        // Later, if somebody queries data should consider that if the subject fails to
        // provide corresponding data then the document won't contain the above properties.
        const rv = await coll.insertMany(data);
        return data.map((d, i) => {
            d._id = rv.insertedIds[i];
            return d;
        });
    }));


    api.post('/:id', jsonify(async (req) => {
        const query = {_id: new ObjectId(req.params.id)};
        const data = verifyUpdateData(query._id, req.body);
        if (!data) throw new BadRequestError('Document failed validation');

        const update = {$set: data};
        // Note that updateOne does not return the updated document this is
        // why I use findOneAndUpdate, but it requires a write lock for the
        // duration of the operation.
        const rv = await coll.findOneAndUpdate(query, update, {returnDocument: 'after'});
        if (!rv) throw new NotFoundError('Document does not exist');
        return rv.value;
    }));

    api.post('/:id/mturk', jsonify(async (req) => {
        if (req.params.id !== req.body.expr_id) throw new BadRequestError('Document failed validation');

        let found = await authzKeysColl.findOne({_id: new ObjectId(req.body.mturk_id)});
        if (!found) throw new UnauthorizedError('Unauthorized request');
        if (found.experiment !== req.body.expr_id) throw new NotFoundError('Document failed validation');
        if (!found.create_subject) throw new BadRequestError('Acces to create a subject is closed');

        const update = {$set: {assigned: true}};
        // findOneAndUpdate, requires a write lock for the duration of the operation. This would allow to make sure
        // that other async requests won't get the same document at the same time.
        // Based on the MongoDB documentation, no reads will be available until the data is written.
        let rv: any = await coll.findOneAndUpdate({assigned: false}, update, {returnDocument: 'after'});
        if (rv && rv.value) return { _id: rv.value._id.toString(), assigned: true, ...rv.value };

        // If the system reaches this point, it means that the experimenter did not create pre-generated runs.
        const data = {
            name: 'unknown',
            email: 'unknown',
            date: new Date()
        };
        const doc = await coll.insertOne(data);
        return { _id: doc.insertedId.toString(), ...data };
    }));


    api.put('/:id', jsonify(async (req) => {
        const query = {_id: new ObjectId(req.params.id)};
        const data = verifyUpdateData(query._id, req.body);
        if (!data) throw new BadRequestError('Document failed validation');

        // Note that replaceOne does not return the updated document this is
        // why I use findOneAndReplace, but it requires a write lock for the
        // duration of the operation.
        const rv = await coll.findOneAndReplace(query, data, {returnDocument: 'after'});
        if (!rv) throw new NotFoundError('Document does not exist');
        return rv.value;
    }));


    return api;
}

