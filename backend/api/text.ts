import express from 'express';
import { BadRequestError, jsonify } from '@janakj/lib/http';
import * as defaults from '../defaults';
import { authorize } from '../auth';
import numWords from 'num-words';
import fetch from 'node-fetch';


const GOOGLE_TTS_URL = process.env.GOOGLE_TTS_URL || defaults.GOOGLE_TTS_URL;
const GOOGLE_STT_URL = process.env.GOOGLE_STT_URL || defaults.GOOGLE_STT_URL;


/**
 * Source: https://en.wikipedia.org/wiki/Word_error_rate
 * Google Speech-to-Text API transcribes numbers in numeric format
 * Method converts numbers to their representation in the original text with package num-words
 */
export function calculateWER(text: string, transcript: string)
{
    let o_arr: any[] = [];
    let n_arr: any[] = [];

    if (text.includes(' ')) {
        o_arr = text.split(' ');
        n_arr = transcript.split(' ');
    } else {
        o_arr[0] = text;
        n_arr[0] = transcript;
    }

    let ind = 0;
    let ind2 = 0;
    for (ind = 0; ind < n_arr.length; ind++) {
        const curr: any = n_arr[ind];
        if (!isNaN(curr) && isNaN(o_arr[ind])) {
            const spelling: string = numWords(curr);
            let s_arr: any[] = [];
            const amt: number = s_arr.length;
            if (spelling.includes(' ')) {
                s_arr = spelling.split(' ');
            } else {
                s_arr[0] = spelling;
            }

            n_arr[ind] = s_arr[0];
            if (s_arr.length > 1) {
                for (ind2 = 1; ind2 < s_arr.length; ind2++) {
                    n_arr.splice(ind + ind2, 0, s_arr[ind2]);
                }
            }

            ind += amt - 1;
        }
    }

    // checks for amount of correct words
    let c = 0;
    for (ind = 0; ind < o_arr.length; ind++) {
        const curr = o_arr[ind].toLowerCase();
        for (ind2 = ind; ind2 < n_arr.length; ind2++) {
            const n_curr = n_arr[ind2].toLowerCase();
            if (curr === n_curr) {
                c++;
                break;
            }
        }
    }

    // checks for number of insertions
    if (n_arr.length > o_arr.length) {
        c -= (n_arr.length - o_arr.length);
    }

    return c / o_arr.length;
}


export async function say(settings: Record<string, any>, opts: any): Promise<Buffer>
{
    const { text, gender, ssml_text } = settings;

    if (typeof text !== 'string')
        throw new BadRequestError('Input for TTS is invalid');

    const name = gender === 'male' ? 'en-US-Standard-B' : 'en-US-Standard-C';

    const ttsReq: any = {
        input       : { text },
        voice       : { languageCode: 'en-US', name },
        audioConfig : { audioEncoding: 'LINEAR16' },
    };

    if (ssml_text && ssml_text.length) {
        ttsReq.input = { ssml: `<speak> ${ssml_text} </speak>` };
        ttsReq.voice.ssmlGender = gender === 'male' ? 'MALE' : 'FEMALE';
    }

    const ttsRes = await fetch(`${GOOGLE_TTS_URL}key=${opts.googleTTSandSTTKey}`, {
        method  : 'POST',
        body    : JSON.stringify(ttsReq),
        headers : { 'Content-Type': 'application/json; charset=utf-8' }
    });

    if (!ttsRes.ok) {
        const res: any = await ttsRes.json();
        throw new Error(res.error.message);
    }

    return Buffer.from(JSON.parse(await ttsRes.text()).audioContent, 'base64');
}


export async function transcribe(audio: Buffer, opts: any): Promise<string | null>
{
    const sttReq = {
        config : { encoding: 'LINEAR16', languageCode: 'en-US' },
        audio  : { content: audio.toString('base64') },
    };

    const sttRes = await fetch(`${GOOGLE_STT_URL}key=${opts.googleTTSandSTTKey}`, {
        method  : 'POST',
        body    : JSON.stringify(sttReq),
        headers : { 'Content-Type': 'application/json' }
    });

    if (!sttRes) {
        throw new Error('Google STT API could not transcribe audio');
    } else if (!sttRes.ok) {
        const res: any = await sttRes.json();
        throw new Error(res.error.message);
    }

    const results = JSON.parse(await sttRes.text()).results;

    // Sometimes, Google STT cannot recognize the speech. For example, when
    // frame loss is 50% or higher. If it is the case, return null
    return results ? results[0].alternatives[0].transcript : null;
}


export default function(opts) {
    if (!opts.googleTTSandSTTKey)
        throw new Error('Invalid or missing Google TTS and STT API key');

    const api = express.Router();
    api.use(express.json({ limit: opts.maxUploadSize }));

    // eslint-disable-next-line @typescript-eslint/no-misused-promises
    api.post('/tts', authorize, async (req, res) => {
        const settings = req.body;
        const wav = await say(settings, opts);
        res.type('audio/wav');
        res.send(wav);
    });

    api.post('/wer', authorize, jsonify(req => {
        const { text, transcript } = req.body;

        if (typeof text !== 'string')
            throw new BadRequestError('Invalid or missing text property');

        if (typeof transcript !== 'string')
            throw new BadRequestError("Invalid or missing transcript property");

        return { wer: calculateWER(text, transcript) };
    }));

    return api;
}