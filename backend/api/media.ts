import express from 'express';
import { normalize, dirname, join, basename, extname } from 'path';
import { promises } from 'fs';
import { listAttributes, getAttribute, removeAttribute, setAttribute, setAttributeSync } from 'fs-xattr';
import multer from 'multer';
import { jsonify, NotFoundError, BadRequestError, ConflictError } from '@janakj/lib/http';
import Lock from '@janakj/lib/mutex';
import { authorize } from '../auth';

const {
    readdir, stat, unlink, writeFile, rename
} = promises;


async function getExtAttrs(path) {
    const prefix = 'user.';
    const names = await listAttributes(path);
    if (names.length === 0) return undefined;

    const kv = await Promise.all(names.map(async (name) => [name, await getAttribute(path, name)]));

    return kv.reduce((a, [k, v]) => {
        const key: any = k;
        if (key.startsWith(prefix)) {
            a[key.slice(prefix.length)] = v.toString();
        }
        return a;
    }, {});
}


// eslint-disable-next-line @typescript-eslint/no-unused-vars
async function deleteAllExtAttrs(path) {
    try {
        const names = await listAttributes(path);
        await Promise.all(names.map(name => removeAttribute(path, name)));
    } catch (error: any) {
        if (error.code !== 'ENOENT') throw error;
    }
}


async function setExtAttrs(path: string, attrs: Record<string, string>) {
    await Promise.all(Object.entries(attrs).map(([name, value]) =>
        setAttribute(path, `user.${name}`, value)));
}


async function generateUniqueEmptyFile(path) {
    const dir = dirname(path);
    const ext = extname(path);
    const base = basename(path, ext);

    let filename: string;
    for (let i = 1; i < 100; i++) {
        try {
            filename = join(dir, `${base}.${i}${ext}`);
            // eslint-disable-next-line no-await-in-loop
            await writeFile(filename, '', { flag: 'wx' });
            return filename;
        } catch (error: any) {
            if (error.code !== 'EEXIST') throw error;
        }
    }
    throw new Error(`Could not find a unique filename for file ${path}`);
}


async function getFileProps(path) {
    const { ctime, mtime, size } = await stat(path);
    return {
        filename: basename(path),
        ctime: ctime,
        mtime: mtime,
        size: size,
        attrs: await getExtAttrs(path)
    };
}


export default function (opts) {
    const dir = normalize(opts.wavDir);
    const api = express.Router();
    const lock = new Lock();

    const upload = multer({
        storage: multer.diskStorage({
            destination: dir,
            filename: async (_req, { originalname }, cb) => {
                const path = await generateUniqueEmptyFile(join(dir, `.${originalname}.partial`));
                cb(null, basename(path));
            }
        })
    });


    const locked = (fn) => async (req, res, next) => {
        await lock.acquire();
        try {
            return await fn(req, res, next);
        } finally {
            lock.release();
        }
    };


    api.use(express.json());

    api.use('/', express.static(dir, {
        index: false,
        redirect: false,
        dotfiles: 'allow',
        setHeaders: (res) => res.set('Content-Disposition', 'attachment')
    }));


    api.delete('/:filename', authorize, jsonify(locked(async (req, res) => {
        const path = normalize(join(dir, req.params.filename));
        if (dirname(path) !== dir) throw new BadRequestError('Invalid directory');

        try {
            await unlink(path);
        } catch (error: any) {
            if (error.code !== 'ENOENT') throw error;
        }
        res.status(204).end();
    })));


    api.get('/:filename/props', jsonify(locked(async (req) => {
        const path = normalize(join(dir, req.params.filename));
        if (dirname(path) !== dir) throw new BadRequestError('Invalid directory');
        try {
            return await getFileProps(path);
        } catch (error: any) {
            if (error.code === 'ENOENT') throw new NotFoundError('File not found');
            throw error;
        }
    })));


    api.post('/:filename/props', authorize, jsonify(locked(async (req) => {
        let dst; let tmp;
        const path = normalize(join(dir, req.params.filename));
        if (dirname(path) !== dir) throw new BadRequestError('Invalid directory');

        try {
            const { attrs, filename } = req.body;
            if (typeof filename === 'string' && req.params.filename !== filename) {
                dst = normalize(join(dir, filename));
                if (dirname(dst) !== dir) throw new BadRequestError('Invalid directory');

                // Make sure that the destination file does not exist yet
                await writeFile(dst, '', { flag: 'wx' });
                tmp = dst;
            }

            await Promise.all(Object.entries(attrs || {}).map(([k, v]) => {
                if (v === null) {
                    return removeAttribute(path, `user.${k}`).catch((error) => {
                        if (error.code !== 'ENODATA') throw error;
                    });
                }

                if (typeof v !== 'string') throw new BadRequestError('Attribute value must be string or null');

                return setAttribute(path, `user.${k}`, v);
            }));

            if (dst) {
                await rename(path, dst);
                tmp = null;
            }
            return await getFileProps(dst || path);
        } catch (error: any) {
            if (tmp) await unlink(tmp);
            if (error.code === 'ENOENT') throw new NotFoundError('File not found');
            if (error.code === 'EEXIST') throw new ConflictError(`File ${basename(dst)} already exists`);
            throw error;
        }
    })));


    api.post(/^\/$/, authorize, upload.array('file'), jsonify(locked((req) => Promise.all(req.files.map(async ({ originalname, filename }) => {
        let path = normalize(join(dir, basename(originalname)));
        if (dirname(path) !== dir) throw new BadRequestError('Invalid directory');

        try {
            await writeFile(path, '', { flag: 'wx' });
        } catch (error: any) {
            if (error.code !== 'EEXIST') throw error;
            path = await generateUniqueEmptyFile(path);
        }
        await rename(join(dir, filename), path);
        return [originalname, basename(path)];
    })))));


    api.get(/^\/$/, authorize, jsonify(locked(async () => {
        const rv = await readdir(dir, { withFileTypes: true });
        return Promise.all(rv
            .filter((v) => v.isFile())
            .filter(({ name }) => !name.startsWith('.'))
            .map(({ name }) => getFileProps(join(dir, name))));
    })));

    api.use('*', jsonify(() => {
        throw new NotFoundError();
    }));

    async function uploadMediaFile(filename, data, attrs?: Record<string, string>) {
        const name = basename(filename);

        let path = normalize(join(dir, name));
        if (dirname(path) !== dir) throw new Error('Pathname outside of the target directory');

        const n = name.startsWith('.') ? name.substring(1) : name;
        const tmp = await generateUniqueEmptyFile(join(dir, `.${n}.partial`));
        try {
            await writeFile(tmp, data);

            try {
                await writeFile(path, '', { flag: 'wx' });
            } catch (error: any) {
                if (error.code !== 'EEXIST') throw error;
                path = await generateUniqueEmptyFile(path);
            }

            try {
                await rename(tmp, path);
                if (attrs) await setExtAttrs(path, attrs);
            } catch(e) {
                unlink(path).catch(() => { /* do nothing */ });
                throw e;
            }

            return `https://mcv-testbed.cs.columbia.edu/api/media/${basename(path)}`;
        } catch(e) {
            await unlink(tmp).catch(() => { /* do nothing */ });
        }
    }

    return { api: api, uploadMediaFile: uploadMediaFile };
}
