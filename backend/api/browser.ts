import express from 'express';
import { jsonifyError, BadRequestError } from '@janakj/lib/http';
import { ServiceBrowser } from '@mcv/common/zeroconf';
import { authorize } from '../auth';


export function browser_api() {
    const api = express.Router();

    api.get('/:srv_type', authorize, (req, res) => {
        const browser = new ServiceBrowser(req.params.srv_type, process.env.DNS_SD_DOMAIN);

        function sse() {
            const found = (name) => res.write(`event: +\ndata: ${name}\n\n`);
            const lost = (name) => res.write(`event: -\ndata: ${name}\n\n`);
            const all = () => res.write('event: state\ndata: all_for_now\n\n');
            const cache = () => res.write('event: state\ndata: cache_exhausted\n\n');

            function stop() {
                res.off('close', stop);
                res.off('error', stop);
                res.end();

                browser.off('cache_exhausted', cache);
                browser.off('all_for_now', all);
                browser.off('lost', lost);
                browser.off('found', found);
                browser.off('error', stop);
                browser.stop().catch(() => { /* do nothing */ });
            }

            browser.once('error', stop);
            res.once('close', stop);
            res.once('error', stop);

            browser.on('found', found);
            browser.on('lost', lost);
            browser.on('all_for_now', all);
            browser.on('cache_exhausted', cache);

            res.header('Cache-Control', 'no-cache');
            res.removeHeader('Transfer-Encoding');
            res.setTimeout(0);

            browser.start().catch(({ message }) => {
                jsonifyError(res, new BadRequestError(message));
            });
        }

        function json() {
            // This handler will be invoked unless the agent specifically requests
            // text/event-stream content type. We create a service browser, wait for
            // the 'AllForNow' signal, and then send a list of all discovered services
            // in one JSON array.
            res.type('application/json');

            browser.once('all_for_now', () => {
                res.json([...Object.keys(browser.services)]);
                browser.stop().catch(() => { /* do nothing */ });
            });

            browser.once('error', (err) => {
                jsonifyError(res, err);
                browser.stop().catch(() => { /* do nothing */ });
            });

            browser.start().catch(({ message }) => {
                jsonifyError(res, new BadRequestError(message));
            });
        }

        res.format({
            '*/*': json,
            'application/json': json,
            'text/event-stream': sse,
            default: json
        });
    });

    return api;
}
