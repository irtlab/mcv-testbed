import express from 'express';
import mongodb from 'mongodb';
import { jsonify, NotFoundError, BadRequestError } from '@janakj/lib/http';
import { authorize } from '../auth';

const { ObjectId } = mongodb;
const conf_coll_str = 'configurations';


async function setup_config_coll(db) {
    const config_validator = {
        validator: {
            $jsonSchema: {
                bsonType: 'object',
                required: ['name', 'settings'],
                properties: {
                    name: {
                        bsonType: 'string',
                        minLength: 1,
                        description: 'Configuration name'
                    },
                    description: {
                        bsonType: 'string',
                        description: 'Longer human-friendly description of the configuration'
                    },
                    settings: {
                        bsonType: 'object',
                        description: 'UT settings'
                    }
                }
            }
        }
    };

    // Since v3.6.0 createCollection will no longer returned a cached Collection
    // instance if a collection already exists in the database, rather it will
    // return a server error stating that the collection already exists.
    try {
        await db.createCollection(conf_coll_str, config_validator);
    } catch (error: any) {
        if (error.codeName !== 'NamespaceExists') throw error;
    }

    // Note that configuration name must be unique. I do not set _id field
    // (indexed by default) as a configuration name because the user should
    // be able to update the configuration name.
    await db.createIndex(conf_coll_str, {name: 1}, {unique: true});
}


export default async function(db: mongodb.Db) {
    await setup_config_coll(db);
    const coll = db.collection(conf_coll_str);

    const api = express.Router();
    api.use(express.json());

    api.get('/:id', authorize, jsonify(async (req) => {
        const rv = await coll.findOne({_id: new ObjectId(req.params.id)});
        if (!rv) throw new NotFoundError('Document does not exist');
        return rv;
    }));


    api.post(/^\/$/, authorize, jsonify(async (req) => {
        if (!Object.prototype.hasOwnProperty.call(req.body, '_id')) {
            const rv = await coll.insertOne(req.body);
            return { _id: rv.insertedId.toString(), ...req.body };
        }
        throw new BadRequestError('Document failed validation');
    }));


    api.get(/^\/$/, authorize, jsonify(async () => {
        const rv = await coll.find({}).toArray();
        return rv;
    }));

    return api;
}
