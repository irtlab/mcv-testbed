import express from 'express';
import mongodb from 'mongodb';
import path from 'path';
import * as fs from 'fs';
import * as crypto from 'crypto';
import { jsonify, NotFoundError, BadRequestError } from '@janakj/lib/http';
import { authorize } from '../auth';

const fsp = fs.promises;
const { ObjectId } = mongodb;
const consent_forms_coll_str = 'consent_forms';
const consent_forms_signed_coll_str = 'consent_forms_signed';


function verifyUpdateData(current_id, body) {
    const data = {...body};
    if (Object.prototype.hasOwnProperty.call(data, '_id')) {
        data._id = new ObjectId(data._id);
        if (current_id.toString() !== data._id.toString()) return null;
    }
    return data;
}


async function setup_consent_form_coll(db: mongodb.Db) {
    const consent_form_validator = {
        validator: {
            $jsonSchema: {
                bsonType: 'object',
                required: ['name', 'form'],
                properties: {
                    name: {
                        bsonType: 'string',
                        minLength: 1,
                        description: 'Consent form name'
                    },
                    form: {
                        bsonType: 'string',
                        minLength: 1,
                        description: 'Consent form'
                    }
                }
            }
        }
    };

    const consent_form_signed_validator = {
        validator: {
            $jsonSchema: {
                bsonType: 'string',
                required: ['experiment', 'experiment_run', 'email', 'consent_form'],
                properties: {
                    experiment: {
                        bsonType: 'string',
                        minLength: 24,
                        description: 'Experiment ID'
                    },
                    experiment_run: {
                        bsonType: 'string',
                        minLength: 24,
                        description: 'Experiment Run ID'
                    },
                    email: {
                        bsonType: 'string',
                        minLength: 1,
                        description: 'Subject email'
                    },
                    consent_form: {
                        bsonType: 'string',
                        minLength: 24,
                        description: 'Consent Form ID'
                    }
                }
            }
        }
    };

    // Since v3.6.0 createCollection will no longer returned a cached Collection
    // instance if a collection already exists in the database, rather it will
    // return a server error stating that the collection already exists.
    try {
        await db.createCollection(consent_forms_coll_str, consent_form_validator);
        await db.createCollection(consent_forms_signed_coll_str, consent_form_signed_validator);
    } catch (error: any) {
        if (error.codeName !== 'NamespaceExists') throw error;
    }
}


export default async function consent_form_api(db: mongodb.Db, opts) {
    await setup_consent_form_coll(db);
    const coll = db.collection(consent_forms_coll_str);
    const coll_signed = db.collection(consent_forms_signed_coll_str);

    const api = express.Router();
    api.use(express.json());


    api.get(/^\/$/, authorize, jsonify(async () => {
        const rv = await coll.find({}).toArray();
        return rv;
    }));


    api.post(/^\/$/, authorize, jsonify(async (req) => {
        if (!Object.prototype.hasOwnProperty.call(req.body, '_id')) {
            if (!req.body.form) {
                req.body.form = await fsp.readFile(path.resolve(opts.dataDir, 'initial_consent_form.md'), 'utf8');
            }

            const rv = await coll.insertOne(req.body);
            return {
                _id: rv.insertedId.toString(),
                ...req.body
            };
        }
        throw new BadRequestError('Document failed validation');
    }));


    api.post('/:id', authorize, jsonify(async (req) => {
        const query = {_id: new ObjectId(req.params.id)};
        const data = verifyUpdateData(query._id, req.body);
        if (!data) throw new BadRequestError('Document failed validation');

        const update = {$set: data};
        // Note that updateOne does not return the updated document this is
        // why I use findOneAndUpdate, but it requires a write lock for the
        // duration of the operation.
        const rv = await coll.findOneAndUpdate(query, update, { returnDocument: 'after' });
        if (!rv) throw new NotFoundError('Document does not exist');
        return rv.value;
    }));


    api.post('/:id/sign', jsonify(async (req) => {
        if (Object.prototype.hasOwnProperty.call(req.body, '_id')) throw new BadRequestError('Document failed validation');
        // The main reason I made _id as a string because length of md5 hex is longer than 12 bytes.
        const data = {
            _id: crypto.createHash('md5').update(`${req.params.id}${req.body.email}${req.body.experiment}`).digest("hex"),
            signed_at: new Date(),
            ...req.body
        };
        await coll_signed.insertOne(data);
        return data;
    }));


    api.delete(/^\/$/, authorize, jsonify(async (_req, res) => {
        await coll.drop();
        res.status(204).end();
    }));


    // The following API endpoint is not authorized. It needs to be accessible by
    // subjects who do not authenticate.
    api.get('/:id', jsonify(async (req) => {
        const rv = await coll.findOne({_id: new ObjectId(req.params.id)});
        if (!rv) throw new NotFoundError('Document does not exist');
        return rv;
    }));


    api.get('/:id/sign', jsonify(async (req) => {
        const id = crypto.createHash('md5').update(`${req.params.id}`).digest("hex");
        const rv = await coll_signed.findOne({_id: id});
        // It is totally fine if the document does not exists. It means no signed document yet.
        return rv;
    }));

    api.put('/:id', authorize, jsonify(async (req) => {
        const query = {_id: new ObjectId(req.params.id)};
        const data = verifyUpdateData(query._id, req.body);
        if (!data) throw new BadRequestError('Document failed validation');

        // Note that replaceOne does not return the updated document this is
        // why I use findOneAndReplace, but it requires a write lock for the
        // duration of the operation.
        const rv = await coll.findOneAndReplace(query, data, { returnDocument: 'after' });
        if (!rv) throw new NotFoundError('Document does not exist');
        return rv.value;
    }));


    api.delete('/:id', authorize, jsonify(async (req, res) => {
        await coll.deleteOne({_id: new ObjectId(req.params.id)});
        res.status(204).end();
    }));

    return api;
}
