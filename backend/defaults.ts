export const MAX_UPLOAD_SIZE            = '128mb';
export const DATA_DIR                   = '.mcv-testbed';
export const WAV_DIR                    = 'wavs';
export const AMBE_URI                   = 'grpc:ambed.mcv:50051';
export const GOOGLE_TTS_URL             = 'https://texttospeech.googleapis.com/v1/text:synthesize?';
export const GOOGLE_STT_URL             = 'https://speech.googleapis.com/v1p1beta1/speech:recognize?';
export const CERTIFICATE_CHECK_INTERVAL = 1000 * 60 * 60 * 24; // Check TLS server certificate once a day
