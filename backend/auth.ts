import express from 'express';
import passport from 'passport';
import crypto from 'crypto';
import google from 'passport-google-oauth20';
import session from 'express-session';
import { jsonify } from '@janakj/lib/http';
import logger from 'debug';
import mongoSession from 'connect-mongodb-session';

const debug = logger('mcv:auth');

let authenticatedIPs: string[] = [];
let authEnabled = false;

/**
 * Check whether any verified email address in the profile is on the admin list.
 * Returns true if that is the case and false otherwise.
 *
 * @param {*} profile User profile
 * @param {*} admins  List of admin (privileged) email addresses
 */
function authorizedEmail(profile: Record<string, any>, admins: string[]) {
    if (!Array.isArray(profile.emails)) return null;

    const emails = profile.emails.filter((v) => v.verified).map((v) => v.value.toLowerCase());
    const adm = admins.map((v) => v.toLowerCase());

    for (const email of emails) if (adm.includes(email)) return email;

    return null;
}


export function authorize(req: express.Request, res: express.Response, next: express.NextFunction) {
    if (!authEnabled || authenticatedIPs.includes(req.ip) || req.isAuthenticated()) {
        next();
    } else {
        res.sendStatus(401);
    }
}


export function authorizeRedirect(req: express.Request, res: express.Response, next: express.NextFunction) {
    if (!authEnabled || authenticatedIPs.includes(req.ip) || req.isAuthenticated()) {
        next();
    } else {
        const nextUrl = `${req.protocol}://${req.get('host')}${req.originalUrl}`;
        const failure = `${req.protocol}://${req.get('host')}/access_denied`;
        const url = `/auth/signin?successRedirect=${encodeURIComponent(nextUrl)}&failureRedirect=${encodeURIComponent(failure)}`;
        res.redirect(302, url);
    }
}


export default function initAuthentication(app: express.Application, port?: number) {
    const { env } = process;
    const opts = {
        googleClientId     : env.GOOGLE_CLIENT_ID,     // Google OAuth2 client ID
        googleClientSecret : env.GOOGLE_CLIENT_SECRET, // Google OAuth2 client secret
        privilegedAccounts : env.PRIVILEGED_ACCOUNTS,  // List of comma-separated privileged account emails
        sessionSecret      : env.SESSION_SECRET,       // HTTP session secret
        mongo              : env.MONGODB_URL           // MongoDB database URL
    };

    if (!opts.mongo)
        throw new Error('Please configure environment variable MONGODB_URL');

    if (!opts.googleClientId)
        throw new Error('Please set environment variable GOOGLE_CLIENT_ID');

    if (!opts.googleClientSecret)
        throw new Error('Please set environment variable GOOGLE_CLIENT_SECRET');

    let admins: string[] = [];
    if (typeof opts.privilegedAccounts === 'string')
        admins = opts.privilegedAccounts
            .split(',')
            .map(s => s.trim())
            .filter(s => s.length !== 0);

    if (admins.length === 0) {
        debug('Warning: Authentication is enabled, but not privileged accounts have been configured');
    } else {
        debug(`Privileged accounts: ${admins.join(',')}`);
    }

    if (typeof process.env.AUTHENTICATED_IPS === 'string')
        authenticatedIPs = process.env.AUTHENTICATED_IPS
            .split(',')
            .map(s => s.trim())
            .filter(s => s.length !== 0);

    debug(`Authenticated IP addresses: ${authenticatedIPs.join(',')}`);

    debug('Enabling HTTP session support');

    if (!opts.sessionSecret) {
        debug('Generating temporary random session secret');
        opts.sessionSecret = crypto.randomBytes(64).toString();
    }

    const MongoStore = mongoSession(session);

    // Since cookies are not scoped by the port number (a cookie set by
    // http://server will be send to http://server:1234), we include the port
    // number in the name of the cookie. Without this, a development instance
    // running on port 4000, for example, would not be able to overwrite the
    // cookie set by the official HTTPS instance on port 443 on the same
    // hostname. Chrome would complain that a s cookie cannot be overwritten by
    // an insecure one.
    app.use(session({
        secret            : opts.sessionSecret,
        store             : new MongoStore({ uri: opts.mongo, collection : 'sessions' }),
        resave            : false,
        saveUninitialized : false,
        cookie            : { secure: false, sameSite: true },
        name              : `mcv.session${port !== undefined ? `.${port}` : ''}`
    }));

    debug('Setting up Passport.js');

    app.use(express.urlencoded({ extended: true }));
    app.use(passport.initialize());
    app.use(passport.session());

    passport.serializeUser((user, done) => done(null, user));
    passport.deserializeUser((user: any, done) => done(null, user));

    passport.use(new google.Strategy({
        clientID     : opts.googleClientId,
        clientSecret : opts.googleClientSecret,
    }, (_accessToken, _refreshToken, profile, cb) => {
        const email = authorizedEmail(profile, admins);
        return cb(null, email ? { name: profile.displayName, email: email } : false);
    }));

    debug('Setting up authentication APIs for the frontend');

    const api = express.Router();

    api.get('/signin', (req, res, next) => {
        let state;

        // If we didn't receive a state query string parameter, the route is
        // being invoked from the frontend. In that case, build a state
        // parameter from the success and failure URLs provided by the frontend.
        //
        // If we get a state query string parameter, we're being called by
        // Google. In that case, decode the previously encoded state variable.

        if (typeof req.query.state === 'string') {
            state = JSON.parse(Buffer.from(req.query.state, 'base64').toString());
        } else {
            state = Buffer.from(JSON.stringify({
                success: typeof req.query.successRedirect === 'string' ? decodeURIComponent(req.query.successRedirect) : undefined,
                failure: typeof req.query.failureRedirect === 'string' ? decodeURIComponent(req.query.failureRedirect) : undefined
            })).toString('base64');
        }

        passport.authenticate('google', {
            scope: ['profile', 'email'],
            callbackURL: req.baseUrl + req.path, // Note: must match the route prefix and the callback URL configured in Google
            state: state
        } as any, (err, user) => {
            if (err) {
                next(err);
                return;
            }

            if (!user) {
                if (state.failure) res.redirect(state.failure);
                else res.sendStatus(401);
                return;
            }

            (req as any).logIn(user, (error) => {
                if (error) {
                    next(error);
                    return;
                }
                if (state.success) res.redirect(state.success);
                else res.send('Signed in');
            });
        })(req, res, next);
    });

    api.get('/signout', (req: any, res: any) => {
        req.logout();
        if (req.query.redirect) {
            res.redirect(decodeURIComponent(req.query.redirect));
        } else {
            res.send('Signed out');
        }
    });

    api.get('/profile', jsonify(req => req.user || {}));

    app.use('/auth', api);

    authEnabled = true;
}
