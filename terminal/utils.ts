import { promises as fs } from 'fs';
import { exec } from 'child_process';


export async function trim_file(filename, encoding: 'ascii' | 'utf8' = 'ascii') {
    return (await fs.readFile(filename, encoding)).toString().trim();
}


export function unquote(s) {
    if (s.charAt(0) === '"' && s.charAt(s.length - 1) === '"')
        return s.substr(1, s.length - 2);
    return s;
}


export function sudo(cmd) {
    return new Promise((resolve, reject) => {
        const c = process.getuid() === 0 ? cmd : `sudo sh -c "${cmd}"`;
        exec(c, (error: any, stdout, stderr) => {
            if (error === null) resolve([stdout, stderr]);
            else {
                // eslint-disable-next-line no-param-reassign
                error.stdout = stdout;
                // eslint-disable-next-line no-param-reassign
                error.stderr = stderr;
                reject(error);
            }
        });
    });
}
