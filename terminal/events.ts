import sqlite from 'better-sqlite3';
import { join } from 'path';
import express from 'express';
import { jsonify, NotFoundError } from '@janakj/lib/http';
import logger from './logger';

const debug = logger.extend('events');

const isoFormat = "%Y-%m-%dT%H:%M:%fZ";


function withoutNulls(o) {
    return Object.entries(o).reduce((a, [k, v]) => (v === null ? a : { ...a, [k]: v }), {});
}


function expandData(o) {
    return o.data ? { ...o, data: JSON.parse(o.data) } : o;
}


export function eventsAPI(events) {
    const api = express.Router();
    api.use(express.json());

    api.get('/:id', jsonify(({ params: { id } }) => {
        const event = events.findById(id);
        if (!event) throw new NotFoundError(`Unknown event id ${id}`);
        return event;
    }));

    api.get(/^\/$/, jsonify(({ query: { from, until } }) => events.find(until, from)));

    api.delete('/:id', jsonify(({ params: { id } }, res) => {
        events.deleteById(id);
        res.status(204).end();
    }));

    api.delete(/^\/$/, jsonify(({ query: { from, until } }, res) => {
        events.delete(until, from);
        res.status(204).end();
    }));

    return api;
}


export class Events {
    db_filename: string;
    _insert?: (v: any) => sqlite.RunResult;
    _selectById?: (id: string | number) => any;
    _select?: (...args: any[]) => any[];
    _deleteById?: (id: string | number) => sqlite.RunResult;
    _delete?: (...args: any[]) => sqlite.RunResult;
    db: any;

    constructor(args) {
        this.db_filename = join(args.data_dir, 'events.sqlite');
    }

    start() {
        debug(`Opening SQLite database ${this.db_filename}`);
        // eslint-disable-next-line new-cap
        const db = new sqlite(this.db_filename);

        db.pragma('journal_mode=WAL');

        debug(`Creating SQLite table 'events'`);
        db.exec(`CREATE TABLE IF NOT EXISTS events (                           \n\
            id        INTEGER PRIMARY KEY,                                     \n\
            timestamp TEXT NOT NULL DEFAULT (strftime('${isoFormat}', 'now')), \n\
            duration  REAL,                                                    \n\
            event     TEXT NOT NULL,                                           \n\
            data      BLOB,                                                    \n\
            media     TEXT)`);

        db.exec('CREATE INDEX IF NOT EXISTS idx ON events(timestamp)');

        let s = db.prepare(`INSERT INTO events        \
            (event, timestamp, duration, data, media) \
            VALUES (@event, @timestamp, @duration, @data, @media)`);
        this._insert = s.run.bind(s);

        s = db.prepare(`SELECT * FROM events where id=?`);
        this._selectById = s.get.bind(s);

        s = db.prepare("SELECT * FROM events WHERE timestamp>=@from AND timestamp<=@until");
        this._select = s.all.bind(s);

        s = db.prepare("DELETE FROM events where id=?");
        this._deleteById = s.run.bind(s);

        s = db.prepare("DELETE FROM events WHERE timestamp>=@from AND timestamp<=@until");
        this._delete = s.run.bind(s);

        this.db = db;
    }

    put(event, opts: any = {}) {
        return this._insert!({
            event,
            timestamp: opts.timestamp || new Date().toISOString(),
            duration: typeof opts.number !== 'undefined' ? Number(opts.number) : null,
            data: JSON.stringify(opts.data),
            media: opts.media
        }).lastInsertRowid;
    }

    findById(id) {
        const rv = this._selectById!(id);
        return rv ? expandData(withoutNulls(rv)) : rv;
    }

    find(until = 'a', from = '') {
        // We use the string 'a' as the default until value. Since timestamps are
        // stored in ISO format, the string 'a' will come after any valid ISO
        // timestamp in a lexicographical ordering.
        return this._select!({ from, until }).map(withoutNulls).map(expandData);
    }

    deleteById(id) {
        return this._deleteById!(id);
    }

    delete(until = 'a', from = '') {
        // We use the string 'a' as the default until value. Since timestamps are
        // stored in ISO format, the string 'a' will come after any valid ISO
        // timestamp in a lexicographical ordering.
        return this._delete!({ from, until });
    }

    stop() {
        if (!this.db) return;
        debug(`Closing SQLite database ${this.db_filename}`);
        this.db.close();
        delete this.db;
    }
}
