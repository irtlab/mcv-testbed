import ffi from 'ffi-napi';
import ref from 'ref-napi';
import logger from './logger';

const debug = logger.extend('alsa');

const snd_mixer_p           = ref.refType(ref.types.void),
    snd_mixer_pp          = ref.refType(snd_mixer_p),
    snd_mixer_selem_id_p  = ref.refType(ref.types.void),
    snd_mixer_selem_id_pp = ref.refType(snd_mixer_selem_id_p),
    snd_mixer_elem_p      = ref.refType(ref.types.void);

const libasound = ffi.Library('libasound.so.2', {
    'snd_mixer_open'                            : [ 'int'            , [ snd_mixer_pp, 'int'                 ]],
    'snd_mixer_attach'                          : [ 'int'            , [ snd_mixer_p, 'string'               ]],
    'snd_mixer_selem_register'                  : [ 'int'            , [ snd_mixer_p, 'void *', 'void *'     ]],
    'snd_mixer_load'                            : [ 'int'            , [ snd_mixer_p                         ]],
    'snd_mixer_selem_id_malloc'                 : [ 'int'            , [ snd_mixer_selem_id_pp               ]],
    'snd_mixer_selem_id_set_name'               : [ 'void'           , [ snd_mixer_selem_id_p, 'string'      ]],
    'snd_mixer_selem_id_free'                   : [ 'void'           , [ snd_mixer_selem_id_p                ]],
    'snd_mixer_find_selem'                      : [ snd_mixer_elem_p , [ snd_mixer_p, snd_mixer_selem_id_p   ]],
    'snd_mixer_selem_has_playback_volume'       : [ 'int'            , [ snd_mixer_elem_p                    ]],
    'snd_mixer_selem_get_playback_volume_range' : [ 'int'            , [ snd_mixer_elem_p, 'long *', 'long *']],
    'snd_mixer_selem_set_playback_volume_all'   : [ 'int'            , [ snd_mixer_elem_p, 'long'            ]],
    'snd_mixer_selem_has_playback_switch'       : [ 'int'            , [ snd_mixer_elem_p                    ]],
    'snd_mixer_selem_set_playback_switch_all'   : [ 'int'            , [ snd_mixer_elem_p, 'int'             ]],
    'snd_mixer_selem_has_capture_volume'        : [ 'int'            , [ snd_mixer_elem_p                    ]],
    'snd_mixer_selem_get_capture_volume_range'  : [ 'int'            , [ snd_mixer_elem_p, 'long *', 'long *']],
    'snd_mixer_selem_set_capture_volume_all'    : [ 'int'            , [ snd_mixer_elem_p, 'long'            ]],
    'snd_mixer_selem_has_capture_switch'        : [ 'int'            , [ snd_mixer_elem_p                    ]],
    'snd_mixer_selem_set_capture_switch_all'    : [ 'int'            , [ snd_mixer_elem_p, 'int'             ]],
    'snd_mixer_close'                           : [ 'void'           , [ snd_mixer_p                         ]]
});


export class Mixer {
    buf: any;
    handle: any;

    constructor(public card) {
        this.card = card;

        let rc;
        const buf = ref.alloc(snd_mixer_pp);

        rc = libasound.snd_mixer_open(buf, 0);
        if (rc) throw new Error(`Cannot open ALSA mixer: ${rc}`);
        const handle = buf.deref();
        try {
            rc = libasound.snd_mixer_attach(handle, card);
            if (rc) throw new Error(`[${card}] Cannot access ALSA mixer: ${rc}`);

            rc = libasound.snd_mixer_selem_register(handle, ref.NULL_POINTER, ref.NULL_POINTER);
            if (rc) throw new Error(`[${card}] Cannot initialize simple ALSA mixer: ${rc}`);

            rc = libasound.snd_mixer_load(handle);
            if (rc) throw new Error(`[${card}] Cannot load ALSA mixer: ${rc}`);

            this.buf = buf;
            this.handle = handle;
        } catch (e) {
            libasound.snd_mixer_close(handle);
            throw e;
        }
    }

    destroy() {
        if (this.handle) {
            libasound.snd_mixer_close(this.handle);
            delete this.handle;
        }
        if (this.buf) delete this.buf;
    }
}


export class MixerControl {
    id: string;
    _initialized: boolean;
    mixer?: Mixer | null;
    has_playback_switch?: boolean;
    has_playback_volume?: boolean;
    has_capture_switch?: boolean;
    has_capture_volume?: boolean;
    playback: any;
    capture: any;
    handle: any;
    playback_volume: number | undefined;
    capture_volume: number | undefined;
    playback_switch: boolean | undefined;
    capture_switch: boolean | undefined;

    constructor(public card, public name) {
        this.card = card;
        this.name = name;
        this.id = `${this.card}:${this.name}`;
        this._initialized = false;
        this._init();
    }

    _destroy() {
        if (this.mixer) this.mixer.destroy();
        this._initialized = false;
    }

    _init() {
        this.mixer = new Mixer(this.card);

        const buf = ref.alloc(snd_mixer_selem_id_pp);
        libasound.snd_mixer_selem_id_malloc(buf);
        const sid = buf.deref();

        try {
            libasound.snd_mixer_selem_id_set_name(sid, this.name);

            const handle = libasound.snd_mixer_find_selem(this.mixer.handle, sid);
            if (handle.isNull())
                throw new Error(`[${this.mixer.card}] Cannot find ALSA mixer control '${this.name}'`);

            this.has_playback_switch = libasound.snd_mixer_selem_has_playback_switch(handle) === 1;
            this.has_playback_volume = libasound.snd_mixer_selem_has_playback_volume(handle) === 1;

            this.has_capture_switch = libasound.snd_mixer_selem_has_capture_switch(handle) === 1;
            this.has_capture_volume = libasound.snd_mixer_selem_has_capture_volume(handle) === 1;

            let rc;
            const min = ref.alloc(ref.types.long),
                max = ref.alloc(ref.types.long);

            if (this.has_playback_volume) {
                rc = libasound.snd_mixer_selem_get_playback_volume_range(handle, min.ref(), max.ref());
                if (rc) throw new Error(`[${this.id}] Error while getting playback volume range: ${rc}`);
                this.playback = { min: min.deref(), max: max.deref() };
            }

            if (this.has_capture_volume) {
                rc = libasound.snd_mixer_selem_get_capture_volume_range(handle, min.ref(), max.ref());
                if (rc) throw new Error(`[${this.id}] Error while getting capture volume range: ${rc}`);
                this.capture = { min: min.deref(), max: max.deref() };
            }

            this.handle = handle;
            this._initialized = true;
        } catch (e) {
            this._destroy();
        } finally {
            libasound.snd_mixer_selem_id_free(sid);
        }
    }

    _reinit() {
        this._destroy();
        return this._init();
    }

    _restore() {
        if (typeof this.playback_volume !== 'undefined')
            this._set_playback_volume(this.playback_volume);

        if (typeof this.capture_volume !== 'undefined')
            this._set_capture_volume(this.capture_volume);

        if (typeof this.playback_switch !== 'undefined')
            this._switch_playback(this.playback_switch);

        if (typeof this.capture_switch !== 'undefined')
            this._switch_capture(this.capture_switch);
    }

    _reset() {
        debug(`[${this.id}] Attempting to re-initialize ALSA mixer`);
        this._reinit();

        try {
            debug(`[${this.id}] Restoring previous mixer state`);
            this._restore();
        } catch (e) {
            this._destroy();
            throw e;
        }
    }

    static _normalize_volume(val: number, range: { min: number, max: number }) {
        if (typeof val !== 'number')
            throw new Error(`Volume must be a number`);

        if (val < 0 || val > 100)
            throw new Error(`Volume must be in the range <0,100>`);

        return range.min + val / 100 * (range.max - range.min);
    }

    _retry_after_reset(fn, ...args) {
        try {
            return fn(...args);
        } catch (error) {
            debug(`${error}`);
            this._reset();
            try {
                return fn(...args);
            } catch (e) {
                this._destroy();
                throw e;
            }
        }
    }

    _set_playback_volume(val) {
        if (!this._initialized) this._reset();

        const v = MixerControl._normalize_volume(val, this.playback);
        const rc = libasound.snd_mixer_selem_set_playback_volume_all(this.handle, v);
        if (rc) throw new Error(`[${this.id}] Error while setting playback volume: ${rc}`);
        this.playback_volume = val;
    }

    set_playback_volume(val) {
        this._retry_after_reset(() => {
            debug(`[${this.id}] Setting playback volume to ${val}%`);
            this._set_playback_volume(val);
        });
    }


    _set_capture_volume(val) {
        if (!this._initialized) this._reset();
        const v = MixerControl._normalize_volume(val, this.capture);
        const rc = libasound.snd_mixer_selem_set_capture_volume_all(this.handle, v);
        if (rc) throw new Error(`[${this.id}] Error while setting capture volume: ${rc}`);
        this.capture_volume = val;
    }

    set_capture_volume(val) {
        this._retry_after_reset(() => {
            debug(`[${this.id}] Setting capture volume to ${val}%`);
            this._set_capture_volume(val);
        });
    }


    _switch_playback(onoff) {
        if (!this._initialized) this._reset();
        const rc = libasound.snd_mixer_selem_set_playback_switch_all(this.handle, onoff ? 1 : 0);
        if (rc) throw new Error(`[${this.id}] Error while toggling playback switch: ${rc}`);
        this.playback_switch = onoff;
    }

    switch_playback(onoff) {
        this._retry_after_reset(() => {
            debug(`[${this.id}] Turning playback switch ${onoff ? 'on' : 'off'}`);
            this._switch_playback(onoff);
        });
    }


    _switch_capture(onoff) {
        if (!this._initialized) this._reset();
        const rc = libasound.snd_mixer_selem_set_capture_switch_all(this.handle, onoff ? 1 : 0);
        if (rc) throw new Error(`[${this.id}] Error while toggling capture switch: ${rc}`);
        this.capture_switch = onoff;
    }

    switch_capture(onoff) {
        this._retry_after_reset(() => {
            debug(`[${this.id}] Turning capture switch ${onoff ? 'on' : 'off'}`);
            this._switch_capture(onoff);
        });
    }
}
