import http from 'http';
import https from 'https';
import path from 'path';
import { Readable } from 'stream';
import xattr from 'fs-xattr';
import tmp from 'tmp-promise';
import Lock from '@janakj/lib/mutex';
import { mkdirSync, createReadStream, createWriteStream, promises } from 'fs';
import { URL } from 'url';
import logger from './logger';

const debug = logger.extend('cache');

const { unlink, rename } = promises;

export class FileCache {
    connectionTimeout: number;
    lock: Lock;
    request: typeof https.request | typeof http.request;
    headers: any;

    constructor(public baseURL, public cacheDir, opts: any = {}) {
        this.cacheDir = cacheDir;
        this.connectionTimeout = opts.connectionTimeout || 1000;
        this.headers = {};

        // Parse the given URL for the query string parameter 'host'. If it has been
        // set, use the value to override request Host header. THis is useful if
        // HTTPS is used as it determines the name the client will expect to receive
        // in the TLS server certificate. If the host query string parameter was
        // found, remove it from the URL so that we don't send it to the server
        // (it's meant to be a client-only parameter).

        const url = new URL(baseURL);
        this.request = url.protocol === 'https:' ? https.request : http.request;

        const host = url.searchParams.get('host');
        if (host) {
            this.headers.host = host;
            url.searchParams.delete('host');
        }
        this.baseURL = url.toString();

        debug(`baseURL=${this.baseURL} cacheDir=${cacheDir}`);
        this.lock = new Lock();

        try {
            debug(`Creating cache directory '${cacheDir}'`);
            mkdirSync(cacheDir, { recursive: true });
        } catch (error: any) {
            if (error.code !== 'EEXIST') throw error;
        }
    }

    filename(name) {
        return `${this.cacheDir}/${name}`;
    }

    static storeHeaders(filename, headers) {
        const p: Promise<any>[] = [];

        for (const name of ['cache-control', 'last-modified', 'etag']) {
            if (typeof headers[name] === 'string') {
                p.push(xattr.set(filename, `user.${name}`, headers[name]));
            } else {
                p.push(xattr.remove(filename, `user.${name}`).catch(error => {
                    if (error.code !== 'ENODATA') throw error;
                }));
            }
        }

        return Promise.all(p);
    }

    async loadHeaders(filename) {
        const headers: { [name: string]: string } = {};

        try {
            await this.lock.acquire();
            await Promise.all(['cache-control', 'last-modified', 'etag'].map(name => {
                return xattr.get(filename, `user.${name}`)
                    .then(v => {
                        headers[name] = v.toString();
                    })
                    .catch(error => {
                        if (error.code !== 'ENODATA') throw error;
                    });
            }));
            return headers;
        } finally {
            this.lock.release();
        }
    }

    async delete(filename) {
        try {
            try {
                await this.lock.acquire();
                await unlink(filename);
            } finally {
                this.lock.release();
            }
        } catch (error: any) {
            if (error.code !== 'ENOENT') throw error;
        }
    }

    async cacheResponse(response, filename) {
        const pathname = await tmp.tmpName({
            mode: 0o0644,
            dir: this.cacheDir,
            prefix: '.',
            postfix: '.partial'
        });

        debug(`Caching response to file '${pathname}'`);
        const file = createWriteStream(pathname);

        // Writing data to a file from the 'data' event handler does not apply
        // back-pressure to the readable stream. Unfortunately, we cannot simply
        // pipe the response to the file here, because the application, which will
        // pipe the stream later, might lose data that way. We assume that the
        // application (consumer) will be slower than writes to the local filesystem
        // and hence that there will be no need to apply back-pressure here. Any
        // back-pressure applied by the application will be sufficient. This is
        // usually the case for media files, which are streamed to a sound card, for
        // example.
        const save = data => { file.write(data); };
        response.on('data', save);
        response.pause();

        response.once('aborted', async () => {
            debug(`HTTP connection aborted, removing partial file '${pathname}'`);
            response.off('data', save);
            response.unpipe(file);
            file.destroy();
            await this.delete(pathname);
        });

        response.once('end', async () => {
            response.off('data', save);
            try {
                try {
                    await this.lock.acquire();
                    await FileCache.storeHeaders(pathname, response.headers);
                    debug(`Renaming '${pathname}' to '${filename}'`);
                    await rename(pathname, filename);
                } finally {
                    this.lock.release();
                }
            } catch (error: any) {
                debug(`Error caching '${pathname}': ${error.message}`);
                await this.delete(pathname);
            }
        });
    }

    async get(name): Promise<Readable | null> {
        const filename = this.filename(name);
        const headers = { ...this.headers };

        try {
            const stored = await this.loadHeaders(filename);
            if ('last-modified' in stored)
                headers['if-modified-since'] = stored['last-modified'];

            if ('etag' in stored)
                headers['if-none-match'] = stored.etag;
        } catch (error: any) {
            // Include no headers in the HTTP request if the cached file does not
            // exist
            if (error.code !== 'ENOENT') throw error;
        }

        const url = new URL(this.baseURL);
        url.pathname = path.normalize(`${url.pathname}/${name}`);

        debug(`Sending GET for ${url.toString()}`);
        const req = this.request(url, {
            method: 'GET',
            headers,
            timeout: this.connectionTimeout
        });

        const processResponse = async (response) => {
            // The library emits 'close' on the response object if the connection is
            // terminated prematurely after the response has been sent, but before the
            // whole body has been sent. Emit error on the response in that case to
            // indicate an error to the consumer of the stream in case it is not
            // listening for 'aborted'.
            response.once('aborted', error => response.emit('error', error));
            const code = response.statusCode;

            if (code === 304) {
                // If the cached file does not exist, e.g., because it was deleted
                // between we sent GET and received 304, throw an error and let the
                // application recover, e.g., by re-trying the request.
                debug(`Serving request for '${name}' from cache`);
                try {
                    await this.lock.acquire();
                    await FileCache.storeHeaders(filename, response.headers);
                    return createReadStream(filename);
                } finally {
                    this.lock.release();
                }
            }

            if (code === 404) {
                debug(`File ${name} not found on server, deleting cached copy`);
                await this.delete(filename);
                return null;
            }

            if (code < 200 || code > 299)
                throw new Error(response.statusMessage);

            const cc = response.headers['cache-control'];
            if (!cc || (typeof cc === 'string' && cc.includes('no-store'))) {
                debug(`Not caching: missing or no-store ache-Control header`);
                await this.delete(filename);
                return response;
            }

            await this.cacheResponse(response, filename);
            return response;
        };

        return new Promise((resolve, reject) => {
            // The following handler will be invoked when the HTTP client fails to
            // establish a connection, or when the connection is aborted before a
            // response is received.
            req.once('error', reject);
            req.once('response', response => {
                req.off('error', reject);
                resolve(processResponse(response));
            });
            req.end();
        });
    }
}
