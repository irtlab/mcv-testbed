import { join } from 'path';
import { shortMachineId } from '@janakj/lib/machine';
import { PA_VOLUME_NORM } from '@mcv/common/volume';

// This file gathers in one place various constants and values needed across the
// source code.

// The following variables are constants that cannot be changed from the command
// line, environment, or settings. The only way to change those values is by
// changing this file.

export const ifb_interface_name         = 'ifb0';
export const baresip_restart_interval   = 3000;
export const baresip_terminate_interval = 3000;
export const evdev_restart_interval     = 5000;
export const dnssd_service_type         = '_ut._tcp';
export const call_restart_interval      = 3000;
export const invite_timeout             = 10000;
export const input_fs_root              = '/dev/input';
export const max_ttl                    = 75 * 60;
export const supported_codecs           = {
    'L16'         : ['l16'   , 'l16',    10, 60],
    'G.711'       : ['g711'  , 'g711',   10, 60],
    'GSM'         : ['gsm'   , 'gsm',    20, 20],
    'AMR'         : ['amr'   , 'amr',    20, 20],
    'Opus'        : ['opus'  , 'opus',   10, 60],
    'CODEC2'      : ['codec2', 'codec2', 20, 20],
    'P.25 Phase 1': ['p25p1' , 'ambe',   20, 20, { 'ambe_rate': '0x0558,0x086b,0x1030,0x0000,0x0000,0x0190' }],
    'P.25 Phase 2': ['p25p2' , 'ambe',   20, 20, { 'ambe_rate': 33 }]
};


// The following variables represent default values for various command line
// arguments and environment variables. Changing one of the variables requires a
// restart of the application.

export async function default_args() {
    return {
        port              : 'random',
        data_dir          : join('.ut', await shortMachineId()),
        media_server      : 'https://bs/api/media?host=mcv-testbed.cs.columbia.edu',
        wav_dir           : 'wavs',
        sysrq             : '/proc/sysrq-trigger',
        led               : '/run/ledd/socket',
        indicator_color   : 'black',
        ptt_color         : 'white',
        ttl               : 15,
        ptt_evdev_device  : 'by-id/usb-Logitech_Logitech_USB_Headset-event-if03',
        ptt_evdev_type    : 'EV_KEY',
        ptt_evdev_code    : 'KEY_VOLUMEUP',
        ifname            : 'mcv',
        baresip_ctrl_port : 'random',
        no_kmod           : false,
        mode              : 'PushToTalk',
        channels          : 1,
        speaker           : null,
        virtual_mic_name  : 'ut-mic'
    };
}


// The following variables represent default values for settings that can be
// changed at runtime via the HTTP API.

export const default_settings = {
    '*': {
        peer           : null,
        codec          : 'g711',
        ptt_delay      : 0,
        packet_time    : 10,
        mic            : null,
        mic_volume     : PA_VOLUME_NORM,
        speaker_volume : PA_VOLUME_NORM,
        noise          : null,
        noise_volume   : PA_VOLUME_NORM,
        ptt_beep       : null,
        ptt_volume     : PA_VOLUME_NORM,
        tx_tc_filter   : 'u32 match ip dsfield 0xb8 0xff',
        rx_tc_filter   : 'u32 match ip dsfield 0xb8 0xff',
        tx_rate        : null,
        rx_rate        : null,
        tx_delay       : null,
        rx_delay       : null,
        tx_loss        : null,
        rx_loss        : null,
        tx_error       : null,
        rx_error       : null
    },
    '92cb01': {
        uri  : 'sip:ut@ut1.mcv'
    },
    '5dd34e': {
        uri : 'sip:ut@ut2.mcv'
    },
    '9de142': {
        uri : 'sip:ut@ut3.mcv'
    }
};
