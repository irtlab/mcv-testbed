import { createServer } from 'http';
import path from 'path';
import cors from 'cors';
import morgan from 'morgan';
import express from 'express';
import socket_io from 'socket.io';
import repl from 'repl';
import { publishService, avahiHostname } from '@mcv/common/zeroconf';
import { machineId, shortMachineId } from '@janakj/lib/machine';
import abort from '@janakj/lib/abort';
import { initPublisher } from '@mcv/common/pubsub/publisher';

import load_args from './args';
import { init_settings } from './settings';
import { dnssd_service_type } from './defaults';
import api from './api';
import { timesync_subscribe, timesync_unsubscribe, timesync_status } from './timesync';
import { StatusIndicator } from './indicator';
import Audio, { audioAPI } from './audio';
import { FileCache } from './cache';
import { Events, eventsAPI } from './events';
import mode from './mode';
import logger from './logger';

declare global {
    const devMode: boolean;
}

const debug = logger.extend('main');
const devMode = process.env.NODE_ENV === "development";
(global as any).devMode = devMode;


const start_server = (server, port) => new Promise((resolve, reject) => {
    server.once('error', reject);
    server.listen(port, () => resolve(server.address()));
});


(async function () {
    debug(`Starting in ${devMode ? 'development' : 'production'} mode`);
    const args = await load_args();

    debug(`Initializing event subsystem`);
    const events = new Events(args);
    events.start();

    // Load the operational mode class based on the argument 'mode'. The argument
    // allows the user to specify whether the user terminal should be running in
    // PTT or two-way call mode.

    const OperationalMode = mode[args.mode];
    if (typeof OperationalMode === 'undefined')
        abort(new Error(`Unsupported mode of operation '${args.mode}'`));

    const settings_filename = path.join(args.data_dir, 'settings.json'),
        settings = await init_settings(settings_filename);

    const app = express();
    app.use(cors());
    app.use(morgan(devMode ? 'dev' : 'combined'));
    const http_server = createServer(app);

    debug(`Setting up socket.io WebSocket server`);
    const ws_server = new socket_io.Server(http_server);
    app.set('socketio_server', ws_server);

    debug('Initializing WebSocket publish-subscribe subsystem');
    const publisher = initPublisher(ws_server);

    debug(`Initializing status indicator LED`);
    const indicator = new StatusIndicator(args.led);

    publisher.indicator = notify => {
        const cb = v => notify('change', v);
        indicator.on('change', cb);
        return [
            () => indicator.off('change', cb),
            indicator.current()
        ];
    };

    debug(`Initializing media cache`);
    const media = new FileCache(args.media_server, args.wav_dir);

    debug(`Initializing audio processing layer`);
    const audio = new Audio(media, args);
    await audio.start(settings);

    debug(`Creating an instance of the ${args.mode} main terminal class`);
    const ut = new OperationalMode(args, audio, indicator, ws_server);
    function apply_settings() {
        return Promise.all([
            audio.reconfigure(settings),
            ut.reconfigure(settings)
        ]);
    }

    publisher.state = notify => {
        const on_call = s => notify('change', { call: s }),
            on_ptt = s => notify('change', { ptt: s });
        ut.on('call', on_call);
        ut.on('ptt', on_ptt);
        return [() => {
            ut.off('call', on_call);
            ut.off('ptt', on_ptt);
        }, ut.state];
    };

    app.use('/api/events', eventsAPI(events));
    app.use('/api', await api(args, settings_filename, settings, apply_settings));
    app.use('/audio', audioAPI(args, audio));

    debug(`Creating timesync publisher`);
    publisher.timesync = notify => {
        const cb = v => notify('change', v),
            handle = timesync_subscribe(cb).catch(error => {
                // Since we cannot terminate the subscription on server side
                // asynchronously, all we can do is log an error. We also re-throw the
                // error to report unhandled promise rejection.
                debug(`Timesync subscription failed: ${error.message}`);
                throw error;
            });

        // Wait for the handle promise to settle before returning the initial value.
        // That way, if we fail to create DBus subscription, the error will
        // propagate to the initial value and the pubsub server will report to the
        // client that the subscription request failed. It will also invoke the
        // unsubscribe method which in this case should not do anything, because the
        // DBus subscription was not created.
        return [
            async () => {
                let h;
                try {
                    h = await handle;
                } catch (error) {
                    // Do nothing if DBus subscription failed. This error was already
                    // reported by the client via the initial value.
                    return undefined;
                }
                return timesync_unsubscribe(h);
            },
            handle.then(timesync_status)
        ];
    };

    const api_ready = (async function () {
        debug(`Starting HTTP API server`);
        const addr: any = await start_server(http_server, args.port);
        debug(`HTTP API server is listening on port ${addr.port}`);
        return addr;
    }());

    await Promise.all([indicator.start(), api_ready, ut.start(settings)]);
    debug('Initialization is complete');
    indicator.enqueue(0, args.indicator_color);

    // Publish the terminal via ZeroConf only after all initialization completed.
    // That ensures that when a client makes a request right away, it won't reach
    // a half-initialized terminal with only a portion of the api available.

    (async function () {
        debug('Announcing the terminal via ZeroConf');
        const name = `User-Terminal-[${await avahiHostname()}-${await shortMachineId()}]`;
        await publishService(name, dnssd_service_type, (await api_ready).port, {
            ttl: args.ttl,
            txt: { 'machine_id': await machineId() }
        });
        debug(`ZeroConf service published under name '${name}'`);
    })().catch(abort);

    let count = 0, return_code;
    const exit = async (sig_name?, sig_num?) => {
        count++;

        if (sig_name) debug(`Got ${sig_name}`);
        return_code = (typeof sig_num === 'number' && sig_num !== 0) ? 128 + sig_num : 0;

        if (count === 1) {
            // On the first invocation attempt to shutdown everything cleanly
            // including the Baresip subprocess. The code may block in ut.stop() until
            // Baresip subprocess terminates, which can take a while.

            debug(`Terminating gracefully`);
            await ut.stop();
            await audio.stop();
            indicator.stop();
            events.stop();
            debug('All subsystems stopped, exiting');
            process.exit(return_code);
        } else if ((count === 2) && ut.baresip && ut.baresip.child) {

            // Baresip can take a while to shutdown, especially if there is a call or
            // registration in progress. If the user is inpatient and presses CTRL-C
            // repeatedly, kill the Baresip subprocess with SIGKILL to terminate the
            // program immediately.

            ut.baresip.kill('SIGKILL');
        } else if (count >= 3) {
            debug('Killing myself with SIGKILL');
            process.kill(process.pid, 'SIGKILL');
        }
    };

    // eslint-disable-next-line @typescript-eslint/no-misused-promises
    process.on('SIGTERM', () => exit());
    // eslint-disable-next-line @typescript-eslint/no-misused-promises
    process.on('SIGHUP', () => exit());
    // eslint-disable-next-line @typescript-eslint/no-misused-promises
    process.on('SIGINT', () => exit());

    // In development mode, start an interactive REPL and export selected
    // variables, e.g., the settings to it. When the REPL terminates the entire
    // application shuts down.

    if (devMode) {
        debug('Development mode detected, starting interactive REPL');
        const loop = repl.start({ prompt: `ut[${(await avahiHostname())}@${await shortMachineId()}]> `, breakEvalOnSigint: true });
        Object.assign(loop.context, { args, settings, apply_settings, ut });
        // eslint-disable-next-line @typescript-eslint/no-misused-promises
        loop.on('exit', () => exit(null, 0));
    }

}()).catch(abort);
