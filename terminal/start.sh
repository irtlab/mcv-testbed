#!/bin/bash
set -euo pipefail

if [ $(whoami) = "root" ] ; then
    echo "Do not run this as root"
    exit 1
fi

if [ $(ulimit -r) != "99" ] ; then
    echo "You do not have RTLIMIT_RTPRIO."
    echo "Please add '$(whoami) - rtprio 99' to /etc/security/limits.conf"
    exit 1
fi

if [ $(ulimit -e) != "40" ] ; then
    echo "You do not have RTLIMIT_NICE."
    echo "Please add '$(whoami) - nice -20' to /etc/security/limits.conf"
    exit 1
fi

if [ -z "${XDG_RUNTIME_DIR}" ] ; then
    echo "Environment variable XDG_RUNTIME_DIR is not set"
    echo "Baresip will not be able to connect to PulseAudio"
    echo "Add 'export XDG_RUNTIME_DIR=/run/user/${UID}' to ~/.profile"
    exit 1
fi

cd $(dirname $0)
exec npm run dev -- -d /srv/mcv -c /srv/mcv/config.json $@
