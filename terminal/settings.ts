import { promises as fs } from 'fs';
import { EventEmitter } from 'events';
import { parseNumber } from '@janakj/lib/parse';
import { machineId } from '@janakj/lib/machine';
import { PA_VOLUME_MUTED, PA_VOLUME_MAX } from '@mcv/common/volume';
import { atomicWrite } from '@janakj/lib/fs';
import { default_settings, supported_codecs } from './defaults';
import logger from './logger';

const debug = logger.extend('settings');


export const supported_settings = {
    peer: v => {
        if (v !== null && typeof v !== 'string')
            throw new Error(`Peer URI must be string or null`);
        return v;
    },
    uri: v => {
        if (v !== null && typeof v !== 'string')
            throw new Error(`SIP URI must be string or null`);
        return v;
    },
    codec: {
        values: supported_codecs,
        validator: v => {
            const supported = Object.values(supported_codecs).map(s => s[0]);
            if (!supported.includes(v))
                throw new Error(`Unsupported CODEC '${v}'`);
            return v;
        }
    },
    ptt_delay: v => parseNumber(v, 0),
    packet_time: v => parseNumber(v, 10, 60),
    mic_volume: v => parseNumber(v, PA_VOLUME_MUTED, PA_VOLUME_MAX),
    mic: v => {
        if (v !== null && typeof v !== 'string')
            throw new Error(`Microphone setting must be string or null`);
        return v;
    },
    speaker_volume: v => parseNumber(v, PA_VOLUME_MUTED, PA_VOLUME_MAX),
    noise: v => {
        if (v !== null && typeof v !== 'string')
            throw new Error(`Background noise must be string or null`);
        return v;
    },
    noise_volume: v => parseNumber(v, PA_VOLUME_MUTED, PA_VOLUME_MAX),
    ptt_beep: v => {
        if (v !== null && typeof v !== 'string')
            throw new Error(`PTT beep must be string or null`);
        return v;
    },
    ptt_volume: v => parseNumber(v, PA_VOLUME_MUTED, PA_VOLUME_MAX),
    tx_tc_filter: v => {
        if (v !== null && typeof v !== 'string')
            throw new Error('Egress traffic control filter must be a string');
        return v;
    },
    rx_tc_filter: v => {
        if (v !== null && typeof v !== 'string')
            throw new Error('Ingress traffic control filter must be a string');
        return v;
    },
    tx_rate: v => {
        if (v !== null && typeof v !== 'string')
            throw new Error('Egress traffic rate must be a string (Linux netem rate parameter)');
        return v;
    },
    rx_rate: v => {
        if (v !== null && typeof v !== 'string')
            throw new Error('Ingress traffic rate must be a string (Linux netem rate parameter)');
        return v;
    },
    tx_delay : v => v === null ? v : parseNumber(v, 0),
    rx_delay : v => v === null ? v : parseNumber(v, 0),
    tx_loss  : v => v === null ? v : parseNumber(v, 0, 100),
    rx_loss  : v => v === null ? v : parseNumber(v, 0, 100),
    tx_error : v => v === null ? v : parseNumber(v, 0, 100),
    rx_error : v => v === null ? v : parseNumber(v, 0, 100)
};


const emitter = new EventEmitter();

// First check whether the property exists on the emitter object. If it does,
// return the property from the emitter object, otherwise return the property
// from the settings object. This allows invoking methods such as "on" and "off"
// directly on the settings object.
//
// When returning EventEmitter's method, bind it to the emitter object first to
// make its this points to the emitter object and not to the settings object.

const handler = {
    get: (obj, prop) => {
        if (prop in emitter)
            return typeof emitter[prop] === 'function' ?
                emitter[prop].bind(emitter) : emitter[prop];
        return obj[prop];
    }
};

const settings = new Proxy(Object.create(null), handler);


export async function load_defaults() {
    const data = JSON.parse(JSON.stringify(default_settings['*']));
    const mid = await machineId();
    for (const key of Object.keys(default_settings)) {
        if (!mid.startsWith(key)) continue;
        Object.assign(data, JSON.parse(JSON.stringify(default_settings[key])));
        break;
    }
    return data;
}


export function save_settings(filename, data) {
    debug(`Writing settings to file '${filename}'`);
    return atomicWrite(filename, JSON.stringify(data, null, 4));
}


export function update_settings(data) {
    Object.assign(settings, data);
}


function clear_settings() {
    Object.keys(settings).forEach(k => delete settings[k]);
}


function replace_settings(data) {
    clear_settings();
    update_settings(data);
}


export async function delete_settings_file(filename) {
    debug(`Deleting settings file '${filename}'`);
    try {
        await fs.unlink(filename);
    } catch (e: any) {
        if (e.code !== 'ENOENT') throw e;
    }
}


export async function restore_default_settings() {
    debug(`Restoring default settings`);
    replace_settings(await load_defaults());
}


export function check_settings(data, ignore_unknown = false) {
    const rv = {};

    // First check that all attribute names refer to known settings. Then check
    // that each setting value is a primitive value and not, e.g., an object,
    // function, or an array. We only support primitive setting values. Finally,
    // check and covert the value using the conversion function provided by
    // supported_settings values.

    Object.keys(data).forEach(name => {
        if (!ignore_unknown && !(name in supported_settings))
            throw new Error(`Unsupported setting '${name}'`);

        const type = typeof data[name];
        switch (type) {
            case 'object':
            case 'function':
            case 'symbol':
                if (data[name] !== null)
                    throw new Error(`Setting ${name} has unsupported value type: ${type}`);
                break;

            default:
                break;
        }

        try {
            const s = supported_settings[name],
                validator = typeof s === 'function' ? s : s.validator;
            rv[name] = validator(data[name]);
        } catch (e: any) {
            if (name in supported_settings)
                throw new Error(`Invalid ${name} value: ${e.message}`);
        }
    });

    return rv;
}


async function load_settings(filename) {
    debug(`Loading settings from file '${filename}'`);
    try {
        return JSON.parse(await fs.readFile(filename, 'utf-8'));
    } catch (e: any) {
        if (e.code !== 'ENOENT') throw e;
        return {};
    }
}


export async function init_settings(filename) {
    const defaults = await load_defaults();
    const saved = check_settings(await load_settings(filename));
    replace_settings(defaults);
    update_settings(saved);
    return settings;
}
