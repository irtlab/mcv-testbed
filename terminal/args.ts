import stdio from 'stdio';
import path from 'path';
import { promises as fs } from 'fs';
import { parseNumber } from '@janakj/lib/parse';
import { randomInt } from '@janakj/lib/random';
import { default_args, max_ttl, input_fs_root } from './defaults';
import logger from './logger';

const debug = logger.extend('args');

const data_dir_root = process.env.HOME || '/';


function parse_cmdline_args() {
    debug(`Parsing command line arguments`);

    const args = stdio.getopt({
        baresip_ctrl_port: {
            key: 'b',
            args: 1,
            description: 'Baresip control port number',
        },
        config: {
            key: 'c',
            args: 1,
            description: 'Path to the configuration file',
        },
        ptt_color: {
            key: 'C',
            args: 1,
            description: 'LED indicator color when PTT button is pressed'
        },
        data_dir: {
            key: 'd',
            args: 1,
            description: `Directory for runtime data (related to ${data_dir_root} if not absolute)`
        },
        ifname: {
            key: 'i',
            args: 1,
            description: 'Network interface for SIP, RTP, and traffic control'
        },
        indicator_color: {
            key: 'I',
            args: 1,
            description: 'LED indicator color when idle'
        },
        no_kmod: {
            key: 'k',
            args: 0,
            description: 'Skip loading the ifb Linux kernel module'
        },
        port: {
            key: 'l',
            args: 1,
            description: 'Port number to listen on for HTTP requests'
        },
        led: {
            key: 'L',
            args: 1,
            description: 'UNIX domain socket of the RGB LED daemon'
        },
        media_server: {
            key: 'm',
            args: 1,
            description: 'Base HTTP URI of the media server'
        },
        mode: {
            key: 'M',
            args: 1,
            description: "Mode of operation, either 'PushToTalk' or 'TwoWayCall'"
        },
        channels: {
            key: 'n',
            args: 1,
            description: 'Number of audio channels (1 or 2)'
        },
        ptt_evdev_device: {
            key: 'p',
            args: 1,
            description: `Input device to poll for PTT (relative to '${input_fs_root}' if not absolute)`
        },
        ptt_evdev_code: {
            key: 'P',
            args: 1,
            description: 'Linux input subsystem event code to minitor for PTT'
        },
        sysrq: {
            key: 'r',
            args: 1,
            description: 'Path to the sysrq-trigger file'
        },
        speaker: {
            key: 's',
            args: 1,
            description: 'PulseAudio sink name to use as speaker'
        },
        ttl: {
            key: 't',
            args: 1,
            description: 'TTL value for ZeroConf service records'
        },
        ptt_evdev_type: {
            key: 'T',
            args: 1,
            description: 'Linux input subsystem event type to monitor for PTT'
        },
        virtual_mic_name: {
            key: 'v',
            args: 1,
            description: 'Name of virtual PulseAudio mic device'
        },
        wav_dir: {
            key: 'w',
            args: 1,
            description: 'Directory for downloaded background noise files, (relative to runtime data directory if not absolute)',
        }
    });

    return args;
}


async function load_config(filename) {
    debug(`Loading configuration file '${filename}'`);
    try {
        return JSON.parse(await fs.readFile(filename, 'utf-8'));
    } catch (e: any) {
        if (e.code !== 'ENOENT') throw e;
        return {};
    }
}


function env_args(names: string[]) {
    const rv = {};
    names.forEach(name => {
        const v = process.env[name.toUpperCase()];
        if (typeof v === 'string') rv[name] = v;
    });
    return rv;
}


function str2bool(str) {
    switch (str.toLowerCase().trim()) {
        case 'true':
        case 'yes':
        case '1':
            return true;

        case 'false':
        case 'no':
        case '0':
        case null:
            return false;

        default:
            throw new Error(`Cannot parse boolean value "${str}"`);
    }
}


export default async function load_args() {
    const defaults = await default_args();
    const cmdline = parse_cmdline_args();

    // To know which environment variable names to retrieve, get the list of
    // arguments that have a default value and add 'config' to the list (which
    // does not have a default value)
    const names = Object.keys(defaults);
    names.push('config');
    const env: any = env_args(names);

    const config = cmdline && cmdline.config || env.config;
    const stored = config ? await load_config(config) : {};

    const args = { ...defaults, ...stored, ...env, ...cmdline };

    args.data_dir = path.resolve(data_dir_root, args.data_dir);
    args.wav_dir = path.resolve(args.data_dir, args.wav_dir);
    args.sysrq = path.resolve('/', args.sysrq);
    args.ptt_evdev_device = path.resolve(input_fs_root, args.ptt_evdev_device);

    if (args.port === 'random') {
        args.port = 0;
    } else {
        try {
            args.port = parseNumber(args.port, 1, 65535);
        } catch (e: any) {
            throw new Error(`Invalid port number: ${e.message}`);
        }
    }

    try {
        args.ttl = parseNumber(args.ttl, 1, max_ttl);
    } catch (e: any) {
        throw new Error(`Invalid TTL value: ${e.message}`);
    }

    if (args.baresip_ctrl_port === 'random')
        args.baresip_ctrl_port = randomInt(1024, 65536);

    try {
        args.baresip_ctrl_port = parseNumber(args.baresip_ctrl_port, 1, 65535);
    } catch (e: any) {
        throw new Error(`Invalid Baresip control port number: ${e.message}`);
    }

    if (typeof args.no_kmod === 'string')
        args.no_kmod = str2bool(args.no_kmod);

    try {
        args.channels = parseNumber(args.channels, 1, 2);
    } catch (e: any) {
        throw new Error(`Invalid number of channels: ${e.message}`);
    }

    return args;
}
