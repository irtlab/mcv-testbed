import AudioRouter from './audio';
import logger from './logger';

const debug = logger.extend('spkrmic');

export default class SpeakerMic {
    audio: AudioRouter;

    constructor(audio) {
        this.audio = audio;
    }

    switch(state) {
        debug(`Setting speaker ${state ? 'off' : 'on'}, mic ${state ? 'on' : 'off'}`);

        return Promise.all([
            this.audio.muteSpeaker(!!state),
            this.audio.muteNoise(!state),
            this.audio.muteMic(!state)
        ]);
    }
}
