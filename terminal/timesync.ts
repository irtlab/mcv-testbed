import dbus from 'dbus-next';

const name_prefix = 'org.freedesktop',
    path_prefix = '/org/freedesktop';


let timesync_connection;


async function connect_timesync() {
    const sysbus = dbus.systemBus(),
        timesync = await sysbus.getProxyObject(`${name_prefix}.timesync1`, `${path_prefix}/timesync1`),
        properties = timesync.getInterface(`${name_prefix}.DBus.Properties`);

    return { sysbus, timesync, properties };
}


export async function timesync_status() {
    if (typeof timesync_connection === 'undefined')
        timesync_connection = connect_timesync();

    const { properties } = await timesync_connection;
    const props = await properties.GetAll(`${name_prefix}.timesync1.Manager`);

    //
    // NTPMessage: Variant {
    //   signature: '(uuuuittayttttbtt)',
    //   value: [
    //     0, 4, 4, 5, -24, 0n, 0n, [Array],
    //     1574806146399669n, 1574806146400202n,
    //     1574806146400322n, 1574806146400921n,
    //     false, 223n, 269n
    //   ]
    // }
    //
    // The following comments and code are taken from systemd's timedatectl
    //
    // "Timestamp Name          ID   When Generated
    //  ------------------------------------------------------------
    //  Originate Timestamp     T1   time request sent by client
    //  Receive Timestamp       T2   time request received by server
    //  Transmit Timestamp      T3   time reply sent by server
    //  Destination Timestamp   T4   time reply received by client
    //
    //  The round-trip delay, d, and system clock offset, t, are defined as:
    //  d = (T4 - T1) - (T3 - T2)     t = ((T2 - T1) + (T3 - T4)) / 2"
    //

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const [T1, T2, T3, T4, spike, packet_count] = props.NTPMessage.value.slice(8, 8 + 6);

    const T14: bigint = <bigint>T1 + <bigint>T4,
        T23: bigint = <bigint>T2 + <bigint>T3,
        sign = T14 < T23,
        offset = Number((sign ? T23 - T14 : T14 - T23) / BigInt(2)) / 1000000;

    // Return offset only if we can calculate it, i.e., we have at least one
    // message from the NTP server. If we have no NTP message, return undefined as
    // offset to indicate to the caller that the offset is unknown.

    return {
        server: props.ServerName.value,
        offset: Number(packet_count) !== 0 ? offset : undefined
    };
}


export async function timesync_subscribe(cb) {
    if (typeof timesync_connection === 'undefined')
        timesync_connection = connect_timesync();

    const { properties } = await timesync_connection;

    const handle = async (_iface, changed) => {
        if (`NTPMessage` in changed) cb(await timesync_status());
    };

    properties.on('PropertiesChanged', handle);
    return handle;
}

export async function timesync_unsubscribe(handle) {
    if (typeof timesync_connection === 'undefined')
        timesync_connection = connect_timesync();

    const { properties } = await timesync_connection;

    properties.off('PropertiesChanged', handle);
}