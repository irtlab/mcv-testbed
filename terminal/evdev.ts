/* eslint-disable no-unused-vars */
/* eslint-disable no-bitwise */

import ffi from 'ffi-napi';
import ref from 'ref-napi';
import { promises, constants } from 'fs';
import util from 'util';
import { EventEmitter } from 'events';
import epoll from 'epoll';
import ref_struct from 'ref-struct-di';
import abort from '@janakj/lib/abort';
import sleep from '@janakj/lib/sleep';
import { evdev_restart_interval } from './defaults';
import logger from './logger';


const debug = logger.extend('evdev');


const struct = ref_struct(ref);

const time_t      = ref.types.long,
    suseconds_t = ref.types.long;

const timeval = struct({
    tv_sec: time_t,
    tv_usec: suseconds_t
});

const InputEvent = struct({
    time: timeval,
    type: ref.types.uint16,
    code: ref.types.uint16,
    value: ref.types.int32
});

const libevdev_p  = ref.refType(ref.types.void),
    libevdev_pp   = ref.refType(libevdev_p),
    input_event_p = ref.refType(InputEvent);

const LIBEVDEV_READ_FLAG_SYNC     = 1, // Process data in sync mode
    LIBEVDEV_READ_FLAG_NORMAL     = 2, // Process data in normal mode
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    LIBEVDEV_READ_FLAG_FORCE_SYNC = 4, // Pretend the next event is a SYN_DROPPED and require the caller to sync
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    LIBEVDEV_READ_FLAG_BLOCKING   = 8; // The fd is not in O_NONBLOCK and a read may block

const LIBEVDEV_READ_STATUS_SUCCESS = 0,
    LIBEVDEV_READ_STATUS_SYNC = 1;


const libevdev = ffi.Library('libevdev.so.2', {
    'libevdev_new_from_fd'          : [ 'int'    , [ 'int', libevdev_pp                ]],
    'libevdev_free'                 : [ 'void'   , [ libevdev_p                        ]],
    'libevdev_next_event'           : [ 'int'    , [ libevdev_p, 'uint', input_event_p ]],
    'libevdev_has_event_type'       : [ 'int'    , [ libevdev_p, 'uint'                ]],
    'libevdev_has_event_code'       : [ 'int'    , [ libevdev_p, 'uint', 'uint'        ]],
    'libevdev_get_event_value'      : [ 'int'    , [ libevdev_p, 'uint', 'uint'        ]],
    'libevdev_event_type_from_name' : [ 'int'    , [ 'string'                          ]],
    'libevdev_event_code_from_name' : [ 'int'    , [ 'uint', 'string'                  ]],
    'libevdev_event_type_get_max'   : [ 'int'    , [ 'uint'                            ]],
    'libevdev_event_type_get_name'  : [ 'string' , [ 'uint'                            ]],
    'libevdev_event_code_get_name'  : [ 'string' , [ 'uint', 'uint'                    ]],
    'libevdev_event_value_get_name' : [ 'string' , [ 'uint', 'uint', 'int'             ]]
});


export function resolve_event(event) {
    const ev = { ...event };
    ev.code = libevdev.libevdev_event_code_get_name(ev.type, ev.code) || ev.code;
    ev.type = libevdev.libevdev_event_type_get_name(ev.type) || ev.type;
    ev.value = libevdev.libevdev_event_value_get_name(ev.type, ev.code, ev.value) || ev.value;
    return ev;
}


async function open_evdev(pathname) {
    const handle = await promises.open(pathname, constants.O_RDONLY | constants.O_NONBLOCK);

    let dev;
    try {
        dev = ref.alloc(libevdev_pp);
        const rc = libevdev.libevdev_new_from_fd(handle.fd, dev);
        if (rc !== 0) throw new Error(`Error while creating libevdev handle: ${rc}`);
    } catch (e) {
        await handle.close();
        throw e;
    }

    return {
        fd: handle.fd,
        filehandle: handle,
        libevdev: dev.deref(),
        close: () => handle.close()
    };
}


function* read_evdev(dev) {
    const event = new InputEvent();
    let flags = LIBEVDEV_READ_FLAG_NORMAL;

    for (; ;) {
        const rc = libevdev.libevdev_next_event(dev, flags, event.ref());
        if (rc === LIBEVDEV_READ_STATUS_SUCCESS) yield event;
        else if (rc === LIBEVDEV_READ_STATUS_SYNC) flags = LIBEVDEV_READ_FLAG_SYNC;
        else if (rc < 0) {
            const error = (util as any).getSystemErrorName(rc);
            if (error !== 'EAGAIN') {
                const ex: any = new Error(`Error while reading input events: ${error}`);
                ex.errno = rc;
                ex.code = error;
                throw ex;
            }
            break;
        } else {
            throw new Error(`Unsupported libevdev return code: ${rc}`);
        }
    }
}


export class InputDevice extends EventEmitter {
    type_num?: number;
    type_str?: string | null;
    code_num?: number;
    code_str?: string | null;
    dev: any;
    _keep_running?: boolean;
    _stopped?: Promise<void>;
    _resolve?: (v: any) => void;

    constructor(public filename, filter: any = {}) {
        super();
        this.filename = filename;

        if (typeof filter.type !== 'undefined') {
            if (typeof filter.type === 'number') {
                this.type_num = filter.type;
                this.type_str = libevdev.libevdev_event_type_get_name(filter.type);
            } else if (typeof filter.type === 'string') {
                this.type_str = filter.type;
                this.type_num = libevdev.libevdev_event_type_from_name(filter.type);
            } else {
                throw new Error(`Unsupported event type format`);
            }

            if (this.type_num === -1 || this.type_str === null)
                throw new Error(`Invalid event type ${filter.type}`);
        }

        if (typeof filter.code !== 'undefined') {
            if (typeof filter.code === 'number') {
                this.code_num = filter.code;
                this.code_str = libevdev.libevdev_event_code_get_name(this.type_num!, filter.code);
            } else if (typeof filter.code === 'string') {
                this.code_str = filter.code;
                this.code_num = libevdev.libevdev_event_code_from_name(this.type_num!, filter.code);
            } else {
                throw new Error(`Unsupported event code format`);
            }

            if (this.code_num === -1 || this.code_str === null)
                throw new Error(`Invalid event code ${filter.code}`);
        }
    }

    process_event(data) {
        const event = new InputEvent(data).toObject();
        event.time = event.time.toObject();

        if (this.type_str && this.type_num !== event.type) return;
        if (this.code_str && this.code_num !== event.code) return;
        this.emit('event', event);
    }

    async _open() {
        debug(`Opening input device '${this.filename}'`);
        this.dev = await open_evdev(this.filename);
        debug(`Opened input device ${this.filename}`);

        if (this.type_str) {
            if (!libevdev.libevdev_has_event_type(this.dev.libevdev, this.type_num!))
                throw new Error(`Input device ${this.filename} does not support event type ${this.type_str}`);
        }

        if (this.type_str && this.code_str) {
            if (!libevdev.libevdev_has_event_code(this.dev.libevdev, this.type_num!, this.code_num!))
                throw new Error(`Input device ${this.filename} does not support event code ${this.code_str}`);

            const value = libevdev.libevdev_get_event_value(this.dev.libevdev, this.type_num!, this.code_num!);
            debug(`Initial state of <${this.type_str},${this.code_str}>: ${value}`);
            this.process_event({
                time: { tv_sec: 0, tv_usec: 0 },
                type: this.type_num,
                code: this.code_num,
                value
            });
        }
    }

    async open() {
        await this._open();
        this._keep_running = true;
        this._stopped = this._run();
    }

    async close() {
        debug(`Stopping evdev reader for input device '${this.filename}'`);
        this._keep_running = false;
        this._resolve!(void 0);
        await this._stopped;
        debug(`Reader stopped`);
    }

    async _run() {
        debug(`Monitoring <${this.type_str || '*'},${this.code_str || '*'}> on input device '${this.filename}'`);
        while (this._keep_running) {
            try {
                if (!this.dev) await this._open();
                debug(`Polling input device '${this.filename}' for events`);
                let poller;
                try {
                    await new Promise((resolve, reject) => {
                        this._resolve = resolve;
                        poller = new epoll.Epoll((err, _fd, mask) => {
                            if (err !== null) reject(err);
                            else if ((mask & epoll.Epoll.EPOLLHUP)) resolve(true);
                            else if ((mask & epoll.Epoll.EPOLLERR)) reject(new Error(`Error in epoll`));
                            else if ((mask & epoll.Epoll.EPOLLIN)) {
                                try {
                                    for (const event of read_evdev(this.dev.libevdev))
                                        this.process_event(event);
                                } catch (e) {
                                    reject(e);
                                }
                            } else reject(new Error(`Unsupported epoll events ${mask}`));
                        });
                        poller.add(this.dev.fd, epoll.Epoll.EPOLLIN);
                    });
                } finally {
                    debug(`Input device '${this.filename}' closed or disconnected`);
                    poller.close();
                }
            } catch (e: any) {
                // Ignore errors indicating that the file does not exist. Those will be
                // thrown when the device has been unplugged. Everything else indicates
                // a serious problem and we terminate the node process to notify the
                // user.
                if (e.code !== 'ENOENT' && e.code !== 'ENODEV')
                    abort(e);

                debug(e);
                if (this._keep_running) await sleep(evdev_restart_interval);
            } finally {
                // Close the file descriptor if it has been opened, otherwise we get a
                // warning from Node's garbage collector.
                if (this.dev) {
                    this.dev.close();
                    delete this.dev;
                }
            }
        }
    }
}
