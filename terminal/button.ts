import { EventEmitter } from 'events';


export class EvdevPushButton extends EventEmitter {
    state: any;

    constructor(public evdev) {
        super();
        // We don't know what state the button is in until we receive an event
        this.state = undefined;
        this.evdev = evdev;
        evdev.on('event', event => {
            this.set(event.value > 0);
        });
    }

    set(state) {
        if (state !== this.state) {
            this.state = state;
            this.emit('state', state);
        }
    }
}


export class MultiInputPushButton extends EventEmitter {
    state: any;
    _inputs: any;
    _i: number;

    constructor() {
        super();
        this.state = undefined;
        this._inputs = {};
        this._i = 0;
    }

    _sync() {
        let state = false;
        for (const v of Object.values(this._inputs)) {
            if (v) {
                state = true;
                break;
            }
        }
        const changed = this.state !== state;
        this.state = state;
        if (changed) this.emit('state', this.state);
    }

    getInput(initialState = false) {
        const i = this._i++;

        if (typeof this._inputs[i] !== 'undefined')
            throw new Error(`Conflicting input index`);

        const set = onoff => {
            this._inputs[i] = onoff;
            this._sync();
        };

        set(initialState);
        return {
            set,
            close: () => {
                delete this._inputs[i];
                this._sync();
            }
        };
    }
}
