import path from 'path';
import { EventEmitter } from 'events';
import { fileURLToPath } from 'url';
import abort from '@janakj/lib/abort';
import sleep from '@janakj/lib/sleep';
import Lock from '@janakj/lib/mutex';
import SpeakerMic from './spkrmic';
import { InputDevice } from './evdev';
import { EvdevPushButton, MultiInputPushButton } from './button';
import { ConstantDelay } from './delay';
import TrafficControl from './tc';
import { call_restart_interval, invite_timeout } from './defaults';
import Baresip from './baresip';
import logger from './logger';

const __dirname = path.dirname(fileURLToPath(import.meta.url));

/**
 * Auto-answer incoming calls. Maintain only one call at a time. If a new call
 * arrives while there is another ongoing one, terminate the old call first and
 * then accept the new call.
 */
class IncomingCallMonitor extends EventEmitter {
    lock: Lock;
    call: any;

    constructor(public baresip, public dbg) {
        super();
        this.baresip = baresip;
        this.dbg = dbg;
        this.lock = new Lock();

        this._on_invite = this._on_invite.bind(this);
        this._on_hangup = this._on_hangup.bind(this);
    }

    start() {
        this.dbg('Starting incoming call monitor');
        this.baresip.on('invite', this._on_invite);
        this.baresip.on('hangup', this._on_hangup);
        this.emit('call', false);
    }

    _on_hangup() {
        this.emit('call', false);
    }

    async _on_invite(event) {
        await this.lock.acquire();
        try {
            if (this.call) {

                // If we have a previous call, attempt to close it by first issuing the
                // callfind command with the call's ID, followed by a hangup command. It
                // is not a failure if callfind fails, so only print a warning if hangup
                // fails.

                this.dbg(`Closing previous call with ${this.call.peer}`);
                try {
                    await this.baresip.callfind(this.call.id);
                    try {
                        await this.baresip.hangup();
                    } catch (e: any) {
                        this.dbg(`Error while closing previous call: ${e.message}`);
                    }
                } catch (e) { /* nothing, call was closed in the mean time */ }
            }

            // Now answer the new incoming call. If we had a previous call, first try
            // to issue callfind with the new call's ID to make the new call current.
            // Then issue the accept command to accept it.
            //
            // Since Baresip only makes the callfind function available if there is an
            // ongoing call, we check if this.call is set before invoking it.

            this.dbg(`Answering call from ${event.param}`);
            try {
                if (this.call) await this.baresip.callfind(event.id);
                this.call = await this.baresip.accept(event.id);
                this.emit('call', true);
            } catch (e: any) {
                this.dbg(`Error while answering call: ${e.message}`);
                delete this.call;
                return;
            }
        } finally {
            this.lock.release();
        }
    }

    async stop() {
        await this.lock.acquire();
        this.dbg('Stopping incoming call monitor');
        try {
            this.baresip.off('invite', this._on_invite);
            if (this.call) {
                this.dbg(`Closing existing call with ${this.call.peer}`);
                try {
                    await this.baresip.hangup();
                } catch (e: any) {
                    this.dbg(`Error while closing existing call: ${e.message}`);
                }
            }
        } finally {
            this.lock.release();
        }
    }
}


/**
 * Attempt to keep a call open to the given call by automatically
 * re-establishing the call when it fails to connects or closes
 */
class OutgoingCallMonitor extends EventEmitter {
    exited: any;
    _running?: boolean;

    constructor(public baresip, public peer, public dbg, public interval = call_restart_interval) {
        super();
        this.baresip = baresip;
        this.peer = peer;
        this.dbg = dbg;
        this.interval = interval;
    }

    start() {
        this.dbg(`Starting outgoing call monitor to ${this.peer}`);
        this.exited = this._run();
        this.emit('call', false);
    }

    async _run() {
        let call;
        this._running = true;
        while (this._running) {
            try {
                this.dbg(`Calling ${this.peer}`);

                call = await Promise.race([this.baresip.dial(this.peer), sleep(invite_timeout, 'timeout')]);
                if (call === 'timeout') {
                    await this.baresip.hangup();
                    throw new Error(`INVITE timed out`);
                }

                this.emit('call', true);
                this.dbg(`Call to ${this.peer} established, waiting for the call to close`);
                let status;
                try {
                    status = await call.closed;
                    this.dbg(`Call to ${this.peer} closed: ${status}`);
                } catch (e: any) {
                    this.dbg(`Call to ${this.peer} aborted: ${e.message}`);
                }
                this.emit('call', false);

            } catch (e: any) {
                this.dbg(`Failed to establish call to ${this.peer}: ${e.message}`);
            }

            if (this._running) await sleep(this.interval);
        }
    }

    async stop() {
        this.dbg(`Stopping outgoing call monitor to ${this.peer}`);
        this._running = false;
        try {
            await this.baresip.hangup();
        } catch (e) { /* nothing */ }
        return this.exited;
    }
}


export class PushToTalk extends EventEmitter {
    dbg: debug.Debugger;
    state: any;
    evdev: InputDevice;
    ptt_delay: ConstantDelay;
    tc: TrafficControl;
    baresip: Baresip;
    _dequeue?: boolean;
    peer: any;
    call_monitor: any;

    constructor(public args, public audio, indicator, ws_server) {
        super();
        this.dbg = logger.extend('ptt');
        this.state = { ptt: false, call: false };
        this.audio = audio;

        this.evdev = new InputDevice(args.ptt_evdev_device, { type: args.ptt_evdev_type, code: args.ptt_evdev_code });
        this.ptt_delay = new ConstantDelay();
        this.tc = new TrafficControl(args.ifname, args.no_kmod);
        this.baresip = new Baresip(path.join(args.data_dir, '.baresip'), path.join(__dirname, '../templates/.baresip'), args);

        const speakermic = new SpeakerMic(this.audio);

        const ptt = new MultiInputPushButton();
        const input = ptt.getInput();

        const button = new EvdevPushButton(this.evdev);
        button.on('state', state => input.set(state));

        ws_server.on('connection', sock => {
            const inp = ptt.getInput();

            sock.on('ptt', (state, cb) => {
                inp.set(state);
                cb(null);
            });

            sock.once('disconnect', () => {
                inp.close();
            });
        });

        // eslint-disable-next-line @typescript-eslint/no-misused-promises
        ptt.on('state', async state => {
            try {
                this.dbg(`PTT button ${state ? 'depressed' : 'released'}`);
                await this.ptt_delay.apply();

                if (state) {
                    indicator.enqueue(1, args.ptt_color);
                    this._dequeue = true;
                    await this.audio.playPttBeep();
                } else if (this._dequeue) {
                    indicator.dequeue(1, args.ptt_color);
                }
                await speakermic.switch(state);
            } catch (e) {
                abort(e);
            }
        });

        ptt.on('state', state => {
            this.state.ptt = state;
            this.emit('ptt', state > 0);
        });

        this.start = this.start.bind(this);
        this.stop = this.stop.bind(this);
        this._on_call = this._on_call.bind(this);
    }

    _on_call(state) {
        this.state.call = state;
        this.emit('call', state);
    }

    async reconfigure(s) {
        try {
            this.ptt_delay.set(s.ptt_delay);
            await Promise.all([
                this.baresip.reconfigure(s),
                this.tc.reconfigure(s)
            ]);

            if (this.peer !== s.peer) {
                this.dbg('Reconfiguring call monitor');

                if (this.call_monitor) {
                    await this.call_monitor.stop();
                    this.call_monitor.off('call', this._on_call);
                    delete this.call_monitor;
                }

                if (s.peer) this.call_monitor = new OutgoingCallMonitor(this.baresip, s.peer, this.dbg);
                else this.call_monitor = new IncomingCallMonitor(this.baresip, this.dbg);
                this.call_monitor.on('call', this._on_call);
                this.call_monitor.start();
                this.peer = s.peer;
            }
        } catch (e) {
            abort(e);
        }
    }

    async start(settings) {
        this.dbg('Starting user terminal in PTT mode');
        this.peer = settings.peer;

        // Baresip may or may not start without its configuration files. Thus,
        // create the configuration files manually here if they don't exist, before
        // attempting to start Baresip.
        this.dbg('Generating initial Baresip configuration');
        await this.baresip.configurator.update(settings);

        this.dbg('Waiting for all components to start');
        await Promise.all([
            this.evdev.open(),
            this.tc.ready,
            this.baresip.start()
        ]);

        // Upon startup the settings object does not generate a commit event.
        // Trigger the initial configuration of all modules manually.
        this.dbg(`Performing initial configuration`);
        await this.reconfigure(settings);

        if (!this.call_monitor) {
            if (this.peer) this.call_monitor = new OutgoingCallMonitor(this.baresip, settings.peer, this.dbg);
            else this.call_monitor = new IncomingCallMonitor(this.baresip, this.dbg);
            this.call_monitor.on('call', this._on_call);
            this.call_monitor.start();
        }
    }

    async stop() {
        this.dbg('Stopping user terminal');

        if (this.call_monitor) {
            this.dbg(`Terminating call monitor`);
            await this.call_monitor.stop();
            this.call_monitor.off('call', this._on_call);
            delete this.call_monitor;
        }

        this.dbg('Waiting for all components to shutdown');
        return Promise.allSettled([
            this.evdev.close(),
            this.baresip.stop(),
            this.tc.clear()
        ]);
    }
}


export class TwoWayCall extends EventEmitter {
    dbg: debug.Debugger;
    state: any;
    color: any;
    tc: TrafficControl;
    baresip: Baresip;
    indicator: any;
    peer: any;
    call_monitor: any;

    constructor(args, public audio, indicator) {
        super();
        this.audio = audio;
        this.dbg = logger.extend('2way');
        this.state = { call: false };
        this.color = args.ptt_color;

        this.tc = new TrafficControl(args.ifname, args.no_kmod);
        this.baresip = new Baresip(path.join(args.data_dir, '.baresip'), path.join(__dirname, 'templates/.baresip'), args);
        this.indicator = indicator;

        this.start = this.start.bind(this);
        this.stop = this.stop.bind(this);
        this._on_call = this._on_call.bind(this);
    }

    _on_call(state) {
        this.state.call = state;

        if (state) {
            // In two-way call mode there is no PTT and terminals transmit audio all the
            // time, so we keep the indicator on as long as there is a call
            this.indicator.enqueue(1, this.color);
        } else {
            try {
                this.indicator.dequeue(1, this.color);
            } catch (e) { /* nothing */ }
        }

        this.emit('call', state);
    }

    async reconfigure(s) {
        try {
            await Promise.all([
                this.baresip.reconfigure(s),
                this.tc.reconfigure(s)
            ]);

            if (this.peer !== s.peer) {
                this.dbg('Reconfiguring call monitor');

                if (this.call_monitor) {
                    await this.call_monitor.stop();
                    this.call_monitor.off('call', this._on_call);
                    delete this.call_monitor;
                }

                if (s.peer) this.call_monitor = new OutgoingCallMonitor(this.baresip, s.peer, this.dbg);
                else this.call_monitor = new IncomingCallMonitor(this.baresip, this.dbg);
                this.call_monitor.on('call', this._on_call);
                this.call_monitor.start();
                this.peer = s.peer;
            }
        } catch (e) {
            abort(e);
        }
    }

    async start(settings) {
        this.dbg('Starting user terminal in two-way call mode');
        this.peer = settings.peer;

        // Baresip may or may not start without its configuration files. Thus,
        // create the configuration files manually here if they don't exist, before
        // attempting to start Baresip.
        this.dbg('Generating initial Baresip configuration');
        await this.baresip.configurator.update(settings);

        this.dbg('Waiting for all components to start');
        await Promise.all([
            this.tc.ready,
            this.baresip.start()
        ]);

        // Upon startup the settings object does not generate a commit event.
        // Trigger the initial configuration of all modules manually.
        this.dbg(`Performing initial configuration`);
        await this.reconfigure(settings);

        if (!this.call_monitor) {
            if (this.peer) this.call_monitor = new OutgoingCallMonitor(this.baresip, settings.peer, this.dbg);
            else this.call_monitor = new IncomingCallMonitor(this.baresip, this.dbg);
            this.call_monitor.on('call', this._on_call);
            this.call_monitor.start();
        }
    }

    async stop() {
        this.dbg('Stopping user terminal');

        if (this.call_monitor) {
            this.dbg(`Terminating call monitor`);
            await this.call_monitor.stop();
            this.call_monitor.off('call', this._on_call);
            delete this.call_monitor;
        }

        this.dbg('Waiting for all components to shutdown');
        return Promise.allSettled([
            this.baresip.stop(),
            this.tc.clear()
        ]);
    }
}

export default { PushToTalk, TwoWayCall };
