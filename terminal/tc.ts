import Lock from '@janakj/lib/mutex';
import { parseNumber } from '@janakj/lib/parse';
import { ifb_interface_name } from './defaults';
import { sudo as _sudo } from './utils';
import logger from './logger';

const debug = logger.extend('tc'),
    dbg_sudo = debug.extend('sudo');

const sudo = (cmd) => {
    dbg_sudo(cmd);
    return _sudo(cmd);
};


/*
 * The TrafficControl contains the logic necessary to emulate a link of a
 * specific bandwidth, delay, packet loss, and error rate over an Ethernet link.
 * The class invokes the tc command (part of iproute2) to accomplish this. It
 * supports traffic shaping for both ingress and egress traffic.
 *
 * Since Linux traffic control subsystem does not support ingress traffic
 * shaping, we create and configure an "ifb" interface and configure the
 * "ingress" qdisc on the real interface to forward selected traffic over to the
 * ifb interface. Egress traffic shaping is then applied on both the ifb and
 * real network interfaces. Only subset of the traffic matching the user
 * supplied filter is forwarded to the ifb interface. For that reason, there
 * needs to be no additional filter on the ifb interface and the netem qdisc can
 * be attached as the root qdisc on the ifb interface.
 *
 * On the real interface we only want to apply traffic shaping on a subset of
 * the traffic that matches the user supplied filter. The rest of the traffic
 * should pass through the interface unmodified. To accomplish this, we attach a
 * prio qdisc to the root of the interface and configure it with 4 bands. The
 * lower three bands will be used as usual. The fourth band will have a netem
 * qdisc attached to it. We then install a filter to the prio qdisc and send all
 * traffic matching the user supplied filter to the fourth band, where it will
 * be subject to netem.
 */
export default class TrafficControl {
    ready: any;
    lock: Lock;
    matchall: string;

    constructor(public ifname, public no_kmod, public ifb_ifname = ifb_interface_name) {
        this.ifname = ifname;
        this.ifb_ifname = ifb_ifname;
        this.no_kmod = no_kmod;
        this.lock = new Lock();
        this.ready = this._init();
        this.matchall = 'u32 match u32 0 0';
    }

    async _init() {
        debug(`Initializing traffic shaping on ${this.ifname}`);

        if (this.no_kmod) {
            debug(`Not loading ifb Linux kernel module, as requested`);
        } else {
            await sudo('modprobe ifb');
        }
        await this._clear();
        await this._setup();
    }

    async _clear() {
        debug(`Removing traffic shaping rules from ${this.ifname}`);
        await sudo(`ip link set ${this.ifb_ifname} down`);
        await sudo(`tc qdisc del dev ${this.ifb_ifname} root || true`);

        await sudo(`tc qdisc del dev ${this.ifname} ingress || true`);
        await sudo(`tc qdisc del dev ${this.ifname} root || true`);
    }

    async _setup() {
        debug(`Setting up traffic shaping rules on ${this.ifname}`);
        await sudo(`ip link set ${this.ifb_ifname} up`);

        await sudo(`tc qdisc add dev ${this.ifname} ingress`);
        await sudo(`tc qdisc add dev ${this.ifname} root handle 1: prio bands 4`);
    }

    async clear() {
        await this.ready;
        await this.lock.acquire();
        try {
            await this._clear();
        } finally {
            this.lock.release();
        }
    }

    async reconfigure(data) {
        const s: any = {};

        for (const n of ['rx_rate', 'tx_rate']) {
            if (data[n] === null) s[n] = '';
            else if (typeof data[n] !== 'string')
                throw new Error(`${n} value must be a string`);
            else s[n] = `rate ${data[n]}`;
        }

        for (const n of ['tx_delay', 'rx_delay']) {
            if (data[n] === null) s[n] = '';
            else s[n] = `delay ${parseNumber(data[n], 0)}ms`;
        }

        for (const n of ['rx_loss', 'tx_loss']) {
            if (data[n] === null) s[n] = '';
            else s[n] = `loss ${parseNumber(data[n], 0, 100)}`;
        }

        for (const n of ['rx_error', 'tx_error']) {
            if (data[n] === null) s[n] = '';
            else s[n] = `corrupt ${parseNumber(data[n], 0, 100)}`;
        }

        for (const n of ['rx_tc_filter', 'tx_tc_filter']) {
            if (data[n] === null) s[n] = this.matchall;
            else if (typeof data[n] === 'string') s[n] = data[n];
            else throw new Error(`${n} must be a string`);
        }

        await this.ready;
        await this.lock.acquire();
        debug(`Updating traffic shaping parameters on ${this.ifname}`);
        try {
            // ingress path
            await sudo(`tc filter replace dev ${this.ifname} parent ffff: prio 1 protocol all ${s.rx_tc_filter} action mirred egress redirect dev ${this.ifb_ifname}`);
            await sudo(`tc qdisc replace dev ${this.ifb_ifname} root handle 1: netem ${s.rx_rate} ${s.rx_loss} ${s.rx_delay} ${s.rx_error}`);

            // egress path
            await sudo(`tc filter replace dev ${this.ifname} parent 1: prio 1 protocol all ${s.tx_tc_filter} classid :4`);
            await sudo(`tc qdisc replace dev ${this.ifname} parent 1:4 handle 10: netem ${s.tx_rate} ${s.tx_loss} ${s.tx_delay} ${s.tx_error}`);
        } finally {
            this.lock.release();
        }
    }
}
