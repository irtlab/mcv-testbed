/**
 * Encoder and decoder for Netstrings:
 * https://cr.yp.to/proto/netstrings.txt
 */

export function netstr_encoder() {
    let msg = '';

    const encoder = function (data) {
        msg += data;
        const rv = `${msg.length}:${msg},`;
        encoder.reset();
        return rv;
    };

    encoder.reset = () => { msg = ''; };
    encoder.append = data => { msg += data; };

    return encoder;
}


export function netstr_decoder() {
    const SIZE = 0,
        STRING = 1;

    let state = SIZE,
        length: any = '',
        string = '';

    return function (data) {
        const rv: string[] = [];
        for (const c of data) {
            switch (state) {
                case SIZE:
                    if (c === ':') {
                        state = STRING;
                        length = parseInt(length, 10);
                        if (Number.isNaN(length)) {
                            state = SIZE;
                            length = '';
                            string = '';
                            throw new Error('Invalid length');
                        }
                    } else {
                        length += c;
                    }
                    break;

                case STRING:
                    if (string.length < length) {
                        string += c;
                    } else {
                        state = SIZE;
                        length = '';
                        if (c !== ',') throw new Error('Missing trailing ,');
                        rv.push(string);
                        string = '';
                    }
                    break;

                default:
                    throw new Error('Bug: unexpected state');
            }
        }
        return rv;
    };
}
