import express from 'express';
import { networkInterfaces } from 'os';
import { exec } from 'child_process';
import { promises as fs, constants as fsConstants } from 'fs';
import Lock from '@janakj/lib/mutex';
import { BadRequestError, NotFoundError, jsonify } from '@janakj/lib/http';
import sleep from '@janakj/lib/sleep';
import { machineId } from '@janakj/lib/machine';
import { unquote, trim_file, sudo } from './utils';
import { supported_settings, restore_default_settings, load_defaults, delete_settings_file, check_settings, save_settings, update_settings } from './settings';
import { timesync_status } from './timesync';
import logger from './logger';

const debug = logger.extend('api');


const boot_id = () => trim_file('/proc/sys/kernel/random/boot_id');
const kernel  = () => trim_file('/proc/version');

async function kv_file(filename, delim = '=') {
    const s = await trim_file(filename);

    const attrs = {};
    s.split('\n').forEach(line => {
        const d = line.indexOf(delim);
        if (d === -1) return;
        attrs[line.slice(0, d).trim()] = unquote(line.slice(d + 1)).trim();
    });

    return attrs;
}

async function os_release() {
    const attrs: any = await kv_file('/etc/os-release');
    return attrs.PRETTY_NAME || attrs.NAME || attrs.ID;
}

async function serial() {
    const s = await trim_file('/sys/firmware/devicetree/base/serial-number');
    // This file can have '\0' characters in it
    // eslint-disable-next-line no-control-regex
    return s.replace(/[\x00]+/g, '');
}

async function revision() {
    const attrs: any = await kv_file('/proc/cpuinfo', ':');
    return attrs.Revision;
}

async function uptime() {
    const s = await trim_file('/proc/uptime');
    return parseFloat(s);
}

async function model() {
    const s = await trim_file('/sys/firmware/devicetree/base/model');
    // This file can have '\0' characters in it
    // eslint-disable-next-line no-control-regex
    return s.replace(/[\x00]+/g, '');
}

// eslint-disable-next-line no-unused-vars
// eslint-disable-next-line @typescript-eslint/no-unused-vars
async function git_version() {
    return new Promise((resolve, reject) => {
        exec('git describe --abbrev=8 --always --tags --dirty=" (dirty)" 2>/dev/null || echo "?"',
            (error, stdout) => {
                if (error !== null) {
                    reject(error);
                    return;
                }
                resolve(stdout.toString().trim());
            }
        );
    });
}


export default async function (args, settings_filename, settings, apply_settings) {

    // Make sure we can write to the sysrq file, abort if not.
    try {
        await fs.access(args.sysrq, fsConstants.W_OK);
    } catch (e) {
        debug(`Warning: You don't seem to have access to ${args.sysrq}, hard reset will not work`);
    }

    const api = express.Router(),
        lock = new Lock();

    api.use(express.json());


    const locked = fn =>
        (req, res, next) => {
            void (async function() {
                await lock.acquire();
                try {
                    return await fn(req, res, next);
                } finally {
                    lock.release();
                }
            })();
        };

    function soft_reboot() {
        return sudo('systemctl reboot');
    }

    async function hard_reboot() {
        // This function attempts to reboot the machine by writing a series of
        // commands to the file /proc/sysrq_trigger. It first attempts to sync and
        // re-mount all file-systems read only. After that it issues the reboot
        // command.
        //
        // Since there is no way to tell when the sync and re-mount operations have
        // completed (it is only indicated on the Linux console), we have to use
        // static wait intervals between commands and hope for the best.

        const f = await fs.open(args.sysrq, 'w');

        await f.write('s');
        await sleep(500);

        await f.write('u');
        await sleep(500);

        debug('Rebooting');
        await sleep(1000);
        await f.write('b');
        await f.close();
    }

    api.get('/restart', jsonify(async () => {
        return {
            boot_id: await boot_id(),
            uptime: await uptime()
        };
    }, devMode));

    api.post('/restart', locked(async (req, res) => {
        res.status(202).write(`${JSON.stringify({
            boot_id: await boot_id(),
            uptime: await uptime()
        })}\n`);
        try {
            if ('hard' in req.query) await hard_reboot();
            else await soft_reboot();
        } catch (error: any) {
            res.write(error.message);
        }
        res.end();
    }));

    api.get('/timesync', jsonify(async () => timesync_status()));

    api.get('/settings', jsonify(locked(() => settings)));

    api.delete('/settings', jsonify(locked(async () => {
        await delete_settings_file(settings_filename);
        await restore_default_settings();
        return settings;
    })));

    api.get('/settings/default', jsonify(load_defaults));

    function body2settings(req) {
        try {
            return check_settings(req.body, 'ignore_unknown' in req.query);
        } catch (error: any) {
            throw new BadRequestError(error.message);
        }
    }

    api.post('/settings', jsonify(locked(async req => {
        const data = body2settings(req);
        await save_settings(settings_filename, data);
        update_settings(data);
        return settings;
    })));

    api.post('/settings/apply', jsonify(locked(async () => {
        await apply_settings();
        return settings;
    })));

    api.get('/settings/supported', jsonify(() => {
        return supported_settings;
    }));

    // This API endpoint can be used by the client to determine if the machine has
    // come back online after a reboot.

    api.get('/ping', (_req, res) => {
        res.status(200).end();
    });

    api.post('/pager', jsonify(() => {
        debug('paging');
    }));

    api.get('/config', jsonify(() => args));

    api.get('/platform', jsonify(async () => {
        const attrs = await Promise.all([
            model(), revision(), serial(), machineId(), boot_id(), uptime(),
            kernel(), os_release() // , git_version()
        ]);
        return {
            model: attrs[0],
            revision: attrs[1],
            serial: attrs[2],
            mac: (networkInterfaces()[args.ifname] || [])[0].mac,
            machine_id: attrs[3],
            boot_id: attrs[4],
            uptime: attrs[5],
            kernel: attrs[6],
            os_release: attrs[7],
            nodejs: process.version,
            // software   : attrs[8]
        };
    }));

    api.get('*', jsonify(() => {
        throw new NotFoundError();
    }));

    return api;
}
