import sleep from '@janakj/lib/sleep';
import { Random } from './random';


export class ConstantDelay {
    delay: number;

    constructor(delay = 0) {
        if (typeof delay !== 'number' || delay < 0)
            throw new Error(`Delay value must be a number >= 0`);

        this.delay = delay;
    }

    set(delay) {
        this.delay = delay;
    }

    async apply(v?) {
        await sleep(typeof v === 'undefined' ? this.delay : v);
        return v;
    }
}


export class NormalDelay {
    nd_random: Random;
    delay?: number;
    sigma?: number;

    // Constructor arguments:
    // - delay: Delay in milliseconds. By default it is set to 0.
    // - sigma: A standard deviation sigma. By default it is set to 1.
    constructor(delay = 0, sigma = 1) {
        this.set(delay, sigma);

        // Box–Muller transform method for generating normally distributed random
        // numbers.
        this.nd_random = new Random();
    }

    set(delay, sigma) {
        this.delay = delay;
        this.sigma = sigma;
    }

    async apply(v) {
        // Generetas and returns normally distributed random number using Box–Muller
        // transform method.
        await sleep(this.nd_random.normal(this.delay!, this.sigma!));
        return v;
    }
}
