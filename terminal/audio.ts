import wav from 'wav';
import express from 'express';
import sleep  from '@janakj/lib/sleep';
import { jsonify, BadRequestError } from '@janakj/lib/http';
import { PA_VOLUME_NORM, volumeToPercent } from '@mcv/common/volume';
import {
    PulseAudio,
    PA_SINK_FLAGS,
    PA_SOURCE_FLAGS,
    PA_SAMPLE_FORMAT,
    PA_ERR,
    PA_NO_INDEX,
    PA_NO_VALUE,
    PA_MAX_CHANNELS,
    frameSize,
    sampleSize,
    sampleFormatStr,
    PulseError
} from 'pulseaudio.js';
import { FileCache } from './cache';
import logger from './logger';

const debug = logger.extend('audio');

const BARESIP_INPUT_NAME = 'VoIP Playback';


function duration({ format, channels, rate }, length) {
    return length / frameSize({ format, channels }) / rate;
}


export function audioAPI(_args, audio) {
    const api = express.Router();

    api.get('/speaker', jsonify(async (req, res) => {
        let channels;
        if (typeof req.query.channels === 'string') {
            channels = parseInt(req.query.channels, 10);
            if (channels < 1 || channels > PA_MAX_CHANNELS)
                throw new BadRequestError(`Invalid number of channels (supported rate: <1, ${PA_MAX_CHANNELS}}`);
        } else {
            channels = 1;
        }

        let rate;
        if (typeof req.query.rate === 'string') {
            rate = parseInt(req.query.rate, 10);
            if (rate < 8000 || rate > 96000)
                throw new BadRequestError('Invalid sample rate (supported range: <8000, 96000>');
        } else {
            rate = 22050;
        }

        let type;
        if (typeof req.query.type === 'string') {
            type = req.query.type;
            if (type !== 'audio/wav' && type !== 'audio/vnd.mcv.pcm')
                throw new BadRequestError(`type must be either 'audio/wav' or 'audio/vnd.mcv.pcm'`);
        } else {
            type = 'audio/wav';
        }

        let format;
        if (typeof req.query.format === 'string') {
            format = sampleFormatStr[req.query.format];
            if (typeof format === 'undefined')
                throw new BadRequestError(`Invalid sample format (supported: ${Object.keys(sampleFormatStr).toString()})`);
        } else {
            format = PA_SAMPLE_FORMAT.S16LE;
        }

        if (type === 'audio/wav') {
            debug(`fmt: ${format} ${typeof format} ${PA_SAMPLE_FORMAT.U8} ${PA_SAMPLE_FORMAT.S16LE}`);
            if (format !== PA_SAMPLE_FORMAT.U8 && format !== PA_SAMPLE_FORMAT.S16LE)
                throw new BadRequestError(`The wav format only supports u8 or S16LE formats`);
        }

        let fragmentSize;
        if (typeof req.query.fragmentSize === 'string') {
            fragmentSize = parseInt(req.query.fragmentSize, 10);
            if (fragmentSize <= 0)
                throw new BadRequestError('Invalid fragmentSize (must be > 0)');
        } else {
            fragmentSize = PA_NO_VALUE;
        }

        res.writeHead(200, { 'Content-Type': type });
        res.setTimeout(0);

        const stream = await audio.pa.createRecordStream({
            name: `${audio.speaker}.monitor`,
            adjustLatency: fragmentSize !== PA_NO_VALUE,
            fragmentSize,
            sampleSpec: { rate, format, channels }
        });

        if (type === 'audio/wav') {
            const writer = new wav.Writer({
                sampleRate: rate,
                channels,
                bitDepth: sampleSize[format] * 8
            });

            stream.pipe(writer);
            writer.pipe(res);

            res.on('close', () => {
                stream.unpipe();
                writer.unpipe();
                stream.destroy();
            });
        } else {
            stream.pipe(res);
            res.on('close', () => {
                stream.unpipe();
                stream.destroy();
            });
        }
    }));

    return api;
}


export default class AudioRouter {
    pa?: PulseAudio;
    media: FileCache;
    args: any;
    samples: any;
    beep: any;
    noise: any;
    mic: any;
    voip: any;
    origDefaultSource: any;
    speaker: any;

    constructor(media, args) {
        this.media = media;
        this.args = args;
        this.samples = Object.create(null);
        this._onNewSinkInput = this._onNewSinkInput.bind(this);

        this.beep = {
            volume: PA_VOLUME_NORM,
            mute: false
        };

        this.noise = {
            index: PA_NO_INDEX,
            volume: PA_VOLUME_NORM,
            mute: true
        };

        this.mic = {
            index: PA_NO_INDEX,
            volume: PA_VOLUME_NORM,
            mute: true
        };

        this.voip = {
            index: PA_NO_INDEX,
            volume: PA_VOLUME_NORM,
            mute: false
        };
    }

    async start(settings) {
        debug(`Initializing audio routing layer`);
        this.pa = new PulseAudio();
        await this.pa.connect();

        const server = await this.pa.getServerInfo();
        this.origDefaultSource = server.defaultSource;

        // Clean-up any leftover PulseAudio configuration from a previous (e.g.,
        // crashed) run. We won't be able to create a virtual microphone if the
        // corresponding module is already loaded
        await this._cleanup();

        // Make sure that both PulseAudio devices support decibel volumes. Without
        // this feature we would not be able to set volumes correctly, so it is
        // better to bail and warn the user early.

        this.speaker = this.args.speaker || server.defaultSink;
        debug(`Using PulseAudio sink '${this.speaker}' as speaker output`);

        const sink = await this.pa.getSinkInfo(this.speaker);
        // eslint-disable-next-line no-bitwise
        if (!(sink.flags & PA_SINK_FLAGS.DECIBEL_VOLUME))
            throw new Error(`PulseAudio sink '${this.speaker}' does not support dB volumes (required by this program)`);

        debug(`Umuting '${this.speaker}' and settings maximum volume`);
        await this.pa.setSinkVolume(PA_VOLUME_NORM, this.speaker);
        await this.pa.setSinkMute(false, this.speaker);

        // eslint-disable-next-line @typescript-eslint/no-misused-promises
        this.pa.on('event.sink_input.new', this._onNewSinkInput);

        await this._createVirtualMic();
        await this.reconfigure(settings);
    }

    async _clearPreviousMicRedirect() {
        debug(`Clearing previous mic redirect`);
        if (typeof this.mic.module_index !== 'undefined') {
            await this.pa!.unloadModule(this.mic.module_index);
            delete this.mic.module_index;
        }

        if (this.mic.player) {
            await this.mic.player.stop();
            delete this.mic.player;
        }
        this.mic.index = PA_NO_INDEX;
    }

    async _redirectSourceToMic(name) {
        const source = name !== null ? name : this.origDefaultSource;
        debug(`Redirecting PulseAudio source '${source}' to '${this.args.virtual_mic_name}'`);

        const { flags, index } = await this.pa!.getSourceInfo(source);
        // eslint-disable-next-line no-bitwise
        if (!(flags & PA_SOURCE_FLAGS.DECIBEL_VOLUME))
            throw new Error(`PulseAudio source '${source}' does not support dB volumes (required by this program)`);

        this.mic.index = index;

        this.mic.module_index = await this.pa!.loadModule('module-loopback', {
            source,
            sink: this.args.virtual_mic_name,
            rate: 48000,
            latency_msec: 10,
            channels: 1
        });
    }

    async _isPulseAudioSource(name) {
        if (name === null) return true;

        for (const source of await this.pa!.getAllSources())
            if (source.name === name) return true;

        return false;
    }

    _redirectWavToMic(filename) {
        debug(`Redirecting file '${filename}' to '${this.args.virtual_mic_name}'`);

        this.mic.player = this.playLoop(filename, {
            name: this.args.virtual_mic_name,
            properties: { mcv: { role: 'mic' } }
        });
    }

    async _syncMic() {
        if (typeof this.mic.module_index !== 'undefined')
            return this._syncSource(this.mic);
        return this._syncSinkInput(this.mic);
    }

    async _redirectMic(name) {
        if (this.mic.name !== name) {
            this.mic.name = name;

            await this._clearPreviousMicRedirect();

            if (await this._isPulseAudioSource(name))
                await this._redirectSourceToMic(name);
            else
                this._redirectWavToMic(name);
        }
    }

    async reconfigure(s) {
        debug(`Reconfiguring audio router`);

        await this._redirectMic(s.mic);

        return Promise.all([
            this.setSpeakerVolume(s.speaker_volume),
            this.setMicVolume(s.mic_volume),
            this.setNoiseVolume(s.noise_volume),
            this.setNoise(s.noise),
            this.setPttBeep(s.ptt_beep),
            this.setPttBeepVolume(s.ptt_volume)
        ]);
    }

    playLoop(filename, opts = {}) {
        let terminate, handle, quit = false;
        const _quit = new Promise(resolve => {
            terminate = () => {
                quit = true;
                resolve(true);
                if (handle) handle.stop();
            };
        });

        const looper = async () => {
            while (!quit) {
                try {
                    handle = await this.playFile(filename, opts);
                    if (quit) await handle.stop();
                    else await handle.finished;
                    handle = null;
                } catch (error: any) {
                    debug(`Error starting player for '${filename}': ${error.message}`);
                    await Promise.race([sleep(3000), _quit]);
                }
            }
        };

        const terminated = looper();
        return {
            stop: async () => {
                terminate();
                await terminated;
            }
        };
    }

    playFile = (filename, opts = {}) => new Promise((resolve, reject) => {
        let connect, finished;

        this.media.get(filename).then(input => {
            if (input === null)
                throw new Error(`File ${filename} was not found in media cache`);

            const reader = new wav.Reader();

            const abort = error => {
                reader.off('format', connect);
                input.destroy();
                if (finished) finished(false);
                reject(error || new Error('Truncated or malformed wav file'));
            };

            // Make sure we clean up if we get a corrupted (or empty) file that does not
            // emit 'format'.
            input.once('end', abort);
            input.once('error', abort);
            input.pipe(reader);

            connect = ({ bitDepth, channels, sampleRate }) => {
                input.off('end', abort);
                input.off('error', abort);

                this.pa!.createPlaybackStream({
                    sampleSpec: {
                        format: bitDepth === 16 ? PA_SAMPLE_FORMAT.S16LE : PA_SAMPLE_FORMAT.U8,
                        rate: sampleRate,
                        channels
                    },
                    ...opts
                }).then(output => {
                    output.once('finish', () => finished(true));
                    output.once('close', () => finished(false));
                    output.once('error', () => finished(false));
                    reader.pipe(output);

                    const done = new Promise(res => { finished = res; });
                    resolve({
                        stream: output,
                        finished: done,
                        stop: () => {
                            input.unpipe();
                            reader.unpipe();
                            input.destroy();
                            output.destroy();
                            return done;
                        }
                    });
                }).catch(abort);
            };

            reader.once('format', connect);
        }).catch(reject);
    });

    createSample = filename => new Promise((resolve, reject) => {
        this.media.get(filename).then(input => {
            if (!input) throw new Error(`File ${filename} was not found`);

            const reader = new wav.Reader();

            const abort = error => {
                reader.off('format', upload);
                input.destroy();
                reject(error || new Error('Truncated or malformed wav file'));
            };

            // Make sure we clean up if we get a corrupted (or empty) file that does not
            // emit 'format'.
            input.once('end', abort);
            input.once('error', abort);
            input.pipe(reader);

            const upload = ({ bitDepth, channels, sampleRate }) => {
                input.off('end', abort);
                input.off('error', abort);

                const data: Buffer[] = [];
                reader.on('data', c => data.push(c));

                reader.once('end', () => {
                    const maximumLength = data.reduce((a, v) => a + v.length, 0),
                        sampleSpec = {
                            format: bitDepth === 16 ? PA_SAMPLE_FORMAT.S16LE : PA_SAMPLE_FORMAT.U8,
                            rate: sampleRate,
                            channels
                        },
                        name = filename;

                    if (!maximumLength)
                        throw new Error('Truncated or malformed wav file');

                    this.pa!.createUploadStream({ name, maximumLength, sampleSpec }).then(output => {
                        output.once('close', () => resolve({
                            length: maximumLength,
                            duration: duration(sampleSpec, maximumLength),
                            name,
                            sampleSpec
                        }));
                        output.once('error', abort);

                        for (const chunk of data) output.write(chunk);
                        output.end();
                    }).catch(abort);
                });
            };

            reader.once('format', upload);
        }).catch(reject);
    }).then(descriptor => {
        this.samples[filename] = descriptor;
        return filename;
    });

    async playSample(name, opts) {
        if (!(name in this.samples))
            throw new Error(`Unknown sample '${name}'`);

        await this.pa!.playSample(name, opts);

        // PulseAudio does not provide a means to get notified once the sample
        // stopped playing. To workaround this, we calculate the duration of the
        // sample from its size and sleep for that duration. The caller can await
        // the result of this function call to get notified when the sample stopped
        // playing.
        await sleep(this.samples[name].duration * 1000);
    }

    async stop() {
        debug(`Shutting down audio routing layer`);

        if (this.mic.player) await this.mic.player.stop();
        if (this.noise.player) await this.noise.player.stop();

        // eslint-disable-next-line @typescript-eslint/no-misused-promises
        this.pa!.off('event.sink_input.new', this._onNewSinkInput);
        await this._cleanup();
        await this.pa!.disconnect();
    }

    async _onNewSinkInput(index) {
        let info, role;
        try {
            info = await this.pa!.getSinkInputInfo(index);
            role = (info.properties.mcv || {}).role;
        } catch (error) {
            if (!(error instanceof PulseError && (error.code === PA_ERR.NOENTITY)))
                throw error;
            return;
        }

        if (info.name === BARESIP_INPUT_NAME) {
            this.voip.index = info.index;
            debug(`Found Baresip input '${info.name}', setting volume to ${volumeToPercent(this.voip.volume)} %`);
            await this._syncSinkInput(this.voip);
        }

        if (role === 'mic') {
            this.mic.index = info.index;
            debug(`Found mic input stream, setting volume to ${volumeToPercent(this.mic.volume)} %`);
            await this._syncSinkInput(this.mic);
        }

        if (role === 'noise') {
            this.noise.index = info.index;
            debug(`Found noise input stream, setting volume to ${volumeToPercent(this.noise.volume)} %`);
            await this._syncSinkInput(this.noise);
        }
    }

    async _syncSinkInput(input) {
        const { index, volume, mute } = input;
        // Sink inputs are dynamic and requests to update the input's volume can
        // fail at any time. If the error is PA_ERR_NOENTITY, the sink input with
        // the given index went away and we need to wait for PulseAudio to tell us
        // the new index number with an event once the sink input is recreated by
        // its client.
        try {
            if (index !== PA_NO_INDEX)
                await Promise.all([
                    this.pa!.setSinkInputVolume(index, volume),
                    this.pa!.setSinkInputMute(index, mute)
                ]);
        } catch (error) {
            if (error instanceof PulseError && error.code === PA_ERR.NOENTITY) {
                // A sink can disappear at any time
                // eslint-disable-next-line no-param-reassign
                input.index = PA_NO_INDEX;
            } else {
                throw error;
            }
        }
    }

    async _syncSource(source) {
        const { index, volume, mute } = source;
        try {
            if (index !== PA_NO_INDEX)
                await Promise.all([
                    this.pa!.setSourceVolume(volume, index),
                    this.pa!.setSourceMute(mute, index)
                ]);
        } catch (error) {
            if (error instanceof PulseError && error.code === PA_ERR.NOENTITY) {
                // A source can disappear at any time
                // eslint-disable-next-line no-param-reassign
                source.index = PA_NO_INDEX;
            } else {
                throw error;
            }
        }
    }

    setSpeakerVolume(volume) {
        this.voip.volume = volume;
        return this._syncSinkInput(this.voip);
    }

    muteSpeaker(mute) {
        this.voip.mute = !!mute;
        return this._syncSinkInput(this.voip);
    }

    setMicVolume(volume) {
        this.mic.volume = volume;
        return this._syncMic();
    }

    muteMic(mute) {
        this.mic.mute = mute;
        return this._syncMic();
    }

    async setPttBeep(name) {
        if (!name) {
            delete this.beep.name;
            return;
        }

        if (!(name in this.samples))
            await this.createSample(name);

        this.beep.name = name;
    }

    setPttBeepVolume(volume) {
        this.beep.volume = volume;
    }

    async playPttBeep() {
        if (!this.beep.name) return undefined;
        return this.playSample(this.beep.name, {
            name: this.speaker,
            volume: this.beep.volume
        });
    }

    async setNoise(name) {
        if (name === this.noise.filename) return;

        if (this.noise.player) {
            await this.noise.player.stop();
            delete this.noise.player;
        }

        if (!name) return;

        this.noise.player = this.playLoop(name, {
            name: this.args.virtual_mic_name,
            properties: { mcv: { role: 'noise' } }
        });
        this.noise.filename = name;
        await this._syncSinkInput(this.noise);
    }

    setNoiseVolume(volume) {
        this.noise.volume = volume;
        return this._syncSinkInput(this.noise);
    }

    muteNoise(mute) {
        this.noise.mute = !!mute;
        return this._syncSinkInput(this.noise);
    }

    async _cleanup() {
        // Delete all samples that we created from the sample cache
        await Promise.allSettled(Object.keys(this.samples).map(name => {
            debug(`Deleting sample '${name}'`);
            return this.pa!.removeSample(name);
        }));
        this.samples = {};

        // If any of the sources or sinks has the special "mcv" properties that we
        // created, restore defaults and unload modules if necessary.

        const data = (await this.pa!.getAllSources()).concat(await this.pa!.getAllSinks());
        for (const s of data) {
            const mcv = ((s.properties as any)).mcv || {};
            if (mcv.oldDefaultSource) {
                debug(`Restoring default source to ${mcv.oldDefaultSource}`);
                try {
                    await this.pa!.setDefaultSource(mcv.oldDefaultSource);
                } catch (error) {
                    debug(error);
                }
            }

            if (mcv.oldDefaultSink) {
                debug(`Restoring default sink to ${mcv.oldDefaulSink}`);
                try {
                    await this.pa!.setDefaultSink(mcv.oldDefaultSink);
                } catch (error) {
                    debug(error);
                }
            }

            if (mcv.unloadModule === 'true') {
                if (typeof s.module !== 'number')
                    throw new Error('Missing module index');

                debug(`Unloading module ${s.module}`);
                try {
                    await this.pa!.unloadModule(s.module);
                } catch (error) {
                    debug(error);
                }
            }
        }
    }


    async _createVirtualMic() {
        debug(`Setting up a virtual microphone device '${this.args.virtual_mic_name}'`);

        // Create a null sink that can be used to mix data from the real and virtual microphones
        await this.pa!.loadModule('module-null-sink', {
            sink_name: this.args.virtual_mic_name,
            sink_properties: {
                mcv: {
                    unloadModule: true,
                    oldDefaultSource: this.origDefaultSource
                }
            }
        });

        // Set the sink's monitor as default microphone
        debug(`Configuring ${this.args.virtual_mic_name}.monitor as PulseAudio's default source`);
        await this.pa!.setDefaultSource(`${this.args.virtual_mic_name}.monitor`);
    }
}
