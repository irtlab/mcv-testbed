import { EventEmitter } from 'events';
import { createConnection } from 'net';
import parse_color from 'color-parse';


function hex(d) {
    let v = Number(d).toString(16);
    while (v.length < 2) v = `0${v}`;
    return v;
}


export class StatusIndicator extends EventEmitter {
    queue: any;
    sock: any;

    constructor(public pathname) {
        super();
        this.pathname = pathname;
        this.queue = [];
    }

    current() {
        return this.queue[0][1];
    }

    enqueue(priority, val) {
        this.queue.push([priority, val]);
        this.queue.sort(([a], [b]) => b - a);
        this._apply();
    }

    _apply() {
        const v = this.current();
        this._set(v);
        this.emit('change', v);
    }

    _set(val) {
        let r, g, b;
        if (Array.isArray(val)) {
            [r, g, b] = val;
        } else if (typeof val === 'string') {
            const { space, values } = parse_color(val);
            if (space !== 'rgb')
                throw new Error(`Unsupported color space ${space}`);
            [r, g, b] = values;
        } else if (typeof val === 'boolean') {
            const v = val ? 255 : 0;
            r = v;
            g = v;
            b = v;
        } else {
            throw new Error(`Unsupported value type ${typeof val}`);
        }

        this.sock.write(`${hex(r)}${hex(g)}${hex(b)}\n`);
    }


    dequeue(priority, value) {
        for (let i = 0; i < this.queue.length; i++) {
            const [pri, val] = this.queue[i];
            if (pri === priority && val === value) {
                this.queue.splice(i, 1);
                this._apply();
                return;
            }
        }
        throw new Error(`Not found`);
    }

    start() {
        return new Promise((resolve, reject) => {
            const sock = createConnection(this.pathname);
            sock.once('error', reject);
            sock.once('connect', () => {
                sock.off('error', reject);
                this.sock = sock;
                resolve(true);
            });
        });
    }

    stop() {
        if (this.sock) {
            this.sock.end();
            delete this.sock;
        }
    }
}