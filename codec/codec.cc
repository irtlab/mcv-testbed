#include <iostream>
#include <fstream>
#include <array>
#include <algorithm>
#include <deque>
#include <byteswap.h>
#include <sndfile.hh>
#include <arpa/inet.h>
#include <getopt.h>
#include <string.h>
#include <gsm/gsm.h>
#include <opencore-amrnb/interf_enc.h>
#include <opencore-amrnb/interf_dec.h>
#include <opencore-amrwb/dec_if.h>
#include <vo-amrwbenc/enc_if.h>
#include "g711.h"
#include <ambe/capi.h>
#include <opus/opus.h>
#include <codec2/codec2.h>

#define FRAME_DURATION 20

#define NB_SAMPLE_RATE 8000
#define NB_FRAME_SIZE  NB_SAMPLE_RATE / 1000 * FRAME_DURATION

#define WB_SAMPLE_RATE 16000
#define WB_FRAME_SIZE  WB_SAMPLE_RATE / 1000 * FRAME_DURATION

#define FULL_SAMPLE_RATE 48000
#define FULL_FRAME_SIZE FULL_SAMPLE_RATE / 1000 * FRAME_DURATION

#define AMBE_RATE_P25_PHASE1 "0x0558,0x086b,0x1030,0x0000,0x0000,0x0190"
#define AMBE_RATE_P25_PHASE2 "33"

using namespace std;

enum class Codec { L16 = 1, G711u, G711a, GSM, AMRNB, AMRWB, AMBE, OPUS, CODEC2 };
enum class Operation { ENCODE = 1, DECODE };

enum class DecoderFlags { PASS_LOST_FRAMES = 1 << 0 };


static void swap(int16_t* dst, const int16_t* src, size_t count) {
	for(uint i = 0; i < count; i++) dst[i] = bswap_16(src[i]);
}


typedef array<int16_t, NB_FRAME_SIZE>   NarrowAudioFrame;
typedef array<int16_t, WB_FRAME_SIZE>   WideAudioFrame;
typedef array<int16_t, FULL_FRAME_SIZE> FullAudioFrame;


// A single frame of (encoded) bits. These bits represent a single BitFrame.
class BitFrame {
	string buffer;

public:
	uint count;

	static uint byteLength(uint count) {
		return count / 8 + (count % 8 > 0);
	}

	BitFrame(const char* data, uint count) :
		buffer(data, byteLength(count)), count(count) {}

	BitFrame() : buffer(), count(0) {}

	const char* data() const {
		return buffer.data();
	}
};


template <class FRAME, int RATE>
static deque<FRAME> loadAudio(const string& filename) {
	size_t n = 0;
	deque<FRAME> rv;
	const int mask = SF_FORMAT_WAV | SF_FORMAT_PCM_16;

	SF_INFO info;
	auto handle = sf_open(filename.c_str(), SFM_READ, &info);
	if (!handle) {
		cerr << "Error: Could not open file for reading" << endl;
		goto error;
	}

	if (info.samplerate != RATE) {
		cerr << "Error: Invalid sample rate, expected " << RATE
		     << ", got " << info.samplerate << endl;
		goto error;
	}

	if (info.channels != 1) {
		cerr << "Error: Invalid number of channels, expected 1, got " << info.channels << endl;
		goto error;
	}

	if ((info.format & mask) != mask) {
		cerr << "Error: Only S16LE sample format is supported" << endl;
		goto error;
	}

	while(true) {
		FRAME f;
		f.fill(0);
		n = sf_read_short(handle, f.data(), f.size());
		if (n < 0) {
			cerr << "Error while reading from file: " << sf_strerror(handle) << endl;
			goto error;
		}

		rv.push_back(move(f));
		if (n < f.size()) break;
	}

	sf_close(handle);
	return rv;

error:
	if (handle) sf_close(handle);
	std::exit(EXIT_FAILURE);
}


static deque<BitFrame> loadBits(const string& filename) {
	deque<BitFrame> rv;
	uint16_t count;

	ifstream file(filename, std::ifstream::binary);
	if (!file) {
		cerr << "Error: Cannot open file " << filename << endl;
		std::exit(EXIT_FAILURE);
	}

	while(true) {
		file.read((char*)&count, sizeof(count));
		if (file.eof()) break;
		if (file.fail()) {
			cerr << "Invalid file format: " << filename << endl;
			goto error;
		}
		count = ntohs(count);
		auto bytes = BitFrame::byteLength(count);
		char buffer[bytes];

		if (count > 0) {
			file.read(buffer, bytes);
			if (!file.good()) {
				cerr << "Error while reading file " << filename << endl;
				goto error;
			}
		}

		rv.push_back(BitFrame(buffer, count));
	}

	return rv;
error:
	file.close();
	std::exit(EXIT_FAILURE);
}


// Save given audio data to .wav file "filename".
template <class FRAME, int RATE>
static void save(const string& filename, const deque<FRAME>& data) {
	SF_INFO info;
	SNDFILE* handle = NULL;

	info.samplerate = RATE;
	info.channels = 1;
	info.format = SF_FORMAT_WAV | SF_FORMAT_PCM_16;

	if (!sf_format_check(&info)) {
		cerr << "BUG: Format not supported by libsndfile" << endl;
		goto error;
	}

	handle = sf_open(filename.c_str(), SFM_WRITE, &info);
	if (!handle) {
		cerr << "Error: Could not open file for writing" << endl;
		goto error;
	}

	for(const auto& frame : data) {
		if (sf_write_short(handle, frame.data(), frame.size()) != (ssize_t)frame.size()) {
			cerr << "Error while writing to file: " << sf_strerror(handle) << endl;
			goto error;
		}
	}
	sf_close(handle);
	return;
error:
	if (handle) sf_close(handle);
	std::exit(EXIT_FAILURE);
}


// Save raw bit data to a file. Each frame starts with two bytes in network byte
// order that contain the number of bits within the frame. The frame data is
// written in the smallest number of bytes that can store all the bits.
static void save(const string& filename, const deque<BitFrame>& data) {
	if (!data.size()) {
		cerr << "Bug: Bits input data is empty" << endl;
		std::exit(EXIT_FAILURE);
	}

	auto file = fstream(filename, std::ios::out | std::ios::binary);
	if (!file) {
		cerr << "Couldn't open file " << filename << endl;
		std::exit(EXIT_FAILURE);
	}

	for(const auto& frame : data) {
		auto bits = htons(frame.count);
		file.write((char*)&bits, sizeof(bits));
		if (!file.good()) goto error;

		if (frame.count > 0) {
			file.write(frame.data(), BitFrame::byteLength(frame.count));
			if (!file.good()) goto error;
		}
	}
	file.close();
	return;

error:
	cerr << "Error while writing to file " << filename << endl;
	file.close();
	std::exit(EXIT_FAILURE);
}


static void help() {
	cout <<
	"Usage: codec [options]\n"
	"Options:\n"
	"  -c <name>       CODEC to use (l16,g711u,g711a,gsm,amrnb,amrwb,p25p1,p25p2,opus)\n"
	"  -O <operation>  Operation to perform (encode,decode)\n"
	"  -i <filename>   Input file\n"
	"  -o <filename>   Output file\n"
	"  -a <URI>        AMBE device URI\n"
	"  -h              This help text\n" << endl;

	std::exit(EXIT_FAILURE);
}


static BitFrame encodeL16(void* state, const NarrowAudioFrame& input) {
	auto src = input.data();
	int16_t dst[input.size()];

	swap(dst, src, input.size());
	return BitFrame((char*)dst, sizeof(dst) * 8);
}


static NarrowAudioFrame decodeL16(void* state, const BitFrame& input) {
	auto bytes = BitFrame::byteLength(input.count);
	if ((input.count % 8) || bytes != sizeof(NarrowAudioFrame))
		throw runtime_error("Invalid BitFrame size for L16");

	NarrowAudioFrame f;
	swap(f.data(), (int16_t*)input.data(), bytes / 2);
	return f;
}


static BitFrame encodeG711u(void* state, const NarrowAudioFrame& input) {
	auto data = input.data();
	auto samples = input.size();

	uint8_t pcmu[samples];
	for(uint i = 0; i < samples; i++)
		pcmu[i] = g711_pcm2ulaw(data[i]);

	return BitFrame((char*)pcmu, samples * 8);
}


static NarrowAudioFrame decodeG711u(void* state, const BitFrame& input) {
	auto samples = input.count / 8;
	NarrowAudioFrame f;

	if (samples > f.size())
		throw runtime_error("Got G711u frame that is too big");

	auto inp = input.data();
	auto outp = f.data();
	for(uint i = 0; i < samples; i++)
		outp[i] = g711_ulaw2pcm(inp[i]);

	return f;
}


static BitFrame encodeG711a(void* state, const NarrowAudioFrame& input) {
	auto data = input.data();
	auto samples = input.size();

	uint8_t pcma[samples];
	for(uint i = 0; i < samples; i++)
		pcma[i] = g711_pcm2alaw(data[i]);

	return BitFrame((char*)pcma, samples * 8);
}


static NarrowAudioFrame decodeG711a(void* state, const BitFrame& input) {
	auto samples = input.count / 8;
	NarrowAudioFrame f;

	if (samples > f.size())
		throw runtime_error("Got g711a frame that is too big");

	auto inp = input.data();
	auto outp = f.data();
	for(uint i = 0; i < samples; i++)
		outp[i] = g711_alaw2pcm(inp[i]);

	return f;
}


static BitFrame encodeGSM(gsm state, const NarrowAudioFrame& input) {
	// libgsm expects 8000 Hz, S16LE, 20ms frames
	if (input.size() != 160)
		throw runtime_error("Audio frame size does not match GSM frame size");

	static gsm_byte buf[33];
	gsm_encode(state, (gsm_signal*)input.data(), buf);
	return BitFrame((char*)buf, sizeof(buf) * 8);
}


static NarrowAudioFrame decodeGSM(gsm state, const BitFrame& input) {
	if (input.count / 8 != 33)
		throw runtime_error("Got GSM frame with invalid size");

	NarrowAudioFrame f;
	if (gsm_decode(state, (gsm_byte*)input.data(), f.data()))
		memset(f.data(), 0, sizeof(f));
	return f;
}


static BitFrame encodeAMRNB(void* state, const NarrowAudioFrame& input) {
	if (input.size() != 160)
		throw runtime_error("Audio frame size does not match AMR-NB frame size");

	unsigned char buf[61];
	buf[0] = 15 << 4; // No mode request is present

	int len = Encoder_Interface_Encode(state, MR122, input.data(), buf + 1, 0);
	if (len <= 0)
		throw runtime_error("AMR-NB encoding failed");

	return BitFrame((char*)buf, (len + 1) * 8);
}


static NarrowAudioFrame decodeAMRNB(void* state, const BitFrame& input) {
	if ((input.count / 8) > 61)
		throw runtime_error("Bit frame is incompatible with AMR-NB");

	NarrowAudioFrame f;
	Decoder_Interface_Decode(state, (unsigned char*)(input.data() + 1), f.data(), 0);
	return f;
}


static BitFrame encodeAMRWB(void* state, const WideAudioFrame& input) {
	if (input.size() != 320)
		throw runtime_error("Audio frame size does not match AMR-WB frame size");

	unsigned char buf[62];
	buf[0] = 15 << 4; // No mode request is present

	int len = E_IF_encode(state, 8, input.data(), &buf[1], 0);
	if (len <= 0)
		throw runtime_error("AMR-WB encoding failed");

	return BitFrame((char*)buf, (len + 1) * 8);
}


static WideAudioFrame decodeAMRWB(void* state, const BitFrame& input) {
	if ((input.count / 8) > 62)
		throw runtime_error("Bit frame is incompatible with AMR-WB");

	WideAudioFrame f;
	D_IF_decode(state, (unsigned char*)(input.data() + 1), f.data(), 0);
	return f;
}


static BitFrame encodeAMBE(void* state, const NarrowAudioFrame& input) {
	static char buf[320];
	size_t count = sizeof(buf);
	if (ambe_compress(buf, &count, state, input.data(), input.size()))
		throw runtime_error("AMBE encoding failed");
	return BitFrame(buf, count);
}


static NarrowAudioFrame decodeAMBE(void* state, const BitFrame& input) {
	NarrowAudioFrame f;
	size_t count=f.size();
	if (ambe_decompress(f.data(), &count, state, input.data(), input.count))
		throw runtime_error("AMBE decoding failed");
	return f;
}


static BitFrame encodeOpus(OpusEncoder* handle, const FullAudioFrame& input) {
	static unsigned char buf[FULL_SAMPLE_RATE * 2];
	size_t size = sizeof(buf);

	auto len = opus_encode(handle, input.data(), input.size(), buf, size);
	if (len < 0)
		throw runtime_error("Opus encoding failed");
	return BitFrame((char*)buf, len * 8);
}


static FullAudioFrame decodeOpus(OpusDecoder* handle, const BitFrame& input) {
	// Allocate buffer big enough for Opus maximum frame size (120ms). We'll
	// only take the first 20ms and ignore the rest.
	FullAudioFrame f[6];

	auto rv = opus_decode(handle, input.count ? (unsigned char*)input.data() : NULL, input.count / 8, f[0].data(), f[0].size() * 6, 0);
	if (rv == 0 || rv == OPUS_INVALID_PACKET) {
		// We either decoded no samples or the encoded packet was corrupted.
		// Fill the audio frame with silence and return it.
		memset(f[0].data(), 0, sizeof(f));
	} else if (rv < 0) {
		throw runtime_error("Opus decoding failed");
	}
	return f[0];
}


static BitFrame encodeCODEC2(struct CODEC2* handle, const NarrowAudioFrame& input) {
	auto bytes = (codec2_bits_per_frame(handle) + 7) / 8;

	if (input.size() != codec2_samples_per_frame(handle))
		throw runtime_error("Invalid audio frame size for CODEC2");

	unsigned char buf[bytes];
	codec2_encode(handle, buf, (short*)input.data());

	return BitFrame((char*)buf, bytes * 8);
}


static NarrowAudioFrame decodeCODEC2(struct CODEC2* handle, const BitFrame& input) {
	auto bytes = (codec2_bits_per_frame(handle) + 7) / 8;

	if (input.count / 8 < (uint)bytes)
		throw runtime_error("Invalid CODEC2 bit frame");

	NarrowAudioFrame f;
	codec2_decode(handle, f.data(), (unsigned char*)input.data());
	return f;
}


template <typename FRAME, int RATE, typename CODEC, typename STATE>
static void encode(const string& output, const string& input, CODEC codec, STATE state) {
	const auto audio = loadAudio<FRAME,RATE>(input);
	deque<BitFrame> bits;

	for(auto f = audio.begin(); f != audio.end(); f++)
		bits.push_back(codec(state, *f));

	save(output, bits);
}


template <typename FRAME, int RATE, typename CODEC, typename STATE>
static void decode(const string& output, const string& input, CODEC codec, STATE state, int flags) {
	const auto bits = loadBits(input);
	deque<FRAME> audio;
	FRAME dst;

	for(auto f = bits.begin(); f != bits.end(); f++) {
		// An empty frame indicates a lost frame. Unless the decoder indicated
		// that it wants to process lost frames, generate a frame of silence.
		if (f->count == 0 && !(flags & (int)DecoderFlags::PASS_LOST_FRAMES))
			memset(dst.data(), 0, sizeof(dst));
		else
			dst = codec(state, *f);

		audio.push_back(dst);
	}

	save<FRAME,RATE>(output, audio);
}


template <typename FRAME, int RATE, typename CODEC, typename STATE>
static void decode(const string& output, const string& input, CODEC codec, STATE state) {
	return decode<FRAME,RATE>(output, input, codec, state, 0);
}



int main(int argc, char* argv[]) {
	int opt = 0, codec = 0, operation = 0;
	string input, output, ambe_uri, ambe_rate;

	while ((opt = getopt(argc, argv, "a:c:O:i:o:h")) != -1) {
		switch (opt) {
		case 'c':
			if (!strcmp(optarg, "l16"))         codec = (int)Codec::L16;
			else if (!strcmp(optarg, "g711u"))  codec = (int)Codec::G711u;
			else if (!strcmp(optarg, "g711a"))  codec = (int)Codec::G711a;
			else if (!strcmp(optarg, "gsm"))    codec = (int)Codec::GSM;
			else if (!strcmp(optarg, "amrnb"))  codec = (int)Codec::AMRNB;
			else if (!strcmp(optarg, "amrwb"))  codec = (int)Codec::AMRWB;
			else if (!strcmp(optarg, "opus"))   codec = (int)Codec::OPUS;
			else if (!strcmp(optarg, "codec2")) codec = (int)Codec::CODEC2;
			else if (!strcmp(optarg, "p25p1")) {
				codec = (int)Codec::AMBE;
				ambe_rate = AMBE_RATE_P25_PHASE1;
			} else if (!strcmp(optarg, "p25p2")) {
				codec = (int)Codec::AMBE;
				ambe_rate = AMBE_RATE_P25_PHASE1;
			} else {
				cerr << "Unsupported codec name " << optarg << endl;
				std::exit(EXIT_FAILURE);
			}
			break;

		case 'O':
			if (!strcmp(optarg, "encode"))      operation = (int)Operation::ENCODE;
			else if (!strcmp(optarg, "decode")) operation = (int)Operation::DECODE;
			else {
				cerr << "Unsupported operation " << optarg << endl;
				std::exit(EXIT_FAILURE);
			}
			break;

		case 'i': input = string(optarg);    break;
		case 'o': output = string(optarg);   break;
		case 'a': ambe_uri = string(optarg); break;
		case 'h': help();                    break;
		}
	}

	if (input.length() == 0) {
		cerr << "Please configure input filename" << endl;
		std::exit(EXIT_FAILURE);
	}

	if (output.length() == 0) {
		cerr << "Please configure output filename" << endl;
		std::exit(EXIT_FAILURE);
	}

	gsm gsm;
	void* handle;
	OpusEncoder* opusEnc;
	OpusDecoder* opusDec;
	int error;
	struct CODEC2* c2;

	switch((Operation)operation) {
	case Operation::ENCODE:
		switch((Codec)codec) {
		case Codec::L16:
			encode<NarrowAudioFrame, NB_SAMPLE_RATE>(output, input, encodeL16, nullptr);
			break;

		case Codec::G711u:
			encode<NarrowAudioFrame, NB_SAMPLE_RATE>(output, input, encodeG711u, nullptr);
			break;

		case Codec::G711a:
			encode<NarrowAudioFrame, NB_SAMPLE_RATE>(output, input, encodeG711a, nullptr);
			break;

		case Codec::GSM:
			gsm = gsm_create();
			encode<NarrowAudioFrame, NB_SAMPLE_RATE>(output, input, encodeGSM, gsm);
			gsm_destroy(gsm);
			break;

		case Codec::AMBE:
			if (ambe_uri.length() == 0) {
				cerr << "Please configure AMBE device URI" << endl;
				std::exit(EXIT_FAILURE);
			}
			handle = ambe_open(ambe_uri.c_str(), ambe_rate.c_str(), 1000);
			if (handle == NULL)
				throw runtime_error("Couldn't open AMBE device");

			encode<NarrowAudioFrame, NB_SAMPLE_RATE>(output, input, encodeAMBE, handle);
			ambe_close(handle);
			break;

		case Codec::AMRNB:
			handle = Encoder_Interface_init(0);
			encode<NarrowAudioFrame, NB_SAMPLE_RATE>(output, input, encodeAMRNB, handle);
			Encoder_Interface_exit(handle);
			break;

		case Codec::AMRWB:
			handle = E_IF_init();
			encode<WideAudioFrame, WB_SAMPLE_RATE>(output, input, encodeAMRWB, handle);
			E_IF_exit(handle);
			break;

		case Codec::OPUS:
			opusEnc = opus_encoder_create(FULL_SAMPLE_RATE, 1, OPUS_APPLICATION_VOIP, &error);
			if (!opusEnc) throw runtime_error("Couldn't initialize Opus encoder");
			encode<FullAudioFrame, FULL_SAMPLE_RATE>(output, input, encodeOpus, opusEnc);
			opus_encoder_destroy(opusEnc);
			break;

		case Codec::CODEC2:
			c2 = codec2_create(CODEC2_MODE_2400);
			if (!c2) throw runtime_error("Could not initialize CODEC2 encoder");
			encode<NarrowAudioFrame,NB_SAMPLE_RATE>(output, input, encodeCODEC2, c2);
			codec2_destroy(c2);
			break;
		}
		break;

	case Operation::DECODE:
		switch((Codec)codec) {
		case Codec::L16:
			decode<NarrowAudioFrame, NB_SAMPLE_RATE>(output, input, decodeL16, nullptr);
			break;

		case Codec::G711u:
			decode<NarrowAudioFrame, NB_SAMPLE_RATE>(output, input, decodeG711u, nullptr);
			break;

		case Codec::G711a:
			decode<NarrowAudioFrame, NB_SAMPLE_RATE>(output, input, decodeG711a, nullptr);
			break;

		case Codec::GSM:
			gsm = gsm_create();
			decode<NarrowAudioFrame, 8000>(output, input, decodeGSM, gsm);
			gsm_destroy(gsm);
			break;

		case Codec::AMBE:
			if (ambe_uri.length() == 0) {
				cerr << "Please configure AMBE device URI" << endl;
				std::exit(EXIT_FAILURE);
			}
			handle = ambe_open(ambe_uri.c_str(), ambe_rate.c_str(), 1000);
			if (handle == NULL)
				throw runtime_error("Couldn't open AMBE device");

			decode<NarrowAudioFrame, 8000>(output, input, decodeAMBE, handle);
			ambe_close(handle);
			break;

		case Codec::AMRNB:
			handle = Decoder_Interface_init();
			decode<NarrowAudioFrame, 8000>(output, input, decodeAMRNB, handle);
			Decoder_Interface_exit(handle);
			break;

		case Codec::AMRWB:
			handle = D_IF_init();
			decode<WideAudioFrame, 16000>(output, input, decodeAMRWB, handle);
			D_IF_exit(handle);
			break;

		case Codec::OPUS:
			opusDec = opus_decoder_create(FULL_SAMPLE_RATE, 1, &error);
			if (!opusDec) throw runtime_error("Couldn't initialize Opus decoder");
			decode<FullAudioFrame, FULL_SAMPLE_RATE>(output, input, decodeOpus, opusDec, (int)DecoderFlags::PASS_LOST_FRAMES);
			opus_decoder_destroy(opusDec);
			break;

		case Codec::CODEC2:
			c2 = codec2_create(CODEC2_MODE_2400);
			if (!c2) throw runtime_error("Could not initialize CODEC2 decoder");
			decode<NarrowAudioFrame,NB_SAMPLE_RATE>(output, input, decodeCODEC2, c2);
			codec2_destroy(c2);
			break;
		}
		break;
	}
}
