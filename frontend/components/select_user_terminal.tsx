import React, { useState } from 'react';
import PropTypes from 'prop-types';
import {
    TextField,
    MenuItem
} from '@material-ui/core';


/*
 * This component represents the user terminal menu to select. We currently have three
 * user terminals: UT1, UT2 and UT3. Interactive experiments involve test subjects and
 * dispatcher interacting with physical user terminals and the terminal name will be
 * selected by the experimenter when starts the experiment, and it will be stored in the database. 
 */
export function SelectUserTerminal({ onUpdate, type, ...props }) {
    const [ value, setValue ] = useState<string>(type === 'Subject' ? 'ut2' : 'ut1');

    const updateValue = (e: any) => {
        setValue(e.target.value);
        onUpdate(e.target.value);
    };

    return (
        <TextField
            fullWidth
            select
            required
            id="user-terminal-name"
            label={`${type} User Terminal`}
            value={value}
            onChange={e => updateValue(e)}
            {...props}
        >
            <MenuItem value="ut1">User Terminal 1</MenuItem>
            <MenuItem value="ut2">User Terminal 2</MenuItem>
            <MenuItem value="ut3">User Terminal 3</MenuItem>
        </TextField>
    );

}

SelectUserTerminal.propTypes = {
    onUpdate : PropTypes.func.isRequired,
    type     : PropTypes.string.isRequired
};
