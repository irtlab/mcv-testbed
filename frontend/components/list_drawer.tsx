import React from 'react';
import PropTypes from 'prop-types';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import {
    Button,
    Drawer,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    IconButton,
    Hidden,
    List,
    ListItem,
    ListItemText,
    ListItemSecondaryAction,
    makeStyles
} from '@material-ui/core';
import { DialogTitle } from './dialog_title';


const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
    },
    drawerPaper: {
        width: (props: any) => props.editListItem ? 300 : 240,
        top: 49,
    },
    drawer: {
        [theme.breakpoints.up('sm')]: {
            width: (props: any) => props.editListItem ? 300 : 240,
            flexShrink: 0,
        },
    }
}));


function DeleteOrEditListItem(props) {
    const { data, itemSelectedId, deleteListItem, setSelectedId, editListItem } = props;
    const [anchorEl, setAnchorEl] = React.useState<any>(null);
    const open = Boolean(anchorEl);

    const handleDialogNoClose = () => {
        setAnchorEl(null);
    };

    const handleDialogYesClose = () => {
        deleteListItem(data);
        setAnchorEl(null);
        if (itemSelectedId === data._id) setSelectedId('');
    };

    return (
        <div>
            {
                editListItem ? (
                    <IconButton
                        aria-label="delete"
                        aria-haspopup="true"
                        onClick={() => editListItem(data._id)}
                    >
                        <EditIcon />
                    </IconButton>
                ) : null
            }
            <IconButton
                aria-label="delete"
                aria-haspopup="true"
                onClick={(event) => {setAnchorEl(event.currentTarget);}}
            >
                <DeleteIcon />
            </IconButton>
            <Dialog
                open={open}
                onClose={handleDialogNoClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">
                    Delete
                    {' '}
                    {data.name}
                </DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        Are you sure you want to permanently delete this data?
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleDialogNoClose} color="primary">
                        No
                    </Button>
                    <Button onClick={handleDialogYesClose} color="primary" autoFocus>
                        Yes
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}

DeleteOrEditListItem.defaultProps = {
    itemSelectedId: '',
    editListItem: undefined
};

DeleteOrEditListItem.propTypes = {
    data: PropTypes.object.isRequired,
    itemSelectedId: PropTypes.string,
    deleteListItem: PropTypes.func.isRequired,
    setSelectedId: PropTypes.func.isRequired,
    editListItem: PropTypes.func
};


function NavigationDrawer(props) {
    const { listItems, itemSelectedId, deleteListItem, setSelectedId, editListItem } = props;

    const handleItemClick = (data) => {
        setSelectedId(data._id);
    };

    return (
        <div  style={{marginBottom: '50px'}}>
            <List>
                {listItems.length > 0 ? listItems.map(data => (
                    <ListItem
                        button
                        selected={itemSelectedId === data._id}
                        key={data._id}
                        onClick={() => handleItemClick(data)}
                    >
                        <ListItemText primary={data.name} />
                        <ListItemSecondaryAction>
                            <DeleteOrEditListItem
                                data={data}
                                itemSelectedId={itemSelectedId}
                                deleteListItem={deleteListItem}
                                setSelectedId={setSelectedId}
                                editListItem={editListItem}
                            />
                        </ListItemSecondaryAction>
                    </ListItem>
                )) : 'No data.'}
            </List>
        </div>
    );
}

NavigationDrawer.defaultProps = {
    itemSelectedId: '',
    editListItem: undefined
};

NavigationDrawer.propTypes = {
    listItems: PropTypes.array.isRequired,
    itemSelectedId: PropTypes.string,
    deleteListItem: PropTypes.func.isRequired,
    setSelectedId: PropTypes.func.isRequired,
    editListItem: PropTypes.func
};


export function ListDrawer(props) {
    const classes = useStyles(props);
    const { selectedId, setSelectedId, listItems, deleteListItem, editListItem } = props;

    return (
        <div className={classes.root}>
            <nav className={classes.drawer}>
                <Hidden xsDown implementation="css">
                    <Drawer
                        classes={{paper: classes.drawerPaper}}
                        variant="permanent"
                        open
                    >
                        <NavigationDrawer
                            listItems={listItems}
                            itemSelectedId={selectedId}
                            deleteListItem={deleteListItem}
                            setSelectedId={setSelectedId}
                            editListItem={editListItem}
                        />
                    </Drawer>
                </Hidden>
            </nav>
        </div>
    );
}

ListDrawer.defaultProps = {
    selectedId: '',
    editListItem: undefined
};

ListDrawer.propTypes = {
    selectedId: PropTypes.string,
    setSelectedId: PropTypes.func.isRequired,
    listItems: PropTypes.array.isRequired,
    deleteListItem: PropTypes.func.isRequired,
    editListItem: PropTypes.func
};
