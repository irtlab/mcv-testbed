import React from 'react';
import PropTypes from 'prop-types';
import copy from 'copy-to-clipboard';
import FileCopyIcon from '@material-ui/icons/FileCopy';
import {
    IconButton,
    InputAdornment,
    OutlinedInput,
    Tooltip,
    FormControl,
    InputLabel
} from '@material-ui/core';


export default function CopyValue({ value, label }) {
    return (
        <FormControl fullWidth variant="outlined">
            <InputLabel htmlFor='display-name'>{label}</InputLabel>
            <OutlinedInput
                fullWidth
                id="outlined-experiment-run-link"
                disabled
                labelWidth={0}
                label={label}
                type='text'
                value={value}
                endAdornment={(
                    <InputAdornment position="end">
                        <Tooltip title="Copy link">
                            <IconButton
                                aria-label="copy"
                                onClick={() => copy(value)}
                                edge="end"
                            >
                                <FileCopyIcon />
                            </IconButton>
                        </Tooltip>
                    </InputAdornment>
                )}
            />
        </FormControl>
    );
}

CopyValue.propTypes = {
    value : PropTypes.string.isRequired,
    label : PropTypes.string.isRequired
};
