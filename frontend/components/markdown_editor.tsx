import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { useSnackbar } from 'notistack';
import axios from 'axios';
import ReactMarkdown from 'react-markdown';
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';
import CodeMirror from 'react-codemirror';
import "codemirror/lib/codemirror.css";
import "codemirror/mode/markdown/markdown";
import {
    makeStyles,
    Button,
    Container,
    Grid,
    Typography
} from '@material-ui/core';
import { useNavigate, useParams } from "react-router-dom";
import { SpinnerPage } from './spinner_page';
import { createAlert, getBackendURL } from '../utils';
import { axiosAuth } from '../auth';

const api = axios.create({baseURL: `${getBackendURL()}api`, withCredentials: true});
api.interceptors.response.use(null as any, axiosAuth);

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
    },
    content: {
        flexGrow: 1
    },
    fab: {
        margin: theme.spacing(1),
        position: "fixed",
        bottom: theme.spacing(2),
        right: theme.spacing(3)
    },
    fabEdit: {
        margin: theme.spacing(1),
        position: "fixed",
        bottom: theme.spacing(8),
        right: theme.spacing(3)
    },
    distance: {
        padding: theme.spacing(3)
    },
    paper: {
        padding: theme.spacing(1),
        color: theme.palette.text.secondary,
    },
    markdownResult: {
        position: 'fixed',
        top: '119px',
        right: '0',
        left: '49.4%',
        bottom: '2px',
        overflow: 'auto',
        padding: '10px',
        paddingLeft: '12px',
        color: '#444',
        fontSize: '14px',
        lineHeight: '1.5em',
        borderLeft: 'ridge',
        textAlign: 'center'
    },
    editor: {
        position: 'fixed',
        top: '119px',
        left: '0',
        bottom: '2px',
        width: '50%',
        padding: '0px'
    }
}));


export function Editor(props) {
    const { markdownSrc, setMarkdownSrc } = props;
    const options = {
        mode: 'markdown',
        lineNumbers: true
    };

    const handleMarkdownChange = (value) => {
        setMarkdownSrc(value);
    };

    const editorRefCallback = (ref) => {
        if (ref) {
            const cm = ref.getCodeMirror();
            cm.setSize(null, "100%");
        }
    };

    return (
        <div style={{height: "100%", maxWidth: '99%', overflowY: "scroll"}}>
            <CodeMirror
                ref={editorRefCallback}
                value={markdownSrc}
                onChange={handleMarkdownChange}
                options={options}
            />
        </div>
    );
}

Editor.defaultProps = {
    markdownSrc: ''
};

Editor.propTypes = {
    markdownSrc: PropTypes.string,
    setMarkdownSrc: PropTypes.func.isRequired
};


export function MarkdownEditorView() {
    const classes = useStyles();
    const params = useParams();
    const navigate = useNavigate();
    const alert = createAlert(useSnackbar().enqueueSnackbar);
    const [ markdownSrc, setMarkdownSrc ] = useState('');
    const [ data, setData ] = useState<any | null>(null);
    const [ load, setLoad ] = useState(true);

    async function updateData(_id, attrs) {
        try {
            const rv = await api.post(`/experiment/${_id}`, attrs);
            setData(rv.data);
        } catch (error) {
            alert('Error while updating experiment', error);
        }
    }

    async function fetchData(paramsId) {
        try {
            const rv = await api.get(`/experiment/${paramsId}`);
            setData(rv.data);
            if (rv.data.finish_notes) {
                setMarkdownSrc(rv.data.finish_notes);
            } else {
                const default_text = 'Thank you for completing the experiment!';
                setMarkdownSrc(default_text);
                await updateData(rv.data._id, {finish_notes: default_text});
            }
        } catch (error) {
            alert('Error while loading experiment', error);
        }
    }

    const disableSaveBtn = () => {
        if (!data) return true;
        if (markdownSrc === data.finish_notes) return true;
        return false;
    };

    async function update() {
        await updateData(data._id, {finish_notes: markdownSrc});
        navigate(`/experiments/${data._id}`);
    }

    useEffect(() => {
        void (async () => {
            if (params.id) await fetchData(params.id);
            setLoad(false);
        })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [params]);

    return (
        <div>
            {load ? <SpinnerPage text='Loading data. Please wait...' /> :
                (
                    <Grid container spacing={0}>
                        <Grid item xs={12} style={{ height: "80%" }}>
                            <div style={{ width: "100%", height: "70px"}}>
                                <div style={{ float: "left", marginLeft: "10px" }}>
                                    <br />
                                    <Typography variant="h6">
                                        {
                                            data ? (
                                                `Final page for experiment ${data.name}`
                                            ) : (
                                                'Data not found'
                                            )
                                        }
                                    </Typography>
                                </div>
                                <div style={{ float: "right" }}>
                                    <Container style={{textAlign: "end"}}>
                                        <Button
                                            style={{paddingTop: "30px"}}
                                            onClick={() => navigate(`/experiments/${data._id}`)}
                                            color="primary"
                                        >
                                        Cancel
                                        </Button>
                                        <Button
                                            disabled={0 || disableSaveBtn()}
                                            style={{marginLeft: "10px", paddingTop: "30px"}}
                                            onClick={update}
                                            color="primary"
                                        >
                                        Save
                                        </Button>
                                    </Container>
                                </div>
                            </div>
                        </Grid>
                        <Grid item xs={6}>
                            <div className={classes.editor}>
                                <Editor
                                    markdownSrc={markdownSrc}
                                    setMarkdownSrc={setMarkdownSrc}
                                />
                            </div>
                            <div className={classes.markdownResult}>
                                <CheckCircleOutlineIcon style={{color: "green", fontSize: "100px"}} />
                                <ReactMarkdown>
                                    {markdownSrc}
                                </ReactMarkdown>
                            </div>
                        </Grid>
                    </Grid>
                )}
        </div>
    );
}
