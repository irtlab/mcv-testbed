import React from 'react';
import PropTypes from 'prop-types';
import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline';
import { CircularProgress, Grid, Typography } from '@material-ui/core';

export function SpinnerPage(props) {
    const { text, height } = props;

    return (
        <div>
            <Grid
                container
                spacing={0}
                direction="column"
                alignItems="center"
                justifyContent="center"
                style={{ minHeight: height }}
            >
                <Grid item xs={3}>
                    {
                        text === 'Something went wrong. Please try again.' ? (
                            <ErrorOutlineIcon style={{color: "red", fontSize: "100px"}} />
                        ) : (
                            <CircularProgress />
                        )
                    }
                </Grid>
                <Grid item xs={3}>
                    <Typography variant="subtitle1" gutterBottom>
                        {text}
                    </Typography>
                </Grid>
            </Grid>
        </div>
    );
}

SpinnerPage.defaultProps = {
    text: undefined,
    height: '100vh'
};

SpinnerPage.propTypes = {
    text: PropTypes.string,
    height: PropTypes.string
};
