import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useSnackbar } from 'notistack';
import {
    Button,
    Grid,
    Typography,
    CircularProgress
} from '@material-ui/core';
import { createAlert } from '../utils';


function getMinDiff(startDate, endDate, minuteInterval) {
    const msInMinute = minuteInterval * (60 * 1000);
    return Math.round(Math.abs(endDate - startDate) / msInMinute);
}


// Collect UT data interval by interval, otherwise it may throw an exception (mostly timeout).
// I set the interval time 1 minute as during the testing it was the reasonable number to set and not to have timout.
export async function collectUTData(api, from, to, setProgress) {
    const interval = 1;
    let startDate = new Date(typeof from === 'string' ? from : from.toString());
    let endDate = new Date(typeof to === 'string' ? to : to.toString());
    endDate.setTime(endDate.getTime() + (interval * 60 * 1000));

    const totalSteps = getMinDiff(startDate, endDate, interval) + 1;

    let progressPct = 0;
    let progressStep = 1;
    while (startDate < endDate) {
        let start = startDate.toISOString();
        startDate.setTime(startDate.getTime() + (interval * 60 * 1000));
        let until = startDate.toISOString();
        await api.post('events/collect', {}, {params: {from: start, until: until}});
        progressPct = Math.floor((progressStep / totalSteps) * 100);
        if (setProgress) setProgress(progressPct);
        progressStep += 1;
    }
}


// FIXME: In material ui 5.0 version I should change the CircularProgress to LoadingButton.
//
// This component is designed for a unique case. If the experimenter presses the FINISH EXPERIMENT button and it fails,
// or it fails for any other reasons, then the RUN table will show the "Click to Collect UT Data" which is the following
// functionality.
export function CollectTerminalData({ exprRunID, setExprRun, api, start, end }) {
    const alert = createAlert(useSnackbar().enqueueSnackbar);
    const [ processing, setProcessing ] = useState(false);
    const [ progressValue, setProgressValue ] = useState(0);

    const startCollecting = async () => {
        setProcessing(true);
        try {
            if (exprRunID) {
                await collectUTData(api, start, end, setProgressValue);
                const rv = await api.post(`/experiment_run/${exprRunID}`, {status: 'Finished'});
                setExprRun(rv.data);
            } else {
                await api.post('events/collect');
            }
        } catch (error) {
            alert('Error while collecting user terminal events:', error);
        }
        setProcessing(false);
    };

    return (
        <>
            {
                processing ? (
                    <>
                        <Grid xs={12} container justify = "center">
                            <CircularProgress size={20} />
                        </Grid>
                        {
                            exprRunID ? (
                                <Grid xs={2}>
                                  {`${progressValue}%`}
                                  {''}
                                </Grid>
                            ) : (
                                <Grid xs={12} container justify = "center">
                                    <Typography variant="subtitle1">Please wait. Collecting events may take longer</Typography>
                                </Grid>
                            )
                        }
                    </>
                ) : (
                    <Button
                        size="medium"
                        variant="outlined"
                        fullWidth
                        onClick={startCollecting}
                    >
                        Collect Data
                    </Button>
                )
            }
        </>
    );
}

CollectTerminalData.defaultProps = {
    exprRunID  : null,
    setExprRun : null,
    start      : null,
    end        : null
};

CollectTerminalData.propTypes = {
    exprRunID  : PropTypes.any.isRequired,
    setExprRun : PropTypes.func.isRequired,
    api        : PropTypes.any.isRequired,
    start      : PropTypes.any.isRequired,
    end        : PropTypes.any.isRequired
};
