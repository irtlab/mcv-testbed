import React from 'react';
import PropTypes from 'prop-types';
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';
import CancelIcon from '@material-ui/icons/Cancel';
import { Grid, Typography, Button } from '@material-ui/core';


export function ExperimentCompleted({ completed, authorText, amtSurveyCode }) {
    return (
        <div>
            <Grid
                container
                spacing={2}
                direction="column"
                alignItems="center"
                justifyContent="center"
                style={{ minHeight: '100vh' }}
            >
                <Grid item xs={12} justifyContent="center" container>
                    {
                        completed === 'done' ? (
                            <CheckCircleOutlineIcon style={{color: "green", fontSize: "100px"}} />
                        ) : (
                            <CancelIcon style={{color: "red", fontSize: "100px"}} />
                        )
                    }
                </Grid>
                <Grid item xs={12} justifyContent="center" container>
                    <Typography variant="h6" gutterBottom style={{minWidth: "60vh", textAlign: "center"}}>
                        {
                            completed === 'done' && authorText ? (
                                authorText
                            ) : completed === 'done' ? (
                                'Thank you for completing the experiment'
                            ): (
                                'The experiment link is no longer valid'
                            )
                        }
                    </Typography>
                </Grid>
                {
                    amtSurveyCode ? (
                        <>
                            <Grid item xs={12} justifyContent="center" container>
                                <Typography variant="h4">
                                    {
                                        `Survey Code: ${amtSurveyCode}`
                                    }
                                </Typography>
                            </Grid>
                            <Grid item xs={12} justifyContent="center" container>
                                <Button
                                    size="medium"
                                    variant="contained"
                                    color="primary"
                                    onClick={() => navigator.clipboard.writeText(amtSurveyCode)}
                                >
                                    Copy to clipboard
                                </Button>
                            </Grid>
                        </>
                    ) : null
                }
            </Grid>
        </div>
    );
}

ExperimentCompleted.defaultProps = {
    authorText    : '',
    amtSurveyCode : null
};

ExperimentCompleted.propTypes = {
    completed     : PropTypes.string.isRequired,
    authorText    : PropTypes.string,
    amtSurveyCode : PropTypes.string
};
