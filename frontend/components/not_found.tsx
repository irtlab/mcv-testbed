import React from 'react';
import CancelIcon from '@material-ui/icons/Cancel';
import PropTypes from 'prop-types';
import { Grid, Typography } from '@material-ui/core';


export default function NotFoundPage({ text }) {
    return (
        <div>
            <Grid
                container
                spacing={0}
                direction="column"
                alignItems="center"
                justifyContent="center"
                style={{ minHeight: '100vh' }}
            >
                <Grid item xs={3} justifyContent="center" container>
                    <CancelIcon style={{color: "red", fontSize: "100px"}} />
                </Grid>
                <Grid item xs={3} justifyContent="center" container>
                    <Typography variant="h6" gutterBottom style={{minWidth: "60vh", textAlign: "center"}}>
                        {
                            `${text}`
                        }
                    </Typography>
                </Grid>
            </Grid>
        </div>
    );
}


NotFoundPage.defaultProps = {
    text : ''
};

NotFoundPage.propTypes = {
    text : PropTypes.string.isRequired
};
