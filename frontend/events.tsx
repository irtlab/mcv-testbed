import React, { useState, useEffect } from 'react';
import debug from 'debug';
import axios from 'axios';
import MaterialTable from 'material-table';
import { Grid } from '@material-ui/core';
import { withSnackbar } from 'notistack';
import { CollectTerminalData } from './components/collect_ut_data';
import { fetchAuth } from './auth';
import { getBackendURL } from './utils';
import { axiosAuth } from './auth';

const dbg = debug('mcv:events');

const api = axios.create({baseURL: `${getBackendURL()}api`, withCredentials: true});
api.interceptors.response.use(undefined, axiosAuth);


export const EventsView = withSnackbar(function({ enqueueSnackbar }) {
    const [ events, setEvents ] = useState([]);

    async function load(opts={}) {
        dbg('Fetching events');
        const qs = Object.entries(opts).map(([k, v]) => `${k}=${v}`).join('&');
        try {
            const res = await fetchAuth(`${getBackendURL()}api/events${qs.length ? `?${qs}` : ''}`);
            if (!res.ok) throw new Error(res.statusText);
            setEvents(await res.json());
        } catch(error: any) {
            enqueueSnackbar(`Error while fetching events: ${error.message}`, {
                variant : 'error'
            });
        }
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
    useEffect(() => { void load(); }, []);

    return (
        <>
            <Grid container>
                <Grid item xs={12} spacing={6}><br/></Grid>
                <Grid item xs={12} spacing={6}>
                    <CollectTerminalData api={api} />
                </Grid>
                <Grid item xs={12} spacing={6}><br/></Grid>
                <Grid item xs={12}>
                    <MaterialTable
                        options={{
                            pageSize : 10,
                            pageSizeOptions : [10, 20, 30, 40, 50]
                        // selection : true
                        }}
                        columns={[{
                            title       : 'Timestamp',
                            field       : 'timestamp',
                            type        : 'datetime',
                            defaultSort : 'asc'
                        }, {
                            title : 'Duration',
                            field : 'duration',
                            type  : 'numeric'
                        }, {
                            title : 'Terminal',
                            field : 'terminal'
                        }, {
                            title : 'Event Name',
                            field : 'event'
                        }, {
                            title : 'Data',
                            field : 'data',
                            render: (rowData: any) => rowData.data ? JSON.stringify(rowData.data) : '-'
                        }, {
                            title : 'Media',
                            field : 'media'
                        }]}
                        data={events}
                        title="Events"
                    />
                </Grid>
            </Grid>
        </>
    );
});
