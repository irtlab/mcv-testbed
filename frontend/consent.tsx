import React, { useEffect } from 'react';
import axios from 'axios';
import { useSnackbar } from 'notistack';
import ReactMarkdown from 'react-markdown';
import "codemirror/lib/codemirror.css";
import "codemirror/mode/markdown/markdown";
import {
    Fab,
    makeStyles,
    Button,
    Container,
    Grid,
    TextField,
    Typography
} from '@material-ui/core';
import { useNavigate, useParams } from "react-router-dom";
import { Editor } from './components/markdown_editor';
import { SpinnerPage } from './components/spinner_page';
import { ListDrawer } from './components/list_drawer';
import { axiosAuth } from './auth';
import { makeUniqueName, createAlert, getBackendURL } from './utils';

const textFieldWidth = 350;
const api = axios.create({baseURL: `${getBackendURL()}api`, withCredentials: true});
api.interceptors.response.use(null as any, axiosAuth);

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
    },
    content: {
        flexGrow: 1
    },
    fab: {
        margin: theme.spacing(1),
        position: "fixed",
        bottom: theme.spacing(2),
        right: theme.spacing(3)
    },
    fabEdit: {
        margin: theme.spacing(1),
        position: "fixed",
        bottom: theme.spacing(8),
        right: theme.spacing(3)
    },
    distance: {
        padding: theme.spacing(3)
    },
    paper: {
        padding: theme.spacing(1),
        color: theme.palette.text.secondary,
    },
    markdownResult: {
        position: 'fixed',
        top: '119px',
        right: '0',
        left: '49.4%',
        bottom: '2px',
        overflow: 'auto',
        padding: '10px',
        paddingLeft: '12px',
        color: '#444',
        fontSize: '14px',
        lineHeight: '1.5em',
        borderLeft: 'ridge'
    },
    editor: {
        position: 'fixed',
        top: '119px',
        left: '0',
        bottom: '2px',
        width: '50%',
        padding: '0px'
    }
}));


// TODO (Artiom): This should be removed and instead I should use MarkdownEditorView
// component which is implemented in the /components/markdown_editor.jsx file.
export function EditConsentFormView() {
    const classes = useStyles();
    const params = useParams();
    const navigate = useNavigate();
    const [name, setName] = React.useState('');
    const [markdownSrc, setMarkdownSrc] = React.useState('');
    const [consentForm, setConsentForm] = React.useState<any>({});
    const alert = createAlert(useSnackbar().enqueueSnackbar);

    async function fetchConsentForm(id) {
        try {
            const res = await api.get(`/consent_form/${id}`);
            setConsentForm(res.data);
            setName(res.data.name);
            setMarkdownSrc(res.data.form);
        } catch (error) {
            alert('Error while fetching consent forms', error);
        }
    }

    async function updateConsentForm() {
        const update = {
            _id: consentForm._id,
            name: name.trim(),
            form: markdownSrc
        };
        try {
            const rv = await api.post(`/consent_form/${consentForm._id}`, update);
            navigate(`/consent_forms/${rv.data._id}`);
        } catch (error) {
            alert('Error while updating the consent form', error);
        }
    }

    const disableSaveBtn = () => {
        if (!name.trim() || !markdownSrc.trim()) return true;
        if (name.trim() === consentForm.name && markdownSrc === consentForm.form) return true;
        return false;
    };

    useEffect(() => {
        if (params.id) void fetchConsentForm(params.id);
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [params]);

    return (
        <div className={classes.root}>
            {markdownSrc ? (
                <Grid container spacing={0}>
                    <Grid item xs={12} style={{ height: "80%" }}>
                        <div style={{ width: "100%", height: "70px"}}>
                            <div style={{ float: "left" }}>
                                <TextField
                                    style={{ width: textFieldWidth, minWidth: 210, marginLeft: "10px" }}
                                    margin="dense"
                                    id="ConsentFormName"
                                    value={name || ''}
                                    label="Consent Form Name *"
                                    onChange={(event) => setName(event.target.value)}
                                />
                            </div>
                            <div style={{ float: "right" }}>
                                <Container style={{textAlign: "end"}}>
                                    <Button
                                        style={{paddingTop: "30px"}}
                                        onClick={() => navigate(`/consent_forms/${consentForm._id}`)}
                                        color="primary"
                                    >
                                        Cancel
                                    </Button>
                                    <Button
                                        disabled={0 || disableSaveBtn()}
                                        style={{marginLeft: "10px", paddingTop: "30px"}}
                                        onClick={updateConsentForm}
                                        color="primary"
                                    >
                                        Save
                                    </Button>
                                </Container>
                            </div>
                        </div>
                    </Grid>
                    <Grid item xs={6}>
                        <div className={classes.editor}>
                            <Editor
                                markdownSrc={markdownSrc}
                                setMarkdownSrc={setMarkdownSrc}
                            />
                        </div>
                        <div className={classes.markdownResult}>
                            <ReactMarkdown>
                                {markdownSrc}
                            </ReactMarkdown>
                        </div>
                    </Grid>
                </Grid>
            ) : (
                <Typography>
                Not Found
                </Typography>
            ) }
        </div>
    );
}


export function MainConsentFormView() {
    const classes = useStyles();
    const params = useParams();
    const navigate = useNavigate();
    const [runSpinningPage, setRunSpinningPage] = React.useState(true);
    const [consentForms, setConsentForms] = React.useState<any[]>([]);
    const [selectedId, _setSelectedId] = React.useState(params.id);
    const alert = createAlert(useSnackbar().enqueueSnackbar);

    if (params.id !== selectedId) _setSelectedId(params.id);

    const setSelectedId = (id) => {
        navigate(`/consent_forms/${id}`);
        _setSelectedId(id);
    };

    async function fetchConsentForms() {
        try {
            const response = await api.get('/consent_form');
            setConsentForms(response.data);

            // If there is NO any ID in the URL, then redirect the user to the ID of
            // the first consent form if there is one.
            if (!params.id && response.data.length) setSelectedId(response.data[0]._id);
        } catch (error) {
            alert('Error while fetching consent forms', error);
        }
    }

    async function saveConsentForm(data) {
        try {
            const rv = await api.post('/consent_form', data);
            return rv.data;
        } catch (error) {
            alert('Error while storing/updating the consent form', error);
        }

        return null;
    }

    async function deleteConsentForm(consent_form) {
        try {
            await api.delete(`/consent_form/${consent_form._id}`);

            const updated_forms = consentForms.filter((data: any) => {
                return data._id !== consent_form._id;
            });

            setConsentForms(updated_forms);

            if (consent_form._id === selectedId && updated_forms.length) {
                setSelectedId(updated_forms[0]._id);
            } else {
                setSelectedId('');
            }
        } catch (error) {
            alert('Error while deleting the consent form', error);
        }
    }


    const handleNewFormBtn = async () => {
        const res = await saveConsentForm({
            name: makeUniqueName(consentForms, 'Unnamed Form'),
            form: '',
            filename: ''
        });
        if (res) navigate(`/consent_forms/edit/${res._id}`);
    };

    const editConsentForm = (id) => {
        navigate(`/consent_forms/edit/${id}`);
    };

    useEffect(() => {
        void (async () => {
            await fetchConsentForms();
            setRunSpinningPage(false);
        })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [params]);


    return (
        <div>
            {
                runSpinningPage ? (
                    <SpinnerPage />
                ) : (
                    <div className={classes.root}>
                        <ListDrawer
                            selectedId={selectedId}
                            setSelectedId={setSelectedId}
                            listItems={consentForms}
                            deleteListItem={deleteConsentForm}
                            editListItem={editConsentForm}
                        />
                        <main className={classes.content}>
                            {
                                selectedId && consentForms.length ? (
                                    <Container>
                                        <ReactMarkdown>
                                            {(consentForms.filter(v => v._id === selectedId)[0]).form}
                                        </ReactMarkdown>
                                    </Container>
                                ) : null
                            }
                        </main>
                        <Fab
                            size="large"
                            variant="extended"
                            color="primary"
                            aria-label="add"
                            className={classes.fab}
                            onClick={handleNewFormBtn}
                        >
                            New Form
                        </Fab>
                    </div>
                )
            }
        </div>
    );
}
