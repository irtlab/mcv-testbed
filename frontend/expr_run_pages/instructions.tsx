import React, { useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import { useNavigate, useParams } from "react-router-dom";
import {
    Button,
    Divider,
    Grid,
    Typography
} from '@material-ui/core';
import MaterialTable from 'material-table';
import { SpinnerPage } from '../components/spinner_page';
import NotFoundPage from '../components/not_found';
import { getLocalStorage } from '../utils';

const textFieldWidth = 825;

const NATOAlphabet = [
    {nato: 'Alpha', english: 'A'},
    {nato: 'Bravo', english: 'B'},
    {nato: 'Charlie', english: 'C'},
    {nato: 'Delta', english: 'D'},
    {nato: 'Echo', english: 'E'},
    {nato: 'Foxtrot', english: 'F'},
    {nato: 'Golf', english: 'G'},
    {nato: 'Hotel', english: 'H'},
    {nato: 'India', english: 'I'},
    {nato: 'Juliet', english: 'J'},
    {nato: 'Kilo', english: 'K'},
    {nato: 'Lima', english: 'L'},
    {nato: 'Mike', english: 'M'},
    {nato: 'November', english: 'N'},
    {nato: 'Oscar', english: 'O'},
    {nato: 'Papa', english: 'P'},
    {nato: 'Quebec', english: 'Q'},
    {nato: 'Romeo', english: 'R'},
    {nato: 'Sierra', english: 'S'},
    {nato: 'Tango', english: 'T'},
    {nato: 'Uniform', english: 'U'},
    {nato: 'Victor', english: 'V'},
    {nato: 'Whiskey', english: 'W'},
    {nato: 'Xray', english: 'X'},
    {nato: 'Yankee', english: 'Y'},
    {nato: 'Zulu', english: 'Z'}
];


function InstructionsPage({ navigate }) {
    const audioLink = 'https://mcv-testbed.cs.columbia.edu/api/media/impaired_222e2918-6ffd-45ff-8952-d479fd1f3f51';
    const audioRef = useRef<any | null>(null);

    const handlePlayBtn = () => {
        audioRef.current.play();
    };

    return (
        <div>
            <Grid spacing={4} container justifyContent="center">
                <Grid item xs={12} justifyContent="center" container>
                    <Typography variant="h3">
                        Experiment Overview and Instructions
                    </Typography>
                </Grid>
                <Grid item xs={12} style={{maxWidth: textFieldWidth}}>
                    <Typography variant="h6">
                        Experiment Overview
                    </Typography>
                    <Divider />
                    <Typography variant="subtitle1">
                        In this experiment, you will be asked to listen to a series of audio recordings that contain license plate numbers.
                        The quality of each audio recording has been intentionally degraded with background noise, clicks, or interruptions to resemble
                        real-world communications. The characters in each license plate number are spelled with the NATO phonetic alphabet (described below).
                        Your goal is to write down each license plate number (original characters, not NATO words) as accurately as possible.
                    </Typography>
                    <br/>
                    <Typography variant="subtitle1">
                        You are welcome to write down license plates on paper before you enter them into the computer if that is faster or more convenient for you.
                        We also recommend using a headset.
                    </Typography>
                    <br/>
                    <Typography variant="subtitle1">
                        Each recording contains the sentence <b>"Reporting license plate Alpha Six Four Charlie Delta Echo”</b> where Alpha, Charlie, Delta, Echo
                        are words from the NATO phonetic alphabet and Six and Four are numbers. When writing down the license plate
                        number, please convert each NATO phonetic word into the corresponding letter (e.g., Alpha to A, Charlie to C, and so on) and each number
                        into the corresponding digit (e.g., Six to 6). Thus, the above license plate number would be recorded as <b>“a64cde”</b>.
                        Skip the <b>“Reporting license plate”</b> part, that is, write down the license plate number only. You can use upper-case or lower-case
                        letters.
                    </Typography>
                    <br/>
                    <br/>
                    <Typography variant="h6">
                        Example Audio Recording
                    </Typography>
                    <Divider />
                    <Typography variant="subtitle1">
                        Click on the following button to play an example audio recording. Use the example to adjust your audio volume to a comfortable level.
                        The example audio has not been degraded, but is otherwise similar to the recording used during the experiment.
                    </Typography>
                    <>
                        <audio
                            ref={audioRef}
                            controls
                            controlsList="nodownload"
                            preload="metadata"
                            src={audioLink}
                            style={{ display: 'none' }}
                        >
                            Your browser does not support audio playback.
                        </audio>
                        <br/>
                        <Button
                            fullWidth={true}
                            variant="contained"
                            color="primary"
                            size="large"
                            onClick={handlePlayBtn}
                        >
                            Play
                        </Button>
                    </>
                    <br/>
                    <br/>
                    <br/>
                    <Typography variant="h6">
                        NATO Phonetic Alphabet
                    </Typography>
                    <Divider />
                    <Typography variant="subtitle1">
                        The NATO phonetic alphabet is a standardized phonetic alphabet that is used by military and civilian organizations to spell out words and
                        communicate information over radio or telephone communication systems in a clear and unambiguous way. The NATO phonetic alphabet consists
                        of 26 code words, each representing a letter of the English alphabet. The code words are as follows:
                    </Typography>
                    <br/>
                    <MaterialTable
                        options={{
                            pageSize : 26
                        }}
                        columns={[{
                            title       : 'NATO Word',
                            field       : 'nato',
                            type        : 'string',
                            defaultSort : 'asc',
                        }, {
                            title : 'English Letter',
                            field : 'english',
                            type  : 'string'
                        }]}
                        data={NATOAlphabet}
                        title="NATO ALphabet Table"
                    />
                    <br/>
                    <Typography variant="subtitle1">
                        Each code word is pronounced using its own unique phonetic pronunciation, which helps to avoid confusion and miscommunication, especially when
                        communicating important information, such as the spelling of names, locations, and other critical details.
                    </Typography>
                    <br/>
                    <br/>
                    <Button
                        fullWidth={true}
                        size="large"
                        variant="contained"
                        color="primary"
                        onClick={navigate}
                    >
                        Next Step
                    </Button>
                </Grid>
            </Grid>
            <br/>
            <br/>
        </div>
    );
}

InstructionsPage.propTypes = {
    navigate : PropTypes.func.isRequired
};



export default function InstructionsPageView() {
    const navigate = useNavigate();
    const params = useParams();

    const [selectedId, setSelectedId] = React.useState((params as any).id);
    const [runSpinningPage, setRunSpinningPage] = React.useState(true);
    const [found, setFound] = React.useState(false);

    if ((params as any).id !== selectedId) setSelectedId((params as any).id);

    const getLocalStorageData = () => {
        const storage = getLocalStorage(`${selectedId}`, 'basic_info');
        if (!storage || !storage.expr_run_id) return null;
        return storage;
    };

    const navigatePage = () => {
        navigate(`/run_experiment/${selectedId}/background_env`);
    };

    useEffect(() => {
        void (async () => {
            const runningExprData = getLocalStorage(selectedId, 'runningExprData');
            if (runningExprData && runningExprData._id === selectedId) {
                navigate(`/run_experiment/${selectedId}/run`);
                return;
            }

            const storage = getLocalStorageData();
            if (storage && storage.expr_run_id === selectedId) {
                if (storage.mturk)  {
                    setFound(true);
                } else {
                    navigatePage();
                }
            }
            setRunSpinningPage(false);
        })();
    }, []);

    return (
        <>
            {
                runSpinningPage ? <SpinnerPage text={''} /> :
                found ? <InstructionsPage navigate={navigatePage} /> : <NotFoundPage text={'Page not found or invalid URL'} />
            }
        </>
    );
}

