import React, { useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import { useNavigate, useParams } from "react-router-dom";
import { useReactToPrint } from 'react-to-print';
import axios from 'axios';
import { useSnackbar } from 'notistack';
import DateFnsUtils from '@date-io/date-fns';
import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers';
import * as EmailValidator from 'email-validator';
import {
    Button,
    Checkbox,
    Grid,
    Link,
    Typography,
    makeStyles,
    TextField,
    FormControl,
    FormControlLabel
} from '@material-ui/core';
import ReactMarkdown from 'react-markdown';
import { axiosAuth } from '../auth';
import { SpinnerPage } from '../components/spinner_page';
import NotFoundPage from '../components/not_found';
import { createAlert, getLocalStorage, getBackendURL, setLocalStorage } from '../utils';
import { ExperimentCompleted } from '../components/expr_completed';


const textFieldWidth = 770;

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
    },
    content: {
        flexGrow: 1
    },
    multilineColor: {
        color: 'black'
    },
    fab: {
        margin   : theme.spacing(1),
        position : "fixed",
        bottom   : theme.spacing(2),
        right    : theme.spacing(3)
    },
}));

const api = axios.create({baseURL: `${getBackendURL()}api`, withCredentials: true});
api.interceptors.response.use(undefined, axiosAuth);


interface PrintConsentFormProps {
    markdownString?: string
}

// This must be class, otherwise the react-to-print library will throw an exception.
class PrintConsentForm extends React.Component<PrintConsentFormProps> {
    render() {
        const { markdownString = '' } = this.props as any;
        return (
            <div>
                <ReactMarkdown>
                    {markdownString}
                </ReactMarkdown>
            </div>
        );
    }
}


function ConsentFormTerms({ termsCheckBox, setTermsCheckBox, printRef }) {
    const handleConsentFormPrint = useReactToPrint({
        pageStyle: '@page {margin: 2000mm !important }',
        content: () => printRef.current,
    });

    return (
        <FormControl style={{ width: textFieldWidth }} required component="fieldset">
            <FormControlLabel
                control={(
                    <Checkbox
                        onChange={() => {
                            const current_status = termsCheckBox;
                            setTermsCheckBox(!current_status);
                        }}
                        color="primary"
                        size="small"
                    />
                )}
                label={(
                    <span style={{fontSize: "15px"}}>
                        I have read and agree to the Terms and Conditions.
                    </span>
                )}
            />
            <Typography style={{marginLeft: "28px"}} variant="caption" display="block" gutterBottom>
                Click
                {' '}
                {' '}
                <Link
                    component="button"
                    variant="caption"
                    onClick={handleConsentFormPrint}
                    download
                >
                here
                </Link>
                {' '}
                to download a PDF copy.
            </Typography>
        </FormControl>
    );
}

ConsentFormTerms.defaultProps = {
    termsCheckBox : false
};

ConsentFormTerms.propTypes = {
    termsCheckBox    : PropTypes.bool,
    setTermsCheckBox : PropTypes.func.isRequired,
    printRef         : PropTypes.object.isRequired
};


function SelectSubjectEmail({ subject, setSubjectData }) {
    const [validEmail, setValidEmail] = React.useState('');
    const [emailValue, setEmailValue] = React.useState(subject.email);

    const handleEmaiChange = (event) => {
        setEmailValue(event.target.value);
        const value = (event.target.value).trim();
        if (value && EmailValidator.validate(value)) {
            setValidEmail('');
            setSubjectData({...subject, email: event.target.value});
        } else {
            setValidEmail('Not valid email');
            setSubjectData({...subject, email: ''});
        }
    };

    return (
        <TextField
            style={{ marginRight: '580px' }}
            type="email"
            value={emailValue || ''}
            error={validEmail.length !== 0}
            helperText={validEmail || ''}
            id="SubjectEmail"
            label="Email *"
            onChange={handleEmaiChange}
        />

    );
}

SelectSubjectEmail.propTypes = {
    subject        : PropTypes.object.isRequired,
    setSubjectData : PropTypes.func.isRequired
};


function SelectSubjectName({ subject, setSubjectData }) {
    const handleNameChange = (event) => {
        const value = (event.target.value).trim();
        if (value) {
            setSubjectData({...subject, name: event.target.value});
        } else {
            setSubjectData({...subject, name: ''});
        }
    };

    return (
        <TextField
            style={{ marginRight: '580px' }}
            value={subject.name}
            error={!subject.name}
            id="Subject"
            label="Name *"
            onChange={handleNameChange}
        />
    );
}

SelectSubjectName.propTypes = {
    subject        : PropTypes.object.isRequired,
    setSubjectData : PropTypes.func.isRequired
};





function SetSubjectSignature({ subject, setSubjectData }) {
    const [signatureError, setSignatureError] = React.useState(false);

    const handleSignatureChange = (event) => {
        const value = (event.target.value).trim();
        if (value) {
            setSignatureError(false);
            setSubjectData({...subject, signature: event.target.value});
        } else {
            setSignatureError(true);
            setSubjectData({...subject, signature: ''});
        }
    };

    return (
        <>
            <TextField
                style={{ width: textFieldWidth, height: '100px' }}
                id="Signature"
                label="Signature *"
                error={signatureError}
                helperText="Provide full name"
                value={subject.signature}
                onChange={handleSignatureChange}
            />
        </>
    );
}


SetSubjectSignature.propTypes = {
    subject        : PropTypes.object.isRequired,
    setSubjectData : PropTypes.func.isRequired
};



function SetCalendar({ subject, setSubjectData }) {

    const handleDateChange = (date) => {
        setSubjectData({...subject, date: date ? date : new Date()});
    };

    return (
        <div style={{ width: textFieldWidth, height: '100px' }}>
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <KeyboardDatePicker
                    disableToolbar
                    variant="inline"
                    format="MM/dd/yyyy"
                    margin="normal"
                    id="date-picker-inline"
                    label="Today's Date"
                    value={subject.date}
                    onChange={handleDateChange}
                    KeyboardButtonProps={{
                        'aria-label': 'change date',
                    }}
                />
            </MuiPickersUtilsProvider>
        </div>
    );
}

SetCalendar.propTypes = {
    subject        : PropTypes.object.isRequired,
    setSubjectData : PropTypes.func.isRequired
};


function ConsentForm({ navigate, exprName, exprType, consentForm, exprRun, practiceExpr, mturk }) {
    const alert = createAlert(useSnackbar().enqueueSnackbar);
    const printRef = useRef<any | null>(null);
    const [submitted, setSubmitted] = React.useState(false);

    const [termsCheckBox, setTermsCheckBox] = React.useState(false);
    const [subjectData, setSubjectData] = React.useState({
        _id: exprRun.subject._id,
        name: exprRun.subject.name,
        email: exprRun.subject.email,
        date: new Date(),
        signature: '',
        experiment_run: exprRun._id,
        consent_form: consentForm.form
    });

    const disableStartExprBtn = () => {
        // 1. If the experiment is practice and the Consent Form is provided, then it should be
        //    required that the subject agrees and signs the Form.
        // 2. If the experiment is practice and the Consent Form is NOT provided, then hide
        //    the required fields and allow to start the experiment.
        // 3. If the the experiment is NOT practice and the Consent Form is NOT provided, then it
        //    requires name and email to start the experiment.
        // 4. If the the experiment is NOT practice and the Consent Form is provided, then the
        //    subject must provide required data, agree and sign the Form.
        if (practiceExpr) {
            if (!consentForm.form) return false;
            return !(termsCheckBox && subjectData.date && subjectData.signature);
        } if (!consentForm.form) {
            return !(subjectData.name && subjectData.email);
        }

        if (mturk) {
            return !(termsCheckBox && subjectData.date);
        }

        return !(
            termsCheckBox &&
            subjectData.date &&
            subjectData.signature &&
            subjectData.name &&
            subjectData.email
        );
    };

    async function updateExperimentRun(data) {
        try {
            await api.post(`/experiment_run/${data._id}`, data);
        } catch (error) {
            alert('Error while updating the experiment run:', error);
        }
    }

    async function updateSubjectData(data) {
        // If the experiment is practice, then we do not update the subject data.
        if (practiceExpr) return;

        try {
            await api.post(`/subject_data/${data._id}`, data);
        } catch (error) {
            alert('Error while updating the experiment subject data:', error);
        }
    }

    async function signConsentForm(data) {
        try {
            await api.post(`/consent_form/${data.consent_form}/sign`, data);
        } catch (error) {
            alert('Error while signing the consent form:', error);
        }
    }

    const handleStartExprBtn = async () => {
        const update: any = {_id: exprRun._id, consent_form_signed: false};
        if (exprType !== 'interactive') update.start = 1;
        if (!practiceExpr && Object.keys(consentForm).length !== 0) {
            await signConsentForm({
                experiment: exprRun.experiment,
                experiment_run: exprRun._id,
                // We don't have email for the MTurk subjects. Instead, I just keep date the subject's _id.
                email: subjectData.email === 'unknown' ? subjectData._id : subjectData.email,
                consent_form: consentForm._id
            });
            update.consent_form_signed = true;
        } else {
            // If the experiment is practice, then it does not change the backend so I won't be
            // able to get the experiment starting time. I create a starting time here and store in
            // the local storage and the elapsed time counter function will get it.
            setLocalStorage(exprRun._id, 'exprStartTime', String(new Date()), false);
        }

        await updateSubjectData(subjectData);
        await updateExperimentRun(update);
        setSubmitted(true);
        // navigate();
    };

    return (
        <div>
            {
                !submitted ? (
                    <Grid container spacing={4} justifyContent="center">
                        <Grid item xs={12} justifyContent="center" container>
                            <Typography variant="h3">Columbia University Consent Form</Typography>
                        </Grid>
                        <Grid item xs={12} justifyContent="center" container>
                            <Typography variant="h5">{`Experiment: ${exprName}`}</Typography>
                        </Grid>
                        <Grid item xs={12} style={{maxWidth: textFieldWidth}} >
                            <PrintConsentForm markdownString={consentForm.form} ref={printRef} />
                        </Grid>
                        <Grid item xs={12} justifyContent="center" container>
                            {
                                !practiceExpr && !mturk ? <SelectSubjectName subject={subjectData} setSubjectData={setSubjectData} /> : null
                            }
                        </Grid>
                        <Grid item xs={12} justifyContent="center" container>
                            {
                                !practiceExpr && !mturk ? <SelectSubjectEmail subject={subjectData} setSubjectData={setSubjectData} /> : null
                            }
                        </Grid>
                        <Grid item xs={12} justifyContent="center" container>
                            <ConsentFormTerms termsCheckBox={termsCheckBox} setTermsCheckBox={setTermsCheckBox} printRef={printRef} />
                        </Grid>
                        <Grid item xs={12} justifyContent="center" container>
                            <SetCalendar subject={subjectData} setSubjectData={setSubjectData} />
                        </Grid>
                        <Grid item xs={12} justifyContent="center" container>
                            {
                                !practiceExpr && !mturk ? <SetSubjectSignature subject={subjectData} setSubjectData={setSubjectData} /> : null
                            }
                        </Grid>
                        <Grid item xs={12} justifyContent="center" container>
                            <Button
                                style={{ width: textFieldWidth }}
                                size="medium"
                                variant="contained"
                                color="primary"
                                disabled={0 || disableStartExprBtn()}
                                onClick={handleStartExprBtn}
                            >
                                {
                                    exprType === 'interactive' ? 'Submit' : 'Start Experiment'
                                }
                            </Button>
                        </Grid>
                    </Grid>
                ) : (
                <ExperimentCompleted
                    completed={'done'}
                    authorText={'Submitted!'}
                    amtSurveyCode={null}
                />
                )
            }
            <br/>
            <br/>
        </div>
    );
}

ConsentForm.defaultProps = {
    practiceExpr : false,
    mturk        : false
};

ConsentForm.propTypes = {
    navigate     : PropTypes.func.isRequired,
    exprName     : PropTypes.string.isRequired,
    exprType     : PropTypes.string.isRequired,
    consentForm  : PropTypes.object.isRequired,
    exprRun      : PropTypes.object.isRequired,
    practiceExpr : PropTypes.bool.isRequired,
    mturk        : PropTypes.bool.isRequired
};


export default function ExprRunConsentFormView() {
    const alert = createAlert(useSnackbar().enqueueSnackbar);
    const classes = useStyles();
    const params = useParams();
    const navigate = useNavigate();

    const [selectedId, setSelectedId] = React.useState((params as any).id);
    const [runSpinningPage, setRunSpinningPage] = React.useState(true);
    const [consentForm, setConsentForm] = React.useState({});
    const [exprName, setExprName] = React.useState('');
    const [exprType, setExprType] = React.useState('');
    const [exprRun, setExprRun] = React.useState({});
    const [found, setFound] = React.useState(false);
    const [practiceExpr, setPracticeExpr] = React.useState(false);
    const [mturk, setMturk] = React.useState(false);

    if ((params as any).id !== selectedId) setSelectedId((params as any).id);

    const getLocalStorageData = () => {
        const storage = getLocalStorage(`${selectedId}`, 'basic_info');
        if (!storage || !storage.expr_id || !storage.expr_run_id) return null;
        setPracticeExpr(storage.practice_expr);
        setExprType(storage.expr_type);
        setMturk(storage.mturk);
        return storage;
    };

    async function fetchConsentForm(id: string) {
        try {
            const rv = await api.get(`/consent_form/${id}`);
            setConsentForm(rv.data);
            return rv.data;
        } catch (error) {
            alert('Error while fetching the consent form:', error);
        }
        return null;
    }

    async function fetchExperimentRun(id: string) {
        try {
            const rv = await api.get(`/experiment_run/${id}`);
            setExprRun(rv.data);
            return rv.data;
        } catch (error) {
            alert('Error while fetching experiment run:', error);
        }
        return null;
    }

    const navigatePage = () => {
        navigate(`/run_experiment/${selectedId}/run`);
    };

    useEffect(() => {
        void (async () => {
            const runningExprData: any = getLocalStorage(selectedId, 'runningExprData');
            if (runningExprData && runningExprData._id === selectedId) {
                navigatePage();
                return;
            }

            const storage = getLocalStorageData();
            if (storage) {
                setExprName(storage.expr_name);
                const run = await fetchExperimentRun(storage.expr_run_id);
                if (run && run.status !== 'Finished' && run.status !== 'Running') {
                    const rv = await fetchConsentForm(storage.consent_form_id);
                    if (rv) setFound(true);
                }
            }
            setRunSpinningPage(false);
        })();
    }, []);

    return (
        <div className={classes.root}>
            <main className={classes.content}>
                {
                    runSpinningPage ? <SpinnerPage text={''} /> :
                    found ? <ConsentForm
                                navigate={navigatePage}
                                consentForm={consentForm}
                                exprName={exprName}
                                exprType={exprType}
                                exprRun={exprRun}
                                practiceExpr={practiceExpr}
                                mturk={mturk}/> : <NotFoundPage text={'The Consent Form is not found or invalid URL'} />
                }
            </main>
        </div>
    );
}
