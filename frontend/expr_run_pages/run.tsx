import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import Moment from 'moment';
import axios from 'axios';
import { useSnackbar } from 'notistack';
import { useNavigate, useParams } from "react-router-dom";
import {
    Button,
    Box,
    Grid,
    Container,
    makeStyles,
    Checkbox,
    FormControl,
    FormControlLabel,
    FormGroup,
    FormLabel,
    LinearProgress,
    Radio,
    RadioGroup,
    TextField,
    Typography
} from '@material-ui/core';
import { axiosAuth } from '../auth';
import { SpinnerPage } from '../components/spinner_page';
import NotFoundPage from '../components/not_found';
import { getCountUpTime, createAlert, setLocalStorage, getLocalStorage, clearLocalStorage, getBackendURL } from '../utils';


const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
    },
    content: {
        flexGrow: 1
    },
    multilineColor: {
        color: 'black'
    },
    fab: {
        margin   : theme.spacing(1),
        position : "fixed",
        bottom   : theme.spacing(2),
        right    : theme.spacing(3)
    },
}));


const textFieldWidth = 820;

const api = axios.create({baseURL: `${getBackendURL()}api`, withCredentials: true});
api.interceptors.response.use(undefined, axiosAuth);


function useForceUpdate() {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const [ value, setValue ] = useState(0);
    return () => setValue(v => v + 1);
}


// Functions takes the shuffled array index and returns the original one.
// When the author sets randomize steps I randomize it but at the same time
// keep the original order which would allow to associate answers with audio
// files on the backend.
function getOriginalIndex(exprRunId, shuffle_idx) {
    const ls_data = getLocalStorage(exprRunId, 'originalStepsIdx');

    // If the local storage data is null, then this means that the author
    // did not set to randomize the experiment steps.
    if (!ls_data) return shuffle_idx;
    return ls_data[shuffle_idx];
}



// This is not an efficient solution but for now it works. If the experiment was generated by the Generator,
// then the exprRunCon[idx].audio field does not provide the audio link and I have to get it from the experiment run,
// and at the same time to make sure that I will get the correct one.
function getAudioLink(exprRun, exprRunCon, idx) {
    if (exprRunCon[idx].audio) return exprRunCon[idx].audio;
    let lsData = getLocalStorage(exprRun._id, 'originalStepsIdx');
    if (lsData) return exprRun.audio[lsData[idx]];
    return '';
}


function LinearProgressWithLabel(props) {
    return (
        <Box display="flex" alignItems="center">
            <Box width="100%" mr={1}>
                <LinearProgress variant="determinate" {...props} />
            </Box>
            <Box minWidth={35}>
                <Typography variant="body2" color="textSecondary">
                    {props.label}
                </Typography>
            </Box>
        </Box>
    );
}

LinearProgressWithLabel.propTypes = {
    value: PropTypes.number.isRequired,
    label: PropTypes.string.isRequired
};


function ExperimentResponses({ currExprRunCon, localStorageData, descriptionRef, response, handleResponse, showResponse }) {
    const [errorText, setErrorText] = React.useState(null);
    const [checkBoxList, setCheckBoxList] = React.useState(() => {
        const data = {};
        if (currExprRunCon.answer.type === 'multiple_choice') {
            currExprRunCon.answer.choices.forEach(element => {
                data[element] = false;
                if (localStorageData && localStorageData.response) {
                    const stored_options = localStorageData.response.split(', ');
                    stored_options.forEach(option => { data[option] = true; });
                }
            });
        }
        return data;
    });

    const handleExprResponse = (event) => {
        setErrorText(null);
        handleResponse(event.target.value);
    };

    const handleCheckBoxResponse = (event) => {
        setErrorText(null);
        const tmp = checkBoxList;
        tmp[event.target.name] = event.target.checked;
        setCheckBoxList({...checkBoxList, ...tmp});
        const value = (Object.keys(tmp).filter(element => tmp[element] === true)).join(', ');
        handleResponse(value);
    };

    return (
        <div>
            {
                currExprRunCon.answer.type === 'free_form_text' ? (
                    <TextField
                        inputRef={descriptionRef}
                        style={{ width: textFieldWidth }}
                        value={response || ''}
                        error={errorText === 'response'}
                        helperText={errorText ? 'Please provide your answer' : ''}
                        id="AudioListeningNotes"
                        label="Answer"
                        multiline
                        spellCheck={false}
                        rows={5}
                        variant="outlined"
                        onChange={handleExprResponse}
                    />
                ) : currExprRunCon.answer.type === 'single_choice' ? (
                    <FormControl component="fieldset">
                        <FormLabel disabled component="legend">Your answer</FormLabel>
                        <RadioGroup
                            aria-label="single_choice"
                            name="single_choice1"
                            value={response || null}
                            onChange={handleExprResponse}
                        >
                            {
                                currExprRunCon.answer.choices.map(choice => (
                                    <FormControlLabel
                                        disabled={!showResponse}
                                        key={choice}
                                        value={choice}
                                        control={<Radio size="small" />}
                                        label={showResponse ? choice : '******'}
                                    />
                                )
                                )
                            }
                        </RadioGroup>
                    </FormControl>
                ) : currExprRunCon.answer.type === 'multiple_choice' ? (
                    <FormControl component="fieldset">
                        <FormLabel disabled component="legend">Select all that apply</FormLabel>
                        <FormGroup>
                            {
                                currExprRunCon.answer.choices.map(choice => (
                                    <FormControlLabel
                                        key={choice}
                                        control={(
                                            <Checkbox
                                                disabled={!showResponse}
                                                size="small"
                                                onChange={handleCheckBoxResponse}
                                                value={choice || ''}
                                                checked={checkBoxList[choice] || false}
                                                name={choice}
                                            />
                                        )}
                                        label={showResponse ? choice : '******'}
                                    />
                                )
                                )
                            }
                        </FormGroup>
                    </FormControl>
                ) : null
            }
        </div>
    );
}

ExperimentResponses.defaultProps = {
    response         : '',
    localStorageData : null
};

ExperimentResponses.propTypes = {
    currExprRunCon   : PropTypes.object.isRequired,
    localStorageData : PropTypes.object,
    descriptionRef   : PropTypes.object.isRequired,
    response         : PropTypes.string,
    handleResponse   : PropTypes.func.isRequired,
    showResponse     : PropTypes.bool.isRequired
};


function ExperimentRunAudioPlay({ navigate, subjectID, exprRunCon, exprRun, updateExprRun, updateSubject, storage, setStorage }) {
    const [response, setResponse] = useState(storage ? storage.response : '');
    const [playingIdx, setPlayingIdx] = useState<number>(storage ? storage.step : 0);
    const [currentPlayingLink, setCurrentPlayingLink] = useState(getAudioLink(exprRun, exprRunCon, playingIdx));
    const [disablePlayBtn, setDisablePlayBtn] = useState(() => {
        const ls_data = getLocalStorage(exprRun._id, 'playedIdx');
        if (!ls_data) return false;
        if (ls_data[playingIdx]) return ls_data[playingIdx];
        return false;
    });
    const forceUpdate = useForceUpdate();
    const [showResponse, setShowResponse] = useState(disablePlayBtn);
    // This is for triggering setTimeout call.
    const [countUpTime, setCountUpTime] = useState<string>('');
    // This is for triggering local storage update
    const [updateLocalStorage, setUpdateLocalStorage] = useState(true);
    const descriptionRef = React.useRef<any | null>(null);
    const audioRef = useRef<any | null>(null);

    const handleAudionEnded = () => {
        setCurrentPlayingLink('');
        setShowResponse(true);
        // I have to set that the given index audio was played so when the subject
        // reloads the page won't be able to play it again.
        let ls_data = getLocalStorage(exprRun._id, 'playedIdx');
        if (!ls_data) ls_data = {};
        ls_data[playingIdx] = true;
        setLocalStorage(exprRun._id, 'playedIdx', ls_data);
    };

    const disableNextButton = () => {
        if (response) return false;
        return true;
    };

    async function updateAudioData(finish = false) {
        // If I want to skip using localStorage here I have to modify Subject's
        // API to push data into MongoDB array instead of set (note that set only updates).
        let steps = getLocalStorage(exprRun._id, 'exprSteps');
        if (!steps) steps = [];

        steps.push({file_idx: getOriginalIndex(exprRun._id, playingIdx), response: response.trim()});
        await updateSubject({_id: subjectID, steps});

        if (finish) {
            setLocalStorage(exprRun._id, 'exprSteps', '', false);
            updateExprRun({_id: exprRun._id, end: 1});
        } else {
            setLocalStorage(exprRun._id, 'exprSteps', steps);
        }
    }

    const handlePlayBtn = () => {
        setDisablePlayBtn(true);
        if (storage && storage.playBackTime &&
            storage._id === exprRun._id) {
            audioRef.current.currentTime = storage.playBackTime;
            setStorage(null);
        }
        audioRef.current.play();
        if (exprRunCon[playingIdx].answer.type === 'free_form_text') {
            // This is to select the text field so the user don't have to do it.
            descriptionRef.current.focus();
        }
    };

    const handleFinish = () => {
        clearLocalStorage(exprRun._id);
        setLocalStorage(exprRun._id, 'feedback', '');
        navigate(`/run_experiment/${exprRun._id}/feedback`);
        return;
    };

    const handleNextBtn = async () => {
        setShowResponse(false);
        const finish = playingIdx + 1 === exprRunCon.length;
        await updateAudioData(finish);
        if (finish) {
            handleFinish();
        } else {
            const newIdx = playingIdx + 1;
            setResponse('');
            setCurrentPlayingLink(getAudioLink(exprRun, exprRunCon, newIdx));
            setPlayingIdx(newIdx);
            setDisablePlayBtn(false);
        }
    };

    useEffect(() => {
        const currentTime = audioRef.current.currentTime;
        const timer = setTimeout(() => {
            const data = {
                _id: exprRun._id,
                type: 'playing',
                step: playingIdx,
                playBackTime: currentTime,
                response: response.trim()
            };
            setLocalStorage(exprRun._id, 'runningExprData', data);
            setUpdateLocalStorage(!updateLocalStorage);
        }, 500);

        return () => clearTimeout(timer);
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [updateLocalStorage]);

    useEffect(() => {
        const timer = setTimeout(() => {
            // Note that of the experiment is practice then is 0 or undefined.
            // In that case I should get the starting time from the local storage.
            const ls_time = getLocalStorage(exprRun._id, 'exprStartTime', false);
            let start_time = exprRun.start ? Moment(exprRun.start).unix()
                : Moment(ls_time).unix();

            // In case if the local storage and experimentRun.start are null then always set
            // the current time. This will not show time.
            if (!start_time) start_time = Moment(new Date()).unix();

            setCountUpTime(getCountUpTime(start_time));
        }, 1000);

        return () => clearTimeout(timer);
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [countUpTime]);

    function onEnter() {
        if (!disablePlayBtn) {
            handlePlayBtn();
            forceUpdate();
        } else if (!disableNextButton()) {
            void handleNextBtn();
            forceUpdate();
        }
    }

    useEffect(() => {
        document.onkeydown = e => {
            if (e.key === 'Enter') {
                e.preventDefault();
                onEnter();
            }
        };
        return () => {
            document.onkeydown = null;
        };
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [ disablePlayBtn, disableNextButton ]);

    return (
        <div>
            <Typography variant="subtitle2">
                { 1 ? null : `Elapsed time: ${countUpTime}` }
            </Typography>
            <br />
                Progress
            <LinearProgressWithLabel
                value={playingIdx * (Number(100) / exprRunCon.length)}
                label={`${playingIdx}/${exprRunCon.length}`}
            />
            <br />
            { exprRunCon[playingIdx].answer.description ? (
                <Typography style={{ width: textFieldWidth }} variant="subtitle2" gutterBottom>
                    {exprRunCon[playingIdx].answer.description}
                </Typography>
            ) : null }
            <br />
            <audio
                ref={audioRef}
                controls
                controlsList="nodownload"
                preload="metadata"
                src={currentPlayingLink}
                style={{ display: 'none' }}
                onEnded={handleAudionEnded}
            >
                Your browser does not support audio playback.
            </audio>
            <Button
                style={{ width: textFieldWidth }}
                disabled={disablePlayBtn}
                variant="contained"
                color="primary"
                size="large"
                onClick={handlePlayBtn}
            >
                Play
            </Button>
            <br />
            <br />
            <ExperimentResponses
                currExprRunCon={exprRunCon[playingIdx]}
                localStorageData={storage}
                descriptionRef={descriptionRef}
                response={response}
                handleResponse={setResponse}
                showResponse={showResponse}
            />
            <br />
            <br />
            <Button
                style={{ width: textFieldWidth }}
                size="large"
                variant="contained"
                color="primary"
                disabled={0 || disableNextButton()}
                onClick={handleNextBtn}
            >
                {
                    (playingIdx + 1 === exprRunCon.length) ? 'Finish' : 'Next Step'
                }
            </Button>
        </div>
    )
}

ExperimentRunAudioPlay.defaultProps = {
    storage : null
};

ExperimentRunAudioPlay.propTypes = {
    navigate      : PropTypes.func.isRequired,
    subjectID     : PropTypes.string.isRequired,
    exprRunCon    : PropTypes.array.isRequired,
    exprRun       : PropTypes.object.isRequired,
    updateExprRun : PropTypes.func.isRequired,
    updateSubject : PropTypes.func.isRequired,
    storage       : PropTypes.object,
    setStorage    : PropTypes.func.isRequired
};


function ExprRun({ navigate, expr, exprRun, exprRunCon, isExprRunning, updateExprRun, updateSubject }) {
    const classes = useStyles();
    const [storage, setStorage] = React.useState(getLocalStorage(exprRun._id, 'runningExprData'));

    const exprTile = isExprRunning ?
        `Experiment "${expr ? expr.name : ''}" is running` :
        `Experiment "${expr ? expr.name : ''}"`;

    return (
        <Container>
            <Grid justifyContent="center" container spacing={5}>
                <Grid item xs={12} justifyContent="center" container>
                    {
                        <Typography variant="h3">
                            {exprTile}
                        </Typography>
                    }
                </Grid>
                {
                    expr.practice_expr ? (
                        <Grid item xs={12} justifyContent="center" container>
                            <Typography style={{textAlign: "center"}} variant="h5">
                                (This is a practice experiment. No data will be saved.)
                            </Typography>
                        </Grid>
                    ) : null
                }
                <br/>
                {
                    !isExprRunning && expr.subject_notes ? (
                        <Grid item xs={12}>
                            <TextField
                                id="subject-notes"
                                label="Notes for Participants"
                                disabled
                                multiline
                                fullWidth
                                rows={5}
                                variant="outlined"
                                value={expr.subject_notes || ''}
                                inputProps={{
                                    className: classes.multilineColor
                                }}
                            />
                        </Grid>
                    ) : null
                }
                {
                    isExprRunning && exprRunCon && exprRunCon.length ? (
                        <Grid item xs={12} justifyContent="center" container>
                            <ExperimentRunAudioPlay
                                navigate={navigate}
                                subjectID={exprRun.subject._id}
                                exprRunCon={exprRunCon}
                                exprRun={exprRun}
                                updateExprRun={updateExprRun}
                                updateSubject={updateSubject}
                                storage={storage}
                                setStorage={setStorage}
                            />
                        </Grid>
                    ) : isExprRunning ?
                        (<h4>NO audio files to play</h4>) : null
                }
            </Grid>
        </Container>
    );
}


ExprRun.defaultProps = {
    isExprRunning : false
};

ExprRun.propTypes = {
    navigate      : PropTypes.func.isRequired,
    expr          : PropTypes.object.isRequired,
    exprRun       : PropTypes.object.isRequired,
    exprRunCon    : PropTypes.array.isRequired,
    isExprRunning : PropTypes.bool.isRequired,
    updateExprRun : PropTypes.func.isRequired,
    updateSubject : PropTypes.func.isRequired
};


export default function ExprRunView({}) {
    const alert = createAlert(useSnackbar().enqueueSnackbar);
    const params = useParams();
    const navigate = useNavigate();

    const [selectedId, setSelectedId] = React.useState((params as any).id);
    const [exprRun, setExprRun] = React.useState({});
    const [exprRunConf, setExprRunConf] = React.useState<any[]>([]);
    const [expr, setExpr] = React.useState<any>({});
    const [found, setFound] = React.useState(false);
    const [isExprRunning, setIsExprRunning] = React.useState(false);
    const [runSpinningPage, setRunSpinningPage] = React.useState(true);

    if ((params as any).id !== selectedId) setSelectedId((params as any).id);

    async function fetchExperiment(id: string) {
        try {
            const rv = await api.get(`/experiment/${id}`);
            if (rv.data) setExpr(rv.data);
            return rv.data;
        } catch (error) {
            alert('Error while fetching experiments:', error);
        }
        return null;
    }

    async function fetchExperimentRun(id: string) {
        try {
            const rv = await api.get(`/experiment_run/${id}`);
            setExprRun(rv.data);
            return rv.data;
        } catch (error) {
            alert('Error while fetching experiment run:', error);
        }
        return null;
    }

    async function updateExperimentRun(data) {
        try {
            const rv = await api.post(`/experiment_run/${data._id}`, data);
            setExprRun(rv.data);
            return rv.data;
        } catch (error) {
            alert('Error while updating the experiment run:', error);
        }
    }

    async function updateSubjectData(data) {
        // Practice experiment must not update backend (database) data.
        if ((expr as any).practice_expr) return;
        try {
            await api.post(`/subject_data/${data._id}`, data);
        } catch (error) {
            alert('Error while updating the experiment run:', error);
        }
    }

    const getLocalStorageData = () => {
        const storage = getLocalStorage(`${selectedId}`, 'basic_info');
        if (!storage || !storage.expr_id || !storage.expr_run_id) return null;
        return storage;
    };

    useEffect(() => {
        void (async () => {
            const storage = getLocalStorageData();
            if (storage && storage.expr_run_id === selectedId) {
                let exp: any = null;
                let run = await fetchExperimentRun(storage.expr_run_id);
                if (run) exp = await fetchExperiment(run.experiment);
                if (run && exp && (run.status === 'Running' || run.status !== 'Ready to start')) {
                    const runningExprData = getLocalStorage(selectedId, 'runningExprData');
                    if (!runningExprData) {
                        const data = { _id: selectedId, step: 0, playBackTime: 0, response: '' };
                        setLocalStorage(selectedId, 'runningExprData', data);
                    }
                    setFound(true);
                    setIsExprRunning(true);
                    setRunSpinningPage(false);
                    setExprRunConf(storage.expr_run_config);
                } else if (run && run.status === 'Finished') {
                    navigate(`/run_experiment/${selectedId}/feedback`);
                }
            }
            setRunSpinningPage(false);
        })();
    }, []);

    return (
        <>
            {
                runSpinningPage ? <SpinnerPage text={''} /> :
                found ? <ExprRun
                            navigate={navigate}
                            expr={expr}
                            exprRun={exprRun}
                            exprRunCon={exprRunConf}
                            isExprRunning={isExprRunning}
                            updateExprRun={updateExperimentRun}
                            updateSubject={updateSubjectData}
                        /> : <NotFoundPage text={'Something went wrong or invalid URL'} />
            }
        </>
    );
}
