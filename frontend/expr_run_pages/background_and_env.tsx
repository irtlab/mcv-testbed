import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import { useSnackbar } from 'notistack';
import { useNavigate, useParams } from "react-router-dom";
import {
    Button,
    Grid,
    Typography,
    FormControl,
    FormControlLabel,
    Radio,
    RadioGroup,
    Select,
    InputLabel,
    MenuItem,
    TextField,
    Checkbox
} from '@material-ui/core';
import { axiosAuth } from '../auth';
import { SpinnerPage } from '../components/spinner_page';
import NotFoundPage from '../components/not_found';
import { createAlert, getLocalStorage, getBackendURL } from '../utils';


const textFieldWidth = 820;

const api = axios.create({baseURL: `${getBackendURL()}api`, withCredentials: true});
api.interceptors.response.use(undefined, axiosAuth);


function getCommonQandA() {
    return [
        {
            question: 'Are you a native English speaker (a person whose first language and primary means of communication is English)?',
            options: [
                'Yes',
                'No'
            ]
        }, {
            question: 'Which of the following best describes your location for this test?',
            options: [
                'Private room with closed door (e.g. office, bedroom)',
                'Private open area (e.g. living room, family room)',
                'Public open area (e.g. lobby, breakroom, library, cafe)'
            ]
        },
        {
            question: 'Which of the following best describes the noise environment of the location?',
            options: [
                'No noises or minimal noises',
                'Moderate noises',
                'Significant noises'
            ]
        },
        {
            question: 'Which of the following best describes the visual environment of the location?',
            options: [
                'No or few visual distractions',
                'Moderate visual distractions',
                'Significant visual distractions'
            ]
        }, {
            question: 'How familiar are you with the NATO alphabet?',
            options: [
                'Not at all familiar',
                'Not so familiar or few code words',
                'Very familiar',
                'Extremely familiar (first responder)'
            ]
        }
    ];
}


function getGameQandA() {
    const qa = getCommonQandA();
    qa.push({
        question: 'Are you familiar with playing First Person Action/Adventure video games?',
        options: [
            'Not at all familiar (never played)',
            'Not so familiar or played a few games',
            'Very familiar',
            'Extremely familiar (gamer)'
        ]
    });
    return qa;
}


function SelectSubjectHearing({ subject, setSubjectData }) {
    const [checked, setChecked] = React.useState(false);


    const handlePreferNottoSay = () => {
        setChecked(checked ? false : true);
        setSubjectData({...subject, hearing_loss_diagnose: checked ? false : true});
    };

    return (
        <div>
            <FormControlLabel
                style={{marginLeft: '28%'}}
                control={(
                    <Checkbox
                        checked={checked || false}
                        onChange={handlePreferNottoSay}
                        name="hearinglossdiagnose"
                        color="primary"
                    />
                )}
                label="Check if you have been diagnosed with hearing loss"
            />
        </div>
    );
}

SelectSubjectHearing.propTypes = {
    subject        : PropTypes.object.isRequired,
    setSubjectData : PropTypes.func.isRequired
};


function SelectSubjectAge({ subject, setSubjectData }) {
    const [errorText, setErrorText] = React.useState('');
    const [preferNotToSay, setPreferNotToSay] = React.useState(false);


    const handleAge = (event) => {
        setErrorText('');
        const age = Number(event.target.value);
        if (age < 18) setErrorText('Age must be 18 or above.');
        setSubjectData({...subject, age});
    };

    const handlePreferNottoSay = () => {
        setPreferNotToSay(preferNotToSay ? false : true);
        if (!preferNotToSay) setSubjectData({...subject, age: '-1'});
        else setSubjectData({...subject, age: ''});
    };

    return (
        <div>
            <TextField
                style={{marginLeft: '28.5%'}}
                type="number"
                disabled={preferNotToSay}
                value={subject.age === '-1' ? '' : subject.age}
                error={errorText.length !== 0}
                helperText={errorText}
                id="Age"
                label="Age *"
                onChange={handleAge}
            />
            <FormControlLabel
                style={{marginLeft: '20px', marginTop: '5px', marginBottom: '-10px'}}
                control={(
                    <Checkbox
                        checked={preferNotToSay || false}
                        onChange={handlePreferNottoSay}
                        name="prefernottosay"
                        color="primary"
                    />
                )}
                label="Prefer not to say"
            />
        </div>
    );
}

SelectSubjectAge.propTypes = {
    subject        : PropTypes.object.isRequired,
    setSubjectData : PropTypes.func.isRequired
};


function SelectSubjectGender({ subject, setSubjectData }) {
    const [preferNotToSay, setPreferNotToSay] = React.useState(false);

    const handlePreferNottoSay = () => {
        setPreferNotToSay(preferNotToSay ? false : true);
        if (!preferNotToSay) setSubjectData({...subject, gender: 'Prefer not to say'});
        else setSubjectData({...subject, gender: ''});
    };

    const handleGender = (event) => {
        setSubjectData({...subject, gender: event.target.value});
    };

    return (
        <>
            <FormControl required style={{minWidth: 205, marginLeft: '28.5%'}}>
                <InputLabel id="gender-select-required-label">Gender</InputLabel>
                <Select
                    labelId="gender-select-required-label"
                    id="gender-select-required"
                    disabled={preferNotToSay}
                    value={subject.gender}
                    onChange={handleGender}
                >
                    {
                        ['Female', 'Male', 'Other'].map(element => (
                            <MenuItem key={element} value={element}>
                                {' '}
                                {element}
                                {' '}
                            </MenuItem>
                        ))
                    }
                </Select>
            </FormControl>
            <FormControlLabel
                style={{marginLeft: '20px', marginTop: '5px', marginBottom: '-10px'}}
                control={(
                    <Checkbox
                        checked={preferNotToSay || false}
                        onChange={handlePreferNottoSay}
                        name="prefernottosay"
                        color="primary"
                    />
                )}
                label="Prefer not to say"
            />
        </>
    );
}

SelectSubjectGender.propTypes = {
    subject        : PropTypes.object.isRequired,
    setSubjectData : PropTypes.func.isRequired
}


function SelectPlatformID({ subject, setSubjectData }) {
    const handleID = (event) => {
        setSubjectData({...subject, platform_id: event.target.value.trim()});
    };

    return (
        <div>
            <TextField
                style={{marginLeft: '28.5%'}}
                type="string"
                value={subject.platform_id}
                id="PlatformID"
                label="Platform (or worker) ID *"
                onChange={handleID}
            />
        </div>
    );
}

SelectPlatformID.propTypes = {
    subject        : PropTypes.object.isRequired,
    setSubjectData : PropTypes.func.isRequired
};


// TODO: Keep the selected values in the local storage and set them again if
// the subject comes back to the page by pressing browser's back button.
function SubjectBackgroundEnv({ navigate, exprRun, updateExprRun, updateSubject, practiceExpr, exprType }) {
    const [subjectData, setSubjectData] = React.useState({
        _id: exprRun.subject._id,
        age: '',
        gender: '',
        hearing_loss_diagnose: false,
        platform_id: '',
        date: new Date(),
        experiment_run: exprRun._id,
    });

    let env_questions = getCommonQandA();
    if (exprType === 'interactive') env_questions = getGameQandA();

    const [value, setValue] = React.useState(
        new Array(env_questions.length).fill(null).map(()=> ({question: null, answer: null}))
    );

    const handleSelect = (event, idx) => {
        const valueCopy: any[] = [...value];
        valueCopy[idx] = {question: env_questions[idx].question, answer: event.target.value};
        setValue(valueCopy);
    };

    const handleNextBtn = async (data) => {
        await updateExprRun({_id: exprRun._id, subject_environment: data});
        await updateSubject(subjectData);
        navigate();
    };

    const disableNextBtn = () => {
        return !((Number(subjectData.age) >= 18 || subjectData.age === '-1') &&
        subjectData.gender.length > 0 &&
        subjectData.platform_id.length > 0 &&
        value.every(elem => elem.answer !== null));
    };

    return (
        <div>
            <Grid container spacing={4} justifyContent="center">
                <Grid item xs={12} justifyContent="center" container>
                    <Typography variant="h3">
                        Background and Environment
                    </Typography>
                </Grid>
                <Grid item xs={12} justifyContent="center" container style={{maxWidth: textFieldWidth}}>
                    <Typography variant="h6">
                        Please tell us about your background, listening environment, and familiarity with the NATO alphabet.
                        Your selections will not be associated with your responses and may only be published in aggregate statistics.
                    </Typography>
                </Grid>
                <br />
                {
                    env_questions.map((val, idx) => (
                        <Grid item xs={12} key={idx}>
                            <FormControl component="fieldset" style={{marginLeft: '28.5%'}}>
                                <Typography style={{ fontWeight: 600 }} variant="subtitle1">{val.question}</Typography>
                                <RadioGroup aria-label="envq" name="envq" value={value[idx].answer} onChange={(event) => handleSelect(event, idx)} >
                                    {
                                        val.options.map(choice => (
                                            <FormControlLabel key={choice} value={choice} control={<Radio />} label={choice} />
                                        ))
                                    }
                                </RadioGroup>
                            </FormControl>
                        </Grid>
                    ))
                }
                <br/>
                <Grid item xs={12}>
                    {
                        !practiceExpr ? <SelectSubjectGender subject={subjectData} setSubjectData={setSubjectData} /> : null
                    }
                </Grid>
                <Grid item xs={12}>
                    {
                        !practiceExpr ? <SelectSubjectAge subject={subjectData} setSubjectData={setSubjectData} /> : null
                    }
                </Grid>
                <Grid item xs={12}>
                    {
                        !practiceExpr ? <SelectSubjectHearing subject={subjectData} setSubjectData={setSubjectData} /> : null
                    }
                </Grid>
                <Grid item xs={12}>
                    {
                        !practiceExpr ? <SelectPlatformID subject={subjectData} setSubjectData={setSubjectData} /> : null
                    }
                </Grid>
                <Grid item xs={12} justifyContent="center" container style={{maxWidth: textFieldWidth}}>
                    <Button
                        fullWidth
                        size="large"
                        variant="contained"
                        color="primary"
                        disabled={0 || disableNextBtn()}
                        onClick={() => handleNextBtn(value)}
                    >
                        Next Step
                    </Button>
                </Grid>
            </Grid>
        </div>
    );
}

SubjectBackgroundEnv.defaultProps = {
    practiceExpr : false
};

SubjectBackgroundEnv.propTypes = {
    navigate      : PropTypes.func.isRequired,
    exprRun       : PropTypes.object.isRequired,
    updateExprRun : PropTypes.func.isRequired,
    updateSubject : PropTypes.func.isRequired,
    practiceExpr  : PropTypes.bool,
    exprType      : PropTypes.string.isRequired
};


export default function ExprRunBackgroundEnvView() {
    const alert = createAlert(useSnackbar().enqueueSnackbar);
    const navigate = useNavigate();
    const params = useParams();

    const [selectedId, setSelectedId] = React.useState((params as any).id);
    const [runSpinningPage, setRunSpinningPage] = React.useState(true);
    const [exprRun, setExprRun] = React.useState({});
    const [localStorage, setLocalStorage] = React.useState({});
    const [found, setFound] = React.useState(false);
    const [practiceExpr, setPracticeExpr] = React.useState(false);

    if ((params as any).id !== selectedId) setSelectedId((params as any).id);

    const navigatePage = (id: string = '') => {
        navigate(`/run_experiment/${selectedId}/consent`);
    };

    async function updateSubjectData(data) {
        // If the experiment is practice, then we do not update the subject data.
        if (practiceExpr) return;

        try {
            await api.post(`/subject_data/${data._id}`, data);
        } catch (error) {
            alert('Error while updating the experiment subject data:', error);
        }
    }

    async function fetchExperimentRun(id: string) {
        try {
            const rv = await api.get(`/experiment_run/${id}`);
            setExprRun(rv.data);
            return rv.data;
        } catch (error) {
            alert('Error while fetching experiment run:', error);
        }
        return null;
    }

    async function updateExperimentRun(data) {
        try {
            const rv = await api.post(`/experiment_run/${data._id}`, data);
            setExprRun(rv.data);
            return rv.data;
        } catch (error) {
            alert('Error while updating the experiment run:', error);
        }
    }

    const getLocalStorageData = () => {
        const storage = getLocalStorage(`${selectedId}`, 'basic_info');
        if (!storage || !storage.expr_run_id) return null;
        setLocalStorage(storage);
        return storage;
    };

    useEffect(() => {
        void (async () => {
            const runningExprData: any = getLocalStorage(selectedId, 'runningExprData');
            if (runningExprData && runningExprData._id === selectedId) {
                navigate(`/run_experiment/${selectedId}/run`);
                return;
            }

            const storage = getLocalStorageData();
            if (storage) {
                const run = await fetchExperimentRun(storage.expr_run_id);
                if (run && run.status !== 'Finished' && run.status !== 'Running') {
                    setPracticeExpr(storage.practice_expr);
                    setFound(true);;
                }
            }
            setRunSpinningPage(false);
        })();
    }, []);

    return (
        <>
            {
                runSpinningPage ? <SpinnerPage text={''} /> :
                found ? <SubjectBackgroundEnv
                            navigate={navigatePage}
                            exprRun={exprRun}
                            updateExprRun={updateExperimentRun}
                            updateSubject={updateSubjectData}
                            practiceExpr={practiceExpr}
                            exprType={(localStorage as any).expr_type}
                        /> : <NotFoundPage text={'Page not found or invalid URL'} />
            }
        </>
    );
}
