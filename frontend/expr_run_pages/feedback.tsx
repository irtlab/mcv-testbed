import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import { useSnackbar } from 'notistack';
import { useNavigate, useParams } from "react-router-dom";
import {
    Button,
    Grid,
    Container,
    Typography,
    TextField
} from '@material-ui/core';
import { ExperimentCompleted } from '../components/expr_completed';
import { axiosAuth } from '../auth';
import { SpinnerPage } from '../components/spinner_page';
import NotFoundPage from '../components/not_found';
import { createAlert, getLocalStorage, getBackendURL, clearLocalStorage, setLocalStorage } from '../utils';


const textFieldWidth = 820;

const api = axios.create({baseURL: `${getBackendURL()}api`, withCredentials: true});
api.interceptors.response.use(undefined, axiosAuth);


function Feedback({ exprRunID, subjectID, updateSubjectData, setExprCompleted, amtSurveyCode }) {
    // This is for triggering setTimeout call.
    const [countUpTime, setCountUpTime] = React.useState<number>(0);
    const [feedbackText, setFeedbackText] = React.useState('');
    const feedbackRef = React.useRef<HTMLInputElement | null>(null);

    const handleDone = () => {
        clearLocalStorage(exprRunID);
        setExprCompleted(true);
    };

    const handleSubmitBtn = () => {
        const text = feedbackText.trim();
        updateSubjectData({_id: subjectID, feedback: text});
        handleDone();
    };

    useEffect(() => {
        const timeout = setTimeout(() => {
            if (!feedbackText) {
                feedbackRef.current?.focus();
            }

            const data = {_id: exprRunID, type: 'feedback', feedback: feedbackText};
            setLocalStorage(exprRunID, 'runningExprData', data);

            setCountUpTime(countUpTime + 1);
        }, 500);

        return () => { clearTimeout(timeout); };
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [feedbackText, countUpTime]);

    return (
        <div>
            <Container>
                <Grid justifyContent="center" container>
                    {[0].map((value) => (
                        <Grid key={value} item>
                            <br />
                            <div>
                                <Typography style={{textAlign: "center",}} variant="h3" gutterBottom>
                                    Experiment Feedback
                                </Typography>
                                <br />
                                <Typography style={{width: textFieldWidth}} variant="body1" gutterBottom>
                                    Thank you for participating! If you wish to provide additional feedback about
                                    the experiment, please use the text field below. In particular, we appreciate
                                    any feedback you think might be important for experiment evaluation.
                                    This step is optional.
                                </Typography>
                                <br />
                                <br />
                                <TextField
                                    inputRef={feedbackRef}
                                    style={{ width: textFieldWidth }}
                                    id="Feedback"
                                    label="Feedback"
                                    helperText="(optional)"
                                    multiline
                                    rows={6}
                                    variant="outlined"
                                    value={feedbackText}
                                    onChange={event => setFeedbackText(event.target.value)}
                                />
                                <br />
                                <br />
                                <div style={{textAlign: "end"}}>
                                    <Button
                                        disabled={!feedbackText}
                                        size="small"
                                        variant="outlined"
                                        color="primary"
                                        onClick={handleSubmitBtn}
                                    >
                                        Submit
                                    </Button>
                                    <Button
                                        style={{marginLeft: "40px"}}
                                        size="small"
                                        variant="outlined"
                                        color="primary"
                                        onClick={handleDone}
                                    >
                                        Skip
                                    </Button>
                                </div>
                            </div>
                            <br />
                            <br />
                            <br />
                        </Grid>
                    ))}
                </Grid>
                <Grid container spacing={2} justifyContent="center" >
                    <Grid item xs={6} md={8}>
                        <Typography variant="h4" style={{ width: "75%", marginLeft: "205px"}} >
                            {
                                `Survey Code: ${amtSurveyCode}`
                            }
                        </Typography>
                    </Grid>
                    <Grid item xs={6} md={4}>
                        <Button
                            style={{ width: "48%"}}
                            size="medium"
                            variant="contained"
                            color="primary"
                            onClick={() => navigator.clipboard.writeText(amtSurveyCode)}
                        >
                            Copy to clipboard
                        </Button>
                    </Grid>
                </Grid>
            </Container>
        </div>
    );
}

Feedback.defaultProps = {
    amtSurveyCode : null
};

Feedback.propTypes = {
    exprRunID        : PropTypes.string.isRequired,
    subjectID        : PropTypes.string.isRequired,
    updateSubjectData: PropTypes.func.isRequired,
    setExprCompleted : PropTypes.func.isRequired,
    amtSurveyCode    : PropTypes.string
};


export default function FeedbackView() {
    const alert = createAlert(useSnackbar().enqueueSnackbar);
    const navigate = useNavigate();
    const params = useParams();

    const [exprRun, setExprRun] = React.useState({});
    const [selectedId, setSelectedId] = React.useState((params as any).id);
    const [runSpinningPage, setRunSpinningPage] = React.useState(true);
    const [found, setFound] = React.useState(false);
    const [exprCompleted, setExprCompleted] = React.useState(false);

    if ((params as any).id !== selectedId) setSelectedId((params as any).id);

    async function fetchExperimentRun(id: string) {
        try {
            const rv = await api.get(`/experiment_run/${id}`);
            setExprRun(rv.data);
            return rv.data;
        } catch (error) {
            alert('Error while fetching experiment run:', error);
        }
        return null;
    }

    async function updateSubjectData(data) {
        try {
            await api.post(`/subject_data/${data._id}`, data);
        } catch (error) {
            alert('Error while updating the experiment subject data:', error);
        }
    }

    useEffect(() => {
        void (async () => {
            const runningExprData: any = getLocalStorage(selectedId, 'runningExprData');
            if (runningExprData && runningExprData._id === selectedId && runningExprData.type !== 'feedback') {
                navigate(`/run_experiment/${selectedId}/run`);
                return;
            }

            const run = await fetchExperimentRun(selectedId);
            if (run && run.status === 'Finished') {
                const ls = getLocalStorage(`${run._id}`, 'feedback');
                if (ls !== null) {
                    setFound(true);
                } else {
                    setExprCompleted(true);
                }
            }
            setRunSpinningPage(false);
        })();
    }, []);

    return (
        <>
            {
                runSpinningPage ? <SpinnerPage text={''} /> : exprCompleted ?
                <ExperimentCompleted completed={'done'} authorText={'Completed!'} amtSurveyCode={(exprRun as any).amt_survey_code} /> :
                found ? <Feedback
                            exprRunID={(exprRun as any)._id}
                            subjectID={(exprRun as any).subject._id}
                            updateSubjectData={updateSubjectData}
                            setExprCompleted={setExprCompleted}
                            amtSurveyCode={(exprRun as any).amt_survey_code}
                        /> : <NotFoundPage text={'Something went wrong or invalid URL'} />
            }
        </>
    );
}
