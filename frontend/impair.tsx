import React, { useState, useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { blue } from '@material-ui/core/colors';
import { useSnackbar } from 'notistack';
import {
    CircularProgress,
    MenuItem,
    TextField,
    TextFieldProps,
    Grid,
    Button,
    InputAdornment,
    Divider,
} from '@material-ui/core';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import StopIcon from '@material-ui/icons/Stop';
import { makeStyles } from '@material-ui/core/styles';
import { fetchAuth } from './auth';
import { createAlert, MediaFile, getBackendURL } from './utils';


const useStyles = makeStyles((theme) => ({
    paper: {
        padding : theme.spacing(1)
    },
    margin: {
        marginTop    : theme.spacing(1),
        marginBottom : theme.spacing(1)
    },
    wrapper: {
        margin   : theme.spacing(1),
        position : 'relative',
    },
    buttonProgress: {
        color      : blue[500],
        position   : 'absolute',
        top        : '50%',
        left       : '50%',
        marginTop  : -12,
        marginLeft : -12,
    }
}));


function AudioInputBackground({ accept, files, loadFiles, value, setValue, ...args }) {
    const STATE_UPLOAD = -2;
    const STATE_NONE   = -1;

    const ref = useRef<any | null>(null);
    const [ state, setState ] = useState(value || STATE_NONE);

    useEffect(() => {
        setState(value || STATE_NONE);
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [value]);

    async function uploadSelect() {
        const input = ref.current;
        if (!input || !input.files || input.files.length === 0) {
            throw new Error('No files selected');
        }

        const f = await loadFiles(input.files);
        setState(f[0].id);
        setValue(f[0].id);
    }

    function openFileDialog() {
        const input = ref.current;
        if (input !== null) {
            // Reset the input's value so that the onChange handler gets always
            // triggered.
            input.value = '';
            input.click();
        }
    }

    function onSelect(event) {
        const id = event.target.value;
        setState(id);
        if (id === STATE_UPLOAD) {
            return;
        }

        if (id === STATE_NONE) {
            setValue(null);
        }

        setValue(id);
    }

    useEffect(() => {
        if (state === STATE_UPLOAD) {
            openFileDialog();
            setState(STATE_NONE);
            setValue(null);
        }

    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [state]);

    return (
        <>
            <input
                ref={ref}
                accept={accept}
                onChange={uploadSelect}
                style={{display: 'none'}}
                multiple
                type="file"
            />
            <TextField
                select
                margin="dense"
                variant="filled"
                onChange={onSelect}
                value={state}
                {...args}
            >
                <MenuItem value={STATE_UPLOAD}>Upload File(s)</MenuItem>
                <MenuItem value={STATE_NONE}><i>(None)</i></MenuItem>
                { files.length ? <Divider /> : null }
                { files.map(({id, name}) => <MenuItem key={id} value={id}>{name}</MenuItem>) }
            </TextField>
        </>
    );

}

AudioInputBackground.propTypes = {
    accept    : PropTypes.string,
    files     : PropTypes.array,
    loadFiles : PropTypes.func.isRequired,
    value     : PropTypes.string,
    setValue  : PropTypes.func.isRequired
};

AudioInputBackground.defaultProps = {
    accept : null,
    value  : null,
    files  : []
};


function AudioInput({ accept, files, loadFiles, value, updatedTTS, setValue, ...args }) {
    const STATE_TTS = -3;
    const STATE_UPLOAD = -2;
    const STATE_NONE   = -1;

    const ref = useRef<any | null>(null);
    const [ state, setState ] = useState(value || STATE_NONE);

    useEffect(() => {
        setState(value || STATE_NONE);
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [value]);

    async function uploadSelect() {
        const input = ref.current;
        if (!input || !input.files || input.files.length === 0) {
            throw new Error('No files selected');
        }

        const f = await loadFiles(input.files);
        setState(f[0].id);
        setValue(f[0].id);
    }

    function openFileDialog() {
        const input = ref.current;
        if (input !== null) {
            // Reset the input's value so that the onChange handler gets always
            // triggered.
            input.value = '';
            input.click();
        }
    }

    function onSelect(event) {
        const id = event.target.value;
        setState(id);
        updatedTTS(false);
        if (id === STATE_UPLOAD) {
            return;
        }

        if (id === STATE_NONE) {
            setValue(null);
            return;
        }

        if (id === STATE_TTS) {
            return;
        }

        setValue(id);
    }

    useEffect(() => {
        if (state === STATE_UPLOAD) {
            openFileDialog();
            setState(STATE_NONE);
            updatedTTS(false);
            setValue(null);
        }

        if (state === STATE_TTS) {
            updatedTTS(true);
        }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [state]);

    return (
        <>
            <input
                ref={ref}
                accept={accept}
                onChange={uploadSelect}
                style={{display: 'none'}}
                multiple
                type="file"
            />
            <TextField
                select
                margin="dense"
                variant="filled"
                onChange={onSelect}
                value={state}
                {...args}
            >
                <MenuItem value={STATE_UPLOAD}>Upload File(s)</MenuItem>
                <MenuItem value={STATE_TTS}>Google Text to Speech</MenuItem>
                <MenuItem value={STATE_NONE}><i>(None)</i></MenuItem>
                { files.length ? <Divider /> : null }
                { files.map(({id, name}) => <MenuItem key={id} value={id}>{name}</MenuItem>) }
            </TextField>
        </>
    );

}

AudioInput.propTypes = {
    accept    : PropTypes.string,
    files     : PropTypes.array,
    loadFiles : PropTypes.func.isRequired,
    value     : PropTypes.string,
    setValue  : PropTypes.func.isRequired,
    updatedTTS: PropTypes.func.isRequired,
};

AudioInput.defaultProps = {
    accept : null,
    value  : null,
    files  : []
};



type NumberInputProps = {
    min?: number;
    max?: number;
    defaultValue?: number;
    value?: number | null | undefined;
    unit?: string;
    setValue: (v: number | null) => void;
} & TextFieldProps;


export function NumberInput({ min, unit, max, value, defaultValue, setValue, ...args }: NumberInputProps) {
    const [ state, setState ] = useState('');

    function invalid() {
        if (state === '') return false; // No value
        const v = parseFloat(state);
        if (Number.isNaN(v)) return true;
        if (min !== undefined && v < min) return true;
        if (max !== undefined && v > max) return true;
        return false;
    }

    useEffect(() => {
        if (value !== undefined && value !== null) setState(`${value}`);
        else if (defaultValue !== undefined) setState(`${defaultValue}`);
        else setState('');
    }, [value, defaultValue]);

    function getValue() {
        if (state === '') return null;
        const v = parseFloat(state);
        if (Number.isNaN(v)) return defaultValue || null;
        return v;
    }

    const inputProps: any = {
        min : min ? `${min}` : null,
        max : max ? `${max}` : null
    };

    if (unit) {
        inputProps.endAdornment = (
            <InputAdornment style={{paddingTop: '18px'}} position="end">
                {unit}
            </InputAdornment>
        );
    }

    return (
        <TextField
            error={invalid()}
            margin="dense"
            variant="filled"
            value={state}
            onChange={e => setState(e.target.value)}
            onBlur={() => {
                const v = getValue();
                setState(`${v || ''}`);
                setValue(v);
            }}
            InputProps={inputProps}
            {...args}
        />
    );
}


export const CodecInput = ({ setCodec, ...args }) => (
    <TextField select variant="filled" margin="dense" label="CODEC" onChange={e => setCodec(e.target.value)} {...args}>
        <MenuItem value="l16">None (raw audio)</MenuItem>
        <MenuItem value="g711u">G.711u</MenuItem>
        <MenuItem value="g711a">G.711a</MenuItem>
        <MenuItem value="gsm">GSM</MenuItem>
        <MenuItem value="codec2">CODEC2 (2400 bit/s)</MenuItem>
        <MenuItem value="amrnb">AMR Narrowband</MenuItem>
        <MenuItem value="amrwb">AMR Wideband</MenuItem>
        <MenuItem value="p25p1">P.25 Phase 1</MenuItem>
        <MenuItem value="p25p2">P.25 Phase 2</MenuItem>
        <MenuItem value="opus">Opus (48 kHz)</MenuItem>
    </TextField>
);

CodecInput.propTypes = {
    setCodec : PropTypes.func.isRequired
};


function ProgressButton({ children, working, ...args }) {
    const classes = useStyles();
    return (
        <div className={classes.wrapper}>
            <Button {...args}>
                {children}
            </Button>
            {working && <CircularProgress size={24} className={classes.buttonProgress} />}
        </div>
    );
}

ProgressButton.propTypes = {
    working  : PropTypes.bool.isRequired,
    children : PropTypes.element.isRequired
};


interface ImpairmentProps {
    _id: string;
    expr_name: string;
    step_number: number;
    files: any[];
    value: Record<string, any>;
    setValue: (v: Record<string, any>) => void;
    dispatch: (v: any) => void;
    audio: string | null;
    setAudio: (v: string | null) => void;
}


function Impairment_({ dispatch, _id, expr_name, step_number, files, value, setValue, audio = null, setAudio }: ImpairmentProps) {
    const alert = createAlert(useSnackbar().enqueueSnackbar);
    const ref = useRef<HTMLAudioElement | null>(null);
    const inputEl = useRef<HTMLInputElement | null>(null);
    const [ settings, setSettings ] = useState({...value});
    const [ processing, setProcessing ] = useState(false);
    const [ playing, setPlaying ] = useState(false);
    const [ useTTS, setTTS ] = useState(false);
    const [ text, setText ] = useState<string | undefined>(value.text);
    const [ prevText, setPrev ] = useState('');
    const [ prevAudio, setPrevAudio ] = useState<string | null>(null);
    const [ invalidTTS, setInvalidTTS ] = useState(false);
    const [ invalidGender, setInvalidGender ] = useState(false);
    const [ errorMessage, setMessage ] = useState('');
    const [ gender, setGender ] = useState('');

    async function setTextToSpeechSettings() {
        if (settings.mic === '-3' && audio) {
            const res = await fetchAuth(`${getBackendURL()}api/media/${audio.split('/')[5]}/props`, {
                method  : 'GET',
                headers : { 'Content-Type': 'application/json' }
            });
            if (!res.ok) {
                alert(`${res.status} ${res.statusText}`, undefined);
                return;
            }

            const data = await res.json();
            setGender(data.attrs.gender);
            setPrev(data.attrs.original_transcript);
            setText(data.attrs.original_transcript);
            setPrevAudio(audio);
        }
    }

    useEffect(() => {
        setSettings({...value});
        void setTextToSpeechSettings();
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [ value ]);

    useEffect(() => {
        setPlaying(false);
    }, [ audio ]);

    function canImpair() {
        if (settings.codec) {
            if (useTTS) {
                if (!gender.length) return false;
                if (text === undefined || !text.trim().length) return false;
                return true;
            } else {
                if (settings.mic) return true;
            }
        }
        return false;
    }

    async function impair() {
        setProcessing(true);
        try {
            const s = {...settings};
            if (s.mic && s.mic !== '-3') {
                s.mic = files.find(({id}) => id === s.mic).url;
            }

            if (s.noise) {
                s.noise = files.find(({id}) => id === s.noise).url;
            }

            if (useTTS || s.mic === '-3') {
                if (!text || text.length === 0) {
                    setMessage('Must enter text to submit to Google Text to Speech API');
                    setInvalidTTS(true);
                    return;
                }
                if (gender.length === 0) {
                    setInvalidGender(true);
                    return;
                }

                s.mic = '-3';
                s.text = text.trim();
                s.gender = gender;
            } else {
                s.text = null;
            }

            s.step_number = step_number;
            s.expr_name = expr_name;
            const res = await fetchAuth(`${getBackendURL()}api/impair`, {
                method  : 'POST',
                body    : JSON.stringify(s),
                headers : {
                    'Content-Type' : 'application/json',
                    'Accept'       : 'application/vnd+mcv-testbed-url'
                }
            });

            if (!res.ok) {
                setMessage(await res.text());
                setInvalidTTS(true);
            }
            else {
                try {
                    setInvalidTTS(false);
                    setInvalidGender(false);
                    setPrev(s.text);
                    const aud = await res.text();
                    setAudio(aud);
                    setPrevAudio(aud);
                } catch(error) {
                    alert('Error while generating audio file', error);
                }
            }
        }
        finally {
            setProcessing(false);
        }
    }

    function play() {
        const audioEl = ref.current;
        if (audioEl) void audioEl.play();
    }

    function stop() {
        const audioEl = ref.current;
        if (audioEl) {
            audioEl.pause();
            audioEl.currentTime = 0;
        }
    }

    async function loadFiles(fileData) {
        // Wrap each File with MediaFile
        const mf = Array.from(fileData).map(f => new MediaFile(f));

        // Obtain the content for each file and calculate a unique ID for the file
        // based on its metadata. Return a simple object that represents the file
        // and contains attributes such as id, url, name, etc.

        // eslint-disable-next-line no-shadow
        const files = await Promise.all(mf.map(async f => {
            const id = await f.metaDigest();
            const url = await f.dataUrl();
            return { ...f.meta(), id, url };
        }));

        dispatch({ type  : 'LOAD_FILES', _id, files });
        return files;
    }

    function update(s: Record<string, any>) {
        setSettings(s);
        setValue(s);
        setAudio(null);
    }

    const updatedTTS = (index) => {
        setTTS(index);
        setText('');
        setAudio(null);
        setGender('');
        update({...settings, mic: '-3'});
        if(!inputEl.current || !inputEl.current.value) {
            setText('');
        }
    };

    function enterTTS(e) {
        if (inputEl.current === null)
            throw new Error('Input element ref is null');

        inputEl.current.value = e.target.value;
        setText(e.target.value);
        if (audio) {
            setAudio(null);
        }

        if (prevText.trim() === e.target.value.trim()) {
            setAudio(prevAudio);
        }

        setInvalidTTS(false);
        setTTS(true);
    }

    function selectGender(e) {
        setGender(e.target.value);
        if (audio) {
            setAudio(null);
        }

        setInvalidGender(false);
    }

    if (useTTS === false && settings.mic !== '-3') {
        return (
            <Grid container spacing={1} alignItems="center">
                <Grid item xs={2}>
                    <AudioInput
                        fullWidth
                        required
                        files={files}
                        value={settings.mic}
                        loadFiles={loadFiles}
                        setValue={f => update({...settings, mic: f})}
                        disabled={processing}
                        id="mic"
                        accept="audio/wav"
                        label="Microphone Input"
                        updatedTTS={updatedTTS}
                    />
                </Grid>
                <Grid item xs={1}>
                    <NumberInput
                        fullWidth
                        disabled={processing}
                        label="Mic Level"
                        unit="%"
                        value={settings.mic_volume as number | null | undefined}
                        setValue={v => update({...settings, mic_volume: v})}
                        min={0}
                        max={500}
                    />
                </Grid>
                <Grid item xs={2}>
                    <AudioInputBackground
                        fullWidth
                        disabled={processing}
                        files={files}
                        value={settings.noise}
                        setValue={f => update({...settings, noise: f})}
                        loadFiles={loadFiles}
                        id="noise"
                        accept="audio/wav"
                        label="Background Noise"
                    />
                </Grid>
                <Grid item xs={1}>
                    <NumberInput
                        fullWidth
                        disabled={processing}
                        label="Noise Level"
                        unit="%"
                        value={settings.noise_volume}
                        setValue={v => update({...settings, noise_volume: v})}
                        min={0}
                        max={500}
                    />
                </Grid>
                <Grid item xs={2}>
                    <CodecInput
                        fullWidth
                        required
                        disabled={processing}
                        id="codec"
                        value={settings.codec}
                        setCodec={c => update({...settings, codec: c})}
                        label="CODEC"
                    />
                </Grid>
                <Grid item xs={1}>
                    <NumberInput
                        fullWidth
                        disabled={processing}
                        label="PTT Delay"
                        value={settings.ptt_delay}
                        setValue={v => update({...settings, ptt_delay: v})}
                        unit="ms"
                        min={0}
                        max={2000}
                    />
                </Grid>
                <Grid item xs={1}>
                    <NumberInput
                        fullWidth
                        disabled={processing}
                        label="Frame Loss"
                        value={settings.rx_loss}
                        setValue={v => update({...settings, rx_loss: v})}
                        unit="%"
                        min={0}
                        max={100}
                    />
                </Grid>
                <Grid item xs={1}>
                    <NumberInput
                        fullWidth
                        disabled={processing}
                        label="Bit Errors"
                        value={settings.rx_error}
                        setValue={v => update({...settings, rx_error: v})}
                        unit="%"
                        min={0}
                        max={100}
                    />
                </Grid>
                <Grid item xs={1}>
                    { audio
                        ? (
                            <Button
                                color="primary"
                                disabled={processing}
                                fullWidth
                                size="large"
                                onClick={() => playing ? stop() : play()}
                                variant="contained"
                            >
                                { playing ? <StopIcon /> : <PlayArrowIcon /> }
                            </Button>
                        ):(
                            <ProgressButton
                                disabled={!canImpair() || processing}
                                working={processing}
                                onClick={impair}
                                variant="outlined"
                                color="primary"
                                size="large"
                                fullWidth
                            >
                                <CloudUploadIcon />
                            </ProgressButton>
                        )}
                    { audio && (
                        <audio
                            onPlay={() => setPlaying(true)}
                            onEnded={() => setPlaying(false)}
                            onPause={() => setPlaying(false)}
                            ref={ref}
                            src={audio}
                            style={{display: 'none'}}
                        />
                    )}
                </Grid>
            </Grid>
        );}

    return (
        <Grid container spacing={1} alignItems="center">
            <Grid item xs={2}>
                <AudioInput
                    fullWidth
                    required
                    files={files}
                    value={settings.mic}
                    loadFiles={loadFiles}
                    setValue={f => update({...settings, mic: f})}
                    disabled={processing}
                    id="mic"
                    accept="audio/wav"
                    label="Microphone Input"
                    updatedTTS={updatedTTS}
                />
            </Grid>
            <Grid item xs={1}>
                <NumberInput
                    fullWidth
                    disabled={processing}
                    label="Mic Level"
                    unit="%"
                    value={settings.mic_volume}
                    setValue={v => update({...settings, mic_volume: v})}
                    min={0}
                    max={500}
                />
            </Grid>
            <Grid item xs={2}>
                <AudioInputBackground
                    fullWidth
                    disabled={processing}
                    files={files}
                    value={settings.noise}
                    setValue={f => update({...settings, noise: f})}
                    loadFiles={loadFiles}
                    id="noise"
                    accept="audio/wav"
                    label="Background Noise"
                />
            </Grid>
            <Grid item xs={1}>
                <NumberInput
                    fullWidth
                    disabled={processing}
                    label="Noise Level"
                    unit="%"
                    value={settings.noise_volume}
                    setValue={v => update({...settings, noise_volume: v})}
                    min={0}
                    max={500}
                />
            </Grid>
            <Grid item xs={2}>
                <CodecInput
                    fullWidth
                    required
                    disabled={processing}
                    id="codec"
                    value={settings.codec}
                    setCodec={c => update({...settings, codec: c})}
                    label="CODEC"
                />
            </Grid>
            <Grid item xs={1}>
                <NumberInput
                    fullWidth
                    disabled={processing}
                    label="PTT Delay"
                    value={settings.ptt_delay}
                    setValue={v => update({...settings, ptt_delay: v})}
                    unit="ms"
                    min={0}
                    max={2000}
                />
            </Grid>
            <Grid item xs={1}>
                <NumberInput
                    fullWidth
                    disabled={processing}
                    label="Frame Loss"
                    value={settings.rx_loss}
                    setValue={v => update({...settings, rx_loss: v})}
                    unit="%"
                    min={0}
                    max={100}
                />
            </Grid>
            <Grid item xs={1}>
                <NumberInput
                    fullWidth
                    disabled={processing}
                    label="Bit Errors"
                    value={settings.rx_error}
                    setValue={v => update({...settings, rx_error: v})}
                    unit="%"
                    min={0}
                    max={100}
                />
            </Grid>
            <Grid item xs={1}>
                { audio
                    ? (
                        <Button
                            color="primary"
                            disabled={processing}
                            fullWidth
                            size="large"
                            onClick={() => playing ? stop() : play()}
                            variant="contained"
                        >
                            { playing ? <StopIcon /> : <PlayArrowIcon /> }
                        </Button>
                    ):(
                        <ProgressButton
                            disabled={!canImpair() || processing}
                            working={processing}
                            onClick={impair}
                            variant="outlined"
                            color="primary"
                            size="large"
                            fullWidth
                        >
                            <CloudUploadIcon />
                        </ProgressButton>
                    )}
                { audio && (
                    <audio
                        onPlay={() => setPlaying(true)}
                        onEnded={() => setPlaying(false)}
                        onPause={() => setPlaying(false)}
                        ref={ref}
                        src={audio}
                        style={{display: 'none'}}
                    />
                )}
            </Grid>
            <Grid item xs={10}>
                <TextField
                    required
                    fullWidth
                    variant="outlined"
                    ref={inputEl}
                    value={text}
                    error={invalidTTS}
                    margin="dense"
                    label="Text to Convert"
                    helperText={invalidTTS ? errorMessage : null}
                    onChange={(e) => enterTTS(e)}
                />
            </Grid>
            <Grid item xs={2}>
                <TextField
                    required
                    select
                    error={invalidGender}
                    fullWidth
                    label="Voice Gender"
                    variant="filled"
                    value={gender}
                    size="small"
                    onChange={(e) => selectGender(e)}
                >
                    <MenuItem value="female"> Female </MenuItem>
                    <MenuItem value="male"> Male </MenuItem>
                </TextField>
            </Grid>
        </Grid>
    );
}


// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
export const Impairment = connect(({ experiments }, { _id }) => {
    return {
        files : (experiments.find(e => e._id === _id) || {}).files || []
    };
})(Impairment_);
