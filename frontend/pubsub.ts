import debug from 'debug';

const dbg = debug('mvc:pubsub');

export function pubsub_subscribe(sock, name) {
    return new Promise((resolve, reject) => {
        dbg(`Subscribing to '${name}'`);
        sock.emit('subscribe', name, (v, error) => {
            if (error) {
                dbg(`Error while subscribing to '${name}': ${error}`);
                reject(new Error(error));
            }
            dbg(`Subscribed to '${name}'`);
            resolve(v);
        });
    });
}