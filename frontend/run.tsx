import React, { useState, useRef, useContext, useEffect } from 'react';
import PropTypes from 'prop-types';
import { useParams, useLocation } from 'react-router-dom';
import axios from 'axios';
import { useSnackbar } from 'notistack';
import { connect } from 'react-redux';
import { useNavigate } from "react-router-dom";
import MaterialTable from 'material-table';
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';
import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline';
import FlagIcon from '@material-ui/icons/Flag';
import TimelapseIcon from '@material-ui/icons/Timelapse';
import * as EmailValidator from 'email-validator';
import {
    Button,
    Divider,
    Grid,
    List,
    Typography,
    makeStyles,
    Fab,
    TextField,
    Dialog,
    DialogActions,
    DialogTitle,
    DialogContent,
    ListItem,
    ListItemIcon,
    ListItemText,
    IconButton,
    ListItemSecondaryAction,
    CircularProgress,
    FormControl,
    FormControlLabel,
    Radio,
    RadioGroup
} from '@material-ui/core';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import DeleteIcon from '@material-ui/icons/Delete';
import AddIcon from '@material-ui/icons/Add';
import CloseIcon from '@material-ui/icons/Close';
import { ExperimentCompleted } from './components/expr_completed';
import CopyValue from './components/copy';
import { axiosAuth, UserContext } from './auth';
import { SpinnerPage } from './components/spinner_page';
import { CollectTerminalData, collectUTData } from './components/collect_ut_data';
import { SelectUserTerminal } from './components/select_user_terminal';
import { getCountUpTime, createAlert, setLocalStorage, getLocalStorage, getBackendURL } from './utils';
import { shuffle } from '@janakj/lib/random';

const textFieldWidth = 820;


const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
    },
    content: {
        flexGrow: 1
    },
    multilineColor: {
        color: 'black'
    },
    fab: {
        margin   : theme.spacing(1),
        position : "fixed",
        bottom   : theme.spacing(2),
        right    : theme.spacing(3)
    },
}));


const api = axios.create({baseURL: `${getBackendURL()}api`, withCredentials: true});
api.interceptors.response.use(undefined, axiosAuth);


// This function randomly reorders the order of experiment steps and returns it.
//
// Note that I need to generate a random order the first time when the subject
// starts the experiment and store it in the local storage until all experiment
// steps have been completed. When the subject reloads the webpage, it needs to
// get the order form the local stoage and shuffle the array in the same order.
//
// Here, instead of storing the experiment steps I create an array with the same
// length which contains indexes, randomize it and store it. Based on that
// randomization I reorder the steps array. The reason I do this, because if the
// author modifies one of the steps (e.g. changes background noise) the subject
// won't have an issue here. If the author changes the number of steps this can
// be an issue. It is assumed that nobody will modify the experiment when it is
// underway.
function randomizeExprSteps(exprRunId, _id, exprSteps) {
    const stepsOrderData = getLocalStorage(exprRunId, 'stepsOrderData');
    if (stepsOrderData && stepsOrderData._id === _id) {
        return stepsOrderData.order.map((idx) => exprSteps[idx]);
    }

    const order: number[] = [];
    for (let i = 0; i < exprSteps.length; i++) {
        order.push(i);
    }
    shuffle(order);

    // I have to keep the original order of the steps as I store steps indexes
    // in the backend which would allow to associate answers with audio files on
    // the backend. I keep it as a JSON object in the local storage where the
    // key is the shuffled array index and the value is the original index. For
    // example, of original_order = [a, b, c] and shuffle = [c, a, b] then the
    // JSON should be the following {0: 2, 1: 0, 2: 1}. Also, I provided
    // getOriginalIndex() function which takes as an argument a shuffled array
    // index and returns original index.
    const originalStepsIdx = {};
    for (let i = 0; i < order.length; i++) {
        originalStepsIdx[i] = order[i];
    }

    setLocalStorage(exprRunId, 'stepsOrderData', {_id, order});
    setLocalStorage(exprRunId, 'originalStepsIdx', originalStepsIdx);
    return order.map((idx) => exprSteps[idx]);
}


// Function does two jobs:
// 1. Randomizes single and/or multiple choices if the author set randomize option.
// 2. Randomizes the order of experiment steps if the author set randomize steps.
function randomize(exprRunId, _id, randomizeSteps, exprSteps) {
    const steps = exprSteps.map((element) => {
        if (element.answer.randomizeChoice) {
            shuffle(element.answer.choices);
        }
        return element;
    });

    if (!randomizeSteps) return steps;
    return randomizeExprSteps(exprRunId, _id, steps);
}

function SubjectsList({ subjects, setSubjects }) {
    const handleDeleteSubject = (_event, email, key) => {
        setSubjects(subjects.filter(item => item.key ? item.key !== key : item.email !== email));
    };

    const handleAddSubject = (_event, name, email) => {
        setSubjects(subj => [...subj, { name, email, key: Date.now() }]);
    };

    return (
        <div>
            <br />
            {
                subjects.length > 0 ? (
                    <Typography>
                        Subjects
                        {' '}
                        {`(${subjects.length})`}
                    </Typography>
                ) : null
            }
            <List component="nav" ria-label="contacts" style={{maxHeight: 210, overflow: 'auto'}}>
                {
                    subjects.map(item => (
                        <ListItem
                            button
                            key={item.key ? item.key : item.email}
                        >
                            <ListItemIcon>
                                <AccountCircleIcon />
                            </ListItemIcon>
                            <ListItemText primary={item.name} secondary={item.email} />
                            <ListItemSecondaryAction>
                                <IconButton
                                    edge="end"
                                    aria-label="delete"
                                    onClick={event => handleAddSubject(event, item.name, item.email)}
                                >
                                    <AddIcon />
                                </IconButton>
                                <IconButton
                                    edge="end"
                                    aria-label="delete"
                                    onClick={event => handleDeleteSubject(event, item.email, item.key)}
                                >
                                    <DeleteIcon />
                                </IconButton>
                            </ListItemSecondaryAction>
                        </ListItem>
                    ))
                }
            </List>
        </div>
    );
}

SubjectsList.propTypes = {
    subjects    : PropTypes.array.isRequired,
    setSubjects : PropTypes.func.isRequired
};


function ScheduleRun({ open, setOpen, submit }) {
    const alert = createAlert(useSnackbar().enqueueSnackbar);
    const [subjects, setSubjects] = useState<any[]>([]);
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [numberOfRuns, setNumberOfRuns] = useState('');
    const nameRef = useRef<any | null>(null);

    async function fetchExperimentRun() {
        try {
            const { data } = await api.get('/experiment_run/');
            const tmpEmailDic = {};
            const allSubjects: any[] = [];
            data.forEach((element) => {
                if (!tmpEmailDic[element.subject.email]) {
                    allSubjects.push({name: element.subject.name, email: element.subject.email});
                    tmpEmailDic[element.subject.email] = true;
                }
            });
            setSubjects(allSubjects);
        } catch (error) {
            alert('Error while fetching experiment runs:', error);
        }
    }

    const handleSaveBtn = async () => {
        try {
            // This is to ensure that the key is unique for each newly added unknown subject,
            // becuse while loop runs fast and sometimes the Date.now() value is the same for different subjects.
            let i = 0;
            let n = Number(numberOfRuns);
            while (n--) subjects.push({ name: 'unknown', email: 'unknown', assigned: false, key: Date.now() + (i++) });
            await submit(subjects);
        } finally {
            setOpen(false);
        }
    };

    const handleAddSubjectBtn = () => {
        const found = subjects.find(element => element.email === email);
        if (!found) setSubjects(subj => [...subj, { name, email, key: Date.now() }]);
        setName('');
        setEmail('');
        nameRef.current?.focus();
    };

    const disableSaaveBtn = () => {
        if (subjects.length === 0 && numberOfRuns.length === 0) return true;
        if (Number(numberOfRuns) < 0) return true;
        return false;
    };

    const validEmail = email === '' ? true : EmailValidator.validate(email);

    useEffect(() => {
        // TODO Not sure if it makes sense to set Spinner page here.
        void fetchExperimentRun();
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <Dialog
            open={open}
            onClose={() => setOpen(false)}
            aria-labelledby="add-subjects-dialog"
            fullWidth>
            <DialogTitle id="add-subjects-dialog-title">
                Schedule Listening Experiment
            </DialogTitle>
            <DialogContent dividers>
                <TextField
                    inputRef={nameRef}
                    fullWidth
                    required
                    margin="dense"
                    variant="filled"
                    id="subject-name"
                    value={name || ''}
                    label="Subject Name"
                    onChange={event => setName(event.target.value)}
                    onBlur={event => setName(event.target.value.trim())}
                />
                <br />
                <br />
                <TextField
                    fullWidth
                    type="email"
                    required
                    margin="dense"
                    error={!validEmail}
                    helperText={validEmail ? '' : 'Invalid e-mail address'}
                    variant="filled"
                    id="subject-email"
                    value={email || ''}
                    label="Subject E-mail"
                    onChange={event => setEmail(event.target.value)}
                    onBlur={event => setEmail(event.target.value.trim())}
                />
                <br />
                <br />
                <Button
                    fullWidth
                    variant="contained"
                    color="primary"
                    disabled={!(name && email && validEmail)}
                    onClick={handleAddSubjectBtn}
                >
                Add
                </Button>
                <br />
                <br />
                <Button
                    fullWidth
                    variant="contained"
                    color="primary"
                    disabled={!(subjects.length)}
                    onClick={() => setSubjects([])}
                >
                Clear List
                </Button>
                <br />
                <SubjectsList
                    subjects={subjects}
                    setSubjects={setSubjects}
                />
                <br />
                <br />
                <br />
                <br />
                <Divider />
                <Typography variant="body2">
                    Specify the number of experiment runs to generate without providing individual names and email addresses (OPTIONAL).
                </Typography>
                <Typography variant="body2">
                    NOTE: It is possible to provide only this field and save.
                </Typography>
                <TextField
                    fullWidth
                    type="number"
                    InputProps={{ inputProps: { min: 0 } }}
                    margin="dense"
                    variant="filled"
                    id="number-of-pre-generated-runs"
                    value={numberOfRuns || ''}
                    label="Provide Number of Pre-generated Runs"
                    onChange={event => setNumberOfRuns(event.target.value)}
                    onBlur={event => setNumberOfRuns(event.target.value.trim())}
                />
            </DialogContent>
            <DialogActions>
                <Button onClick={() => setOpen(false)} color="primary">
                Cancel
                </Button>
                <Button
                    disabled={disableSaaveBtn() || false}
                    onClick={handleSaveBtn}
                    color="primary"
                    autoFocus
                >
                Save
                </Button>
            </DialogActions>
        </Dialog>
    );
}

ScheduleRun.propTypes = {
    open    : PropTypes.bool.isRequired,
    setOpen : PropTypes.func.isRequired,
    submit  : PropTypes.func.isRequired
};

import { UserTerminalRegistry } from './registry';
import { UserTerminal } from '@mcv/common/terminal';

function RunInteractiveExprRun({ exprRun, setExprRun, open, setOpen }) {
    const alert = createAlert(useSnackbar().enqueueSnackbar);
    const [ button, setButton ] = useState(exprRun.status === 'Running' ? false : true);
    const [ countUpTime, setCountUpTime ] = useState<string>('');
    const [ processing, setProcessing ] = useState(false);
    const [ selectedDispatcherTermnal, setSelectedDispatcherTermnal ] = useState<string>('ut1');
    const [ selectedSubjectTermnal, setSelectedSubjectTermnal ] = useState<string>('ut2');
    const [ progressValue, setProgressValue ] = useState(0);

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const [ registry, setRegistry ] = useState<UserTerminalRegistry | null>(null);
    const [ online, setOnline ] = useState<Record<any, UserTerminal>>({});

    useEffect(() => {
        const r = new UserTerminalRegistry(name => {
            return `${getBackendURL()}proxy/${name}._ut._tcp`;
        });

        const onChange = () => {
            setOnline({...r.online});
        };
        r.on('change', onChange);
        // FIXME: We should await the following call
        r.start();
        setRegistry(r);

        return () => {
            setRegistry(null);
            // FIXME: We should await the following call
            r.stop();
            r.off('change', onChange);
        };
    }, []);

    const terminals = Object.entries({...online}).sort(([a], [b]) => a < b ? -1 : (a > b ? 1 : 0));

    async function updateExperimentRun(data) {
        try {
            const rv = await api.post(`/experiment_run/${data._id}`, data);
            setExprRun(rv.data);
            return rv.data;
        } catch (error) {
            alert('Error while updating the experiment run:', error);
        }
    }

    const handleStartBtn = async () => {
        setButton(false);
        await updateExperimentRun({_id: exprRun._id, start: 1, dispatcher_terminal: selectedDispatcherTermnal, subject_terminal: selectedSubjectTermnal});
        await Promise.all(terminals.map(([_name, ut]) => ut.startRun(exprRun._id.toString())));
    };

    const handleStopBtn = async () => {
        setProcessing(true);
        await updateExperimentRun({_id: exprRun._id, end: 1, status: 'UT data not collected'});
        await Promise.all(terminals.map(([_name, ut]) => ut.stopRun()));
        try {
            const rv = await updateExperimentRun({_id: exprRun._id, status: 'Finished'});
            await collectUTData(api, rv.start, rv.end, setProgressValue);
        } catch (error) {
            alert('Error while collecting the experiment run events:', error);
        }
        setButton(true);
        setOpen(false);
        setProcessing(false);
    };

    useEffect(() => {
        const timer = setTimeout(() => {
            const startTime = exprRun.start ? (new Date(exprRun.start)).getTime() / 1000 : 0;
            if (startTime && exprRun.status !== 'Finished') setCountUpTime(getCountUpTime(startTime));
        }, 1000);

        return () => clearTimeout(timer);
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [countUpTime, exprRun]);

    return (
        <Dialog
            open={open}
            onClose={() => setOpen(false)}
            aria-labelledby="add-subjects-dialog"
            fullWidth>
            <DialogTitle id="add-subjects-dialog-title">
                <Grid container direction="row" justify="space-between" alignItems="center">
                    Experiment Run
                    <IconButton onClick={() => setOpen(false)}>
                        <CloseIcon />
                    </IconButton>
                </Grid>
            </DialogTitle>
            <DialogContent dividers>
                <Grid container spacing={2}>
                    {
                        processing ? (
                            <>
                                <Grid xs={12} container justify = "center">
                                    <CircularProgress size={30} />
                                </Grid>
                                <Grid xs={12} container justify = "center">
                                    <Typography variant="subtitle1">{`Please wait. Collecting events may take longer - ${progressValue}%`}</Typography>
                                </Grid>
                            </>
                        ) : (
                            <>
                                <Grid item xs={12}>
                                    <Typography variant="subtitle2">
                                        { `Elapsed time: ${countUpTime}` }
                                    </Typography>
                                </Grid>
                                <Grid item xs={12}>
                                    <SelectUserTerminal onUpdate={setSelectedDispatcherTermnal} type='Dispatcher' disabled={!button} />
                                </Grid>
                                <Grid item xs={12}>
                                    <SelectUserTerminal onUpdate={setSelectedSubjectTermnal} type='Subject' disabled={!button} />
                                </Grid>
                                <Grid item xs={12}>
                                    <Button
                                        disabled={button === false || exprRun.status === 'Finished'}
                                        onClick={handleStartBtn}
                                        fullWidth
                                        variant="contained"
                                        color="primary"
                                    >
                                        Start
                                    </Button>
                                </Grid>
                                <Grid item xs={12}>
                                    <Button
                                        disabled={button === true}
                                        onClick={handleStopBtn}
                                        fullWidth
                                        variant="contained"
                                        color="primary"
                                    >
                                        Stop
                                    </Button>
                                </Grid>
                            </>
                        )
                    }
                </Grid>
            </DialogContent>
        </Dialog>
    );
}

RunInteractiveExprRun.propTypes = {
    exprRun    : PropTypes.object.isRequired,
    setExprRun :  PropTypes.func.isRequired,
    open       : PropTypes.bool.isRequired,
    setOpen    : PropTypes.func.isRequired
};


function RunStatus({ row, practiceExpr, exprType }) {
    const [ exprRun, setExprRun ] = useState(row);

    return (
        <>
            { exprRun.failed
                ? (
                    exprRun.status === 'Failed to generate audio' ? (
                        <Typography variant="subtitle1" style={{display: 'inline-flex'}}>
                            <ErrorOutlineIcon color="error" /> Audio processing error
                        </Typography>
                    ) : <ErrorOutlineIcon color="error" />
                )
                : exprRun.status === 'Finished'
                    ? <CheckCircleOutlineIcon style={{color: "green"}} />
                    : practiceExpr ? 'Practice' :
                        exprRun.status === 'Running' ? (
                            <Typography>
                                {exprRun.status}
                            </Typography>
                        ) : exprRun.status === 'Ready to start' || exprRun.status === 'Not yet started' ? (
                            <Typography variant="subtitle1" style={{display: 'inline-flex'}}>
                                <FlagIcon style={{color: "lightgreen"}} /> Ready to start
                            </Typography>
                        ) : exprRun.status === 'Generating audio' ? (
                            <Typography variant="subtitle1" style={{display: 'inline-flex'}}>
                                <TimelapseIcon style={{color: "blue"}} /> Generating audio
                            </Typography>
                        ) : exprRun.status === 'UT data not collected' && exprType === 'interactive' ? (
                            <Typography variant="subtitle1" style={{display: 'inline-flex'}}>
                                <CollectTerminalData
                                    exprRunID={exprRun._id}
                                    setExprRun={setExprRun}
                                    api={api}
                                    start={exprRun.start}
                                    end={exprRun.end}
                                />
                            </Typography>
                        ) : null
            }
        </>
    );
}

RunStatus.defaultProps = {
    practiceExpr : undefined
};

RunStatus.propTypes = {
    row          : PropTypes.object.isRequired,
    practiceExpr : PropTypes.string,
    exprType     : PropTypes.string.isRequired
};


function ExprRunFeedback({ run, setExprRun, open, setOpen }) {
    const alert = createAlert(useSnackbar().enqueueSnackbar);
    /**
     * 5 - Excellent - Imperecptible
     * 4 - Good      - Perceptible but not annoying
     * 3 - Fair      - Slightly annoying
     * 2 - Poor      - Annoying
     * 1 - Bad       - Very annoying
     */
    const questions = [
        {
            question: 'How would you assess the quality of audio in the experiment?',
            options: [
                'Excellent',
                'Good',
                'Fair',
                'Poor',
                'Bad'
            ]
        }, {
            question: 'What impact did the audio impairments have on your ability to complete the tasks efficiently and effectively?',
            options: [
                'No impact',
                'Minimal impact',
                'Moderate impact',
                'Serious Impact',
                'Unable to complete task'
            ]
        }
    ];

    const [value, setValue] = React.useState(
        new Array(questions.length).fill(null).map(()=> ({question: null, answer: null}))
    );

    const handleSelect = (event, idx) => {
        const valueCopy: any[] = [...value];
        valueCopy[idx] = {question: questions[idx].question, answer: event.target.value};
        setValue(valueCopy);
    };

    const disableBtn = () => {
        return !value.every(elem => elem.answer !== null);
    };

    const handleSubmit = async () => {
        try {
            await api.post(`/subject_data/${run.subject._id}`, {feedback: {questions: value}});
            const rv = await api.post(`/experiment_run/${run._id}`, {feedback_provided: true});
            setExprRun(rv.data);
        } catch (error) {
            alert('Error while updating the subject feedback', error);
        }
        setOpen(false);
    };

    return (
        <Dialog
            open={open}
            onClose={() => setOpen(false)}
            aria-labelledby="subject-feedback-dialog"
            fullWidth
        >
            <DialogTitle id="subject-feedback-dialog-title">
                <Grid container direction="row" justify="space-between" alignItems="center">
                    Experiment Feedback
                    <IconButton onClick={() => setOpen(false)}>
                        <CloseIcon />
                    </IconButton>
                </Grid>
            </DialogTitle>
            <DialogContent dividers>
                <Grid container spacing={4}>
                    <Grid item xs={12}>
                    {
                        questions.map((val, idx) => (
                            <Grid item xs={12} key={idx}>
                                <FormControl component="fieldset">
                                    <Typography style={{ fontWeight: 600 }} variant="subtitle1">{val.question}</Typography>
                                    <RadioGroup aria-label="envq" name="envq" value={value[idx].answer} onChange={(event) => handleSelect(event, idx)} >
                                        {
                                            val.options.map(choice => (
                                                <FormControlLabel key={choice} value={choice} control={<Radio />} label={choice} />
                                            ))
                                        }
                                    </RadioGroup>
                                </FormControl>
                            </Grid>
                        ))
                    }
                    </Grid>
                    <Grid item xs={12}>
                        <Button
                            disabled={0 || disableBtn()}
                            onClick={() => handleSubmit()}
                            fullWidth
                            variant="contained"
                            color="primary"
                        >
                            Submit
                        </Button>
                    </Grid>
                </Grid>
            </DialogContent>
        </Dialog>
    );
}

ExprRunFeedback.propTypes = {
    run        : PropTypes.object.isRequired,
    setExprRun : PropTypes.func.isRequired,
    open       : PropTypes.bool.isRequired,
    setOpen    : PropTypes.func.isRequired
};


function ExprRunDetailogPanel({ row, exprType, consentForm, genAudio }) {
    const exprRunLink = `${window.location.origin}/run_experiment/${row._id}`;
    const [ runDialogOpen, setRunDialogOpen ] = useState(false);
    const [ feedbackDialogOpen, setFeedbackDialogOpen ] = useState(false);
    const [ exprRun, setExprRun ] = useState(row);

    return (
        <List>
            <Grid container spacing={2}>
                <Grid item xs={12}>
                    {
                        exprRun.failed ? (
                            <>
                                <Typography style={{marginLeft: "25px"}} variant="body2" gutterBottom>
                                    {' '}
                                    <b>Error while generating audio:</b>
                                    {' '}
                                    {exprRun.reason}
                                </Typography>
                                <Button onClick={() => genAudio(exprRun._id)} style={{marginLeft: "25px"}} disabled={!exprRun.failed} variant="contained" color="primary">
                                    Try Again
                                </Button>
                            </>
                        ) : exprType  === 'listening' ? (
                            <CopyValue value={exprRunLink} label='Experiment Run Link' />
                        ) : (
                            <Button onClick={() => setRunDialogOpen(true)} disabled={exprRun.status === 'Finished' || exprRun.status === 'UT data not collected'} fullWidth variant="contained" color="primary">
                                {
                                    exprRun.status === 'Running' ? 'Click to See the Experiment Status' : exprRun.status === 'Finished' ? 'Finished' :
                                        exprRun.status === 'UT data not collected' ? 'Subject Finished' : 'Start Experiment Run'
                                }
                            </Button>
                        )
                    }
                </Grid>
                <Grid item xs={12}>
                    {
                        exprType === 'interactive' ? (
                            <Button
                                onClick={() => setFeedbackDialogOpen(true)}
                                disabled={exprRun.status !== 'Finished' || exprRun.feedback_provided === true}
                                fullWidth
                                variant="contained"
                                color="primary">
                                {
                                    exprRun.feedback_provided ? 'Feedback Provided' : 'Provide Feedback'
                                }
                            </Button>
                        ) : null
                    }
                </Grid>
                { !exprRun.failed && exprType === 'interactive' && consentForm ? (
                    <Grid item xs={12}>
                        <CopyValue value={exprRunLink} label='Experiment Run Link'/>
                    </Grid>
                ) : '' }
            </Grid>
            { runDialogOpen ? (
                <RunInteractiveExprRun exprRun={exprRun} setExprRun={setExprRun} open={runDialogOpen} setOpen={setRunDialogOpen} />
            ) : null }
            {
                feedbackDialogOpen ? (
                    <ExprRunFeedback run={row} setExprRun={setExprRun} open={feedbackDialogOpen} setOpen={setFeedbackDialogOpen} />
                ) : null
            }
        </List>
    );
}

ExprRunDetailogPanel.propTypes = {
    row         : PropTypes.any.isRequired,
    exprType    : PropTypes.string.isRequired,
    consentForm : PropTypes.any,
    genAudio    : PropTypes.func.isRequired
};


function RunTable_({ experiment, data, dispatch, style }) {
    const alert = createAlert(useSnackbar().enqueueSnackbar);
    const classes = useStyles();
    const { _id, type, practice_expr, consent_form } = experiment;
    const [ dialogOpen, setDialogOpen ] = useState(false);
    const user = useContext(UserContext);

    async function deleteRun(run) {
        try {
            await api.delete(`/experiment_run/${run._id}`);
            dispatch({ type: 'DELETE_RUN', _id: run._id });
        } catch(error) {
            alert('Error while deleting experiment run', error);
        }
    }

    async function saveSubjectData(subj) {
        if (!Array.isArray(subj)) subj = [subj];
        return (await api.post('/subject_data', {list: subj})).data;
    }

    async function createRuns(subjects) {
        try {
            const subj = await saveSubjectData(subjects);
            const runs = subj.map(subject => ({
                subject,
                experiment   : _id,
                experimenter : user ? user.name : 'Developer'
            }));
            const rv = await api.post('/experiment_run', { list: runs });
            rv.data.forEach(run => dispatch({ type: 'ADD_RUN', run }));
        } catch(error) {
            alert('Error while scheduling listening experiments', error);
        }
    }

    async function genAudio(id: string) {
        try {
            const rv = await api.post(`/experiment_run/${id}/genaudio`);
            dispatch({ type: 'UPDATE_RUN', run: rv.data});
        } catch(error) {
            alert('Error while updating listening experiments', error);
        }
    }

    function getDefaultPageSize(rows): number {
        if (rows.length <= 5) {
            return 5;
        } else if (rows.length > 5 && rows.length <= 10) {
            return 10;
        } else if (rows.length > 10 && rows.length <= 20) {
            return 20;
        } else if (rows.length > 20 && rows.length <= 25) return 25;
        return 30;
    }

    return (
        <>
            <Grid container style={style}>
                <Grid item xs={12}>
                    <MaterialTable
                        title=""
                        data={data}
                        options={{
                            sorting            : true,
                            actionsColumnIndex : -1,
                            pageSize           : getDefaultPageSize(data),
                            pageSizeOptions    : [5, 10, 15, 20, 25, 30],
                        }}
                        columns={[
                            {
                                title  : 'Status',
                                field  : 'status',
                                render : row => (
                                    <div>
                                        <RunStatus
                                            row={row}
                                            practiceExpr={practice_expr}
                                            exprType={type}
                                        />
                                    </div>
                                )
                            }, {
                                title : 'Subject(s)',
                                field : 'subject.name'
                            }, {
                                title : 'Experimenter',
                                field : 'experimenter'
                            }, {
                                title       : 'Started',
                                field       : 'start',
                                type        : 'datetime',
                                defaultSort : 'desc',
                                render : row => (
                                    <>
                                        {
                                            row.start === 0 ? (
                                                'N/A'
                                            ) : (
                                                row.start
                                            )
                                        }
                                    </>
                                )
                            }, {
                                title : 'Ended',
                                field : 'end',
                                type  : 'datetime',
                                render : row => (
                                    <>
                                        {
                                            row.end === 0 ? (
                                                'N/A'
                                            ) : (
                                                row.end
                                            )
                                        }
                                    </>
                                )
                            },
                        ]}
                        editable={{ onRowDelete: deleteRun }}
                        onRowClick={(_event, _row, togglePanel) => togglePanel && togglePanel()}
                        detailPanel={
                            row => {
                                return (
                                    <ExprRunDetailogPanel row={row} exprType={type} consentForm={consent_form} genAudio={genAudio}/>
                                );
                            }
                        }
                    />
                </Grid>
            </Grid>
            { dialogOpen ? (
                <ScheduleRun open={dialogOpen} setOpen={setDialogOpen} submit={createRuns} />
            ) : null }
            <Fab
                variant="extended"
                color="primary"
                aria-label="add"
                onClick={() => setDialogOpen(true)}
                className={classes.fab}
            >
            Schedule Experiment
            </Fab>
        </>
    );
}

RunTable_.propTypes = {
    experiment : PropTypes.object.isRequired,
    data       : PropTypes.array.isRequired,
    dispatch   : PropTypes.func.isRequired,
    style      : PropTypes.object
};

RunTable_.defaultProps = {
    style : {}
};

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
const RunTable = connect(({ runs }, { experiment: { _id } }) => {
    return {
        data: runs.filter(r => r.experiment === _id)
    };
})(RunTable_);

export default RunTable;


export function RunExperimentView() {
    const alert = createAlert(useSnackbar().enqueueSnackbar);
    const classes = useStyles();
    const params = useParams();
    const location = useLocation();
    const navigate = useNavigate();

    const [runSpinningPage, setRunSpinningPage] = React.useState(true);
    const [spinnerText ] = React.useState('Generating experiment data. Please wait...');
    const [experiment, setExperiment] = React.useState(null);
    const [experimentRun, setExperimentRun] = React.useState({});
    const [selectedId, setSelectedId] = React.useState((params as any).id);
    const [exprCompleted, setExprCompleted] = React.useState('');

    if ((params as any).id !== selectedId) setSelectedId((params as any).id);

    async function fetchExperiment(id) {
        try {
            const response = await api.get(`/experiment/${id}`);
            if (response.data) setExperiment(response.data);
            return response.data;
        } catch (error) {
            alert('Error while fetching experiments:', error);
        }
        return null;
    }

    async function fetchExperimentRun(id) {
        try {
            // If the experiment associated with this run has no pre-generated
            // audio, it is being generated by background tasks. Wait for all
            // background tasks to complete if they have not already.
            await api.post(`/experiment_run/${id}/await`);
            const rv = await api.get(`/experiment_run/${id}`);
            setExperimentRun(rv.data);
            return rv.data;
        } catch (error) {
            alert('Error while fetching experiment runs:', error);
        }

        return null;
    }

    async function createSubject(data, practice_expr) {
        // Practice experiment must not update backend (database) data.
        if (practice_expr) return;
        try {
            const rv = await api.post(`/subject_data/${data.expr_id}/mturk`, data);
            return rv.data;
        } catch (error) {
            alert('Error while updating the experiment run:', error);
        }
    }

    async function createRun(selectedId: string) {
        try {
            const expr = await fetchExperiment(selectedId);
            if (!expr) return null;
            const subject = await createSubject({mturk_id: expr.mturk_id, expr_id: selectedId}, expr.practice_expr);
            if (!subject) return null;
            let rv: any = {};
            // This means that the audio files were not pre-generated, and I need to generate new audio files for the subject.
            if (!subject.assigned) {
                rv = await api.post('/experiment_run', { subject : subject, experiment : selectedId, experimenter: expr.author });
            } else {
                rv = await api.get(`/experiment_run/${subject._id}/subject_run`);
            }
            return rv.data;
        } catch(error) {
            alert('Error while creating experiment run', error);
        }
    }

    useEffect(() => {
        void (async () => {
            // If this is a Amazon MTurk experiment, then create a new experiment run if it is not stored in local storage.
            // NOTE: For the MTurk experiments the selectedId is the experiment's _id, otherwise the experiment run's _id.
            let exprRunID = selectedId;
            if (location.pathname.includes('mturk')) {
                // If the experiment run already created for the current subject, then it does not make sense to create one more.
                const mTurkLocStorData = getLocalStorage(exprRunID, 'mturk');
                if (mTurkLocStorData === null) {
                    const run = await createRun(selectedId);
                    if (!run) return;
                    setLocalStorage(run._id, 'mturk', {expr_run_id: run._id});
                    exprRunID = run._id;
                } else {
                    exprRunID = mTurkLocStorData.expr_run_id;
                }
            }

            if (exprRunID) {
                let localStorageData = getLocalStorage(exprRunID, 'runningExprData');
                if (localStorageData && localStorageData._id === exprRunID) {
                    if (localStorageData.type === 'feedback') {
                        navigate(`/run_experiment/${exprRunID}/feedback`);
                        return;
                    } else {
                        navigate(`/run_experiment/${exprRunID}/run`);
                        return;
                    }
                }

                const exprRun = await fetchExperimentRun(exprRunID);
                if (!exprRun) return;
                const expr = await fetchExperiment(exprRun.experiment);
                if (!expr) return;
                if (exprRun.end && exprRun.status === 'Finished' && !expr.practice_expr) {
                    setExprCompleted('Already Finished');
                    setRunSpinningPage(false);
                    return;
                } else if (exprRun.status === 'Running') {
                    // This is just in case if the subject is running the experiment, then removed the local storage data without
                    // finishing the current experiment, and then try to run again the experiment again.
                    navigate(`/run_experiment/${exprRunID}/run`);
                    return;
                }

                // If the program reaches here it means that the subject hasn't completely finished
                // the experiment. It is possible that the subject on the Feedback page, and if it
                // is the case, then it does not makes sense to fetch the Experiment Config, Consent Form
                // and randomize the experiment steps.
                const finished = localStorageData ? localStorageData.type : null;
                let exprRunConfig: any = [];
                if (expr.type !== 'interactive') {
                    if (finished !== 'feedback') {
                        const exprSteps = randomize(exprRunID, expr._id, expr.randomize_steps, expr.steps);
                        exprRunConfig = [...(exprSteps || [])];
                    } else if (finished === 'feedback') {
                        navigate(`/run_experiment/${exprRunID}/feedback`);
                        return;
                    }
                }

                const mcvExpLocalStorageData = {
                    expr_name: expr.name,
                    expr_id: expr._id,
                    expr_run_id: exprRun._id,
                    subject_id: exprRun.subject._id,
                    consent_form_id: expr.consent_form,
                    practice_expr: expr.practice_expr,
                    mturk: expr.type === 'mturk' ? true : false,
                    mturk_id: expr.mturk_id ? expr.mturk_id : '',
                    expr_type: expr.type,
                    expr_run_config: exprRunConfig
                };
                setLocalStorage(`${exprRunID}`, 'basic_info', mcvExpLocalStorageData);
                // For the interactive experiment the subject needs only sign the consent form.
                if (expr.type === 'interactive') {
                    navigate(`/run_experiment/${exprRunID}/consent`);
                    return;
                }
                if (expr.type === 'mturk') navigate(`/run_experiment/${exprRunID}/instructions`);
                else navigate(`/run_experiment/${exprRunID}/background_env`);
            }
        })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);


    return (
        <div className={classes.root}>
            <main className={classes.content}>
                {
                    runSpinningPage ? <SpinnerPage text={spinnerText} />
                    : experiment && (experiment as any).type === 'interactive' && (experimentRun as any).consent_form_signed === true ?
                        <ExperimentCompleted completed={'done'} authorText={'The consent form has been signed'} />
                    : exprCompleted ?
                        <ExperimentCompleted
                            completed={exprCompleted}
                            authorText={(experiment as any).finish_notes}
                            amtSurveyCode={(experimentRun as any).subject._id === 'unknown' ? (experimentRun as any).amt_survey_code : null}
                        /> : null
                }
            </main>
        </div>
    );
}
