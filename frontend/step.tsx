import React, { useState, useEffect, useRef } from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { useSnackbar } from 'notistack';
import {
    Box,
    TextField,
    Grid,
    MenuItem,
    Button,
    Menu,
    Fab,
    makeStyles,
    Dialog,
    DialogTitle,
    DialogContent,
    Typography,
    FormGroup,
    FormControlLabel,
    Checkbox,
    Radio,
    RadioGroup,
    FormControl,
    FormLabel,
    InputLabel,
    Select,
    IconButton
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import { Paper } from '@material-ui/core';
import { grey } from '@material-ui/core/colors';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import StopIcon from '@material-ui/icons/Stop';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import { axiosAuth } from './auth';
import { SpinnerPage } from './components/spinner_page';
import { createAlert, getBackendURL } from './utils';
import { Impairment, CodecInput, NumberInput } from './impair';


const api = axios.create({baseURL: `${getBackendURL()}api`, withCredentials: true});
api.interceptors.response.use(undefined, axiosAuth);


const useStyles = makeStyles(theme => ({
    fab: {
        margin   : theme.spacing(1),
        position : "fixed",
        bottom   : theme.spacing(2),
        right    : theme.spacing(3)
    },
}));


// TODO: Find a way to present the current settings without JSON.stringify() anything.
function ShowCurrentSettings({ settings, numberOfSteps }) {
    const getValue = (v) => {
        return (typeof v === 'object') ? JSON.stringify(v, null, 4) : v;
    };

    return (
        <Paper style={{ padding: '1em' }}>
            <Typography variant="h6">
                Current Settings
            </Typography>
            <Typography display="inline" variant="subtitle2">Number of steps</Typography>
            <Typography display="inline" variant="body1">{ `: ${numberOfSteps}` }</Typography>
            <br/>
            <Typography display="inline" variant="subtitle2">Codec</Typography>
            <Typography display="inline" variant="body1">{ `: ${getValue(settings.codec)}` }</Typography>
            <br/>
            <Typography display="inline" variant="subtitle2">Gender</Typography>
            <Typography display="inline" variant="body1">{ `: ${getValue(settings.gender)}` }</Typography>
            <br/>
            <Typography display="inline" variant="subtitle2">Frame loss</Typography>
            <Typography display="inline" variant="body1">
                { `: ${settings.frame_loss ? (settings.frame_loss.map(v => getValue(v))) : 'Not provided'}` }
            </Typography>
            <br/>
            <Typography display="inline" variant="subtitle2">Bit error</Typography>
            <Typography display="inline" variant="body1">
                { `: ${settings.bit_errors ? (settings.bit_errors.map(v => getValue(v))) : 'Not provided'}` }
            </Typography>
        </Paper>
    );
}

ShowCurrentSettings.propTypes = {
    settings      : PropTypes.object.isRequired,
    numberOfSteps : PropTypes.number.isRequired
};


const SelectMode = ({ label, setMode, ...args }) => (
    <FormControl fullWidth>
        <InputLabel id="simple-select-mode">{label}</InputLabel>
        <Select
            labelId="select-label-rate"
            id="select-P"
            label={label}
            onChange={e => setMode(e.target.value)} {...args}
        >
            {
                ['Independent', 'Correlated', 'None'].map((v) => (
                    <MenuItem key={v} value={v}>{v}</MenuItem>
                ))
            }
        </Select>
    </FormControl>
);

SelectMode.propTypes = {
    label   : PropTypes.string.isRequired,
    setMode : PropTypes.func.isRequired
};

const VoiceGenderInput = ({ setGender, ...args }) => (
    <FormControl component="fieldset">
        <FormLabel component="legend">Voice gender</FormLabel>
        <RadioGroup
            row
            aria-label="gender"
            name="controlled-radio-buttons-group"
            defaultValue="female"
            onChange={(e) => setGender(e.target.value)}
            {...args}
        >
            <FormControlLabel value="female" control={<Radio color='primary' />} label="Female" />
            <FormControlLabel value="male" control={<Radio color='primary' />} label="Male" />
        </RadioGroup>
    </FormControl>
);

VoiceGenderInput.propTypes = {
    setGender : PropTypes.func.isRequired
};

const SetAudioParameters = ({ mode, modeKey, value, setValue, processing }) => (
    <>
        {
            mode !== 'None' ? (
                <>
                    <Grid item xs={6}>
                        <NumberInput
                            fullWidth
                            disabled={processing}
                            label={modeKey === 'rx_loss' ? "Loss rate" : "Error rate"}
                            value={(value !== null && value.p !== undefined) ? value.p : value}
                            setValue={v => setValue(modeKey, v, 'lossrate')}
                            unit="%"
                            min={0}
                            max={100}
                        />
                    </Grid>
                    <Grid item xs={6}>
                        {
                            mode === 'Correlated' ? (
                                <NumberInput
                                    fullWidth
                                    disabled={processing}
                                    label="Burst factor"
                                    value={(value && value.k !== undefined) ? value.k : null}
                                    setValue={v => setValue(modeKey, v, 'burstfactor')}
                                    min={1}
                                />
                            ) : null
                        }
                    </Grid>
                </>
            ) : null
        }
    </>
);

SetAudioParameters.defaultProps = {
    value : null
};

SetAudioParameters.propTypes = {
    mode       : PropTypes.string.isRequired,
    modeKey    : PropTypes.string.isRequired,
    value      : PropTypes.any,
    setValue   : PropTypes.func.isRequired,
    processing : PropTypes.bool.isRequired
};

function GenAudioSample({ open, setOpen }) {
    const alert = createAlert(useSnackbar().enqueueSnackbar);
    const ref = useRef<HTMLAudioElement | null>(null);
    const [ processing, setProcessing ] = useState(false);
    const [ audioURL, setAudioURL ] = useState('');
    const [ playing, setPlaying ] = useState(false);
    const [ errorMessage, setErrorMessage ] = useState('');
    const [ settings, setSettings ] = useState({
        mic      : '-3',
        gender   : 'female',
        codec    : 'p25p1',
        text     : '',
        rx_loss  : null,
        rx_error : null
    });
    const [ mode, setMode ] = useState({
        rx_loss  : 'None',
        rx_error : 'None'
    });

    const handleGenerateBtn = async () => {
        try {
            setProcessing(true);

            // I need to process data before sending, because if I send the frame drop or error rate
            // as a number, then the value is interpreted as independent loss or error rate and the
            // range is <0, 100> which is the user's input value. If I send the value as an object,
            // then the p value is interpreted as loss or error probability in the range <0, 1>
            // so I need to divide the p by 100.
            //
            // Also, to make sure that the input text does not have whitespace from both ends.
            settings.text = settings.text.trim();
            const body = JSON.parse(JSON.stringify(settings));
            for (const key of Object.keys(mode)) {
                if (body[key] && typeof body[key] === 'object' && body[key].p) {
                    body[key].p = body[key].p / 100;
                }
            }

            const headers = {
                'Content-Type' : 'application/json',
                'Accept'       : 'audio/wav'
            };
            const rv = await api.post('/impair', body, { responseType: 'arraybuffer', headers: headers });
            const blob = new Blob([rv.data], { type: "audio/wav" });
            setAudioURL(window.URL.createObjectURL(blob));
        } catch (error) {
            alert('Error while generating audio file', error);
        } finally {
            setProcessing(false);
        }
    };

    const setText = (e) => {
        if (audioURL.length) setAudioURL('');
        // Note that I do settings.text.trim() before making a request.
        // This would allow to detect an empty string while typing.
        setSettings({...settings, text: e.target.value});
        setErrorMessage('');
        const text = e.target.value.trim();
        if (!text.length) {
            setSettings({...settings, text: ''});
            setErrorMessage('The text must not be empty');
        }
    };

    const disableGenBtn = (m, s) => {
        // m - mode
        // s - settings
        if (!s.text.length) return true;

        for (const key of Object.keys(m)) {
            if (m[key] === 'Correlated') {
                if (s[key] === null) return true;
                if (s[key].p === null || s[key].k === null) return true;
            } else if (m[key] === 'Independent' && s[key] === null) return true;
        }
        return false;
    };

    const handleModeSwitch = (key, value) => {
        if (audioURL.length) setAudioURL('');
        setMode({...mode, [key]: value});
        // There are two reasons why I am doing the following switch/case:
        //
        // 1. Set the value to null when user switches to the None mode.
        // 2. To keep the loss rate or error rate value when switching from Correlated to Independent or the opposite.
        const rateValue = (settings[key] !== null && settings[key].p !== undefined) ? settings[key].p : settings[key];
        switch (value) {
            case 'None':
                setSettings({...settings, [key]: null});
                break;
            case 'Independent':
                setSettings({...settings, [key]: rateValue});
                break;
            case 'Correlated':
                setSettings({...settings, [key]: {type: 'gilbert-elliot', p: rateValue, k: null}});
                break;
            default:
                // Do nothing
                break;
        }
    };

    const setParams = (key, value, valType) => {
        if (audioURL.length) setAudioURL('');
        switch (mode[key]) {
            case 'Correlated':
                {
                    const data = {type: 'gilbert-elliot', p: null, k: null};
                    if (valType === 'lossrate') {
                        data.p = value;
                        data.k = (settings[key] !== null && settings[key].k !== undefined) ? settings[key].k : settings[key];
                    } else if (valType === 'burstfactor') {
                        data.k = value;
                        data.p = (settings[key] !== null && settings[key].p !== undefined) ? settings[key].p : settings[key];
                    }
                    setSettings({...settings, [key]: data});
                }
                break;
            case 'Independent':
                setSettings({...settings, [key]: value});
                break;
            default:
                // Do nothing
                break;
        }
    };


    const play = () => {
        const audioEl = ref.current;
        if (audioEl) void audioEl.play();
    };

    const stop = () => {
        const audioEl = ref.current;
        if (audioEl) {
            audioEl.pause();
            audioEl.currentTime = 0;
        }
    };

    return (
        <Dialog
            open={open}
            onClose={() => setOpen(false)}
            aria-labelledby="add-subjects-dialog"
            fullWidth>
            <DialogTitle id="add-subjects-dialog-title">
                <Box display="flex" alignItems="center">
                    <Box flexGrow={1} >Generate Test Audio</Box>
                    <Box>
                        <IconButton onClick={() => setOpen(false)} >
                            <CloseIcon />
                        </IconButton>
                    </Box>
                </Box>
            </DialogTitle>
            <DialogContent dividers>
                <Grid container spacing={3} alignItems="center">
                    <Grid item xs={6}>
                        <VoiceGenderInput
                            setGender={g => {
                                if (audioURL.length) setAudioURL('');
                                setSettings({...settings, gender: g});
                            }}
                            value={settings.gender}
                            defaultValue="female"
                        />
                    </Grid>
                    <Grid item xs={6}>
                        <CodecInput
                            fullWidth
                            required
                            disabled={false}
                            id="codec"
                            value={settings.codec}
                            setCodec={c => {
                                if (audioURL.length) setAudioURL('');
                                setSettings({...settings, codec: c});
                            }}
                            label="CODEC"
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            required
                            fullWidth
                            variant="outlined"
                            value={settings.text}
                            error={!(errorMessage.length === 0)}
                            margin="dense"
                            label="Text for text-to-speech conversion"
                            helperText={errorMessage.length > 0 ? errorMessage : null}
                            onChange={(e) => setText(e)}
                        />
                    </Grid>
                    <Grid item xs={12} >
                        <SelectMode label='Frame loss' setMode={(v) => handleModeSwitch('rx_loss', v)} value={mode.rx_loss} />
                    </Grid>
                    <SetAudioParameters mode={mode.rx_loss} modeKey='rx_loss' value={settings.rx_loss} setValue={setParams} processing={processing} />
                    <Grid item xs={12} >
                        <SelectMode label='Bit error' setMode={(v) => handleModeSwitch('rx_error', v)} value={mode.rx_error} />
                    </Grid>
                    <SetAudioParameters mode={mode.rx_error} modeKey='rx_error' value={settings.rx_error} setValue={setParams} processing={processing} />
                    <Grid item xs={12} >
                        {
                            !processing ? (
                                <Button
                                    disabled={disableGenBtn(mode, settings) || processing || playing}
                                    onClick={handleGenerateBtn}
                                    color="primary"
                                    fullWidth
                                    size="large"
                                    variant="contained"
                                >
                                    Generate
                                </Button>
                            ) : null
                        }
                    </Grid>
                    <Grid item xs={12} justifyContent="center">
                        {
                            processing ? (
                                <SpinnerPage text="Processing..." height="10vh" />
                            ) : (
                                <Button
                                    color="primary"
                                    disabled={audioURL.length === 0}
                                    fullWidth
                                    size="large"
                                    onClick={() => playing ? stop() : play()}
                                    variant="contained"
                                >
                                    { playing ? <StopIcon /> : <PlayArrowIcon /> }
                                </Button>
                            )
                        }
                    </Grid>
                    {
                        audioURL.length ? (
                            <audio
                                onPlay={() => setPlaying(true)}
                                onEnded={() => setPlaying(false)}
                                onPause={() => setPlaying(false)}
                                ref={ref}
                                src={audioURL}
                                style={{display: 'none'}}
                            />
                        ) : null
                    }
                </Grid>
            </DialogContent>
        </Dialog>
    );
}

GenAudioSample.propTypes = {
    open    : PropTypes.bool.isRequired,
    setOpen : PropTypes.func.isRequired
};


function AnswerFormat({ value, setValue }) {
    const [ type, setType ] = useState(value.type || 'free_form_text');
    const [ description, setDescription ] = useState(value.description || '');
    const [ choices, setChoices] = useState((value.choices || []).join(','));
    const [ randomizeChoice, setRandomizeChoice ] = useState(value.randomizeChoice || false);

    useEffect(() => {
        setType(value.type || 'free_form_text');
        setDescription(value.description || '');
        setChoices((value.choices || []).join(','));
        setRandomizeChoice(value.randomizeChoice || false);
    }, [ value ]);

    function parseChoices(choices: string) {
        return choices.split(',').map(v => v.trim()).filter(v => v.length);
    }

    function sendUpdate(obj) {
        const v = {type, description, choices, randomizeChoice, ...obj};
        v.choices = parseChoices(v.choices);
        setValue(v);
    }

    const validChoices = parseChoices(choices).length > 0;

    const handleRandomizeChoice = () => {
        const tmpData = !randomizeChoice;
        setRandomizeChoice(!randomizeChoice.single);
        sendUpdate({randomizeChoice: tmpData});
    };


    return (
        <Grid container spacing={1} alignItems="center">
            <Grid item xs={2}>
                <TextField
                    variant="filled"
                    fullWidth
                    required
                    margin="dense"
                    label="Answer Type"
                    value={type}
                    onChange={e => {
                        const v = e.target.value;
                        setType(v);
                        sendUpdate({type: v});
                    }}
                    select
                >
                    <MenuItem value="free_form_text">Free-form Text</MenuItem>
                    <MenuItem value="single_choice">Single Choice</MenuItem>
                    <MenuItem value="multiple_choice">Multiple Choices</MenuItem>
                </TextField>
            </Grid>
            <Grid item xs={type === 'free_form_text' ? 10 : 4}>
                <TextField
                    variant="outlined"
                    fullWidth
                    margin="dense"
                    value={description}
                    onChange={e => setDescription(e.target.value)}
                    onBlur={e => {
                        const v = e.target.value.trim();
                        setDescription(v);
                        sendUpdate({description: v});
                    }}
                    label="Answer Description"
                />
            </Grid>
            { type !== 'free_form_text'
                ? (
                    <>
                        <Grid item xs={4}>
                            <TextField
                                variant="outlined"
                                fullWidth
                                required
                                margin="dense"
                                error={!validChoices}
                                helperText={!validChoices ? 'Please select at least one option' : null}
                                value={choices}
                                onChange={e => setChoices(e.target.value)}
                                onBlur={e => {
                                    const v = e.target.value;
                                    sendUpdate({choices: parseChoices(v)});
                                }}
                                label="Choices (comma-delimited)"
                            />
                        </Grid>
                        <Grid item xs={1}>
                            <FormControlLabel
                                control={(
                                    <Checkbox
                                        checked={randomizeChoice}
                                        onChange={handleRandomizeChoice}
                                        name="checkedSingleChoice"
                                        color="primary"
                                    />
                                )}
                                label="Randomize"
                            />
                        </Grid>
                    </>
                ) : null }
        </Grid>
    );
}

AnswerFormat.propTypes = {
    value    : PropTypes.object.isRequired,
    setValue : PropTypes.func.isRequired
};


function StepBody({ _id, expr_name, number, type, dispatch, step }) {
    const { settings, audio, answer } = step;

    return (
        <>
            <Impairment
                _id={_id}
                expr_name={expr_name}
                step_number={number}
                value={settings}
                audio={audio}
                setValue={s => dispatch({ type: 'UPDATE_STEP_SETTINGS', settings: s })}
                setAudio={a => dispatch({ type: 'UPDATE_STEP_AUDIO', audio: a })}
            />
            { type === 'listening' ? (
                <AnswerFormat
                    value={answer}
                    setValue={a => dispatch({ type: 'UPDATE_STEP_ANSWER', answer: a })}
                />
            ) : null }
        </>
    );
}

StepBody.propTypes = {
    _id            : PropTypes.string.isRequired,
    expr_name      : PropTypes.string.isRequired,
    number         : PropTypes.number.isRequired,
    type           : PropTypes.string,
    step           : PropTypes.object.isRequired,
    dispatch       : PropTypes.func.isRequired
};

StepBody.defaultProps = {
    type : 'listening'
};


function StepHeader({ moveUp, moveDown, remove, number }) {
    const [ anchorEl, setAnchorEl ] = useState(null);
    const open  = event => { setAnchorEl(event.currentTarget); };
    const close = ()    => { setAnchorEl(null); };

    return (
        <>
            <Button fullWidth color="inherit" aria-haspopup="true" onClick={open}>
                <Typography variant="h4">
                    {number}
                </Typography>
            &nbsp;
                <ArrowDropDownIcon />
            </Button>
            <Menu anchorEl={anchorEl} keepMounted open={Boolean(anchorEl)} onClose={close}>
                {moveUp   ? <MenuItem onClick={() => { close(); moveUp();  }}>Move Up</MenuItem>   : null}
                {moveDown ? <MenuItem onClick={() => { close(); moveDown(); }}>Move Down</MenuItem> : null}
                <MenuItem onClick={() => { close(); remove(); }}>Delete</MenuItem>
            </Menu>
        </>
    );
}

StepHeader.propTypes = {
    number   : PropTypes.number.isRequired,
    moveUp   : PropTypes.func,
    moveDown : PropTypes.func,
    remove   : PropTypes.func.isRequired,
};

StepHeader.defaultProps = {
    moveUp   : null,
    moveDown : null
};


function Step_({ _id, expr_name, type, number, dispatch, step, moveUp, moveDown, remove }) {
    return (
        <Grid container justifyContent="center" alignItems="center">
            <Grid item xs={1}>
                <StepHeader number={number} moveUp={moveUp} moveDown={moveDown} remove={remove} />
            </Grid>
            <Grid item xs={11}>
                <StepBody _id={_id} expr_name={expr_name} number={number} dispatch={dispatch} type={type} step={step} />
            </Grid>
        </Grid>
    );
}

Step_.propTypes = {
    _id       : PropTypes.string.isRequired,
    expr_name : PropTypes.string.isRequired,
    dispatch  : PropTypes.func.isRequired,
    number    : PropTypes.number.isRequired,
    step      : PropTypes.object.isRequired,
    type      : PropTypes.string.isRequired,
    moveUp    : PropTypes.func,
    moveDown  : PropTypes.func,
    remove    : PropTypes.func.isRequired
};

Step_.defaultProps = {
    moveUp   : null,
    moveDown : null
};

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
const Step = connect(null, (_dispatch_, { dispatch, number }) => {
    return {
        dispatch: args => dispatch({...args, i: number - 1})
    };
})(Step_);


function StepList({ _id, expr_name, dispatch, steps, type, settings, randomize_steps, normalize_audio }) {
    const classes = useStyles();
    const [ dialogOpen, setDialogOpen ] = useState(false);

    return (
        <>
            {
                steps.length ? (
                    <FormGroup row>
                        <FormControlLabel
                            style={{marginLeft: '20px', marginTop: '5px', marginBottom: '-10px'}}
                            control={(
                                <Checkbox
                                    checked={randomize_steps || false}
                                    onChange={() => dispatch({ type: 'UPDATE_EXPERIMENT', _id, attrs: { randomize_steps: !randomize_steps }})}
                                    name="randomizeSteps"
                                    color="primary"
                                />
                            )}
                            label="Randomize Steps"
                        />
                        <FormControlLabel
                            style={{marginLeft: '20px', marginTop: '5px', marginBottom: '-10px'}}
                            control={(
                                <Checkbox
                                    disabled
                                    checked={normalize_audio || false}
                                    onChange={() => dispatch({ type: 'UPDATE_EXPERIMENT', _id, attrs: { normalize_audio: !normalize_audio }})}
                                    name="normalizeAudio"
                                    color="primary"
                                />
                            )}
                            label="Normalize Audio Recordings"
                        />
                    </FormGroup>
                ) : null
            }
            <br />
            {
                settings ? (
                    <Grid container spacing={4} style={{ padding: '1em' }}>
                        <Grid item xs={6}>
                            <Button
                                variant="contained"
                                disabled={false}
                                color="primary"
                                fullWidth
                                size="large"
                            >
                                Generate Experiment Steps
                            </Button>
                        </Grid>
                        <Grid item xs={6}>
                            <Button
                                variant="contained"
                                disabled={false}
                                color="primary"
                                fullWidth
                                size="large"
                                onClick={() => setDialogOpen(true)}
                            >
                                Generate Test Audio
                            </Button>
                        </Grid>
                        <Grid item xs={12}>
                            <ShowCurrentSettings
                                settings={settings}
                                numberOfSteps={steps.length}
                            />
                        </Grid>
                    </Grid>
                ) : null
            }
            { dialogOpen ? (
                <GenAudioSample open={dialogOpen} setOpen={setDialogOpen} />
            ) : null }
            { steps.map((step, i: number) => (
                <div
                    key={i}
                    style={{ padding : '1em', backgroundColor : i % 2 ? grey[200] : 'inherit' }}
                >
                    <Step
                        _id={_id}
                        expr_name={expr_name}
                        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                        // @ts-ignore
                        dispatch={dispatch}
                        number={i + 1}
                        type={type}
                        step={step}
                        moveUp={i > 0 ? () => dispatch({ type: 'SWAP_STEPS', i, j: i - 1 }) : null}
                        moveDown={i < steps.length - 1 ? () => dispatch({ type: 'SWAP_STEPS', i, j: i + 1 }) : null}
                        remove={() => dispatch({ type: 'DELETE_STEP', i })}
                    />
                </div>
            )) }
            <Fab
                variant="extended"
                color="primary"
                aria-label="add"
                className={classes.fab}
                onClick={() => dispatch({
                    type     : 'ADD_STEP',
                    settings : { codec: 'g711u' },
                    answer   : { type: 'free_form_text' }
                })}
            >
                Add Step
            </Fab>
        </>
    );
}

StepList.propTypes = {
    expr_name       : PropTypes.string.isRequired,
    dispatch        : PropTypes.func.isRequired,
    steps           : PropTypes.array.isRequired,
    randomize_steps : PropTypes.bool,
    normalize_audio : PropTypes.bool,
    _id             : PropTypes.string.isRequired,
    type            : PropTypes.string,
    settings        : PropTypes.object
};

StepList.defaultProps = {
    type            : 'listening',
    randomize_steps : undefined,
    normalize_audio : undefined
};

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
export default connect(null, (dispatch, { _id }) => {
    return {
        dispatch: args => dispatch({ ...args, _id })
    };
})(StepList);

