import './style.css';
import 'typeface-roboto';
import 'react-sortable-tree/style.css';
import 'material-icons/iconfont/material-icons.css';
import CssBaseline from '@material-ui/core/CssBaseline';
import React, { useEffect, useState } from 'react';
import ReactDOM from 'react-dom';
import { ThemeProvider } from '@material-ui/core/styles';
import { BrowserRouter as Router } from 'react-router-dom';
import { SnackbarProvider } from 'notistack';
import theme from './theme';
import logger from 'debug';
import App from './app';
import { authEnabled, User, getUserProfile, UserContext } from './auth';

const debug = logger('mcv:main');

// The following global variables are generated for us by the build system (Webpack)
declare global {
    const NODE_ENV            : string | undefined;
    const GIT_VERSION         : string | undefined;
    const BUILD_DATE          : string | undefined;
    const AUTHENTICATION      : string | undefined;
    const BACKEND_URL         : string | undefined;
    const npm_package_name    : string | undefined;
    const npm_package_version : string | undefined;
}



class ErrorBoundary extends React.Component<{ children: any },{ hasError: boolean }> {
    constructor(props) {
        super(props);
        this.state = {
            hasError: false
        };
    }

    static getDerivedStateFromError() {
        return { hasError: true };
    }

    componentDidCatch(error) {
        console.error(error);
    }

    render() {
        if (this.state.hasError) {
            return <h1>Error!</h1>;
        }

        return this.props.children;
    }
}


/*
 * This component is the main entry point to the entire React application. We
 * perform initialization here and setup various middleware and contexts that
 * need to be available across the entire application. We also load the user
 * profile from the backend server if authentication is enabled.
 */
function Main() {
    const [ user, setUser ] = useState<User | undefined>();

    useEffect(() => {
        void (async function() {
            if (authEnabled) setUser(await getUserProfile());
        })();
    }, []);

    return (
        <ErrorBoundary>
            <CssBaseline />
            <ThemeProvider theme={theme}>
                <Router>
                    <SnackbarProvider maxSnack={5}>
                        <UserContext.Provider value={user}>
                            <App />
                        </UserContext.Provider>
                    </SnackbarProvider>
                </Router>
            </ThemeProvider>
        </ErrorBoundary>
    );
}


/**
 * Provide a function called 'debug' on the global windows object that can be
 * used to enable or disable debugging messages in the JavaScript console.
 * @param prefix Namespace for the NPM debug module
 */
function initDebugging(prefix: string) {
    if (NODE_ENV === 'development')
        console.log(`Use function debug() to configure debugging in the JavaScript console`);

    (window as any).debug = (onoff: boolean) => {
        if (typeof onoff !== 'boolean') {
            console.log('Usage: debug(true|false)');
            return;
        }

        if (onoff) localStorage.debug = `${prefix}*`;
        else localStorage.removeItem('debug');

        console.log('Please reload the application to apply changes.');
    };
}


// Initialize various subsystems and render the top-level React component or an
// error page if initialization fails.
let el;
try {
    console.log(`Starting ${npm_package_name} version ${npm_package_version}${GIT_VERSION ? `, git revision ${GIT_VERSION}` : ''}, built on ${BUILD_DATE}`);
    initDebugging('mcv');
    el = <Main/>;
} catch(error) {
    console.error(error);
    el = <h1>Error!</h1>;
}

debug(`Using backend ${BACKEND_URL || '/'}`);

// Let the script embedded in the DOM know that we're taking over
(window as any).mcvRunning = true;
ReactDOM.render(el, document.getElementById('mcv-container'));

