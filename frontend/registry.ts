import debug from 'debug';
import EventEmitter from 'events';
import { UserTerminal } from '@mcv/common/terminal';
import { signIn } from './auth';
import { getBackendURL } from './utils';

const dbg = debug('mcv:registry');


export class UserTerminalRegistry extends EventEmitter {
    srv_resolver: any;
    online: Record<any, any>;
    offline?: Record<any, any>;
    browser: any;
    opts: Record<string, any>;

    constructor(srv_resolver, opts: Record<string, any> = {}) {
        super();
        this.srv_resolver = srv_resolver;
        this.online = Object.create(null);
        this.opts = {...opts};

        this._reset = this._reset.bind(this);
        this._onerror = this._onerror.bind(this);
        this._lost  = this._lost.bind(this);
        this._found = this._found.bind(this);
    }

    _reset() {
        Object.keys(this.online).forEach(ut => { void this._lost({ data : ut }); });
    }

    _onerror() {
        this._reset();
        // Unfortunately, there doesn't appear to be a way to obtain response
        // status code on an EventSource error. Thus, we assume the error is due
        // to authentication and redirect the user to the sign-in page
        // unconditionally. In some cases, e.g., if the backend API is broken on
        // unreachable, the user will be redirected to sign-in instead of
        // getting an error. A possible fix would be to switch to some mechanism
        // other than EvenSource.
        signIn();
    }

    start() {
        const browser = new EventSource(`${getBackendURL()}api/browser/_ut._tcp`, {withCredentials: true});
        browser.onerror = this._onerror;
        browser.onopen = this._reset;
        browser.addEventListener('+', (e: any) => { void this._found(e); });
        browser.addEventListener('-', (e: any) => { this._lost(e); });
        this.browser = browser;
    }

    stop() {
        this.browser.removeEventListener('-', this._lost);
        this.browser.removeEventListener('+', this._found);
        this.browser.onopen = null;
        this.browser.onerror = null;
        this.browser.close();
    }

    async _found({ data }) {
        const ut = new UserTerminal(data, this.srv_resolver, {
            ...this.opts,
            withCredentials : true,
            authCallback: () => {
                signIn();
                // Return a promise that never resolves because the above call
                // to signIn redirects the browser.
                return new Promise(() => { /* do nothing */ });
            }
        });
        await ut.start();
        this.online[data] = ut;
        dbg(`Registry: Discovered new user terminal '${data}'`);
        this.emit('change', data);
        this.emit('online', data);
    }

    _lost({ data }) {
        const ut = this.online[data];
        delete this.online[data];
        ut.stop();
        dbg(`Registry: User terminal '${data}' went away`);
        this.emit('change', data);
        this.emit('offline', data);
    }

    async _invoke(match, method, ...args) {
        const ids = UserTerminal.select(match, Object.keys(this.online));

        if (ids.length === 0)
            throw new Error(`No user terminals selected`);

        const rv = await Promise.all(ids.map(async id => {
            const ut = this.online[id];

            if (typeof ut === 'undefined')
                throw new Error(`Unknown user terminal '${id}'`);

            return {
                [id] : await ut[method].call(ut, ...args)
            };
        }));
        return Object.assign({}, ...rv);
    }

    platform = match => this._invoke(match, 'platform');
    config   = match => this._invoke(match, 'config');

    getSettings    = match => this._invoke(match, 'getSettings');
    resetSettings  = match => this._invoke(match, 'resetSettings');
    updateSettings = (...a) => this._invoke(null, 'updateSettings', ...a);
    applySettings  = match => this._invoke(match, 'applySettings');

    getRun = match => this._invoke(match, 'getRun');

    fetchEvents  = (opts: any={}, ...a) => this._invoke(opts.terminals, 'fetchEvents', opts, ...a);
    deleteEvents = (opts: any={}, ...a) => this._invoke(opts.terminals, 'deleteEvents', opts, ...a);

    restart = (...a) => this._invoke(null, 'restart', ...a);
    ping    = (...a) => this._invoke(null, 'ping', ...a);

    // Returns true of the time offsets on all terminals are below the given
    // threshold value, false or an exception otherwise.
    async checkTime(match, threshold=0.001) {
        return Object.values(await this._invoke(match, 'checkTime'))
            .map((v: any) => v.offset)
            .reduce((v, c) => c > v ? c : v, Number.NEGATIVE_INFINITY) < threshold;
    }
}
