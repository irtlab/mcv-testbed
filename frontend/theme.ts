import { createTheme } from '@material-ui/core/styles';

export default createTheme({
    shape: {
        borderRadius: 5
    },
    overrides: {
        MuiFab: {
            root: {
                position: 'absolute',
                bottom: '2rem',
                right: '2rem'
            }
        },
        MuiCssBaseline: {
            "@global": {
                "*, *::before, *::after": {
                    transition: "none !important",
                    animation: "none !important"
                }
            }
        }
    },
    props: {
        MuiButtonBase: {
            disableRipple: true
        }
    },
    transitions: {
        create: () => "none"
    }
});
