import React, { useEffect, useState, useRef } from 'react';
import PropTypes from 'prop-types';
import { Typography, Grid, Fab, Dialog, DialogContent, CircularProgress } from '@material-ui/core';
import { useSnackbar } from 'notistack';
import AddIcon from '@material-ui/icons/Add';
import MaterialTable from 'material-table';
import debug from 'debug';
import { SpinnerPage } from './components/spinner_page';
import { fetchAuth } from './auth';
import { getBackendURL } from './utils';

const dbg = debug('mcv:media');

const api = `${getBackendURL()}api/media`;


function UploadingDialog({ open, setOpen }) {
    return (
        <Dialog
            disableBackdropClick
            disableEscapeKeyDown
            onClose={() => setOpen(false)}
            aria-labelledby="Uploading"
            open={open}
            style={{
                userSelect: 'none'
            }}
        >
            <DialogContent>
                <Grid container>
                    <Grid item xs={12} style={{ marginBottom: "1em", marginTop: '1em' }}>
                        <CircularProgress />
                    </Grid>
                    <Grid item xs={12}>
                        <Typography align="center" style={{ marginBottom: "1em" }}>
                        Uploading files, please wait.
                        </Typography>
                    </Grid>
                </Grid>
            </DialogContent>
        </Dialog>
    );
}

UploadingDialog.propTypes = {
    open    : PropTypes.bool.isRequired,
    setOpen : PropTypes.func.isRequired
};


function Upload({ reload }) {
    const { enqueueSnackbar } = useSnackbar();
    const [ open, setOpen ] = useState(false);
    const ref = useRef<HTMLInputElement | null>(null);

    const upload = async () => {
        const input = ref.current,
            body = new FormData();

        if (input && input.files) {
            for (let i = 0; i < input.files.length; i++)
                body.append('file', input.files[i]);
        }

        const resp = await fetchAuth(api, { method : 'POST', body });
        if (!resp.ok) throw new Error(`File upload failed`);
    };

    const submit = async () => {
        try {
            setOpen(true);
            await upload();
        } catch(error: any) {
            enqueueSnackbar(`Error while listing media files: ${error.message}`, {
                variant : 'error'
            });
        } finally {
            try {
                await reload();
            } catch(error) { /* nothing */ }
            setOpen(false);
        }
    };

    return (
        <>
            <form>
                <input
                    ref={ref}
                    type='file'
                    name='file'
                    multiple
                    accept='audio/wav'
                    style={{ display : 'none' }}
                    onChange={() => { void submit(); }}
                />
            </form>
            <Fab
                color='primary'
                aria-label='add'
                variant='extended'
                size='large'
                onClick={e => {
                    e.preventDefault();
                    if (ref.current !== null) {
                        // Reset the input's value so that the onChange handler gets always
                        // triggered.
                        ref.current.value = '';
                        ref.current.click();
                    }
                }}
            >
                <AddIcon />
                Upload
            </Fab>
            <UploadingDialog open={open} setOpen={setOpen} />
        </>
    );
}

Upload.propTypes = {
    reload : PropTypes.func.isRequired
};


export default function MediaView() {
    const { enqueueSnackbar } = useSnackbar();
    const [ files, setFiles ] = useState<any[]>([]);
    const [runSpinningPage, setRunSpinningPage] = React.useState(true);

    async function load() {
        dbg(`Fetching list of media files`);
        try {
            const resp = await fetchAuth(api);
            if (!resp.ok) throw new Error(`${resp.status} ${resp.statusText}`);
            setFiles(await resp.json());
        } catch(error: any) {
            enqueueSnackbar(`Error while listing media files: ${error.message}`, {
                variant : 'error'
            });
        }
    }

    useEffect(() => {
        void (async () => {
            await load();
            setRunSpinningPage(false);
        })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <>
            {
                runSpinningPage ? (
                    <SpinnerPage text='Please wait...' />
                ) : (
                    <Grid container>
                        <Grid item xs={12}>
                            <MaterialTable
                                options={{
                                    pageSize : 10,
                                    pageSizeOptions : [5, 10, 15, 20, 25, 30]
                                    // selection : true
                                }}
                                columns={[{
                                    title : 'Filename',
                                    field : 'filename'
                                }, {
                                    title : 'Description',
                                    field : 'attrs.description'
                                }, {
                                    title : 'Transcript',
                                    field : 'attrs.transcript'
                                }, {
                                    title : 'Word Error Rate',
                                    field : 'attrs.wer'
                                }, {
                                    title       : 'Date Modified',
                                    field       : 'mtime',
                                    type        : 'datetime',
                                    editable    : 'never',
                                    defaultSort : 'desc'
                                }, {
                                    title    : 'Size [B]',
                                    field    : 'size',
                                    editable : 'never',
                                    type     : 'numeric'
                                }]}
                                data={files}
                                title="Media Files"
                                actions={[{
                                    tooltip : 'Download File',
                                    icon    : 'get_app',
                                    onClick : (_event, row) => {
                                        window.location.href = `${api}/${row.filename}`;
                                    }
                                }]}
                                editable={{
                                    onRowUpdate: (cur, prev) => fetchAuth(`${api}/${prev.filename}/props`, {
                                        method  : 'POST',
                                        headers : { 'Content-Type': 'application/json' },
                                        body    : JSON.stringify(cur)
                                    })
                                        .then(async resp => {
                                            if (!resp.ok) throw new Error(resp.statusText);
                                            const data = files.map(v => (v.filename !== prev.filename) ? v : resp.json());
                                            setFiles(await Promise.all(data));
                                        })
                                        .catch(error => {
                                            enqueueSnackbar(`Could not modify ${prev.filename}: ${error.message}`, { variant : 'error' });
                                            throw error;
                                        }),

                                    onRowDelete: ({ filename }) => fetchAuth(`${api}/${filename}`, { method : 'DELETE' })
                                        .then(resp => {
                                            if (!resp.ok) throw new Error(resp.statusText);
                                            setFiles(files.filter((v: any) => v.filename !== filename));
                                        })
                                        .catch(error => {
                                            enqueueSnackbar(`Could not delete ${filename}: ${error.message}`, { variant : 'error' });
                                            throw error;
                                        })
                                }}
                                detailPanel={row => (
                                    <audio
                                        controls
                                        preload="metadata"
                                        src={`${api}/${row.filename}`}
                                        style={{
                                            display : 'block',
                                            outline : 'none',
                                            width   : '100%'
                                        }}
                                    >
                                        Your browser does not support audio playback.
                                    </audio>
                                )}
                                onRowClick={(_event, _row, togglePanel) => togglePanel && togglePanel()}
                            />
                            <Upload reload={load} />
                        </Grid>
                    </Grid>
                )
            }
        </>
    );
}
