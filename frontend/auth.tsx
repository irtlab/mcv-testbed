import React, { useState } from 'react';
import logger from 'debug';
import { withStyles } from '@material-ui/core/styles';
import { Container, Typography, Link, Button, Menu, MenuItem, Divider } from '@material-ui/core';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import { parseBool } from '@janakj/lib/parse';
import { getBackendURL } from './utils';

const debug = logger('mcv:auth');

const authAPI = `${getBackendURL()}auth`;

export const authEnabled = parseBool(AUTHENTICATION);


export interface User {
    name: string;
    email: string;
}


const styles = {
    profile: {
        paddingLeft  : '12px',
        paddingRight : '12px',
        minWidth     : '160px',
    },
};


export const UserContext = React.createContext<User | undefined>(undefined);


export function signIn(success?: string, failure?: string) {
    // Note: These success and failure callbacks will not be passed to Google.
    // The signin API will encode them into an opaque state variable that Google
    // will return. Thus, the actual redirection to these will be performed by
    // the signin API (backend).
    const { href, origin } = window.location;
    if (success === undefined) success = href;
    if (failure === undefined) failure = `${origin}/access_denied`;
    window.location.href = `${authAPI}/signin?successRedirect=${encodeURIComponent(success)}&failureRedirect=${encodeURIComponent(failure)}`;
}


export function signOut(redirect?: string) {
    const { origin } = window.location;
    if (redirect === undefined) redirect = `${origin}/signed_out`;
    window.location.href = `${authAPI}/signout?redirect=${encodeURIComponent(redirect)}`;
}


export async function fetchAuth(url: string, opts: Record<string,any>={}): Promise<Response> {
    const res = await fetch(url, {...opts, credentials: 'include' });
    if (!res.ok && res.status === 401) {
        signIn();
        // Intentionally return a promise that never resolves here. The above
        // call to signIn redirects the browser. We won't know the result until
        // the browser is redirected back, which will re-create the frontend and
        // all API fetches will be performed again.
        return new Promise(() => { /* do nothing */ });
    }
    return res;
}


export function axiosAuth(error) {
    if (error.response.status === 401) {
        signIn();
        // Intentionally return a promise that never resolves here. The above
        // call to signIn redirects the browser. We won't know the result until
        // the browser is redirected back, which will re-create the frontend and
        // all API fetches will be performed again.
        return new Promise(() => { /* do nothing */ });
    }
    return Promise.reject(error);
}




export async function getUserProfile() {
    debug('Fetching user profile');
    const res = await fetch(`${authAPI}/profile`, {
        method      : 'GET',
        credentials : 'include'
    });
    if (!res.ok) throw new Error(`${res.status} ${res.statusText}`);
    const profile = await res.json();
    if (profile.email === undefined) {
        debug('Not authenticated');
        return null;
    } else {
        debug(`Authenticated as ${profile.email}`);
        return profile;
    }
}


export const SignIn = withStyles(styles)(function({ classes }: { classes: any }) {
    // On success redirect back to the current UI component. On failure redirect
    // to an "Access Denied" page.
    return (
        <Button color="inherit" className={ classes.profile } onClick={() => signIn()}>
            Sign in
        </Button>
    );
});


export const Profile = withStyles(styles)(function({ user, classes }: { user: any, classes: any }) {
    const [ anchorEl, setAnchorEl ] = useState(null);

    const open      = event => { setAnchorEl(event.currentTarget); };
    const close     = ()    => { setAnchorEl(null); };
    const onSignOut = ()    => {
        close();
        // Once signed out, redirect to a UI component notifying the user that
        // they have been signed out.
        signOut();
    };

    return (
        <>
            <Button color="inherit" className={classes.profile} aria-haspopup="true" onClick={open}>
                {user.name}
                &nbsp;
                <ArrowDropDownIcon />
            </Button>
            <Menu anchorEl={anchorEl} keepMounted open={Boolean(anchorEl)} onClose={close}>
                <MenuItem style={{backgroundColor: 'inherit', cursor: 'auto'}}>
                    {`${user.name} <${user.email}>`}
                </MenuItem>
                <Divider />
                <MenuItem onClick={onSignOut}>Sign out</MenuItem>
            </Menu>
        </>
    );
});


export const AccessDenied = () => {
    const { href, origin } = window.location;

    // On successful sign in redirect to the main URL of the user interface. On
    // failure redirect back to the current URL (which shows access denied).
    const onClick = event => {
        event.preventDefault();
        signIn(origin, href);
    };

    return (
        <Container component="main" maxWidth="sm" style={{marginTop: '4em'}}>
            <Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>
                Access Denied
            </Typography>
            <Typography variant="h5" align="center" color="textSecondary" component="p">
                Please <Link href={origin} onClick={onClick}>sign in</Link> with an admin account to continue.
            </Typography>
        </Container>
    );
};


export const SignedOut = () => {
    const { origin } = window.location;

    // On successful sign in redirect to the main URL of the user interface. On
    // failure redirect to the access denied page.
    const onClick = event => {
        event.preventDefault();
        signIn(origin);
    };

    return (
        <Container component="main" maxWidth="sm" style={{marginTop: '4em'}}>
            <Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>
                Signed Out
            </Typography>
            <Typography variant="h5" align="center" color="textSecondary" component="p">
                You have been signed out. <Link href={origin} onClick={onClick}>Sign back in</Link>.
            </Typography>
        </Container>
    );
};
