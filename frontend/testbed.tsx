/* eslint-disable react/prop-types */
/* eslint-disable @typescript-eslint/ban-ts-comment */
import debug from 'debug';
import clsx from 'clsx';
import React, { useState, useEffect, PropsWithChildren } from 'react';
import PropTypes from 'prop-types';
import { dBToPercent, percentToVolume, volumeToPercent } from '@mcv/common/volume';
import { UserTerminal } from '@mcv/common/terminal';
import Container from '@material-ui/core/Container';
import Card from '@material-ui/core/Card';
import { range } from 'lodash';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Select, { SelectProps } from '@material-ui/core/Select';
import Input from '@material-ui/core/Input';
import { withSnackbar } from 'notistack';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import RadioGroup from '@material-ui/core/RadioGroup';
import Radio from '@material-ui/core/Radio';
import Menu from '@material-ui/core/Menu';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Slider, { SliderProps } from '@material-ui/core/Slider';
import { makeStyles } from '@material-ui/core/styles';
import { red, green } from '@material-ui/core/colors';
import { Grid, GridProps } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import CloudOffIcon from '@material-ui/icons/CloudOff';
import CloudQueueIcon from '@material-ui/icons/CloudQueue';
import AccessTimeIcon from '@material-ui/icons/AccessTime';
import MicIcon from '@material-ui/icons/Mic';
import MicOffIcon from '@material-ui/icons/MicOff';
import SpeakerIcon from '@material-ui/icons/Speaker';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import PhoneIcon from '@material-ui/icons/Phone';
import { UserTerminalRegistry } from './registry';
import { fetchAuth } from './auth';
import { getBackendURL } from './utils';


const dbg = debug('mcv:settings');

const useStyles = makeStyles(theme => ({
    card: {
        maxWidth: '500px'
    },
    sliderLabel: {
        top: '-10px'
    },
    expandOpen: {
        transform: 'rotate(180deg)',
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
    radioLabel: {
        fontSize: "0.8rem",
        lineHeight: "1",
    },
    icon: {
        borderRadius: '50%',
        width: 16,
        height: 16,
        boxShadow: 'inset 0 0 0 1px rgba(16,22,26,.2), inset 0 -1px 0 rgba(16,22,26,.1)',
        backgroundColor: '#f5f8fa',
        backgroundImage: 'linear-gradient(180deg,hsla(0,0%,100%,.8),hsla(0,0%,100%,0))',
        '$root.Mui-focusVisible &': {
            outline: '2px auto rgba(19,124,189,.6)',
            outlineOffset: 2,
        },
        'input:hover ~ &': {
            backgroundColor: '#ebf1f5',
        },
        'input:disabled ~ &': {
            boxShadow: 'none',
            background: 'rgba(206,217,224,.5)',
        },
    },
    checkedIcon: {
        backgroundColor: '#137cbd',
        backgroundImage: 'linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))',
        '&:before': {
            display: 'block',
            width: 16,
            height: 16,
            backgroundImage: 'radial-gradient(#fff,#fff 28%,transparent 32%)',
            content: '""',
        },
        'input:hover ~ &': {
            backgroundColor: '#106ba3',
        },
    }
}));


function alignChunks(size) {
    let saved: Uint8Array | null;
    return (chunk: Uint8Array) => {
        if (saved) {
            const t = new Uint8Array(saved.byteLength + chunk.byteLength);
            t.set(saved);
            t.set(chunk, saved.byteLength);
            saved = null;
            chunk = t;
        }

        const rem = chunk.byteLength % size;
        if (rem) saved = chunk.slice(-rem);

        const left = chunk.byteLength - rem;
        if (left)
            return new DataView(chunk.buffer, chunk.byteOffset, left);

        return undefined;
    };
}


function decodeS16LE(output, input, channels) {
    const o: any[] = [];
    for(let c = 0; c < channels; c++)
        o.push(output.getChannelData(c));

    let v;
    const frameSize = channels * 2;
    const n = input.byteLength / frameSize;

    for (let i = 0; i < n; i++)
        for (let c = 0; c < channels; c++) {
            v = input.getInt16(i * frameSize, true);
            o[c][i] = v / (v < 0 ? 32768 : 32767);
        }
}


const SpeakerMonitor = ({ terminal, rate, channels, fragmentDuration }) => {
    const [ player, setPlayer ] = useState<Promise<any> | null>(null);
    const [ reader, setReader ] = useState<ReadableStreamDefaultReader<Uint8Array> | null>(null);
    const frameSize = channels * 2;
    const fragmentSize = rate * frameSize * fragmentDuration;

    async function play() {
        const ctx = new AudioContext();

        try {
            const res = await fetchAuth(await terminal.url(`audio/speaker?fragmentSize=${fragmentSize}&rate=${rate}&channels=${channels}&format=S16LE&type=audio/vnd.mcv.pcm`));
            if (!res.ok)
                throw new Error(`Cannot stream terminal speaker: ${res.statusText}`);

            if (res.body === null)
                throw new Error('Bug: Got null body from fetchAuth');

            const r = res.body.getReader();
            setReader(r);

            const align = alignChunks(frameSize);

            let rv, time = -1;
            // eslint-disable-next-line no-constant-condition
            while(true) {
                try {
                    rv = await r.read();
                    if (rv.done) break;
                } catch(error: any) {
                    dbg(error.message);
                    break;
                }

                const chunk = align(rv.value);
                if (chunk) {
                    const buf = ctx.createBuffer(channels, chunk.byteLength / frameSize, rate);
                    decodeS16LE(buf, chunk, channels);

                    const s = ctx.createBufferSource();
                    s.buffer = buf;
                    s.connect(ctx.destination);

                    if (time < ctx.currentTime) time = ctx.currentTime;
                    s.start(time);
                    time += buf.duration;
                }
            }
        } finally {
            setReader(null);
            void ctx.close();
            setPlayer(null);
        }
    }

    return (
        <IconButton
            onClick={() => {
                if (!player)
                    setPlayer(play());
                else if (reader)
                    void reader.cancel();
            }}
        >
            <SpeakerIcon style={{ color: player ? red[500] : green[500] }} />
        </IconButton>
    );
};

SpeakerMonitor.propTypes = {
    terminal         : PropTypes.object.isRequired,
    rate             : PropTypes.number,
    channels         : PropTypes.number,
    fragmentDuration : PropTypes.number
};

SpeakerMonitor.defaultProps = {
    rate             : 44100,
    channels         : 1,
    fragmentDuration : 0.1
};


interface SettingProps extends PropsWithChildren<GridProps> {
    label: string;
    modified?: boolean;
}

const Setting = ({ label, children, modified, ...props }: SettingProps) => (
    <Grid item {...props} style={{minHeight: "56px", outline: modified ? "1px solid red" : 0, ...props.style}}>
        <InputLabel shrink>
            {label}
        </InputLabel>
        {children}
    </Grid>
);


type SelectSettingProps = PropsWithChildren<SelectProps>;

const SelectSetting = ({ children, ...props }: SelectSettingProps) => (
    <Select fullWidth {...props}>
        {children}
    </Select>
);


type RangeSettingProps = SliderProps;

const RangeSetting = ({ valueLabelDisplay, value, ...props }: RangeSettingProps) => {
    const [ val, setVal ] = React.useState(value);
    React.useEffect(() => setVal(value), [value]);
    return (
        // @ts-ignore
        <Slider value={val || 0} onChange={(_e, v) => { setVal(v); }} valueLabelDisplay={valueLabelDisplay || "auto"} {...props} />
    );
};


interface CodecSettingProps extends SelectSettingProps {
    supported: {
        values: Record<string, any[]>
    }
}

const CodecSetting = ({ value, supported, ...props }: CodecSettingProps) => (
    <SelectSetting label="codec" value={value} {...props}>
        { Object.entries(supported.values).map(([k, v]) =>
            <MenuItem key={k} value={v[0]}>{k}</MenuItem>)}
    </SelectSetting>
);


interface PtimeSettingProps extends Omit<RangeSettingProps, 'value'> {
    value: number;
    codec: string;
    set: (value: number) => void;
    supported: {
        values: Record<string, any[]>
    }
}

const PtimeSetting = ({ value, set, codec, supported, ...props }: PtimeSettingProps) => {
    const marks: { value: number, label: string }[] = [];
    const codecs = {};

    for(const c of Object.values(supported.values))
        codecs[c[0]] = [c[2], c[3]];

    const [min, max]: [number, number] = codecs[codec] || [10, 60];

    if (value < min) set(min);
    else if (value > max) set(max);

    for(const v of range(min, max + 1, 10))
        marks.push({value: v, label: `${v}`});

    return (
        // @ts-ignore
        <RangeSetting
            value={value}
            valueLabelDisplay="off"
            step={null}
            track={false}
            marks={marks}
            min={10}
            max={60}
            {...props}
        />
    );
};


type PercentSettingProps = RangeSettingProps;

const PercentSetting = ({ value, min, max, ...props }: PercentSettingProps ) => (
    // @ts-ignore
    <RangeSetting
        // @ts-ignore
        value={value}
        min={typeof min === 'number' ? min : 0}
        max={typeof max === 'number' ? max : 100}
        {...props}
    />
);


const VolumeSetting = ({ value, onChangeCommitted, ...props }: PercentSettingProps) => (
    // @ts-ignore
    <PercentSetting
        value={Math.round(volumeToPercent(value))}
        onChangeCommitted={(e, v) => {
            if (onChangeCommitted) onChangeCommitted(e, percentToVolume(v));
        }}
        max={Math.round(dBToPercent(11))}
        {...props}
    />
);


const TextSetting = ({ value, ...props }) => (
    <Input
        type="text"
        fullWidth
        value={value}
        {...props}
    />
);


const NumberSetting = ({ value, ...props }) => (
    <Input
        type="number"
        fullWidth
        value={value}
        {...props}
    />
);

function build_rate(rate) {
    if (rate.length === 0) return null;
    return parseInt(rate, 10) ? `${rate} -14` : null;
}

function parse_rate(rate) {
    if (rate === null) return '';
    return parseInt(rate, 10);
}

// Inspired by blueprintjs
function StyledRadio(props) {
    const classes = useStyles();

    return (
        <Radio
            className={(classes as any).root}
            disableRipple
            color="default"
            checkedIcon={<span className={clsx(classes.icon, classes.checkedIcon)} />}
            icon={<span className={classes.icon} />}
            {...props}
        />
    );
}

const PTTDistributionSetting = () => {
    const classes = useStyles();
    return (
        <RadioGroup aria-label="distribution" name="distribution" row>
            <FormControlLabel checked classes={{ label: classes.radioLabel }} value="uniform" control={<StyledRadio />} labelPlacement="bottom" label="Unif." />
            <FormControlLabel classes={{ label: classes.radioLabel }} value="exponential" control={<StyledRadio />} labelPlacement="bottom" label="Exp."   />
            <FormControlLabel classes={{ label: classes.radioLabel }} value="normal" control={<StyledRadio />} labelPlacement="bottom" label="Norm."  />
        </RadioGroup>
    );
};


const Settings = ({ settings, orig_settings, supported, set }) => (
    <Grid container alignItems="stretch" justifyContent="space-evenly" spacing={2}>
        { 'peer' in settings && (
            <Setting label="Peer SIP URI" xs={6} modified={settings.peer !== orig_settings.peer}>
                <TextSetting
                    value={settings.peer || ''}
                    onChange={e => set({ peer: e.target.value || null })}
                />
            </Setting>
        )}

        { ('codec' in settings) && ('codec' in supported) && (
            <Setting label="Audio CODEC" xs={6} modified={settings.codec !== orig_settings.codec}>
                <CodecSetting
                    supported={supported.codec}
                    value={settings.codec}
                    onChange={e => set({ codec: e.target.value })}
                />
            </Setting>
        )}

        { 'noise' in settings && (
            <Setting label="Microphone background" xs={6} modified={settings.noise !== orig_settings.noise}>
                <TextSetting
                    value={settings.noise || ''}
                    onChange={e => set({ noise: e.target.value || null })}
                />
            </Setting>
        )}

        { 'tx_rate' in settings && (
            <Setting label="Link bit rate [bit/s]" xs={6} modified={settings.tx_rate !== orig_settings.tx_rate}>
                <NumberSetting
                    inputProps={{min: 0, step: 100}}
                    value={parse_rate(settings.tx_rate)}
                    onChange={e => {
                        const s = build_rate(e.target.value);
                        set({ tx_rate: s, rx_rate: s });
                    }}
                />
            </Setting>
        )}

        { 'tx_rate' in settings && (
            <Setting label="Audio bandwidth [Hz]" xs={6}>
                <NumberSetting inputProps={{min: 0, step: 100}} value={4000} />
            </Setting>
        )}

        { 'tx_delay' in settings && (
            <Setting label="Mouth to ear delay [ms]" xs={6} modified={settings.tx_delay !== orig_settings.tx_delay}>
                <NumberSetting
                    inputProps={{min: 0, step: 100}}
                    value={settings.tx_delay === null ? '' : settings.tx_delay}
                    onChange={e => {
                        set({ tx_delay: e.target.value !== '' ? parseInt(e.target.value, 10) : null });
                    }}
                />
            </Setting>
        )}

        { 'ptt_delay' in settings && (
            <Setting label="PTT button delay distribution" xs={6}>
                <PTTDistributionSetting />
            </Setting>
        )}

        { ('packet_time' in settings) && ('codec' in settings) && ('codec' in supported) && (
            <Setting label="Packet time [ms]" xs={6} modified={settings.packet_time !== orig_settings.packet_time}>
                <PtimeSetting
                    value={settings.packet_time}
                    set={v => set({ packet_time: v })}
                    codec={settings.codec}
                    supported={supported.codec}
                    onChangeCommitted={(_e, v) => set({ packet_time: v })}
                />
            </Setting>
        )}

        { 'ptt_delay' in settings && (
            <Setting label="PTT button delay [ms]" xs={6} modified={settings.ptt_delay !== orig_settings.ptt_delay}>
                <RangeSetting
                    value={settings.ptt_delay}
                    step={50}
                    min={0}
                    max={2000}
                    onChangeCommitted={(_e, v) => set({ ptt_delay: v })}
                />
            </Setting>
        )}

        { 'ptt_delay' in settings && (
            <Setting label="PTT beep volume [%]" xs={6}>
                <VolumeSetting value={percentToVolume(100)} />
            </Setting>
        )}

        { 'mic_volume' in settings && (
            <Setting label="Microphone volume [%]" xs={6} modified={settings.mic_volume !== orig_settings.mic_volume}>
                <VolumeSetting
                    value={settings.mic_volume}
                    onChangeCommitted={(_e, v) => set({ mic_volume: v })}
                />
            </Setting>
        )}

        { 'speaker_volume' in settings && (
            <Setting label="Speaker volume [%]" xs={6} modified={settings.speaker_volume !== orig_settings.speaker_volume}>
                <VolumeSetting
                    value={settings.speaker_volume}
                    onChangeCommitted={(_e, v) => set({ speaker_volume: v })}
                />
            </Setting>
        )}

        { 'noise_volume' in settings && (
            <Setting label="Mic. background volume [%]" xs={6} modified={settings.noise_volume !== orig_settings.noise_volume}>
                <VolumeSetting
                    disabled={!settings.noise}
                    value={settings.noise_volume}
                    onChangeCommitted={(_e, v) => set({ noise_volume: v })}
                />
            </Setting>
        )}

        { 'rx_error' in settings && (
            <Setting label="Link error rate [%]" xs={6} modified={settings.rx_error !== orig_settings.rx_error}>
                <PercentSetting
                    value={settings.rx_error || 0}
                    onChangeCommitted={(_e, v) => set({ rx_error: v || null, tx_error: v || null})}
                />
            </Setting>
        )}

        { 'rx_loss' in settings && (
            <Setting label="Link packet loss rate [%]" xs={6} modified={settings.rx_loss !== orig_settings.rx_loss}>
                <PercentSetting
                    value={settings.rx_loss || 0}
                    onChangeCommitted={(_e, v) => set({ rx_loss: v || null, tx_loss: v || null })}
                />
            </Setting>
        )}

        { 'noise_volume' in settings && (
            <Setting label="In-room audio volume [%]" xs={6}>
                <VolumeSetting value={percentToVolume(0)} />
            </Setting>
        )}

        { 'mic' in settings && (
            <Setting label="Microphone input" xs={6} modified={settings.mic !== orig_settings.mic}>
                <TextSetting
                    value={settings.mic || ''}
                    onChange={e => set({ mic: e.target.value || null })}
                />
            </Setting>
        )}

        { 'noise_volume' in settings && (
            <Setting label="In-room audio" xs={6}>
                <SelectSetting value="none">
                    <MenuItem value="none">(None)</MenuItem>
                    <MenuItem value="file1">File 1</MenuItem>
                    <MenuItem value="file2">File 2</MenuItem>
                </SelectSetting>
            </Setting>
        )}

    </Grid>
);


const CardMenu = ({ id, el, close, restart_terminal, undo_changes, load_defaults, page_terminal }) => (
    <Menu
        id={id}
        anchorEl={el}
        keepMounted
        open={Boolean(el)}
        onClose={close}
    >
        <MenuItem onClick={async () => { await load_defaults(); close(); }}>Load Defaults</MenuItem>
        <MenuItem onClick={async () => { await undo_changes(); close(); }}>Undo Changes</MenuItem>
        <MenuItem onClick={async () => { await page_terminal(); close(); }}>Page Terminal</MenuItem>
        <MenuItem onClick={async () => { await restart_terminal(); close(); }}>Restart Terminal</MenuItem>
    </Menu>
);

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export const UserTerminalCard = withSnackbar<{ terminal: UserTerminal, enqueueSnackbar: any, closeSnackbar: any, style: any }>(function({ terminal, closeSnackbar, enqueueSnackbar, ...props }) {
    const classes = useStyles(),
        [ orig_settings, setOrigSettings] = React.useState<any>({}),
        [ settings, setSettings ] = React.useState<any>({}),
        [ platform, setPlatform ] = React.useState<any>({}),
        [ supported, setSupported ] = React.useState<any>({}),
        [ run, setRun ] = React.useState<any>({}),
        [ online, setOnline ] = React.useState(false),
        [ timesync, setTimesync ] = React.useState(false),
        [ state, setState ] = React.useState<any>({}),
        [ indicator, setIndicator ] = React.useState<any>(),
        [ anchorEl, setAnchorEl ] = React.useState(null),
        [ pttRequested, setPTTRequested ] = React.useState(false);

    React.useEffect(() => {
        void (async function() {
            try {
                const [s, p, su, r] = await Promise.all([terminal.getSettings(), terminal.platform(), terminal.supportedSettings(), terminal.getRun()]);
                setOrigSettings({...s});
                setSettings({...s});
                setPlatform({...p});
                setSupported({...su});
                setRun({...r});
            } catch(e: any) {
                enqueueSnackbar(e.message, { variant: 'error' });
            }
        })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [terminal]);

    React.useEffect(() => {
        setOnline(terminal.online);
        terminal.on('online', setOnline);
        return () => { terminal.off('online', setOnline); };
    }, [terminal]);

    React.useEffect(() => {
        setTimesync(terminal.timesync);
        terminal.on('timesync', setTimesync);
        return () => { terminal.off('timesync', setTimesync); };
    }, [terminal]);

    React.useEffect(() => {
        setState({...terminal.state});
        const cb = s => setState({...s});
        terminal.on('state', cb);
        return () => { terminal.off('state', cb); };
    }, [terminal]);

    React.useEffect(() => {
        setIndicator({...terminal.indicator});
        const cb = s => setIndicator(s);
        terminal.on('indicator', cb);
        return () => { terminal.off('indicator', cb); };
    }, [terminal]);

    const openMenu = event => {
        setAnchorEl(event.currentTarget);
    };

    const closeMenu = () => {
        setAnchorEl(null);
    };

    async function apply() {
        try {
            await terminal.updateSettings(settings);
            const s = await terminal.applySettings();
            setOrigSettings({...s});
            setSettings({...s});
        } catch(e: any) {
            enqueueSnackbar(e.message, { variant: 'error' });
        }
    }

    const modified = JSON.stringify(orig_settings) !== JSON.stringify(settings);

    async function restart_terminal() {
        enqueueSnackbar(`Restarting terminal ${terminal.name}`);
        try {
            await terminal.restart(0);
        } catch(e: any) {
            enqueueSnackbar(e.message, { variant: 'error' });
        }
    }

    async function load_defaults() {
        try {
            const d = await terminal.loadDefaults();
            setSettings({...d});
        } catch(e: any) {
            enqueueSnackbar(e.message, { variant: 'error' });
        }
    }

    async function page_terminal() {
        try {
            await terminal.invokePager();
        } catch(e: any) {
            enqueueSnackbar(e.message, { variant: 'error' });
        }
    }

    function undo_changes() {
        setSettings({ ...orig_settings });
    }

    function set_ptt(onoff) {
        setPTTRequested(onoff);
        return terminal.setPtt(onoff);
    }

    return (
        <>
            <Card className={classes.card} {...props}>
                <CardHeader
                    avatar={(
                        <Avatar aria-label="User Terminal" style={{ border: "3px solid black", backgroundColor: indicator }}>
                        &nbsp;
                        </Avatar>
                    )}
                    action={(
                        <>
                            <IconButton aria-label="settings" aria-controls="card-menu" aria-haspopup="true" onClick={openMenu}>
                                <MoreVertIcon />
                            </IconButton>
                            <CardMenu id="card-menu" el={anchorEl} close={closeMenu} load_defaults={load_defaults} undo_changes={undo_changes} restart_terminal={restart_terminal} page_terminal={page_terminal} />
                        </>
                    )}
                    title={`${terminal.name} (${run.id})`}
                    subheader={platform.model}
                />
                <CardContent>
                    <Settings settings={settings} orig_settings={orig_settings} supported={supported} set={v => setSettings({ ...settings, ...v })} />
                </CardContent>
                <CardActions disableSpacing>
                    <IconButton aria-label="online">
                        { online ? <CloudQueueIcon style={{ color: green[500] }} /> : <CloudOffIcon style={{ color: red[500] }} /> }
                    </IconButton>
                    { online ? (
                        <IconButton aria-label="timesync">
                            <AccessTimeIcon style={{ color: timesync ? green[500] : red[500] }} />
                        </IconButton>
                    ) : null }
                    { online ? (
                        <IconButton aria-label="session">
                            <PhoneIcon style={{ color: state.call ? green[500] : red[500] }} />
                        </IconButton>
                    ) : null }
                    { online ? (
                        <IconButton
                            aria-label="ptt"
                            onMouseLeave={() => set_ptt(false)}
                            onMouseDown={() => set_ptt(true)}
                            onMouseUp={() => set_ptt(false)}
                        >
                            { state.ptt
                                ? <MicIcon style={{ color: red[500] }} />
                                : pttRequested
                                    ? <MicIcon style={{ color: green[500] }} />
                                    : <MicOffIcon style={{ color: green[500] }} />}
                        </IconButton>
                    ) : null }
                    <SpeakerMonitor terminal={terminal} />
                    <Button variant="outlined" onClick={apply} style={{marginLeft: "auto"}} size="small" color={modified ? "secondary" : "primary"}>
                        Apply Settings
                    </Button>
                </CardActions>
            </Card>
        </>
    );
});


export function TestbedView() {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const [ registry, setRegistry ] = useState<UserTerminalRegistry | null>(null);
    const [ online, setOnline ] = useState<Record<any, UserTerminal>>({});
    const [ offline, setOffline ] = useState<Record<any, UserTerminal>>({});

    useEffect(() => {
        const r = new UserTerminalRegistry(name => {
            return `${getBackendURL()}proxy/${name}._ut._tcp`;
        }, {
            subscribe: true
        });

        const onChange = () => {
            setOnline({...r.online});
            setOffline({...r.offline});
        };
        r.on('change', onChange);
        // FIXME: We should await the following call
        r.start();
        setRegistry(r);

        return () => {
            setRegistry(null);
            // FIXME: We should await the following call
            r.stop();
            r.off('change', onChange);
        };
    }, []);

    // Obtain both offline and online terminals and sort the resulting list by
    // their names so that when the number of items in the list changes, we
    // preserve the order.
    const terminals = Object.entries({...online, ...offline}).sort(([a], [b]) => a < b ? -1 : (a > b ? 1 : 0));

    return (
        <Container maxWidth={false} style={{paddingTop: '2em', paddingBottom: '2em', backgroundColor: '#f5f5f5' }}>
            <Grid justifyContent="center" spacing={0} style={{minHeight: '100vh'}} container>
                { terminals.map(([k, v]) => (
                    <Grid item key={k} xs={4}>
                        <UserTerminalCard terminal={v} style={{marginLeft: "auto", marginRight: "auto"}} />
                    </Grid>
                ))}
            </Grid>
        </Container>
    );
}
