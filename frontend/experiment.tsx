/* eslint-disable @typescript-eslint/ban-ts-comment */
/* eslint-disable react/display-name */
import React, { useEffect, useState, useContext } from 'react';
import PropTypes from 'prop-types';
import { useNavigate, useParams, useLocation } from "react-router-dom";
import { useSnackbar } from 'notistack';
import { createStore } from 'redux';
import { Provider as StoreProvider, connect } from 'react-redux';
import { red } from '@material-ui/core/colors';
import axios from 'axios';
import {
    Fab,
    makeStyles,
    Grid,
    TextField,
    MenuItem,
    Button,
    Typography,
    Container,
    Tabs,
    Tab,
    Paper,
    CircularProgress,
    FormControlLabel,
    Checkbox
} from '@material-ui/core';
import { ListDrawer } from './components/list_drawer';
import CopyValue from './components/copy';
import { SpinnerPage } from './components/spinner_page';
import { axiosAuth, UserContext } from './auth';
import Steps from './step';
import Runs from './run';
import { createAlert, makeUniqueName, getBackendURL } from './utils';
import type { Experiment, ExperimentRun } from './types';

const api = axios.create({baseURL: `${getBackendURL()}api`, withCredentials: true});
api.interceptors.response.use(null as any, axiosAuth);


interface ExperimentState {
    experiments: Experiment[],
    runs: ExperimentRun[],
    consent_forms: any[]
}

const initialState: ExperimentState = {
    experiments   : [],
    runs          : [],
    consent_forms : []
};

type ResetPayload = [ Experiment[], ExperimentRun[], any[] ];


function reducer(state=initialState, action): ExperimentState {
    switch(action.type) {
        case 'RESET': {
            const [ experiments, runs, consent_forms ] = action.data as ResetPayload;
            return { experiments, runs, consent_forms };
        }

        case 'DELETE_EXPERIMENT': {
            return {
                ...state,
                experiments: state.experiments.filter(({_id}) => _id !== action._id)
            };
        }

        case 'ADD_EXPERIMENT': {
            const experiment = action.data;
            return {
                ...state,
                experiments: [...state.experiments, experiment]
            };
        }

        case 'UPDATE_EXPERIMENT':
            return {
                ...state,
                experiments: state.experiments.map(e => {
                    if (e._id !== action._id) return e;
                    return {...e, ...action.attrs as Partial<Experiment>};
                })
            };

        case 'RESET_EXPERIMENT': {
            const experiment = action.data;
            return {
                ...state,
                experiments: state.experiments.map(e => {
                    if (e._id !== experiment._id) return e;
                    return {...experiment};
                })
            };
        }

        case 'ADD_STEP': {
            return {
                ...state,
                experiments: state.experiments.map(e => {
                    if (e._id !== action._id) return e;
                    const steps = [...(e.steps || [])];
                    steps.push({ settings: action.settings, answer: action.answer });
                    return {...e, steps};
                })
            };
        }

        case 'UPDATE_STEP': {
            const rv = {
                ...state,
                experiments: state.experiments.map(e => {
                    if (e._id !== action._id) return e;
                    const steps = [...(e.steps || [])];
                    steps[action.i] = {
                        settings : action.settings,
                        audio    : action.audio,
                        answer   : action.answer
                    };
                    return {...e, steps};
                })
            };
            return rv;
        }

        case 'UPDATE_STEP_SETTINGS': {
            const rv = {
                ...state,
                experiments: state.experiments.map(e => {
                    if (e._id !== action._id) return e;
                    const steps = [...(e.steps || [])];
                    steps[action.i] = {
                        ...steps[action.i],
                        settings : action.settings
                    };
                    return {...e, steps};
                })
            };
            return rv;
        }

        case 'UPDATE_STEP_AUDIO': {
            const rv = {
                ...state,
                experiments: state.experiments.map(e => {
                    if (e._id !== action._id) return e;
                    const steps = [...(e.steps || [])];
                    steps[action.i] = {
                        ...steps[action.i],
                        audio : action.audio
                    };
                    return {...e, steps};
                })
            };
            return rv;
        }

        case 'UPDATE_STEP_ANSWER': {
            const rv = {
                ...state,
                experiments: state.experiments.map(e => {
                    if (e._id !== action._id) return e;
                    const steps = [...(e.steps || [])];
                    steps[action.i] = {
                        ...steps[action.i],
                        answer : action.answer
                    };
                    return {...e, steps};
                })
            };
            return rv;
        }

        case 'DELETE_STEP': {
            return {
                ...state,
                experiments: state.experiments.map(e => {
                    if (e._id !== action._id) return e;
                    const steps = [...(e.steps || [])];
                    steps.splice(action.i, 1);
                    if (steps.length) return {...e, steps};
                    delete e.steps;
                    return {...e};
                })
            };
        }

        case 'SWAP_STEPS': {
            return {
                ...state,
                experiments: state.experiments.map(e => {
                    if (e._id !== action._id) return e;
                    const steps = [...(e.steps || [])];
                    const v = steps[action.i];
                    steps[action.i] = steps[action.j];
                    steps[action.j] = v;
                    return {...e, steps};
                })
            };
        }

        case 'ADD_RUN': {
            return {
                ...state,
                runs: [...state.runs, action.run]
            };
        }

        case 'UPDATE_RUN': {
            return {
                ...state,
                runs: state.runs.map(e => {
                    if (e._id !== action._id) return e;
                    return action.run;
                })
            };
        }

        case 'DELETE_RUN': {
            return {
                ...state,
                runs: state.runs.filter(({_id}) => _id !== action._id)
            };
        }

        case 'LOAD_FILES': {
            return {
                ...state,
                experiments: state.experiments.map(e => {
                    if (e._id !== action._id) return e;
                    const files = [...(e.files || [])];
                    action.files.forEach(f => {
                        const i = files.findIndex(({id}) => id === f.id);
                        if (i === -1) files.push(f);
                        else files[i] = {...f};
                    });
                    return {...e, files};
                })
            };
        }

        default:
            return state;
    }
}

const store = createStore(reducer);

const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
    },
    drawerPaper: {
        width : drawerWidth,
        top   : 50,
    },
    drawer: {
        [theme.breakpoints.up('sm')]: {
            width      : drawerWidth,
            flexShrink : 0,
        },
    },
    fab: {
        margin   : theme.spacing(1),
        position : "fixed",
        bottom   : theme.spacing(2),
        right    : theme.spacing(3)
    },
    distance: {
        padding: theme.spacing(3)
    },
    wrapper: {
        margin   : theme.spacing(1),
        position : 'relative',
    },
    buttonProgress: {
        color      : red[500],
        position   : 'absolute',
        top        : '50%',
        left       : '50%',
        marginTop  : -12,
        marginLeft : -12,
    }
}));


/*
 * This component represents an experiment name. Apart from the usual editing
 * features, the component also checks that the name is globally unique (across
 * all experiments). It does so by obtaining the list of all existing experiment
 * names from the  Redux store.
 */
// eslint-disable-next-line @typescript-eslint/no-unused-vars
function Name_({ otherNames, dispatch, value, onUpdate, ...props }) {
    const empty = () => value.trim().length === 0;

    function unique() {
        const val = value.trim();
        return !otherNames.find(v => v === val);
    }

    function errorText() {
        if (empty()) return "Experiment name must not be empty";
        if (!unique()) return "Experiment name must be unique";
        return null;
    }

    return (
        <TextField
            required
            error={empty() || !unique()}
            helperText={errorText()}
            id="experiment-name"
            value={value}
            onChange={event => onUpdate(event.target.value)}
            onBlur={event => onUpdate(event.target.value.trim())}
            label="Experiment Name"
            {...props}
        />
    );
}

Name_.propTypes = {
    _id        : PropTypes.string.isRequired,
    value      : PropTypes.string.isRequired,
    otherNames : PropTypes.array.isRequired,
    onUpdate   : PropTypes.func.isRequired,
    dispatch   : PropTypes.func.isRequired
};

/*
 * Use mapStateToProps to pass a list of all the names of all experiments other
 * than the selected one via the otherNames prop. This will be used by the Name
 * component to determine whether the current name is globally unique.
 */
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
const Name = connect(({ experiments }, { _id }) => {
    return {
        otherNames: experiments.filter(e => e._id !== _id).map(({ name }) => name)
    };
})(Name_);


/*
 * This component represents the type of the experiment. We currently support
 * three types of experiments: interactive, listening and Amazon MTurk listening.
 * Interactive experiments involve test subjects interacting with physical user
 * terminals. Listening experiments involve subjects listening to pre-recorded
 * audio samples on their computers. In case the component receives an invalid
 * type value, it reports an error.
 */
// eslint-disable-next-line react/display-name
const Type = React.memo(({value, onUpdate, ...props}: any) => {
    function invalid() {
        return value !== 'interactive' && value !== 'listening' && value !== 'mturk';
    }

    return (
        <TextField
            select
            required
            error={invalid()}
            helperText={invalid() ? "Please select 'interactive' or 'listening'." : null}
            id="experiment-type"
            label="Experiment Type"
            value={value}
            onChange={e => onUpdate(e.target.value)}
            {...props}
        >
            <MenuItem value="interactive">Interactive</MenuItem>
            <MenuItem value="listening">Listening</MenuItem>
            <MenuItem value="mturk">Amazon MTurk Listening</MenuItem>
            { invalid() ? <MenuItem value={value}>{value}</MenuItem> : null }
        </TextField>
    );
});


/*
 * This component allows the experiment administrator to assign a consent form
 * to an experiment. The consent form is optional. This component needs to know
 * what consent forms are available in the system. It obtains the list of all
 * existing consent forms from the Redux store.
 */
// eslint-disable-next-line @typescript-eslint/no-unused-vars
const ConsentForm_ = React.memo(({ consent_forms, dispatch, value, onUpdate, ...props }: any) => {

    function invalid() {
        if (value) {
            if (!consent_forms.find(f => f._id === value)) return true;
        }
        return false;
    }

    return (
        <TextField
            id="consent-form"
            select
            error={invalid()}
            helperText={invalid() ? 'Unknown consent form' : null}
            label="Consent Form"
            value={value}
            onChange={e => onUpdate(e.target.value)}
            {...props}
        >
            <MenuItem value=''><i>(None)</i></MenuItem>
            { consent_forms.map(({ _id, name }) => <MenuItem key={_id} value={_id}>{name}</MenuItem>) }
            { invalid() ? <MenuItem value={value}>{value}</MenuItem> : null }
        </TextField>
    );
});

// @ts-ignore
const ConsentForm = connect(({consent_forms}) => ({consent_forms}))(ConsentForm_);


/*
 * This component represents free-form text notes for experimenters or test
 * subjects. The notes are meant to include additional information relevant to
 * experiments or test subjects.
 */
// eslint-disable-next-line react/display-name
const Notes = React.memo(({ value, onUpdate, ...props }: any) => {
    return (
        <TextField
            value={value}
            onChange={e => onUpdate(e.target.value)}
            onBlur={e => onUpdate(e.target.value.trim())}
            {...props}
        />
    );
});


function Detail_({ experiment, dispatch, create, style }) {
    const amazonMTurkLink = `${window.location.origin}/run_experiment/${experiment._id}/mturk`;
    const alert = createAlert(useSnackbar().enqueueSnackbar);
    const [ saving, setSaving ] = useState(false);
    const classes = useStyles();
    const navigate = useNavigate();

    const {
        name,
        type,
        author,
        consent_form,
        subject_notes,
        experimenter_notes,
        practice_expr
    } = experiment;

    function update(attrs) {
        dispatch({type: 'UPDATE_EXPERIMENT', _id: experiment._id, attrs});
    }

    async function saveChanges() {
        try {
            setSaving(true);
            const { data } = await api.put(`/experiment/${experiment._id}`, experiment);
            dispatch({ type: 'RESET_EXPERIMENT', data });
        } catch(error) {
            alert('Error while updating experiment', error);
        } finally {
            setSaving(false);
        }
    }

    return (
        <>
            <Container maxWidth="sm" style={style}>
                <Grid justifyContent="center" container spacing={3}>
                    <Grid item xs={12}>
                        <Name
                            fullWidth
                            variant="filled"
                            disabled={saving}
                            value={name}
                            _id={experiment._id}
                            onUpdate={v => update({name: v})}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <Type
                            fullWidth
                            variant="filled"
                            disabled={saving}
                            value={type}
                            onUpdate={v => update({type: v})}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            id="experiment-author"
                            value={author}
                            variant="filled"
                            fullWidth
                            required
                            disabled={saving}
                            label="Experiment Author"
                            onChange={({target: {value}}) => update({author: value})}
                            onBlur={({target: {value}}) => update({author: value.trim()})}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <ConsentForm
                            variant="filled"
                            fullWidth
                            disabled={saving}
                            value={consent_form || ''}
                            onUpdate={v => update({consent_form: v || undefined})}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        {
                            experiment.type === 'mturk' ? (
                                <CopyValue value={amazonMTurkLink} label='Amazon Mechanical Turk Link'/>
                            ) : null
                        }
                    </Grid>
                    <Grid item xs={12}>
                        <FormControlLabel
                            control={(
                                <Checkbox
                                    checked={practice_expr || false}
                                    onChange={() => update({practice_expr: !practice_expr})}
                                    name="practiceExperiment"
                                    color="primary"
                                />
                            )}
                            label="Practice Experiment"
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <Notes
                            id="experimenter-notes"
                            label="Notes for Experimenters"
                            multiline
                            fullWidth
                            disabled={saving}
                            rows={5}
                            variant="outlined"
                            value={experimenter_notes || ''}
                            onUpdate={v => update({experimenter_notes: v || undefined})}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <Notes
                            id="subject-notes"
                            label="Notes for Test Subjects"
                            multiline
                            fullWidth
                            disabled={saving}
                            rows={5}
                            variant="outlined"
                            value={subject_notes || ''}
                            onUpdate={v => update({subject_notes: v || undefined})}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <div className={classes.wrapper}>
                            <Button
                                variant="contained"
                                disabled={saving}
                                color="primary"
                                fullWidth
                                size="large"
                                onClick={() => navigate(`/experiments/edit_final_page/${experiment._id}`)}
                            >
                                Edit Final Page
                            </Button>
                        </div>
                    </Grid>
                    <Grid item xs={12}>
                        <div className={classes.wrapper}>
                            <Button
                                variant="contained"
                                disabled={saving}
                                color="secondary"
                                fullWidth
                                size="large"
                                onClick={saveChanges}
                            >
                                { saving ? "Saving..." : "Save Changes" }
                            </Button>
                            {saving && <CircularProgress size={24} className={classes.buttonProgress} />}
                        </div>
                    </Grid>
                </Grid>
            </Container>
            <Fab
                variant="extended"
                color="primary"
                aria-label="add"
                disabled={saving}
                className={classes.fab}
                onClick={create}
            >
                New Experiment
            </Fab>
        </>
    );
}

Detail_.propTypes = {
    experiment : PropTypes.object.isRequired,
    dispatch   : PropTypes.func.isRequired,
    create     : PropTypes.func.isRequired,
    style      : PropTypes.object
};

Detail_.defaultProps = {
    style : {}
};

const Detail = connect()(Detail_);


/*
 * This component serves as the main entry point to all information related to a
 * particular experiment. That information includes experiment detail,
 * configuration steps, and information about experiment runs (scheduled and
 * past). This component only selects which portion of experiment information to
 * show based on the value of the 'tab' query string parameter. The actual job
 * of data presentation is left to children component.
 */
function ExperimentView({ experiment, create }) {
    const navigate = useNavigate(),
        location = useLocation(),
        tabs = [ 'Detail', 'Steps', 'Runs' ],
        search = new URLSearchParams(location.search),
        tab = search.get('tab') || tabs[0],
        val = tabs.findIndex(t => t === tab);

    function select(i) {
        search.set('tab', tabs[i]);
        navigate(`?${search}`);
    }

    const { _id, steps, type } = experiment;

    let children;
    switch(tabs[val]) {
        case 'Detail':
            children = <Detail experiment={experiment} create={create} style={{marginTop: '2em'}} />;
            break;

        case 'Steps':
            children = <Steps _id={_id} expr_name={experiment.name} settings={experiment.settings} type={type} steps={steps || []} randomize_steps={experiment.randomize_steps} normalize_audio={experiment.normalize_audio} />;
            break;

        case 'Runs':
            children = <Runs experiment={experiment} style={{marginTop: '2em'}} />;
            break;

        default:
            children = null;
            break;
    }

    return (
        <main style={{flexGrow: 1}}>
            <Paper square>
                <Tabs value={val < 0 ? 0 : val} indicatorColor='primary' textColor='primary'>
                    { tabs.map((t, i) => <Tab key={t} label={t} onClick={() => select(i)} />) }
                </Tabs>
            </Paper>
            {children}
        </main>
    );
}

ExperimentView.propTypes = {
    experiment : PropTypes.object.isRequired,
    create     : PropTypes.func.isRequired
};


/*
 * This component is presented when the user has an experiment selected via the
 * URL parameter, but the experiment cannot be found (e.g., because it was
 * deleted).
 */
function ExperimentNotFound() {
    return (
        <Typography variant="h5" align="center">
        Experiment Not Found
        </Typography>
    );
}


/*
 * This component is responsible for presenting a list of all existing
 * experiments. It makes it possible to create, select, and delete experiments
 * from the list. Showing the details of the selected experiment is delegated to
 * the component Experiment.
 */
const ExperimentList_ = (({ dispatch, experiments }: any) => {
    const classes = useStyles();
    const navigate = useNavigate();
    const params = useParams();
    const { search } = useLocation();
    const selected = params.id || null;
    const user = useContext(UserContext);
    const alert = createAlert(useSnackbar().enqueueSnackbar);

    // Make sure we have some selected experiment if there are any in the list.
    useEffect(() => {
        if (selected === null) {
            if (experiments.length) {
                // Replace the URL so that we don't pollute the history.
                navigate(`/experiments/${experiments[0]._id}`, { replace: true });
            }
        }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [ selected, experiments ]);

    // Select the experiment with the given ID by updating the URL. This will
    // trigger a re-render which will select the appropriate experiment.
    const select = (id?: any) => {
        navigate(`/experiments${id ? `/${id}` : ''}${search}`);
    };


    // Create a new experiment. First submit a POST request to obtain a new
    // experiment ID. If successful, dispatch a 'ADD_EXPERIMENT' action to update
    // the user interface. Finally, select the new experiment. Pre-fill the author
    // field with the currently logged in user and generate a unique name.
    async function create() {
        try {
            const { data } = await api.post('/experiment', {
                name   : makeUniqueName(experiments, 'Unnamed Experiment'),
                type   : 'listening',
                author : user ? `${user.name} <${user.email}>` : ''
            });
            dispatch({ type: 'ADD_EXPERIMENT', data });
            select(data._id);
        } catch(error) {
            alert('Error while creating experiment', error);
        }
    }

    // Remove an experiment. First delete the experiment on the backend server. If
    // successful, trigger UI update by dispatching a DELETE_EXPERIMENT action.
    // If the experiment was selected, de-select it.
    async function remove({ _id }) {
        try {
            await api.delete(`/experiment/${_id}`);
            dispatch({ type: 'DELETE_EXPERIMENT', _id });
            if (selected === _id) select();
        } catch(error) {
            alert('Error while deleting experiment', error);
        }
    }

    // The currently selected experiment object or undefined if no experiment is
    // selected or the selected experiment cannot be found.
    const experiment = experiments.find(({ _id }) => _id === selected);

    return (
        <div className={classes.root}>
            <ListDrawer
                selectedId={selected}
                setSelectedId={select}
                listItems={experiments}
                deleteListItem={remove}
            />
            { experiment
                ? <ExperimentView experiment={experiment} create={create} />
                : ( selected
                    ? <ExperimentNotFound />
                    : (
                        <Fab
                            variant="extended"
                            color="primary"
                            aria-label="add"
                            className={classes.fab}
                            onClick={create}
                        >
                            New Experiment
                        </Fab>
                    ))}
        </div>
    );
});

// @ts-ignore
const ExperimentList = connect(({experiments}) => ({experiments}))(ExperimentList_);


/*
 * This is the main component for Experiments. This component only initializes
 * the Redux story by fetching experiment, run, and consent form data. It shows
 * a progress bar while it is downloading the data. Once the data has been
 * loaded, the actual UI rendering is delegated to the component Experiments.
 */
export default function Main() {
    const [ loading, setLoading ] = useState(true);
    const alert = createAlert(useSnackbar().enqueueSnackbar);

    // The following function is meant to be executed only once after the
    // component has been mounted for the first time. It loads all the data from
    // the backend server and initializes the Redux store.
    useEffect(() => {
        void (async () => {
            setLoading(true);
            try {
                const rv = await Promise.all([
                    api.get('/experiment'),
                    api.get('/experiment_run'),
                    api.get('/consent_form')
                ]);
                store.dispatch({ type: 'RESET', data: rv.map(v => v.data) });
            } catch(error) {
                alert('Error while loading experiment data', error);
            } finally {
                setLoading(false);
            }
        })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <StoreProvider store={store}>
            { loading
                ? <SpinnerPage text='Loading data. Please wait...' />
                : <ExperimentList />}
        </StoreProvider>
    );
}
