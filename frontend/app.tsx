import React, { useContext } from 'react';
import { Routes, Route, Link, Navigate, useLocation, Outlet } from 'react-router-dom';
import { AppBar, Tabs, Tab, Container, Typography } from '@material-ui/core';
import  MediaView from './media';
import Experiments from './experiment';
import ExprRunConsentFormView from './expr_run_pages/consent';
import ExprRunBackgroundEnvView from './expr_run_pages/background_and_env';
import InstructionsPageView from './expr_run_pages/instructions';
import ExprRunView from './expr_run_pages/run';
import FeedbackView from './expr_run_pages/feedback';
import { MarkdownEditorView } from './components/markdown_editor';
import { MainConsentFormView, EditConsentFormView } from './consent';
import { EventsView } from './events';
import { TestbedView } from './testbed';
import { authEnabled, AccessDenied, SignedOut, SignIn, Profile, UserContext } from './auth';
import { RunExperimentView } from './run';


function MainMenu() {
    const location = useLocation();
    const tab = location.pathname.split('/')[1] || 'testbed';
    const user = useContext(UserContext);

    return (<>
        <AppBar position="sticky" color="primary">
            <div style={{ display: 'flex' }}>
                <Tabs value={tab}>
                    <Tab label="Testbed" value="testbed" component={Link} to="/testbed" />
                    <Tab label="Experiments" value="experiments" component={Link} to="/experiments" />
                    <Tab label="Consent Forms" value="consent_forms" component={Link} to="/consent_forms" />
                    <Tab label="Media" value="media" component={Link} to="/media" />
                    <Tab label="Events" value="events" component={Link} to="/events" />
                    <Tab label="Notebooks" value="notebooks" target="_blank" href="/jupyter" />
                </Tabs>
                <div style={{ flexGrow: 1 }} />
                { (authEnabled && typeof user !== 'undefined') &&
                (user !== null
                    ? <Profile user={user} />
                    : <SignIn /> )}
            </div>
        </AppBar>
        <Outlet/>
    </>);
}


function NotFoundPage() {
    return (
        <Container component="main" maxWidth="sm" style={{marginTop: '4em'}}>
            <Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>
                Not Found
            </Typography>
            <Typography variant="h5" align="center" color="textSecondary" component="p">
                The requested URL was not found. <Link to='/'>Go back</Link>.
            </Typography>
        </Container>
    );
}


export default function App() {
    return (
        <Routes>
            <Route path="/" element={<Navigate to="/testbed" />} />
            <Route path="access_denied" element={<AccessDenied />} />
            <Route path="signed_out" element={<SignedOut />} />
            <Route path="run_experiment/:id" >
                <Route path="" element={<RunExperimentView />} />
                <Route path="background_env" element={<ExprRunBackgroundEnvView />} />
                <Route path="consent" element={<ExprRunConsentFormView />} />
                <Route path="feedback" element={<FeedbackView />} />
                <Route path="instructions" element={<InstructionsPageView />} />
                <Route path="mturk" element={<RunExperimentView />} />
                <Route path="run" element={<ExprRunView />} />
            </Route>
            <Route element={<MainMenu />}>
                <Route path="testbed" element={<TestbedView />} />
                <Route path="experiments">
                    <Route path="" element={<Experiments />} />
                    <Route path=":id" element={<Experiments />} />
                    <Route path="edit_final_page/:id" element={<MarkdownEditorView />} />
                </Route>
                <Route path="consent_forms">
                    <Route path="" element={<MainConsentFormView />} />
                    <Route path=":id" element={<MainConsentFormView />}/>
                    <Route path="edit/:id" element={<EditConsentFormView />} />                
                </Route>
                <Route path="media" element={<MediaView />} />
                <Route path="events" element={<EventsView />} />
            </Route>
            <Route path='*' element={<NotFoundPage />} />
        </Routes>
    );
}
