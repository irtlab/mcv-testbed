/* eslint-disable @typescript-eslint/no-var-requires */
/* eslint-env node */
const path = require('path');
const webpack = require('webpack');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const childProcess = require('child_process');

const { runtimeValue } = webpack.DefinePlugin;


function gitVersion() {
    try {
        return childProcess
            .execSync('git describe --abbrev=8 --always --tags --dirty=" (modified)" 2>/dev/null || echo "?"')
            .toString().trim();
    } catch(error) {
        return '?';
    }
}


function buildDate() {
    return new Date().toLocaleString();
}


function quote(v) {
    if (typeof v === 'string') return `"${v}"`;
}


module.exports = {
    entry: path.resolve(__dirname, 'main.tsx'),
    output: {
        path: path.resolve(__dirname, 'public'),
        filename: '[name].[contenthash].js',
        publicPath: '/frontend/'
    },
    devtool: 'eval-cheap-source-map',
    optimization: {
        // Generate a single separate chunk with runtime webpack boilerplate
        // code to be shared by all other chunks.
        runtimeChunk: 'single',
        moduleIds: 'deterministic',
        // Generate a separate chunk with all external third-party libraries
        // from node_modules.
        splitChunks: {
            cacheGroups: {
                vendor: {
                    test: /[\\/]node_modules[\\/]/,
                    name: 'libs',
                    chunks: 'all',
                }
            }
        }
    },
    plugins: [
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, 'index.html'),
            minify: {
                minifyCSS: true,
                minifyJS: true,
                collapseWhitespace: true,
                keepClosingSlash: true,
                removeComments: true,
                removeRedundantAttributes: true,
                removeScriptTypeAttributes: true,
                removeStyleLinkTypeAttributes: true,
                useShortDoctype: true
            }
        }),
        // If a dependency library requests NodeJS's process global variable,
        // shim it using the process npm package (loaded below)
        new webpack.ProvidePlugin({
            process: 'process'
        }),
        new webpack.DefinePlugin({
            // The second parameter (true) marks any module that uses these
            // defines as non cacheable, i.e., it will be rebuilt every single
            // time. runtimeValue() recomputes the value of the variable on each
            // build.
            BUILD_DATE          : runtimeValue(() => quote(buildDate()), true),
            NODE_ENV            : runtimeValue(() => quote(process.env.NODE_ENV), true),
            GIT_VERSION         : runtimeValue(() => quote(gitVersion()), true),
            AUTHENTICATION      : runtimeValue(() => quote(process.env.AUTHENTICATION), true),
            BACKEND_URL         : runtimeValue(() => quote(process.env.BACKEND_URL), true),
            npm_package_name    : runtimeValue(() => quote(process.env.npm_package_name), true),
            npm_package_version : runtimeValue(() => quote(process.env.npm_package_version), true)
        }),
        // Ignore node_modules when watching for file changes. Files in that folder
        // normally don't get edited while developing.
        new webpack.WatchIgnorePlugin({
            paths: [ path.resolve(__dirname, "node_modules") ]
        }),
        new webpack.HotModuleReplacementPlugin()
    ],
    resolve: {
        extensions: ['.wasm', '.tsx', '.ts', '.jsx', '.js', '.json'],
        //  Webpack 5 no longer polyfills selected NodeJS core modules by
        //  default. Browser-compatible versions of those need to be installed
        //  manually via npm. The modules then need to be mapped via the entries
        //  below. The trailing slash at the end of events tells the system to
        //  bypass the NodeJS core module of the same name and to load it from
        //  the npm_modules directory instead.
        fallback: {
            "path"    : require.resolve("path-browserify"),
            "events"  : require.resolve("events/"),
            "process" : require.resolve("process/browser"),
            "crypto"  : require.resolve("crypto-browserify"),
            "stream"  : require.resolve("stream-browserify"),
            "buffer"  : require.resolve("buffer/")
        }
    },
    module: {
        rules: [{
            test: /\.(jsx|tsx?)$/i,
            exclude: /node_modules/,
            use: ['ts-loader']
        }, {
            test: /\.css$/i,
            use: ['style-loader', 'css-loader']
        }, {
            test: /\.(woff|woff2|eot|ttf|otf)$/i,
            type: 'asset/resource'
        }, {
            test: /\.(png|svg|jpg|jpeg|gif|ico)$/i,
            type: 'asset/resource'
        }]
    }
};
