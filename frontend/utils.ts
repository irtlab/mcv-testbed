// Set local storage data for the given experiment. I have to make sure
// that every experiment has unique local storage keys what is why I concatenate
// the experimentRunId and storageKey.
export function setLocalStorage(experimentRunId, storageKey, data, dataType = true) {
    // If dataType is true then data is object, otherwise string.
    let storageData = data;
    if (dataType) storageData = JSON.stringify(data);

    const key = `${experimentRunId}_${storageKey}`;
    localStorage.setItem(key, storageData);
}


export function getLocalStorage(experimentRunId, storageKey, dataType = true) {
    // If dataType is true then data is object, otherwise string.
    const key = `${experimentRunId}_${storageKey}`;
    const data = localStorage.getItem(key) ? localStorage.getItem(key) : null;

    if (!data) return null;

    if (dataType) return JSON.parse(data);
    return data;
}


export function clearLocalStorage(experimentRunId) {
    [
        'runningExprData',
        'exprStartTime',
        'playedIdx',
        'exprSteps',
        'stepsOrderData',
        'originalStepsIdx',
        'basic_info',
        'mturk',
        'feedback'
    ].forEach((storageKey) => {
        const key = `${experimentRunId}_${storageKey}`;
        localStorage.removeItem(key);
    });
}


export function getCountUpTime(start_time) {
    const setDigits = (t) => {
        if (t < 10) t = `0${t}`;
        return t;
    };
    // Diff in seconds between now and start.
    const diff = (Math.floor(Date.now() / 1000)) - start_time;

    const hour = setDigits(Math.floor(diff / 3600));
    const minute = setDigits(Math.floor((diff - hour * 3600) / 60));
    const second = setDigits(Math.floor(diff % 60));

    return `${hour}h ${minute}m ${second}s`;
}


// Take the given name and try to make it unique within the list of all
// existing names by appending "(number)" to the end. Gives up if (99) is
// reached and it is still not unique. The name parameter should be something
// like "Unnamed Form".
//
// Note that the data argument's elements must be an object and every single
// object must contain the 'name' property.
export function makeUniqueName(data, name) {
    if (!data.find(e => e.name === name)) return name;

    for (let i = 2; i < 100; i++) {
        const n = `${name} (${i})`;
        if (!data.find(e => e.name === n))
            return n;
    }

    throw new Error("Couldn't generate unique name");
}


export function createAlert(notifier, opts={ variant : 'error' }) {
    return function(message, error) {
        if (error && error.response && error.response.status === 500)  {
            notifier(`${message}: ${error.response.data.message}`, opts);
        } else if (error && error.request) {
            notifier(`${message}: ${error}`, opts);
        } else if (error) {
            notifier(`${message}: ${error.message}`, opts);
        } else {
            notifier(`${message}`, opts);
        }
    };
}


/**
 * A utility class for working with media (mostly .wav) files. Includes
 * convenience methods for working with file metadata and for calculating
 * SHA-256 digests.
 */
export class MediaFile {
    file: File;
    /**
     * Create a MediaFile instance from a standard JavaScript File object
     *
     * @param {File} file The file to work
     */
    constructor(file) {
        this.file = file;
    }

    /**
     * Calculate a SHA-256 checksum of the given data and return its hexadecimal
     * representation.
     *
     * @param {ArrayBuffer} data An array buffer of input data
     */
    static async digest(data) {
        const hash = await crypto.subtle.digest('SHA-256', data);
        const array = Array.from(new Uint8Array(hash));
        return array.map(b => b.toString(16).padStart(2, '0')).join('');
    }

    /**
     * Return selected metadata for the file. The following attributes are
     * included: name, lastModified, size, type.
     */
    meta() {
        const f = this.file;
        return {
            name         : f.name,
            lastModified : f.lastModified,
            size         : f.size,
            type         : f.type
        };
    }

    /**
     * Calculate a SHA-256 digest over the file's metadata. The attributes
     * returned by meta() are included in an alphabetically sorted order.
     */
    async metaDigest() {
        const meta = this.meta();
        return MediaFile.digest(new TextEncoder().encode(JSON.stringify(
            Object.keys(meta).sort().map(name => meta[name])
        )));
    }

    /**
     * Return the contents of the file as an ArrayBuffer
     */
    data() {
        return new Promise((resolve, reject) => {
            const f = new FileReader();
            f.onabort = reject;
            f.onerror = event => {
                f.abort();
                reject(event);
            };
            f.onload = event => resolve(event.target?.result);
            f.readAsArrayBuffer(this.file);
        });
    }

    /**
     * Return the contents of the file as a data URL.
     */
    dataUrl() {
        return new Promise((resolve, reject) => {
            const f = new FileReader();
            f.onabort = reject;
            f.onerror = event => {
                f.abort();
                reject(event);
            };
            f.onload = event => resolve(event.target?.result);
            f.readAsDataURL(this.file);
        });
    }

    /**
     * Return the SHA-256 digest calculated over the contents of the file. The
     * digest is returned in a hexadecimal representation.
     */
    async contentDigest() {
        return MediaFile.digest(await this.data());
    }
}


// Make sure to always return a URL with a trailing slash
export function getBackendURL() {
    let url = BACKEND_URL || '/';
    if (url[url.length - 1] !== '/') url += '/';
    return url;
}

