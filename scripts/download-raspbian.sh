#!/bin/bash
set -euo pipefail

dir=${dir:-$(dirname $(realpath -s $(readlink -f $0)))}
. $dir/config.sh

res=$(ensure_tools wget sha256sum unzip) || {
    bail "Missing commands: $res"
}

download_to_cache $raspbian_image_url

filename=$(basename $raspbian_image_url)
info -n "Verifying checksum of $filename..."

checksum=$(sha256sum $cache/$filename | cut -d ' ' -f 1)
if [ $checksum != $raspbian_image_sha256 ] ; then
    bail "Image $filename has invalid checksum"
fi
info "OK"

info -n "Unzipping $filename..."
unzip -o -qq $cache/$filename -d $cache
info "done."