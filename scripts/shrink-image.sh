#!/bin/bash
set -euo pipefail

dir=${dir:-$(dirname $(realpath -s $(readlink -f $0)))}
. $dir/config.sh

res=$(ensure_tools sudo e2fsck tune2fs resize2fs losetup parted truncate umount) || {
    bail "Missing commands: $res"
}

image=$1
shift

$dir/chroot.sh $image /bin/bash <<EOF
set -euo pipefail

apt-get -y autoremove --purge
apt-get clean
find /var/cache/apt     -type f -exec rm {} \;
find /var/lib/apt/lists -type f -exec rm {} \;

truncate -s 0 /etc/machine-id

rm -f /usr/bin/qemu-arm-static
EOF

trap cleanup EXIT

info -n "Attaching a loopback device to $image..."
disk=$(sudo losetup --find --partscan --show $cache/$image)
info $disk
root_partition=${disk}p${root_partition_number}

info "Shrinking the filesystem in $root_partition"
sudo e2fsck -f $root_partition

fs_block_size=$(sudo tune2fs  -l $root_partition | grep "^Block size" | cut -d ':' -f 2 | tr -d ' ')
fs_blocks=$(sudo resize2fs -P $root_partition 2>/dev/null | cut -d ':' -f 2 | tr -d ' ')
bytes=$(($fs_blocks * $fs_block_size))
sudo resize2fs -M $root_partition $fs_blocks

trap - EXIT
cleanup

info "Shrinking partition $root_partition_number in $image to $bytes bytes"
begin=$(sudo parted -ms $cache/$image unit B print | grep "^${root_partition_number}:" | cut -d ':' -f 2 | tr -d 'B')
sudo parted $cache/$image ---pretend-input-tty resizepart $root_partition_number $(($begin + $bytes))B Yes

last_end=$(sudo parted -ms $cache/$image unit B print | tail -1 | cut -d ':' -f 3 | tr -d 'B')
size=$(($last_end+1))
info "Truncating Raspbian image $image to $size bytes"
truncate -s $size $cache/$image
