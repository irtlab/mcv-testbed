#!/bin/bash
set -euo pipefail

root="/srv/mcv"
env="/root/.duplicity/.env_variables.conf"
mongo_archive="$root/mongo_backup/mongo_backup.bin"
bucket_name="mcv_testbed_backup_irtlab"

echo "### Starting backup script at $(date)"

[ $(whoami) == 'root' ] || {
	echo "This script must be run under root"
	exit 1
}

[ -r "$env" ] || {
	echo "Cannot read environment file $env"
	exit 1
}

dir="$(dirname $mongo_archive)"
[ -d "$dir" ] || {
	mkdir -p "$dir"
	chown root:root "$dir"
	chmod 0770 "$dir"
}

touch "$mongo_archive"
chown root:root "$mongo_archive"
chmod 0600 "$mongo_archive"

echo "### Running mongodump at $(date)"

mongodump --forceTableScan --archive="$mongo_archive"

function cleanup {
	rm "$mongo_archive"
	echo "### Backup script succeeded at $(date)"
	exit
}
trap cleanup EXIT INT TERM

# Run duplicity to encrypt data and then copy to GCS.
#
# Always use --archive-dir /root/.cache/duplicity/ in your Duplicity commands.
# This makes sure that Duplicity uses the same local cache even if another user
# runs Duplicity commands. If you don’t do this, Duplicity will rebuild the local
# cache by downloading files from the cloud storage.
#

source $env

echo "### Running duplicity at $(date)"

duplicity                               \
  --archive-dir /root/.cache/duplicity/ \
  --exclude "$root/mongo"               \
  --full-if-older-than 1M               \
  --volsize 4096                        \
  "$root"                               \
  "gs://$bucket_name"

# Show collection status
#duplicity collection-status gs://$bucket_name

# Restore files
#duplicity restore gs://$bucket_name /srv/restored_mcv
