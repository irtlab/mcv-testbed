#!/bin/bash

error () {
    echo -en "\033[31mERROR: "
    echo -e $@"\033[0m"
}

green () {
    echo -en "\033[32m"
    echo -e $@"\033[0m"
}

info () {
    echo -en "\033[33m"
    echo -e $@"\033[0m"
}

bail () {
    [ $# -ge 1 ] && error $@
    exit 1
}


# Make sure that all the tools given on the command line can be located on the
# path and executed. If one or more tools are missing, the function returns a
# positive value and outputs a list of all the missing commands.
#
# If all tools can be located, the function returns 0 and outputs a string
# which, when passed to eval, sets an environment variable with the same name as
# the tool with the full path to the tool.
#
# The function can be used at the beginning of a script as follows:
#
# res=$(ensure_tools cmd1 cmd2 cmd3) && {eval $res
# } || {
#    echo "Missing commands: $res"
# }

ensure_tools () {
    local t found notfound
    found=""
    notfound=""
    for name in $@ ; do
        t=$(which $name)
        if [ -x "$t" ] ; then
            found="$found$name=\"$t\" "
        else
            notfound="$notfound$name "
        fi
    done
    [ -n "$notfound" ] && {
        echo $notfound
        return 1
    } || {
        echo $found
        return 0
    }
}


ensure_dir () {
    for _d in "$@" ; do
        mkdir -p $_d
    done
}


# Download the file specified by a URL in the first parameter to the local
# cache. Expects set -e. Overwrites traps. The name of the file in the cache
# will be the same as the name of the file in the URL. Expects the environment
# variable $cache to be set to a directory. The directory must exist. You can
# override the local name to be used using the second parameter.

download_to_cache () {
    local name;
    name=${2:-$(basename $1)}

    if [ -n "$no_cache" -o ! -f "$cache/$name" ] ; then
        info "Downloading to cache: $name"
        ensure_dir $cache
        trap "rm -f $cache/.$name.partial" EXIT
        wget -q $1 -O $cache/.$name.partial
        mv $cache/.$name.partial $cache/$name
        trap - EXIT
    else
        info "File $name found in cache, skipping download"
    fi
}


cleanup () {
    [ -d "$mnt/host" ] && {
        info -n "Unmounting $mnt/host..."
        sudo umount $mnt/host || true
        sudo rmdir $mnt/host
        info "done."
    }

    [ -d "$mnt/boot" ] && {
        info -n "Unmounting $mnt/boot..."
        sudo umount $mnt/boot || true
        info "done."
    }

    [ -d "$mnt" ] && {
        info -n "Unmounting $mnt..."
        sudo umount $mnt || true
        sudo rmdir $mnt
        info "done."
    }

    [ -n "${disk:-}" ] && {
        info -n "Detaching loopback device $disk..."
        sudo losetup -d $disk
        unset disk
        info "done."
    }
}
