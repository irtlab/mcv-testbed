#!/bin/bash
set -euo pipefail

dir=${dir:-$(dirname $(realpath -s $(readlink -f $0)))}
. $dir/config.sh

res=$(ensure_tools chroot sudo qemu-img qemu-arm-static parted losetup e2fsck resize2fs) || {
    bail "Missing commands: $res"
}

[ "$#" -eq 2 ] || {
    bail "Usage: $0 <image> <size>"
}

image=$1
size=$2

info -n "Creating $image..."
cp $cache/$raspbian_image $cache/$image
info "done."

info "Resizing Raspbian image $image to $size"
qemu-img resize -f raw $cache/$image $size

info "Expanding partition $root_partition_number in $image to ful disk size"
sudo parted -s $cache/$image resizepart $root_partition_number 100%

trap cleanup EXIT

info -n "Attaching a loopback device to $image..."
disk=$(sudo losetup --find --partscan --show $cache/$image)
info $disk
root_partition=${disk}p${root_partition_number}

info "Resizing the filesystem on $root_partition"
sudo e2fsck -f $root_partition
sudo resize2fs $root_partition

info -n "Mounting Raspbian root filesystem to $mnt..."
sudo mkdir -p $mnt
sudo mount $root_partition -o rw $mnt
info "done."

info -n "Installing qemu into the filesystem..."
sudo cp /usr/bin/qemu-arm-static $mnt/usr/bin
info "done."
