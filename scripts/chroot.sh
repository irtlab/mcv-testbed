#!/bin/bash
set -euo pipefail

dir=${dir:-$(dirname $(realpath -s $(readlink -f $0)))}
. $dir/config.sh

res=$(ensure_tools chroot sudo losetup umount) || {
    bail "Missing commands: $res"
}

[ "$#" -lt 1 ] && {
    bail "Usage: $0 <image> ..."
}

image=$1
shift

trap cleanup EXIT

info -n "Attaching a loopback device to $image..."
disk=$(sudo losetup --find --partscan --show $cache/$image)
info $disk
boot_partition=${disk}p${boot_partition_number}
root_partition=${disk}p${root_partition_number}

info -n "Mounting Rasbpian filesystems...$mnt "
sudo mkdir -p $mnt
sudo mount $root_partition -o rw $mnt

info "$mnt/boot"
sudo mount $boot_partition -o rw $mnt/boot

info -n "Bind-mounting /host..."
sudo mkdir -p $mnt/host
sudo mount -o bind,rw $root $mnt/host
info "done."

sudo chroot $mnt $@