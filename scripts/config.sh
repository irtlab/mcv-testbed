raspbian_image_url='http://downloads.raspberrypi.org/raspbian_lite/images/raspbian_lite-2020-02-14/2020-02-13-raspbian-buster-lite.zip'
raspbian_image_sha256='12ae6e17bf95b6ba83beca61e7394e7411b45eba7e6a520f434b0748ea7370e8'

raspbian_image="$(basename -s .zip $raspbian_image_url).img"
mcv_image=mcv.$raspbian_image
terminal_image=terminal.$mcv_image

boot_partition_number=1
root_partition_number=2

# Mount all Raspbian partitions under the following directory
mnt='/mnt/rpi'

# Hostname to set in the Raspbian image
hostname='ut'

# Define dir and root variables if they haven't been defined already
dir=${dir:-$(dirname $(realpath -s $(readlink -f $0)))}
root=${root:-$(realpath -s $(readlink -f $dir/..))}

# Store copies of downloaded files in the following cache directory so that they
# can be re-used without re-downloading.
cache=$root/.cache

# If the no_cache variable is not set, set it to an empty string to make sure
# the variable is bound.
no_cache=${no_cache:-}

. $dir/func.sh
