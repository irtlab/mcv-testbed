#!/bin/bash
set -euo pipefail

dir=${dir:-$(dirname $(realpath -s $(readlink -f $0)))}
. $dir/config.sh

if [ -z "$no_cache" -a -f "$cache/$mcv_image" -a -z "${rebuild_base_image:-}" ] ; then
    info "Reusing existing base image $mcv_image"
    exit 0
fi

info "Building base image $mcv_image"

$dir/download-raspbian.sh

image=".$mcv_image.partial"

trap "rm -f $cache/$image" EXIT

$dir/expand-image.sh              $image 4G
$dir/raspbian/configure-os.sh     $image
$dir/raspbian/install-ledd.sh     $image
$dir/raspbian/install-ambe.sh     $image
$dir/raspbian/install-rohc.sh     $image
$dir/raspbian/install-external.sh $image

info -n "Renaming $image to $mcv_image..."
mv -f $cache/$image $cache/$mcv_image
info "done."
