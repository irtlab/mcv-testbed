#!/bin/bash
set -euo pipefail

dir=${dir:-$(dirname $(realpath -s $(readlink -f $0)))}
. $dir/config.sh

$dir/build-base-image.sh

image=".${terminal_image}.partial"

info -n "Cloning $mcv_image to $image..."
cp -a $cache/$mcv_image $cache/$image
info "done."

$dir/raspbian/install-app.sh $image

if [ -n "${dev:-}" ] ; then
    $dir/raspbian/setup-dev.sh $image
fi

$dir/shrink-image.sh $image

dst_dir=$(pwd)
info -n "Moving $image to $dst_dir/$terminal_image..."
mv $cache/$image $dst_dir/$terminal_image
info "done."
