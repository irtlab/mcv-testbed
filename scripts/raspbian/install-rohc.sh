#!/bin/bash
set -euo pipefail

rohc_url="https://rohc-lib.org/download/rohc-2.1.x/2.1.0/rohc-2.1.0.tar.xz"

# Re-execute itself through the chroot.sh script if not invoked in the Raspbian
# image chroot already.

[ "${1:-}" != "_chroot_" ] && {
    image=$1 ; shift
    dir=$(dirname $(realpath -s $(readlink -f $0)))
    exec $dir/../chroot.sh $image bash -s "_chroot_" $@ < $0
}
shift

echo "running rohc chrooted"

dir="/host/scripts"
. $dir/config.sh

download_to_cache $rohc_url

archive=$(basename $rohc_url)
info -n "Extracting $archive..."
sudo mkdir -p /src
cd /src
tar xf $cache/$archive
info "done."

info "Installing dependencies"
DEBIAN_FRONTEND=noninteractive apt-get install --assume-yes --no-install-recommends \
  make        \
  gcc         \
  autoconf    \
  automake    \
  libtool     \
  pkg-config

info "Building RoHC library"
cd $(basename -s .tar.xz $archive)

autoreconf -i
./configure
make all
make install
make clean

ldconfig