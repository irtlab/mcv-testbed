#!/bin/bash
set -euo pipefail

# Re-execute itself through the chroot.sh script if not invoked in the Raspbian
# image chroot already.

[ "${1:-}" != "_chroot_" ] && {
    image=$1 ; shift
    dir=$(dirname $(realpath -s $(readlink -f $0)))
    exec $dir/../chroot.sh $image bash -s "_chroot_" $@ < $0
}
shift

dir="/host/scripts"
. $dir/config.sh

info "Reconfiguring locales"
sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen
sed -i -e 's/en_GB.UTF-8 UTF-8/# en_GB.UTF-8 UTF08/' /etc/locale.gen
locale-gen en_US.UTF-8

info "Reconfiguring keyboard layout"
sed -i -e 's/XKBLAYOUT="gb"/XKBLAYOUT="us"/' /etc/default/keyboard
dpkg-reconfigure -f noninteractive keyboard-configuration

info "Reconfiguring timezone"
echo "America/New_York" > /etc/timezone
rm /etc/localtime
ln -s /usr/share/zoneinfo/America/New_York /etc/localtime
dpkg-reconfigure -f noninteractive tzdata

export LANGUAGE=en_US.UTF-8
export LANG=en_US.UTF-8

if [ -n "$hostname" ] ; then
    info "Setting hostname to $hostname"
    echo $hostname > /etc/hostname
    sed -i -e "s/raspberrypi/$hostname/g" /etc/hosts
fi

info "Setting up network interfaces"
cat > /etc/network/interfaces.d/mcv <<EOF
allow-hotplug eth0
iface eth0 inet dhcp

auto mcv 
iface mcv inet dhcp
  pre-up ip link add link eth0 name mcv type vlan id 10
  post-down ip link del mcv
EOF

echo 'interface_order="mcv eth0"' >> /etc/resolvconf.conf

info "Enabling systemd-timesyncd"
systemctl enable systemd-timesyncd

info "Configuring systemd-timesyncd synchronization interval"
sed -i -e 's/#PollIntervalMinSec=32/PollIntervalMinSec=32/'   /etc/systemd/timesyncd.conf
sed -i -e 's/#PollIntervalMaxSec=2048/PollIntervalMaxSec=64/' /etc/systemd/timesyncd.conf


info "Running apt-get update"
apt-get update

info "Installing iptables"
DEBIAN_FRONTEND=noninteractive apt-get install --assume-yes --no-install-recommends \
    iproute2 \
    iptables

info "Installing PulseAudio"
DEBIAN_FRONTEND=noninteractive apt-get install --assume-yes --no-install-recommends \
    pulseaudio \
    pulseaudio-utils

info "Installing Avahi"
DEBIAN_FRONTEND=noninteractive apt-get install --assume-yes --no-install-recommends \
    avahi-daemon \
    avahi-utils

info "Installing NodeJS"
cat > /etc/apt/sources.list.d/node.list << EOF
deb https://deb.nodesource.com/node_13.x buster main
deb-src https://deb.nodesource.com/node_13.x buster main
EOF

curl -sSL https://deb.nodesource.com/gpgkey/nodesource.gpg.key | sudo apt-key add -
apt-get update
DEBIAN_FRONTEND=noninteractive apt-get install --assume-yes --no-install-recommends \
    build-essential \
    nodejs

info "Configuring user account pi"
usermod -a -G systemd-journal pi

# Unfortunately, we can't use loginctl to enable linger here because systemd is
# not running yet. Instead, let's create the corresponding file manually.
# loginctl enable-linger mcv

mkdir -p /var/lib/systemd/linger
touch /var/lib/systemd/linger/pi

cat >> /etc/security/limits.conf << EOF
pi - rtprio 99
pi - nice -20
EOF

# This appears to be necessary to make systemctl and friends work when a pi user
# session is entered via su - pi.
runuser - pi -c 'echo "XDR_RUNTIME_DIR=/run/user/$UID" > ~/.profile'
