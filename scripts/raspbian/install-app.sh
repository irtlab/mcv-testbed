#!/bin/bash
set -euo pipefail

# Re-execute itself through the chroot.sh script if not invoked in the Raspbian
# image chroot already.

[ "${1:-}" != "_chroot_" ] && {
    image=$1 ; shift
    dir=$(dirname $(realpath -s $(readlink -f $0)))
    exec $dir/../chroot.sh $image bash -s "_chroot_" $@ < $0
}
shift

dir="/host/scripts"
. $dir/config.sh

sudo mkdir -p /src
tar -c -C /host --exclude=*node_modules* common terminal | sudo tar -x -C /src

export npm_config_cache=$cache/npm
export npm_config_devdir=$cache/node-gyp
mkdir -p $npm_config_cache $npm_config_devdir
chmod 0777 $npm_config_cache $npm_config_devdir

info "Installing @mcv/common"

cd /src/common
npm install --no-optional --production

info "Installing @mcv/terminal"

DEBIAN_FRONTEND=noninteractive apt-get install --assume-yes --no-install-recommends \
  libevdev2     \
  libasound-dev \
  sqlite3       \
  iproute2

cd /src/terminal
npm install --no-optional
npm run build

mkdir /srv/mcv
chown -R pi:pi /srv/mcv

runuser - pi -c bash << EOF
set -euo pipefail

mkdir -p ~/.config/systemd/user

cat > ~/.config/systemd/user/terminal.service << EOF2
[Unit]
Description=LMR User Terminal
StartLimitIntervalSec=0

[Service]
Type=simple
Restart=always
RestartSec=5
WorkingDirectory=/src/terminal
Environment=DEBUG=mcv*,-mcv:baresip:api
ExecStart=node --experimental-specifier-resolution=node --title=@mcv/terminal dist/main.js -d /srv/mcv -c /srv/mcv/config.json

[Install]
WantedBy=default.target
EOF2

systemctl --user enable terminal
EOF

info "Installing PulseAudio configuration files to ~/.config/pulse"
runuser - pi -c bash << EOF
set -euo pipefail
mkdir -p ~/.config/pulse
cp /host/config/pulseaudio/* ~/.config/pulse
EOF
