#!/bin/bash
set -euo pipefail

# Re-execute itself through the chroot.sh script if not invoked in the Raspbian
# image chroot already.

[ "${1:-}" != "_chroot_" ] && {
    image=$1 ; shift
    dir=$(dirname $(realpath -s $(readlink -f $0)))
    exec $dir/../chroot.sh $image bash -s "_chroot_" $@ < $0
}
shift

dir="/host/scripts"
. $dir/config.sh

sudo mkdir -p /src
tar -c -C /host --exclude=.obj ledd | sudo tar -x -C /src

info "Installing ledd"
DEBIAN_FRONTEND=noninteractive apt-get install --assume-yes --no-install-recommends \
  libpigpio-dev  \
  libsystemd-dev \
  socat

cd /src/ledd
make clean
make all
make install
make clean

cp -a systemd/* /etc/systemd/system
systemctl enable ledd.socket
