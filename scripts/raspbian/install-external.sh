#!/bin/bash
set -euo pipefail

# Re-execute itself through the chroot.sh script if not invoked in the Raspbian
# image chroot already.

[ "${1:-}" != "_chroot_" ] && {
    image=$1 ; shift
    dir=$(dirname $(realpath -s $(readlink -f $0)))
    exec $dir/../chroot.sh $image bash -s "_chroot_" $@ < $0
}
shift

dir="/host/scripts"
. $dir/config.sh

sudo mkdir -p /src
tar -c -C /host --exclude=*node_modules* external | sudo tar -x -C /src

info "Installing Baresip"

DEBIAN_FRONTEND=noninteractive apt-get install --assume-yes --no-install-recommends \
    libasound-dev          \
    libssl-dev             \
    libspandsp-dev         \
    libopencore-amrnb-dev  \
    libopencore-amrwb-dev  \
    libvo-amrwbenc-dev     \
    libgsm1-dev            \
    libsndfile-dev         \
    libpulse-dev           \
    make                   \
    gcc                    \
    pkg-config             \
    patch

cd /src/external/baresip
patch -p1 < ../ambe.patch

cd /src/external

for n in re rem baresip ; do
    pushd $n >/dev/null 2>&1
    make
    make install
    ldconfig
    make clean
    popd >/dev/null 2>&1
done
