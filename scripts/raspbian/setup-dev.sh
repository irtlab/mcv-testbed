#!/bin/bash
set -euo pipefail

# Re-execute itself through the chroot.sh script if not invoked in the Raspbian
# image chroot already.

[ "${1:-}" != "_chroot_" ] && {
    image=$1 ; shift
    dir=$(dirname $(realpath -s $(readlink -f $0)))
    exec $dir/../chroot.sh $image bash -s "_chroot_" $@ < $0
}
shift


dir="/host/scripts"
. $dir/config.sh

info "Enabling development mode"

info -n "Disabling SSH password authentication..."
sed -i -e 's/^#PasswordAuthentication yes/PasswordAuthentication no/' /etc/ssh/sshd_config
info "done."

info "Enabling SSH server"
systemctl enable ssh
dpkg-reconfigure openssh-server

info "Enabling NFS-based home directories"
echo "bs:/home /home nfs defaults 0 0" >> /etc/fstab

DEBIAN_FRONTEND=noninteractive apt-get install --assume-yes --no-install-recommends \
  nfs-common

# Delete SSH host keys to have them re-generated on first boot
rm -f /etc/ssh/*_host_*

info "Installing development npm dependencies"
cd /src/common
npm install --no-optional

cd /src/terminal
npm install --no-optional

info "Installing systemd unit override to enable debugging"
runuser - pi -c bash << EOF
set -euo pipefail

mkdir ~/.config/systemd/user/terminal.service.d
cat > ~/.config/systemd/user/terminal.service.d/dev.conf << EOF2
[Service]
Environment=DEBUG=mcv*,-mcv:baresip:api
EOF2
EOF
