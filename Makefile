.PHONY: dev
dev:
	cd backend && npm run dev

.PHONY: npm_install
npm_install:
	cd common && npm install && npm run build
	cd backend && npm install
	cd frontend && npm install
	#cd terminal && npm install
	cd generator && npm install

