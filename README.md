# Mission Critical Voice Testbed

The mission critical voice (MCV) testbed is an open platform for conducting experiments with MCV communication systems. The testbed provides components that can be used to build a model of a MCV communication system in a controlled environment (lab), plan and execute experiments involving human subjects, and systematically evaluate the influence of various environmental and system parameters on intelligibility.

The testbed source code is released under a permissive software license and is designed to work with commercial off-the-shelf hardware components. Various Push To Talk (PTT) and full-duplex communication scenarios can be implemented. Key parameters that influence intelligibility are run-time configurable including background noise, audio levels, speech codecs, channel acquisition time, packet loss, and error rate. The testbed is extensible and provides straightforward HTTP APIs for experiment analysis and integration with third-party services and applications.

# Key Features

*  A web-based software architecture that provides:
    *  Browser-based user interface for experiment design, execution (programmable & interactive), and evaluation
    *  Remote user terminal control, with access to audio streams in real-time from the browser
    *  Generation of experimental data on user terminals (time-synchronized events, audio recordings) and collection of the data on the base station
    *  HTTP APIs for experimental data access, analysis, evaluation, and integration with third-party tools
*  Flexible hardware architecture:
    *  User terminals running on Raspberry Pis or virtualized in Docker containers
    *  Ethernet or Wi-Fi networking with link emulation independently configurable on each user terminal (bandwidth, bitrate, delay, loss, error injection)
    *  Support for DVSI's hardware AMBE codecs
    *  Software Defined Radio (SDR) gateway for communication with real radios (experimental, analog FM only)
*  Programmable audio processing architecture in user terminals:
    *  Low-latency audio processing (down to 10 ms)
    *  Push-to-talk (PTT) and full-duplex communication support, configurable PTT beeps
    *  Background noise injection and mixing, input/output redirection and recording
    *  Fine-grained volume control for each audio stream with dB steps
    *  Supported codecs include: IMBE & AMBE (Project 25), GSM & AMR (FirstNet, TETRA), G.711, Opus, CODEC2 (experimental)

